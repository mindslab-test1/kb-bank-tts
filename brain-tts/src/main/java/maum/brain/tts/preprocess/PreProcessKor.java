package maum.brain.tts.preprocess;


import maum.brain.tts.client.G2PClient;
import maum.brain.tts.client.KonglishClient;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Primary
@ConditionalOnProperty(name = "tts.lang", havingValue = "ko_KR")
public class PreProcessKor implements IPreProcess {

    // 패턴확인용 regex
    final private String regNumCheck = "([+-]?\\d[\\d,]*)([.\\d]*)";
    final private String regCntCheck = "(시간|명|가지|살|마리|포기|송이|수|톨|통|개|벌|척|채|다발|그루|자루|줄|켤레|그릇|잔|마디|상자|사람|곡|병|판|(?!달러)달|자리|건|배)";

    // etc 사전
    private HashMap<String, String> dictEtc;

    // 숫자+단위 환산 사전
    // 숫자 단위 환산에서
    // 사전 1 == double, 사전 2 == single 입니다
    // 처리 순서는 1 -> 2 (두글자 단위 우선 변경을 위해서)
    private HashMap<String, String> dictMetrics;

    private HashMap<String,String> dictPostposition;
    // 숫자+수량 환산 사전
    private HashMap<String, String> dictCntNumSingle;
    private HashMap<String, String> dictCntNumDouble;

    // 숫자 단순 변환 자릿수 환산 기준
//    private HashMap<Integer, String> numDigDict;

    // 숫자 기본 독음
    private HashMap<String, String> dictSimpleNum;

    // 영어 사전
    private HashMap<String, String> dictEngLetter;
    private HashMap<String, String> dictEngWord;
    private Logger logger;
    @Value("${tts.resources.preprocess.root}")
    private String dictRoot;
    @Value("${tts.resources.preprocess.base}")
    private String dictBase;

    @Autowired
    private G2PClient g2p;
    @Autowired
    private KonglishClient konglish;
    @Autowired
    private OverTextCheck overTextCheck;
    private DictReader dictReader;

    public PreProcessKor(){
    }

    @PostConstruct
    public void init() throws IOException {
        this.dictReader = new DictReader();
        this.dictReader.setProp(dictRoot + dictBase);
        this.readDictionaries();
    }

    public PreProcessKor(int maxlength, String propFileName, String kongAddress, String g2pAddress) {
        konglish = new KonglishClient(kongAddress);
        overTextCheck = new OverTextCheck(maxlength);
        g2p = new G2PClient(g2pAddress);
        this.logger = null;
        this.dictRoot = "";
        this.dictBase = propFileName;
    }

    public PreProcessKor(int maxlength, String propFileName, Logger logger, String kongAddress) {
        konglish = new KonglishClient(kongAddress);
        overTextCheck = new OverTextCheck(maxlength);
        this.logger = logger;
        this.dictRoot = "";
        this.dictBase = propFileName;
    }

    public PreProcessKor(int maxlength, String propFileRoot, String propFileBase, Logger logger, String kongAddress) {
        konglish = new KonglishClient(kongAddress);
        overTextCheck = new OverTextCheck(maxlength);
        this.logger = logger;
        this.dictRoot = propFileRoot;
        this.dictBase = propFileBase;
    }

    private void readDictionaries() {
        try {
            Iterator<Map.Entry<String, String>> it = this.dictReader.getDictionary().entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, String> entry = it.next();
                // TODO - upgrade, make custom dictionary addition more easy
                switch (entry.getKey()) {
                    case "kor.etc":
                        dictReader.setProp(dictRoot + entry.getValue());
                        dictEtc = dictReader.getDictionary();
                        break;
                    case "kor.eng.letter":
                        dictReader.setProp(dictRoot + entry.getValue());
                        dictEngLetter = dictReader.getDictionary();
                        break;
                    case "kor.eng.word":
                        dictReader.setProp(dictRoot + entry.getValue());
                        dictEngWord = dictReader.getDictionary();
                        break;
                    case "kor.metrics":
                        dictReader.setProp(dictRoot + entry.getValue());
                        dictMetrics = dictReader.getDictionary();
                        break;
                    case "kor.num_count_single":
                        dictReader.setProp(dictRoot + entry.getValue());
                        dictCntNumSingle = dictReader.getDictionary();
                        break;
                    case "kor.num_count_double":
                        dictReader.setProp(dictRoot + entry.getValue());
                        dictCntNumDouble = dictReader.getDictionary();
                        break;
                    case "kor.num_simple":
                        dictReader.setProp(dictRoot + entry.getValue());
                        dictSimpleNum = dictReader.getDictionary();
                        break;
                    case "kor.postposition":
                        dictReader.setProp(dictRoot + entry.getValue());
                        dictPostposition = dictReader.getDictionary();
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }


    // Entire preprocessing
    public ArrayList<String> preProcessText(String text) throws IOException {
        String processed = text;
        ArrayList<String > resultStringLst;
        // 괄호 전처리
        processed = normPare(processed);
        // 기타 사전 전처리
        processed = normByDict(processed);
        //119
        processed = norm119(processed);
        // 전화번호 전처리
        processed = cvtPhoneNum(processed);
        // 계좌번호 전처리
        processed = cvtAccountNum(processed);

        processed = cvtLandLineNum(processed);
        //분수,시속-초속, ~ (에서) KBS
        processed = cvtEtcNum(processed);
        // hh:mm 전처리
        processed = cvtTime(processed);
        // 단위 전처리
        processed = cvtMetrics(processed);
        // 숫자 읽는 방식 전처리
        processed = cvtCountNum(processed);
        // 영어 사전 전처리
        processed = normEnglish(processed);

        processed = normKonglish(processed);
        // 쩜xxx 소수점 전처리
        processed = cvtJUM(processed);
        //주소 전처리
        processed = cvtAddress(processed);
        // 숫자 전처리
        processed = normNum(processed);
        //치환 후 추가 전처리
        processed = normAfter(processed);

        processed = changePronun(processed);

        resultStringLst = overTextCheck.overCheck(processed);
        return resultStringLst;
    }    // Parentheses removal

    private String normPare(String text) {
        String retText = text.replaceAll("[\\(]([^\\(|\\)])*[\\)]", "");
//        retText = retText.replaceAll("[!@#$%^&*(){}\\[\\]]{2,}","");
        retText = retText.replaceAll("·|·",",");
        retText = retText.replaceAll("\\(+[⺀-⺙⺛-⻳⼀-⿕々〇〡-〩〸-〺〻㐀-䶵一-鿃豈-鶴侮-頻並-龎]+\\)", "");
        retText = retText.replaceAll("(\n {1,}\n)|(\n\\.\n)","\n\n");

        retText =retText.replaceAll("\\!{2,}","!");
        retText =retText.replaceAll("\\?{2,}","?");
        retText =retText.replaceAll("\\,{2,}",",");
        retText =retText.replaceAll("\\.{2,}",".");
        retText = retText.replaceAll("규모","규모,");
        retText = retText.replaceAll("주의","주이");
        return retText;
    }

    private String normByDict(String text) {
        String retText = text;
        Iterator<Map.Entry<String, String>> it = dictEtc.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();
            if (retText.contains(entry.getKey())) {
                retText = retText.replace(entry.getKey(), entry.getValue());
            }
        }
        retText = retText.replaceAll("\\sSOL\\s"," 쏠 ");
        retText = retText.replaceAll("\\sFAN\\s"," 판 ");
        retText = retText.replaceAll("\\sPONEY\\s"," 포니 ");
        retText = retText.replaceAll("\\sS-MORE\\s"," 에스모아 ");
        retText = retText.replaceAll("#"," 샵 ");
        retText = retText.replaceAll("\\sSOHO\\s"," 소호 ");
        retText = retText.replaceAll("\\sS20\\s"," 에스이공 ");
        retText = retText.replaceAll("\\skids&teens\\s"," 키즈앤틴즈 ");
        retText = retText.replaceAll("\\su\\+\\s|\\sU\\+\\s"," 유플러스 ");
        retText = retText.replaceAll("\\sYOLO\\s"," 욜로 ");
        retText = retText.replaceAll("\\sK-CASH\\s"," 케이캐쉬 ");
        retText = retText.replaceAll("\\sSHOP ATM\\s"," 샵 에이티엠 ");
        retText = retText.replaceAll("\\sTmoney\\s"," 티머니 ");
        retText = retText.replaceAll("\\sTops Club\\s"," 탑스클럽 ");
        retText = retText.replaceAll("\\sWechat\\s"," 위챗 ");
        retText = retText.replaceAll("\\swechat\\s"," 위챗 ");
        retText = retText.replaceAll("\\sWeChat\\s"," 위챗 ");
        retText = retText.replaceAll("\\sWechat Pay\\s"," 위챗페이 ");
        retText = retText.replaceAll("\\sSkypass\\s"," 스카이패스 ");
        retText = retText.replaceAll("\\sHi-Point\\s"," 하이포인트 ");
        retText = retText.replaceAll("\\sHi point\\s"," 하이포인트 ");
        retText = retText.replaceAll("\\sThe more\\s"," 더 모아 ");
        retText = retText.replaceAll("\\sTEENS\\s"," 틴즈 ");
        retText = retText.replaceAll("\\sPLUS\\s"," 플러스 ");
        retText = retText.replaceAll("\\s신한sign\\s"," 신한싸인 ");
        retText = retText.replaceAll("\\sPOS\\s"," 포스 ");
        retText = retText.replaceAll("\\sCube\\s"," 큐브 ");


//        Pattern pText = Pattern.compile("[가-힣a-zA-Z]\\-[가-힣a-zA-Z]");
//        Matcher matcherWord = pText.matcher(retText);
//        while (matcherWord.find()){
//            String tmp = matcherWord.group();
//            tmp = tmp.replace("-"," ");
//            retText = retText.replace(matcherWord.group(), tmp);
//        }
        return retText;
    }
    private String norm119(String text){
        String retText = text;

        for (Map.Entry<String, String> entry : dictPostposition.entrySet()) {
                retText = retText.replaceAll("\\b119"+entry.getKey(), "일릴구"+entry.getKey());
        }
        return retText;
    }
    // Eng to Kor, dict -> upper -> default
    private String normEnglish(String text) {
        String retText = text;
        List<String> subStrList = new ArrayList<>();
        Pattern pText = Pattern.compile("([a-zA-Z]+'[a-zA-Z]+)|[a-zA-Z]+");
        Matcher matcherWord = pText.matcher(text);
        while (matcherWord.find()) {
            subStrList.add(matcherWord.group());
        }
        for (String engSubStr : subStrList) {
            // dict
            if (dictEngWord.get(engSubStr) != null) {
                retText = retText.replaceFirst(String.format("%s",engSubStr), dictEngWord.get(engSubStr));
            }
            // upper
            else if (engSubStr.equals(engSubStr.toUpperCase())) {
                String upString = engSubStr.toUpperCase();
                String tmp = "";
                for (char a : upString.toCharArray()) {
                    if (dictEngLetter.get(String.valueOf(a)) != null) {
                        tmp += dictEngLetter.get(String.valueOf(a));
                    }
                    else {
                        tmp += String.valueOf(a);
                    }
                }
                retText = retText.replaceFirst(String.format("%s",engSubStr), tmp);
            }
        }

        return retText;
    }

    private String normKonglish(String text) {
        String retText=text;
        Pattern pText = Pattern.compile("([a-zA-Z]+'[a-zA-Z]+)|[a-zA-Z]+");
        Matcher matcherWord = pText.matcher(text);
        List<String> engWordList = new ArrayList<>();
        while (matcherWord.find()) {
            engWordList.add(matcherWord.group());
        }

        if (engWordList.size() > 0) {
            List<String> korWordList = konglish.callKonglish(engWordList);
            if (korWordList != null)
                for (int wordIdx = 0; wordIdx < engWordList.size(); wordIdx++) {
                    String engWord = engWordList.get(wordIdx);
                    String korWord = korWordList.get(wordIdx);
                    retText = retText.replaceFirst(engWord, korWord);
                }
        }

        return retText;
    }

    private String normNum(String text) {
        String retText = text;

        // Remaining digit process
        Pattern sPat = Pattern.compile(regNumCheck);
        Matcher sMat = sPat.matcher(retText);
        String tmp="";
        while (sMat.find()) {
            tmp = sMat.group();
            int floatCheck = tmp.length() - tmp.replace(".", "").length();
            if (1 < floatCheck) {
                retText = retText.replace(tmp, normSimpleNum(tmp.replace(".", " 쩜 ")));
            } else if (1 == floatCheck) {
                String[] arr = tmp.split("\\.",2);
                String intNum = arr[0];
                if(arr[1].isEmpty()){
                    intNum = normIntNum(intNum);
                    retText = retText.replace(tmp,intNum);
                }else {
                    String fltNum = arr[1];
                    intNum = normIntNum(intNum);
                    fltNum = normSimpleNum(fltNum);
                    retText = retText.replace(tmp, intNum + " 쩜 " + fltNum);
                }
            } else {

                retText = retText.replaceFirst(tmp, normIntNum(tmp));

            }
        }
        return retText;
    }
    //치환 후 추가 전처리
    private  String normAfter(String text){
        String retText= text;
        retText = retText.replaceAll("[^가-힣\\!\\'\\(\\)\\,\\-\\.\\:\\~\\? \n\\/]","");
        if(!retText.substring(retText.length()-1).matches("(\\.|\\?|\\!|\n)")){
            retText= retText+",,,";
        }
        retText = retText.replaceAll(" +\n","\n");
        retText = retText.replaceAll("^([가-힣])([\\!\\?\\.\\~])$","$1$2$2");
        retText = retText.replaceAll("  "," ");
        Pattern twentyCntCheckPat = Pattern.compile("스물"+regCntCheck);
        Matcher twentyCntCheckMat = twentyCntCheckPat.matcher(retText);
        while(twentyCntCheckMat.find()){
            String tmp = twentyCntCheckMat.group();
            tmp = tmp.replace("스물","스무");
            retText = retText.replace(twentyCntCheckMat.group(),tmp);
        }
        return retText;
    }

    private String changePronun(String text) {
        ArrayList<String> textList = new ArrayList<>();
        textList.add(text);
        List<String> sentenceList = g2p.callG2p(textList);
        return sentenceList.get(0);
    }

    // Phone number process
    private String cvtPhoneNum(String text){
        String retText = text;
        Pattern sPat = Pattern.compile("((\\d{3}|\\d{2}) (\\d{4}|\\d{3}) (\\d{4}))");
        Matcher sMat = sPat.matcher(retText);
        while (sMat.find()) {
            String tmp = normSimpleNum(sMat.group());
            tmp = tmp.replace("영","공");
            tmp = tmp.replace(" ", ", ");
            retText = retText.replace(sMat.group(), tmp);

        }
        return retText;
    }

    //landline process
    private String cvtLandLineNum(String text) {
        String retText = text;
        Pattern landLinePattern = Pattern.compile("(\\d{4})\\-(\\d{4})");
        Matcher landLineMatcher = landLinePattern.matcher(retText);

        while (landLineMatcher.find()) {
            String tmp = normSimpleNum(landLineMatcher.group());
            tmp = tmp.replace("영","공");
            tmp = tmp.replace("-", ",");
//            tmp = tmp.replace(" ", ", ");
            retText = retText.replace(landLineMatcher.group(), tmp);
        }
        return retText;
     }

    //계좌번호
    private String cvtAccountNum(String text){
        String retText = text;
        Pattern sPat = Pattern.compile("\\d+\\-+\\d+\\-\\d+");
        Matcher sMat = sPat.matcher(retText);
        while (sMat.find()){
            String tmp = normSimpleNum(sMat.group());
            tmp = tmp.replace("영","공");
            tmp = tmp.replace("-",", ");
            retText = retText.replace(sMat.group(), tmp);
        }
        return retText;
    }

    private String cvtEtcNum(String text) {
        String retText =text;

        // 숫자+ ~ + 숫자 치환
        Pattern sPat = Pattern.compile(regNumCheck + "(\\~| \\~|\\~ | \\~ )" + regNumCheck);
        Matcher sMat = sPat.matcher(retText);
        while (sMat.find()) {
            String tmp = sMat.group().replace("~", " 에서 ");
            retText = retText.replace(sMat.group(), tmp);
        }

        //분수형태 / 치환
        sPat = Pattern.compile(regNumCheck+"\\/"+regNumCheck);
        sMat = sPat.matcher(retText);
        String numeratorNum = "";
        String denominatorNum = "";

        while(sMat.find()){
            numeratorNum = sMat.group().split("\\/")[0];
            denominatorNum= sMat.group().split("\\/")[1];
            retText = retText.replace(sMat.group(),denominatorNum+" 분의 "+numeratorNum);
        }

        //초속, 시속 형태 치환
        sPat = Pattern.compile(regNumCheck+"( |)(m\\/s|km\\/h|m\\/h)");
        sMat = sPat.matcher(retText);
        String numCat="";
        Pattern speedPat;
        Matcher speedMat;
        while(sMat.find()){
            speedPat = Pattern.compile(regNumCheck);
            speedMat = speedPat.matcher(sMat.group());
            if (speedMat.find()){
                numCat = speedMat.group();
            }
            if(sMat.group().matches(regNumCheck+"(( |)m\\/s)")) {
                retText = retText.replaceAll(String.format("\\b%s", sMat.group()), "초속 " + numCat + " 미터");
            }
            else if(sMat.group().matches(regNumCheck+"(( |)km\\/h)")){
                retText = retText.replaceAll(String.format("\\b%s", sMat.group()), "시속 " + numCat + " 키로미터");
            }
            else if(sMat.group().matches(regNumCheck+"(( |)m\\/h)")){
                retText = retText.replaceAll(String.format("\\b%s", sMat.group()), "시속 " + numCat + " 미터");
            }
        }

        return retText;
    }

    //hh:mm 치환
    private  String cvtTime(String text) {
        Pattern sPat = Pattern.compile(regNumCheck + ":" + regNumCheck);
        Matcher sMat = sPat.matcher(text);
        while (sMat.find()) {
            String[] textArray = sMat.group().split(":");
            if(textArray[1].equals("0") || textArray[1].equals("00")){
                text = text.replace(sMat.group(), textArray[0] + "시 ");
            }else {
                text = text.replace(sMat.group(), textArray[0] + "시 " + textArray[1] + "분");
            }
        }
        return text;
    }

    private String cvtJUM(String text ) {
        Pattern sPat = Pattern.compile("쩜( |\\d)(\\d)+");
        Matcher sMat = sPat.matcher(text);
        while (sMat.find()) {
            String tmp = normSimpleNum(sMat.group());
            text = text.replace(sMat.group(), tmp);
        }
        return text;
    }

    private String cvtMetrics(String text) {
        //숫자+단위 형태 (영어) 치환
        Pattern sPat = Pattern.compile(regNumCheck + "( |)[a-zA-Z|%|㎡|℃|℉|㎍\\/㎥|˚]+");
        Matcher sMat = sPat.matcher(text);
        while (sMat.find()) {
            String metricStr = korMetrics(sMat.group());
            text = text.replaceAll(String.format("%s", sMat.group()), metricStr);
        }
        return text;
    }
    private String cvtAddress(String text) {
        //숫자+단위 형태 (영어) 치환
        Pattern sPat = Pattern.compile(regNumCheck +"-"+regNumCheck);
        Matcher sMat = sPat.matcher(text);
        while (sMat.find()) {
            text = text.replace("-"," 다시 ");
        }
        return text;
    }
    //숫자+글자 조합의 텍스트 치환
    private String korMetrics(String text) {
        String partMetrics = "";
        String partNum = "";
        Pattern pText = Pattern.compile("[a-zA-Z|%|㎡|℃|℉|㎍\\/㎥|˚]+");
        Matcher pMat = pText.matcher(text);
        if (pMat.find()) {
            partMetrics = pMat.group();
            partNum = text.replace(partMetrics, "");

            for (Map.Entry<String, String> entry : dictMetrics.entrySet()) {
                if (entry.getKey().equals(pMat.group())) {
                    partMetrics = partMetrics.replace(entry.getKey(), entry.getValue());
                    break;
                }
            }
        }
        return partNum + partMetrics;
    }

    // Count to Kor
    private String cvtCountNum(String text) {
        Pattern monthCheckPattern = Pattern.compile("\\d+개월");
        Matcher monthCheckMatcher = monthCheckPattern.matcher(text);
        while(monthCheckMatcher.find()){
            String monthText = monthCheckMatcher.group();
            Pattern getNumberInMonthCheckPattern = Pattern.compile(regNumCheck);
            Matcher getNumberInMonthCheckMatcher = getNumberInMonthCheckPattern.matcher(monthText);
            while(getNumberInMonthCheckMatcher.find()){
                String getNumberText = getNumberInMonthCheckMatcher.group();
                monthText = monthText.replace(getNumberText,normIntNum(getNumberText));
            }
            text = text.replace(monthCheckMatcher.group(),monthText);
        }
        Pattern timeClockPattern = Pattern.compile("([0-9]+)(시간)");
        Matcher timeClockMatcher = timeClockPattern.matcher(text);
        while(timeClockMatcher.find()){
            Pattern getNumberInTimeClockPattern = Pattern.compile(regNumCheck);
            Matcher getNumberInTimeClockMatcher = getNumberInTimeClockPattern.matcher(timeClockMatcher.group());
            String changeStr = timeClockMatcher.group();
            while(getNumberInTimeClockMatcher.find()){
                String numberStr = getNumberInTimeClockMatcher.group();
                if(Integer.parseInt(numberStr)<24){
                    String tmp = normCountNum(numberStr);
                    changeStr = changeStr.replace(getNumberInTimeClockMatcher.group(),tmp);
                    text = text.replaceFirst(timeClockMatcher.group(),changeStr);
                }else {
                    String tmp = normIntNum(numberStr);

                    changeStr = changeStr.replace(getNumberInTimeClockMatcher.group(),tmp);
                    text = text.replaceFirst(timeClockMatcher.group(),changeStr);

                }
            }
        }
        Pattern timeHourPattern = Pattern.compile("(1[0-2]|([\\d]|)[0-9])시");
        Matcher timeHourMatcher = timeHourPattern.matcher(text);
        while(timeHourMatcher.find()){
            Pattern getNumberInTimeHourPattern = Pattern.compile(regNumCheck);
            Matcher getNumberInTimeHourMatcher = getNumberInTimeHourPattern.matcher(timeHourMatcher.group());
            String changeStr = timeHourMatcher.group();
            while(getNumberInTimeHourMatcher.find()){
                String numberStr = getNumberInTimeHourMatcher.group();
                if(Integer.parseInt(numberStr)<13){
                    String tmp = normCountNum(numberStr);
                    changeStr = changeStr.replace(getNumberInTimeHourMatcher.group(),tmp);
                    text = text.replaceFirst(timeHourMatcher.group(),changeStr);
                }else {
                    String tmp = normIntNum(numberStr);

                    changeStr = changeStr.replace(getNumberInTimeHourMatcher.group(),tmp);
                    text = text.replaceFirst(timeHourMatcher.group(),changeStr);

                }
            }
        }

        // Count process
        Pattern numberWithCountTextPattern = Pattern.compile(regNumCheck + regCntCheck);
        Matcher numberWithCountTextMatcher = numberWithCountTextPattern.matcher(text);
        while (numberWithCountTextMatcher.find()) {

            String changeStr = normCountNum(numberWithCountTextMatcher.group());
            text = text.replaceFirst(numberWithCountTextMatcher.group(), changeStr);
        }


        return text;
    }
    private String normCountNum(String text) {
        String partNum;
        String partCount;
        Pattern tmpPat = Pattern.compile(regNumCheck);
        Matcher tmpMat = tmpPat.matcher(text);
        if(tmpMat.find()) {
            partNum = tmpMat.group();
            partCount = text.replace(partNum, ""); //text = 7개  partNum = 7 partCount = 개

            if(partNum.contains(".")){
                partNum = normNum(partNum);
            }
            else if(partNum.length() > 2 && !partCount.contains(".")){
                String smallInt = partNum.substring(partNum.length()-2);
                String largeInt = partNum.substring(0, partNum.length() -2) + "00";

                largeInt = normIntNum(largeInt);
                smallInt = normIntNum(smallInt);


                for(Map.Entry<String, String> entry: dictCntNumSingle.entrySet()){
                    smallInt = smallInt.replace(entry.getKey(), entry.getValue());
                }

                for(Map.Entry<String, String> entry: dictCntNumDouble.entrySet()){
                    smallInt = smallInt.replace(entry.getKey(), entry.getValue());
//                    smallInt=smallInt.replace("십","열");
                }
                smallInt = smallInt.replace("영","");
                partNum = largeInt.replace("영십영", "") + smallInt;
                partNum = partNum.replace("영십", "");
            }
            else {

                partNum = normIntNum(partNum);
                for (Map.Entry<String, String> entry : dictCntNumSingle.entrySet()) {
                    partNum = partNum.replace(entry.getKey(), entry.getValue());
                }
                for (Map.Entry<String, String> entry : dictCntNumDouble.entrySet()) {
                    partNum = partNum.replace(entry.getKey(), entry.getValue());
                }

                partNum=partNum.replace("십","열");
            }
        }
        else {
            logger.error("No intger given for count");
            return "";
        }
        String returnText = partNum + partCount;
        return returnText;
    }

    // Integer to Kor
    private String normIntNum(String text) {
        String tmpText = text.replace(",", "");
        if (tmpText.length() < 3 && Integer.parseInt(tmpText) == 0) {
            return "영";
        }

        String retText = "";
        String sign = "";
        if (tmpText.toCharArray()[0] == '-') {
            sign = "마이너스 ";
            tmpText = tmpText.substring(1);
        }
        retText= cvtNumToKor(tmpText);

        retText = sign + retText;

        retText = retText.replace("영만", "만");
        retText = retText.replace("영억", "억");
        retText = retText.replace("영조", "조");
        retText = retText.replace("영경", "경");
        retText = retText.replace("영해", "해");
        retText = retText.replaceAll("일십", "십");
        retText = retText.replaceAll("일백", "백");
        retText = retText.replaceAll("일천", "천");

        retText = retText.replaceAll("(영.|영)", "");


        retText=retText.replace("해 경","해");
        retText=retText.replace("해 조","해");
        retText=retText.replace("해 억","해");
        retText=retText.replace("해 만","해");
        retText=retText.replace("경 조","경");
        retText=retText.replace("경 억","경");
        retText=retText.replace("경 만","경");
        retText=retText.replace("조 억","조");
        retText=retText.replace("조 만","조");
        retText=retText.replace("억 만","억");

        return retText;
    }

    // Digit to Kor
    private String normSimpleNum(String text) {
        String retText = text;

        for (Map.Entry<String, String> entry : dictSimpleNum.entrySet()) {
            retText = retText.replace(entry.getKey(), entry.getValue());
        }

        return retText;
    }

    public static String cvtNumToKor(String money) {
        String[] han1 = {"","일","이","삼","사","오","육","칠","팔","구"};
        String[] han2 = {"","십","백","천"};
        String[] han3 = {"","만 ","억 ","조 ","경 ","해 "};

        StringBuffer result = new StringBuffer();
        try {
            int len = money.length();
            for (int i = len - 1; i >= 0; i--) {
                result.append(han1[Integer.parseInt(money.substring(len - i - 1, len - i))]);
                if (Integer.parseInt(money.substring(len - i - 1, len - i)) > 0)
                    result.append(han2[i % 4]);
                if (i % 4 == 0)
                    result.append(han3[i / 4]);
            }
        }catch (Exception e){
            System.out.println("자릿수 초과");
        }

        return result.toString();
    }

    public static void main(String[] args) {
        try {
            PreProcessKor preProcessKorTest = new PreProcessKor(500,"tts.kor.dict.list.properties","114.108.173.117:20001","114.108.173.117:19002");
            preProcessKorTest.init();
//            System.out.println(preProcessKorTest.preProcessText("2019년 08월 13일 Preprocess SOL FAN PONEY S-MORE # SOHO S20 kids&teens u+ YOLO K-CASH SHOP ATM 11:00 광명시,과천시,안산시,시흥시,안양시,평택시,군포시,화성시,광주광역시,나주시,담양군,곡성군,구례군,장성군,화순군,광양시,순천시,장흥군,영암군,함평군,영광군,세종시,계룡시,홍성군,서천군,당진시,예산군,청양군,부여군,금산군,논산시,아산시,공주시,천안시,대전광역시,영월군 지역에 폭염경보가 발효되었습니다."));
            System.out.println(preProcessKorTest.preProcessText(" SOL 앱 연동하여 로그인하시겠습니까?"));
            System.out.println(preProcessKorTest.preProcessText(" SOL Pay관리하기에서 결제계좌 변경 가능해요."));
            System.out.println(preProcessKorTest.preProcessText("신한 SOL Biz 사용 기업은 이런 점이 좋아요."));
            System.out.println(preProcessKorTest.preProcessText("신한Pay FAN 내에서 카드번호, 유효기간, CVC 확인이 가능합니다."));
            System.out.println(preProcessKorTest.preProcessText("카드 뒷면의 CVC2 코드값을 잘못 입력하셨어요."));
            System.out.println(preProcessKorTest.preProcessText("선불 Tmoney 탑재 기능은 TEENS , PLUS PONEY 체크카드만 가능합니다."));
            System.out.println(preProcessKorTest.preProcessText(" Tops Club 은 카드, 은행, 생명, 금융투자까지 아우르는 신한금융그룹의 우수 고객 우대서비스입니다."));
            System.out.println(preProcessKorTest.preProcessText("IC CHIP 거래 신용카드 POS 단말기 인증시스템 개발 전까지 서비스 이용이 제한될 수 있습니다."));
            System.out.println(preProcessKorTest.preProcessText("신한카드에는 Cube , Hi-Point 계열의 Nano, S-More포함, Love, Big Plus, Simple, Lady , 이공삼공 등이 있다."));
            System.out.println(preProcessKorTest.preProcessText(" Wechat Pay는 중국 메신저인 위챗의 모바일 결제 시스템이다."));
            System.out.println(preProcessKorTest.preProcessText("1+1 이벤트 및 사은품 증정 설정은 어떻게 하나요?"));
            System.out.println(preProcessKorTest.preProcessText("행사기간은 6월 30일부터 10월 1일까지 입니다."));
            System.out.println(preProcessKorTest.preProcessText("입력하신 휴대폰 번호는 010-1346-7946 입니다." ));
            System.out.println(preProcessKorTest.preProcessText("가상계좌 5621-78123-89012로 입금하시겠습니까?" ));
            System.out.println(preProcessKorTest.preProcessText("신한카드 1544-7200 삼성카드 1588-8900 현대카드 1577-6200 롯데카드 1588-8300 입니다." ));
            System.out.println(preProcessKorTest.preProcessText("개인사업자대출 119란 일시적 유동성 위기에 처한 개인사업자에 대해 은행이 선제적으로 관리하는 거예요," ));
            System.out.println(preProcessKorTest.preProcessText("가상계좌 입금가능 시간은 01시 00분 부터 23시 30분까지 입니다." ));
            System.out.println(preProcessKorTest.preProcessText("심사기간은 은행영업일 기준 약 2~3일 가량 소요 돼요." ));
            System.out.println(preProcessKorTest.preProcessText("불고기용 소고기 150g, 양파 2개, 쪽파, 계란 1개, 물 100ml," ));
            System.out.println(preProcessKorTest.preProcessText("내집마련 디딤돌 대출 중도상환수수료율은 1.2%에요." ));
            System.out.println(preProcessKorTest.preProcessText("쌀 한가마니는 약 80kg을 의미합니다." ));
            System.out.println(preProcessKorTest.preProcessText("1Km 는 10만cm이고, 100만mm이다." ));
            System.out.println(preProcessKorTest.preProcessText("23.5˚ 서비스 그대로를 직접 선택한 아트카드로 이용하실 수 있습니다." ));
            System.out.println(preProcessKorTest.preProcessText("민주택 전용면적 85㎡ 이하 아파트라고 생각하면 됩니다." ));
            System.out.println(preProcessKorTest.preProcessText("1%p는 두 백분율과의 산술적 차이를 나타낼 때 쓰는 단위이다. " ));
            System.out.println(preProcessKorTest.preProcessText("참고로 1hPa(헥토파스칼)이란 단어는 단순히 기압의 한 단위로," ));
            System.out.println(preProcessKorTest.preProcessText("신촌 방면에서 오실 경우 200m 앞 첫 골목 우회전" ));
            System.out.println(preProcessKorTest.preProcessText("1k㎡는 한변의 길이가 1km인 정사각형의 면적입니다. " ));
            System.out.println(preProcessKorTest.preProcessText("My car 서비스는 My shop My care 코너에서 찾아주세요" ));
            System.out.println(preProcessKorTest.preProcessText("Moneygram 은 Hey young 입니다" ));
            System.out.println(preProcessKorTest.preProcessText("Zero pay로 SOL Rich SOHO 샵 저렴하게 이용하자" ));
            System.out.println(preProcessKorTest.preProcessText("My world에서 지금 바로 S Dream카드를 발급받아보세요" ));
            System.out.println(preProcessKorTest.preProcessText("Readygo 서비스를 이용해주셔서 감사합니다" ));
            System.out.println(preProcessKorTest.preProcessText("U드림과 S드림 서비스가 통합됩니다" ));
            System.out.println(preProcessKorTest.preProcessText(" kids&teens 서비스를 사용하신 분들은 그대로 S20 이 사용 가능합니다" ));
            System.out.println(preProcessKorTest.preProcessText("LG u+ 알뜰폰 고객님이십니까" ));
            System.out.println(preProcessKorTest.preProcessText(" 신한sign 과 SOL PB는 Inside bank 내에서 찾아주세요" ));
            System.out.println(preProcessKorTest.preProcessText("서비스에는 S Lite와 S choice, 그리고 Deep Dream이 있습니다." ));
            System.out.println(preProcessKorTest.preProcessText(" YOLO 서비스 안내드립니다." ));
            System.out.println(preProcessKorTest.preProcessText("CMA 통장은 S Line에서 개설 가능해요" ));
            System.out.println(preProcessKorTest.preProcessText(" Skypass 와 hipass 로 Hi point 적립해보세요" ));
            System.out.println(preProcessKorTest.preProcessText(" Banksign 의 The more 서비스 코너입니다." ));
            System.out.println(preProcessKorTest.preProcessText("Bizbank 내 SOL Global 서비스 이용해주세요." ));
            System.out.println(preProcessKorTest.preProcessText("Swift code가 필요합니다." ));
            System.out.println(preProcessKorTest.preProcessText(" SHOP ATM 에서 K-CASH 바로 출금이 가능합니다." ));
            System.out.println(preProcessKorTest.preProcessText("우물정 또는 # 버튼을 입력하여 주세요." ));
            System.out.println(preProcessKorTest.preProcessText("무이자 할부는 2 ~ 6개월 까지 가능해요."));
//            System.out.println(preProcessKorTest.preProcessText("12개월"));
//            System.out.println(preProcessTest.preProcessText("지금은 -12.35%였고 종류는 5가지와 19가지, 그리고 55가지였다"));
//            System.out.println(preProcessTest.preProcessText("100명 100기지 1000명 10000포기"));
//            System.out.println(preProcessTest.preProcessText("100"));
//            System.out.println(preProcessKorTest.preProcessText("20시간"));
//            System.out.println(preProcessKorTest.preProcessText("20개 23마리 20마리 20가지 20 "));
//            System.out.println(preProcessKorTest.preProcessText("1시간 2시간 3시간 4시간 5시간 6시간 7시간 8시간 9시간 10시간 11시간 12시간 13시간 14시간 15시간 16시간 17시간 18시간 19시간 20시간 21시간 22시간 23시간 24시간 25시"));
//            System.out.println(preProcessKorTest.preProcessText(" 170가지 270가지 270마리 250개 260개 290개 270개 260개   280개 170개 160개 370개 3070개 3370"));
//            System.out.println(preProcessKorTest.preProcessText("1달 2달 3달 4달 5달 6달 7달 8달 9달 10달"));
//            System.out.println(preProcessKorTest.preProcessText("260개" ));
//            System.out.println(preProcessTest.preProcessText("1995년 10월 5일 / 2019년 1월 16일"));
//            System.out.println(preProcessKorTest.preProcessText("1시 2시 3시 4시 5시 6시 7시 8시 9시 10시 11시 12시 13시 14시 15시 16시 17시 18시 19시 20시 21시 22시 23시 24시 24 시 15시간"));
//            System.out.println(preProcessTest.preProcessText("1분2분3분 4분 5분 6분 7분 8분 9분 10분 11분 21분 23분 24분 "));
//            System.out.println(preProcessTest.preProcessText("1초 2초 3초 4초 5초 6초 7초 8초 9초 10초 11초 12초 13초 14초 15초"));
//            System.out.println(preProcessTest.preProcessText("1주일 2주일 3주일 4주일 5주일 10주일"));
//            System.out.println(preProcessTest.preProcessText("1,2 ,3 , 13,23 1개2개2개,4개 "));
//            System.out.println(preProcessTest.preProcessText("10원 100원 1,000원 100000원 1,000,000원 123m 123km 134cm"));
//            System.out.println(preProcessTest.preProcessText("12345가지*******"));
//            System.out.println(preProcessTest.preProcessText("45가지"));
//            System.out.println(preProcessTest.preProcessText("123 ℃  123㎡ 1434℃ 1233 3434℉"));
//            System.out.println(preProcessTest.normKoreanOrEnglish("ㅁㄴㅇㄻㄴㅇㄹ가 ","eng"));

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }
}
