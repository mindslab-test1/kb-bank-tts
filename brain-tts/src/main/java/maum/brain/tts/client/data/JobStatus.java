package maum.brain.tts.client.data;

import maum.brain.tts.CommonCode;

import java.io.Serializable;

public class JobStatus implements Serializable {

    protected String jobId;
    protected CommonCode.JOB_STATUS status;
    protected long timestamp;

    public JobStatus(){
        timestamp = System.currentTimeMillis();
    }

    public JobStatus(String jobId, CommonCode.JOB_STATUS status){
        timestamp = System.currentTimeMillis();
        this.jobId = jobId;
        this.status = status;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public CommonCode.JOB_STATUS getStatus() {
        return status;
    }

    public void setStatus(CommonCode.JOB_STATUS status) {
        this.status = status;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "JobStatus{" +
                "jobId='" + jobId + '\'' +
                ", status=" + status +
                ", timestamp=" + timestamp +
                '}';
    }
}
