package maum.brain.tts.client;


import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import maum.brain.konglish.KonglishGrpc;
import maum.brain.konglish.KonglishOuterClass;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KonglishClient {

    @Value("${tts.ko_KR.konglish.address}")
    private String konglishAddress;

    public KonglishClient(){
    }

    public KonglishClient(String konglishAddress){
        this.konglishAddress = konglishAddress;
    }

    public List<String> callKonglish (List<String> wordList) {
        if (konglishAddress.isEmpty()) {
            return wordList;
        }

        ManagedChannel konglishChannel = null;
        try {
            KonglishOuterClass.English inputText = KonglishOuterClass.English.newBuilder().addAllWords(wordList).build();

            konglishChannel = ManagedChannelBuilder.forTarget(konglishAddress).usePlaintext().build();

            KonglishGrpc.KonglishBlockingStub kongStub = KonglishGrpc.newBlockingStub(konglishChannel);

            return kongStub.transliterate(inputText).getWordsList();

        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            konglishChannel.shutdown();
        }
    }
}
