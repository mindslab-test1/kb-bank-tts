package maum.brain.tts.rest.controller.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import maum.brain.tts.rest.CommonCode;

@JsonInclude(Include.NON_EMPTY)
public class BaseResponse<T> implements Serializable {
    private int code;
    private String msg;
    private T data;

    public BaseResponse() {
        this.code = CommonCode.TTS_RES_SUCCESS;
        this.msg = CommonCode.TTS_ERR_MSG_SUCCESS;
    }

    public BaseResponse(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
