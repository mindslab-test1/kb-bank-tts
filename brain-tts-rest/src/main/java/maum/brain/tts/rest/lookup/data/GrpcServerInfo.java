package maum.brain.tts.rest.lookup.data;

import java.io.Serializable;
import maum.brain.tts.rest.CommonCode.Lang;

public class GrpcServerInfo implements Serializable {
    protected String ip;
    protected int port;
    protected Lang lang;

    public GrpcServerInfo() {
    }

    public GrpcServerInfo(String ip, int port, Lang lang) {
        this.ip = ip;
        this.port = port;
        this.lang = lang;
    }

    public Lang getLang() {
        return this.lang;
    }

    public void setLang(Lang lang) {
        this.lang = lang;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return this.port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String toString() {
        return "GrpcServerInfo{lang=" + this.lang + ", ip='" + this.ip + '\'' + ", port=" + this.port + '}';
    }
}
