package maum.brain.tts.rest;

public class CommonCode {
    public static int TTS_RES_SUCCESS = 0;
    public static int TTS_ERR_CONN_REFUSED = 50001;
    public static int TTS_ERR_CONN_TIMEOUT = 50002;
    public static int TTS_ERR_REQ_TIMEOUT = 50003;
    public static int TTS_ERR_TACOTRON_ERROR = 51001;
    public static int TTS_ERR_WAVENET_ERROR = 52001;
    public static int TTS_ERR_JOB_STILL_RUNNING = 59001;
    public static int TTS_ERR_JOB_NOT_EXIST = 59002;
    public static int TTS_ERR_AUTH_VERIFICATION_ERROR = 40001;
    public static int TTS_ERR_SERVER_NOT_FOUND_ERROR = 40002;
    public static int TTS_ERR_NOT_SUPPORTED_LANG_ERROR = 40003;
    public static int TTS_ERR_NOT_SUPPORTED_AUDIOENCODING_ERROR = 40004;
    public static int TTS_ERR_SVC_ERROR = 1;
    public static int TTS_ERR_SVC_EXCEEDED_TEXT_LENGTH = 60001;
    public static String TTS_ERR_MSG_SVC_EXCEEDED_TEXT_LENGTH = "Requested text exceeded max. length.";
    public static String TTS_ERR_MSG_SUCCESS = "Success";

    public CommonCode() {
    }

    public static enum AudioEncoding {
        WAV,
        PCM;

        private AudioEncoding() {
        }
    }

    public static enum JOB_STATUS {
        notexist,
        started,
        running,
        completed,
        error;

        private JOB_STATUS() {
        }
    }

    public static enum Lang {
        ko_KR,
        en_US;

        private Lang() {
        }
    }

    public static enum ServerType {
        tacotron,
        wavenet;

        private ServerType() {
        }
    }

    public static enum TTS_STATUS {
        notexist,
        ready,
        beforeTacotron,
        tacotronStarted,
        tacotronCompleted,
        beforeWavenet,
        wavenetStarted,
        wavenetCompleted,
        completed;

        private TTS_STATUS() {
        }
    }
}
