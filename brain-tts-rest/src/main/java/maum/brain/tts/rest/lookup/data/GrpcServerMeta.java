package maum.brain.tts.rest.lookup.data;

import java.io.Serializable;
import maum.brain.tts.rest.CommonCode.Lang;

public class GrpcServerMeta implements Serializable {
    protected Lang lang;

    public GrpcServerMeta() {
    }

    public GrpcServerMeta(Lang lang) {
        this.lang = lang;
    }

    public Lang getLang() {
        return this.lang;
    }

    public void setLang(Lang lang) {
        this.lang = lang;
    }

    public String toString() {
        return "GrpcServerMeta{lang=" + this.lang + '}';
    }
}
