package maum.brain.tts.rest.controller.data;

import java.io.Serializable;
import maum.brain.tts.rest.CommonCode.AudioEncoding;
import maum.brain.tts.rest.CommonCode.Lang;

public class TtsRestRequest implements Serializable {
    private Lang lang;
    private int speaker;
    private String text;
    private String profile;
    private String fileName;
    private AudioEncoding audioEncoding;

    public AudioEncoding getAudioEncoding() {
        return this.audioEncoding;
    }

    public void setAudioEncoding(AudioEncoding audioEncoding) {
        this.audioEncoding = audioEncoding;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getProfile() {
        return this.profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public TtsRestRequest() {
    }

    public TtsRestRequest(Lang lang, int speaker, String text) {
        this.lang = lang;
        this.speaker = speaker;
        this.text = text;
    }

    public Lang getLang() {
        return this.lang;
    }

    public void setLang(Lang lang) {
        this.lang = lang;
    }

    public String getText() {
        return this.text;
    }

    public void setSpeaker(int speaker) {
        this.speaker = speaker;
    }

    public int getSpeaker() {
        return this.speaker;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String toString() {
        return "TtsRestRequest{lang=" + this.lang + ", speaker=" + this.speaker + ", text='" + this.text + '\'' + ", profile='" + this.profile + '\'' + ", fileName='" + this.fileName + '\'' + ", audioEncoding=" + this.audioEncoding + '}';
    }
}