package maum.brain.tts.rest.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import maum.brain.tts.NgTtsServiceGrpc;
import maum.brain.tts.PreprocessingRequest;
import maum.brain.tts.PreprocessingResponse;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.tts.TtsRequest;
import maum.brain.tts.NgTtsServiceGrpc.NgTtsServiceBlockingStub;
import maum.brain.tts.rest.CommonCode.AudioEncoding;
import maum.brain.tts.rest.CommonCode.Lang;
import maum.brain.tts.rest.lookup.GrpcTtsServerLookup;
import maum.brain.tts.rest.lookup.data.GrpcServerInfo;
import maum.brain.tts.rest.utils.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TtsGrpcClient {
    @Autowired
    private GrpcTtsServerLookup resolver;
    private final long deadline = 90L;
    private Logger logger = LoggerFactory.getLogger(TtsGrpcClient.class);

    public TtsGrpcClient() {
    }

    public Iterator<TtsMediaResponse> speakBlocking(String site, Lang lang, int speaker, String text, String profile, AudioEncoding audioEncoding) {
        Iterator<TtsMediaResponse> iter = null;
        ManagedChannel channel = null;

        try {
            GrpcServerInfo server = this.resolver.lookup(lang);
            if ("KBCard".equals(site)) {
//                int port = true;
                short port;
                if (speaker == 0) {
                    port = 30991;
                } else if (speaker == 1) {
                    port = 30992;
                } else {
                    port = 30993;
                }

                channel = ManagedChannelBuilder.forAddress(server.getIp(), port).usePlaintext().build();
            } else {
                channel = ManagedChannelBuilder.forAddress(server.getIp(), server.getPort()).usePlaintext().build();
            }

            maum.common.LangOuterClass.Lang ttsLang = ObjectMapper.convertLang(lang);
            maum.brain.tts.AudioEncoding ttsAudioEncoding = ObjectMapper.convertAudioEncoding(audioEncoding);
            NgTtsServiceBlockingStub stub = NgTtsServiceGrpc.newBlockingStub(channel);
            TtsRequest req = TtsRequest.newBuilder().setLang(ttsLang).setSpeaker(speaker).setText(text).setProfile(profile).setAudioEncoding(ttsAudioEncoding).build();
            iter = ((NgTtsServiceBlockingStub)stub.withDeadlineAfter(90L, TimeUnit.SECONDS)).speakWav(req);
            return iter;
        } catch (Exception var14) {
            this.logger.error("TtsGrpcClient:speakBlocking:" + var14.getMessage());
            return iter;
        }
    }

    public PreprocessingResponse preprocBlocking(String site, Lang lang, int speaker, String text, String profile) {
        PreprocessingResponse preproc = null;
        ManagedChannel channel = null;

        try {
            GrpcServerInfo server = this.resolver.lookup(lang);
            if ("KBCard".equals(site)) {
//                int port = true;
                short port;
                if (speaker == 0) {
                    port = 30991;
                } else if (speaker == 1) {
                    port = 30992;
                } else {
                    port = 30993;
                }

                channel = ManagedChannelBuilder.forAddress(server.getIp(), port).usePlaintext().build();
            } else {
                channel = ManagedChannelBuilder.forAddress(server.getIp(), server.getPort()).usePlaintext().build();
            }

            maum.common.LangOuterClass.Lang ttsLang = ObjectMapper.convertLang(lang);
            NgTtsServiceBlockingStub stub = NgTtsServiceGrpc.newBlockingStub(channel);
            PreprocessingRequest req = PreprocessingRequest.newBuilder().setLang(ttsLang).setText(text).setProfile(profile).build();
            preproc = stub.preprocess(req);
        } catch (Exception var20) {
            this.logger.error("TtsGrpcClient:preprocBlocking:" + var20.getMessage());
        } finally {
            if (channel != null) {
                channel.shutdown();

                try {
                    channel.awaitTermination(1L, TimeUnit.SECONDS);
                } catch (InterruptedException var19) {
                    this.logger.error("TtsGrpcClient:preprocBlocking:" + var19.getMessage());
                }
            }

        }

        return preproc;
    }
}
