package maum.brain.tts.rest.exception;

public class TtsRestException extends Exception {
    private int errCode;

    public TtsRestException() {
    }

    public TtsRestException(String message) {
        super(message);
    }

    public TtsRestException(int errCode, String message) {
        super(message);
        this.errCode = errCode;
    }

    public TtsRestException(String message, Throwable cause) {
        super(message, cause);
    }

    public TtsRestException(Throwable cause) {
        super(cause);
    }

    public TtsRestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public int getErrCode() {
        return this.errCode;
    }
}
