package maum.brain.tts.rest.lookup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.PostConstruct;
import maum.brain.tts.rest.CommonCode;
import maum.brain.tts.rest.CommonCode.Lang;
import maum.brain.tts.rest.exception.TtsRestException;
import maum.brain.tts.rest.lookup.data.GrpcServerInfo;
import maum.brain.tts.rest.lookup.data.GrpcServerMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class GrpcTtsServerLookup {
    private Logger logger = LoggerFactory.getLogger(GrpcTtsServerLookup.class);
    @Autowired
    private Environment env;
    protected HashMap<String, ArrayList<GrpcServerInfo>> servers = new HashMap();
    protected AtomicInteger krIndex = new AtomicInteger();
    protected AtomicInteger enIndex = new AtomicInteger();

    public GrpcTtsServerLookup() {
    }

    @PostConstruct
    private void initData() {
        ArrayList<GrpcServerInfo> krList = new ArrayList();
        ArrayList enList = new ArrayList();

        try {
            String krIps = this.env.getProperty("tts.grpc.ko_KR.ips");
            this.logger.info("ko_KR servers:{}", krIps);
            String[] krIpArray = krIps.split(",");
            int krPort = Integer.parseInt(this.env.getProperty("tts.grpc.ko_KR.port"));
            this.logger.info("ko_KR port: {}", krPort);
            String[] var9 = krIpArray;
            int enPort = krIpArray.length;

            String enIps;
            for(int var7 = 0; var7 < enPort; ++var7) {
                enIps = var9[var7];
                GrpcServerInfo info = new GrpcServerInfo(enIps, krPort, Lang.ko_KR);
                krList.add(info);
            }

            this.servers.put("ko_KR", krList);
            enIps = this.env.getProperty("tts.grpc.en_US.ips");
            this.logger.info("en_US servers:{}", enIps);
            String[] enIpArray = enIps.split(",");
            enPort = Integer.parseInt(this.env.getProperty("tts.grpc.en_US.port"));
            this.logger.info("en_US port: {}", enPort);
            String[] var12 = enIpArray;
            int var11 = enIpArray.length;

            for(int var15 = 0; var15 < var11; ++var15) {
                String ip = var12[var15];
                GrpcServerInfo info = new GrpcServerInfo(ip, enPort, Lang.en_US);
                enList.add(info);
            }

            this.servers.put("en_US", enList);
        } catch (Exception var14) {
            this.logger.error("GRPC server info parsing error. {}", var14.getMessage());
        }

    }

    public GrpcServerInfo lookup(GrpcServerMeta meta) throws TtsRestException {
        if (meta == null) {
            return null;
        } else {
            ArrayList<GrpcServerInfo> list = (ArrayList)this.servers.get(meta.getLang().name());
            if (list == null) {
                return null;
            } else {
//                int index = false;
                int serverCount = list.size();
                Lang lang = meta.getLang();
                int index;
//                switch($SWITCH_TABLE$maum$brain$tts$rest$CommonCode$Lang()[lang.ordinal()]) {
                switch(lang.ordinal()) {
                    case 0:
                        index = this.krIndex.incrementAndGet();
                        break;
                    case 1:
                        index = this.enIndex.incrementAndGet();
                        break;
                    default:
                        throw new TtsRestException(CommonCode.TTS_ERR_NOT_SUPPORTED_LANG_ERROR, "Not Supported Language");
                }

                return (GrpcServerInfo)list.get(index % serverCount);
            }
        }
    }

    public GrpcServerInfo lookup(Lang lang) throws TtsRestException {
        return this.lookup(new GrpcServerMeta(lang));
    }
}
