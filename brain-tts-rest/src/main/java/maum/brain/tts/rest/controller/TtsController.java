package maum.brain.tts.rest.controller;

import com.google.protobuf.ByteString;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import maum.brain.tts.PreprocessingResponse;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.tts.rest.CommonCode.AudioEncoding;
import maum.brain.tts.rest.client.TtsGrpcClient;
import maum.brain.tts.rest.controller.data.TtsRestRequest;
import maum.brain.tts.rest.utils.ConvertAudio;
import maum.brain.tts.rest.utils.FileDownloadUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@RestController
@RequestMapping({"/tts"})
public class TtsController {
    private Logger logger = LoggerFactory.getLogger(TtsController.class);
    @Autowired
    private TtsGrpcClient client;
    @Autowired
    private ConvertAudio convertAudio;
    @Value("${tts.maxlength}")
    private int maxLength;
    @Value("${tts.downloads}")
    private String downloads;

    public TtsController() {
    }

    @RequestMapping(
            value = {"/ttsPreproc"},
            method = {RequestMethod.POST},
            consumes = {"application/json"}
    )
    @ResponseBody
    public String preproc(@RequestBody TtsRestRequest request, HttpServletResponse response, HttpServletRequest servletRequest) {
        this.logger.info("tts/ttsPreproc:{}", request);
        String retText = request.getText().trim();
        if (retText == null | retText.equals("")) {
            this.logger.error("tts/ttsPreproc:Input text is Empty");
            return null;
        } else {
            String result = "";

            try {
                if (request.getProfile() == null | request.getProfile().equals("")) {
                    request.setProfile("1");
                }

                String site = System.getProperty("site");
                if (site == null || site.isEmpty()) {
                    site = "Mindslab";
                }

                PreprocessingResponse preproc = this.client.preprocBlocking(site, request.getLang(), request.getSpeaker(), retText, request.getProfile());
                result = preproc.getText().trim();
            } catch (Exception var11) {
                this.logger.error("tts/ttsPreproc:" + var11.getMessage());
            } finally {
                this.logger.info("tts/ttsPreproc:close");
            }

            return result;
        }
    }

    @RequestMapping(
            value = {"/block/speak"},
            method = {RequestMethod.POST},
            consumes = {"application/json"},
            produces = {"audio/x-wav"}
    )
    public StreamingResponseBody blockingSpeak(@RequestBody TtsRestRequest request, HttpServletResponse response, HttpServletRequest servletRequest) {
        this.logger.info("tts/block/speak:{}", request);
        String retText = request.getText().trim();
        String fileId = request.getFileName().trim();
        if (retText != null && !retText.isEmpty()) {
            if (fileId != null && !fileId.isEmpty()) {
                if (request.getProfile() == null || request.getProfile().isEmpty()) {
                    request.setProfile("1");
                }

                if (request.getAudioEncoding() == null) {
                    request.setAudioEncoding(AudioEncoding.WAV);
                }

                String site = System.getProperty("site");
                if (site == null || site.isEmpty()) {
                    site = "Mindslab";
                }

                final Iterator<TtsMediaResponse> iter = this.client.speakBlocking(site, request.getLang(), request.getSpeaker(), retText, request.getProfile(), request.getAudioEncoding());
                File fileDir = new File(this.downloads);
                if (!fileDir.exists()) {
                    fileDir.mkdir();
                }

                FileOutputStream fos = null;

                try {
                    this.logger.info("tts/block/speak:file={}", this.downloads + fileId + ".wav");
                    fos = new FileOutputStream(new File(this.downloads + fileId + ".wav"));
                } catch (Exception var11) {
                    this.logger.error("tts/block/speak:" + var11.getMessage());
                }

                FileOutputStream finalFos = fos;
                return new StreamingResponseBody() {
                    public void writeTo(OutputStream out) throws IOException {
                        TtsMediaResponse ttsResp = null;
                        WritableByteChannel och = Channels.newChannel(out);

                        ByteBuffer buff;
                        try {
                            for(; iter.hasNext(); och.write(buff)) {
                                ttsResp = (TtsMediaResponse)iter.next();
                                ByteString data = ttsResp.getMediaData();
                                buff = data.asReadOnlyByteBuffer();
                                if (finalFos != null) {
                                    finalFos.write(ttsResp.getMediaData().toByteArray());
                                }
                            }
                        } finally {
                            TtsController.this.logger.info("tts/block/speak:close");
                            och.close();
                            out.close();
                            if (finalFos != null) {
                                finalFos.close();
                            }

                        }

                    }
                };
            } else {
                this.logger.error("tts/block/speak:request.getFileName() is Empty");
                return null;
            }
        } else {
            this.logger.error("tts/block/speak:request.getText() is Empty");
            return null;
        }
    }

    @RequestMapping(
            value = {"/block/preListen"},
            method = {RequestMethod.POST, RequestMethod.GET},
            consumes = {"application/json"},
            produces = {"audio/x-wav"}
    )
    public StreamingResponseBody preListen(@RequestBody TtsRestRequest request, @RequestParam("file") String fileSet, @RequestParam("speed") String speed, @RequestParam("volume") String volume, @RequestParam("tone") String tone, @RequestParam("fileName") String fileId, HttpServletResponse response) throws Exception {
        this.logger.info("tts/block/preListen:{}", request);
        this.logger.info("tts/block/preListen:fileSet={}", fileSet);
        this.logger.info("tts/block/preListen:speed={}", speed);
        this.logger.info("tts/block/preListen:volume={}", volume);
        this.logger.info("tts/block/preListen:tone={}", tone);
        this.logger.info("tts/block/preListen:fileId={}", fileId);
        File f = new File(this.downloads + fileId + ".wav");
        String audioFile = this.convertAudio.convertAudio(f, fileSet, speed, volume, tone, 22050);
        InputStream is = new FileInputStream(this.downloads + audioFile);
        return (os) -> {
            this.readAndWrite(is, os);
        };
    }

    @RequestMapping(
            value = {"/block/preListenEx"},
            method = {RequestMethod.POST}
    )
    @ResponseBody
    public ResponseEntity<InputStreamResource> preListenEx(@RequestBody TtsRestRequest request, @RequestParam("file") String fileSet, @RequestParam("speed") String speed, @RequestParam("volume") String volume, @RequestParam("tone") String tone, @RequestParam("fileName") String fileId, HttpServletResponse response) throws IOException {
        this.logger.info("tts/block/preListenEx:{}", request);
        File f = new File(this.downloads + fileId + ".wav");
        String audioFile = this.convertAudio.convertAudio(f, fileSet, speed, volume, tone, 22050);
        Path path = Paths.get(this.downloads + audioFile);
        String contentType = Files.probeContentType(path);
        FileSystemResource file = new FileSystemResource(this.downloads + audioFile);
        return ResponseEntity.ok().contentLength(file.contentLength()).contentType(MediaType.parseMediaType(contentType)).body(new InputStreamResource(file.getInputStream()));
    }

    private void readAndWrite(InputStream is, OutputStream os) throws IOException {
        byte[] data = new byte[2048];
        boolean var4 = false;

        int read;
        while((read = is.read(data)) > 0) {
            os.write(data, 0, read);
        }

        os.flush();
    }

    @RequestMapping(
            value = {"/ttsOriginalDwn"},
            method = {RequestMethod.GET}
    )
    public ModelAndView originalFileDownload(HttpServletRequest request, @RequestParam("fileName") String fileId) {
        this.logger.info("tts/ttsOriginalDwn:start");
        AnnotationConfigApplicationContext context = null;
        HashMap fileInfo = null;

        ModelAndView var10;
        try {
            context = new AnnotationConfigApplicationContext(new Class[]{FileDownloadUtil.class});
            FileDownloadUtil fileDownloadUtil = (FileDownloadUtil)context.getBean(FileDownloadUtil.class);
            File file = new File(this.downloads + fileId + ".wav");
            String fileName = this.convertAudio.convertOriginalFile(file);
            fileInfo = new HashMap();
            fileInfo.put("fileNameKey", fileName);
            fileInfo.put("fileName", fileName);
            fileInfo.put("filePath", this.downloads);
            var10 = new ModelAndView(fileDownloadUtil, "fileInfo", fileInfo);
            return var10;
        } catch (Exception var13) {
            this.logger.error("tts/ttsOriginalDwn:" + var13.getMessage());
            var10 = new ModelAndView("redirect:/errorRedirect.html");
        } finally {
            if (context != null) {
                context.close();
            }

            this.logger.info("tts/ttsOriginalDwn:close");
        }

        return var10;
    }

    @RequestMapping(
            value = {"/ttsDwn"},
            method = {RequestMethod.GET}
    )
    public ModelAndView fileDownload(HttpServletRequest request, @RequestParam("file") String fileSet, @RequestParam("speed") String speed, @RequestParam("volume") String volume, @RequestParam("tone") String tone, @RequestParam("fileName") String fileId) {
        this.logger.info("tts/ttsDwn:start");
        AnnotationConfigApplicationContext context = null;
        HashMap fileInfo = null;

        ModelAndView var14;
        try {
            context = new AnnotationConfigApplicationContext(new Class[]{FileDownloadUtil.class});
            FileDownloadUtil fileDownloadUtil = (FileDownloadUtil)context.getBean(FileDownloadUtil.class);
            File file = new File(this.downloads + fileId + ".wav");
            String fileName = this.convertAudio.convertAudio(file, fileSet, speed, volume, tone, 22050);
            fileInfo = new HashMap();
            fileInfo.put("fileNameKey", fileName);
            fileInfo.put("fileName", fileName);
            fileInfo.put("filePath", this.downloads);
            var14 = new ModelAndView(fileDownloadUtil, "fileInfo", fileInfo);
            return var14;
        } catch (Exception var17) {
            this.logger.error("tts/ttsDwn:" + var17.getMessage());
            var14 = new ModelAndView("redirect:/errorRedirect.html");
        } finally {
            if (context != null) {
                context.close();
            }

            this.logger.info("tts/ttsDwn:close");
        }

        return var14;
    }
}
