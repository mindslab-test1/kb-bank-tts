package maum.brain.tts.rest.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TtsErrorController implements ErrorController {
    private static final String ERROR_PATH = "/error";

    public TtsErrorController() {
    }

    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping({"/error"})
    public String handleError(HttpServletRequest request, Model model) {
        return "redirect:/errorRedirect.html";
    }
}
