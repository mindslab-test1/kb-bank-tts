package maum.brain.tts.rest.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConvertAudio {
    private static Logger logger = LoggerFactory.getLogger(ConvertAudio.class);
    private String filePath;

    public ConvertAudio() {
    }

    private String makePhoneAudio(String filePath, String fileName) {
//        String[] cmd = new String[]{"/usr/bin/sox", filePath + fileName, "-e", "a-law", "-b", "8", "-c", "1", "-r", "8000", filePath + "8k_" + fileName};
        String[] cmd = new String[]{"C:\\sox\\sox", filePath + fileName, "-e", "a-law", "-b", "8", "-c", "1", "-r", "8000", filePath + "8k_" + fileName}; // TODO: use above
        StringBuilder sb = new StringBuilder();

        try {
            Process process = Runtime.getRuntime().exec(cmd);
            InputStream is = process.getInputStream();
            Scanner sc = new Scanner(is);

            while(sc.hasNextLine()) {
                sb.append(sc.nextLine()).append("\n");
            }

            sc.close();
            is.close();
            process.waitFor();
            logger.info(Arrays.toString(cmd));
        } catch (Exception var8) {
            logger.error(var8.getMessage());
        }

        return "8k_" + fileName;
    }

    public String convertAudio(File f, String fileSet, String speed, String volume, String tone, int samplerate) {
        String inputFile = "";
        String outputFile = "";

        try {
            this.filePath = f.getCanonicalPath().replace(f.getName(), "");
            inputFile = f.getName();
            // String cmdLine = "ffmpeg -i " + this.filePath + inputFile;
            String cmdLine = "C:\\ffmpeg\\bin\\ffmpeg -i " + this.filePath + inputFile; // TODO: use line above
            String nTone = "1.0";
            cmdLine = cmdLine + " -af asetrate=" + samplerate + "*" + nTone;
            cmdLine = cmdLine + ",atempo=" + speed;
            cmdLine = cmdLine + ",volume=" + volume;
            cmdLine = cmdLine + ",aresample=" + samplerate;
            outputFile = tone + "_" + speed + "_" + inputFile.replaceAll("\\..*", "." + fileSet);
            cmdLine = cmdLine + " -y " + this.filePath + outputFile;
            logger.info(cmdLine);
            logger.info("----------------------------------------------------------");
            Process p = Runtime.getRuntime().exec(cmdLine);
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            String line = "";

            while((line = in.readLine()) != null) {
                logger.info(line);
            }

            logger.info("----------------------------------------------------------");
            in.close();
            String var15 = this.makePhoneAudio(this.filePath, outputFile);
            return var15;
        } catch (Exception var18) {
            logger.error(var18.getMessage());
        } finally {
            logger.info("convert:end");
        }

        return outputFile;
    }

    public String convertOriginalFile(File f) {
        logger.error("convertOriginalFile:start");
        String new_fileName = "";

        try {
            this.filePath = f.getCanonicalPath().replace(f.getName(), "");
            String fileName = f.getName();
            new_fileName = this.makePhoneAudio(this.filePath, fileName);
            logger.info(fileName + "->" + new_fileName);
        } catch (Exception var7) {
            logger.error(var7.getMessage());
        } finally {
            logger.error("convertOriginalFile:end");
        }

        return new_fileName;
    }

    public void deleteFile() {
        Calendar cal = Calendar.getInstance();
        long todayMil = cal.getTimeInMillis();
        long oneMinMil = 60000L;
        Calendar fileCal = Calendar.getInstance();
        Date fileDate = null;
        File path = new File(this.filePath);
        File[] list = path.listFiles();

        for(int j = 0; j < list.length; ++j) {
            fileDate = new Date(list[j].lastModified());
            fileCal.setTime(fileDate);
            long diffMil = todayMil - fileCal.getTimeInMillis();
            int diffMin = (int)(diffMil / oneMinMil);
            if (diffMin > 180 && list[j].exists()) {
                list[j].delete();
                logger.error(list[j].getName() + " 파일을 삭제했습니다.");
            }
        }

    }
}