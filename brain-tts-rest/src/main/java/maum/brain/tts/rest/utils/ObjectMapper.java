package maum.brain.tts.rest.utils;

import maum.brain.tts.AudioEncoding;
import maum.brain.tts.rest.CommonCode;
import maum.brain.tts.rest.exception.TtsRestException;
import maum.common.LangOuterClass.Lang;

public class ObjectMapper {
    public ObjectMapper() {
    }

    public static Lang convertLang(maum.brain.tts.rest.CommonCode.Lang lang) throws TtsRestException {
        Lang tgt = Lang.ko_KR;
//        switch($SWITCH_TABLE$maum$brain$tts$rest$CommonCode$Lang()[lang.ordinal()]) {
        switch(lang.ordinal()) {
            case 0:
                tgt = Lang.ko_KR;
                break;
            case 1:
                tgt = Lang.en_US;
                break;
            default:
                throw new TtsRestException(CommonCode.TTS_ERR_NOT_SUPPORTED_LANG_ERROR, "Not supported language");
        }

        return tgt;
    }

    public static AudioEncoding convertAudioEncoding(maum.brain.tts.rest.CommonCode.AudioEncoding audioEncoding) throws TtsRestException {
        AudioEncoding result;
//        switch($SWITCH_TABLE$maum$brain$tts$rest$CommonCode$AudioEncoding()[audioEncoding.ordinal()]) {
        switch(audioEncoding.ordinal()) {
            case 0:
                result = AudioEncoding.WAV;
                break;
            case 1:
                result = AudioEncoding.PCM;
                break;
            default:
                throw new TtsRestException(CommonCode.TTS_ERR_NOT_SUPPORTED_AUDIOENCODING_ERROR, "Not supported audioEncoding");
        }

        return result;
    }
}
