FROM openjdk:8

RUN APT_INSTALL="apt-get install -y --no-install-recommends" && \
    cd /tmp && wget https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB && \
    apt-key add GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive $APT_INSTALL \
        maven  && \
# ==================================================================
# config & cleanup
# ------------------------------------------------------------------
    ldconfig && \
    apt-get clean && \
    apt-get autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/*

RUN mkdir /root/tts

COPY . /root/tts

RUN cd /root/tts && \
    mvn install
