(function($){
    $.fn.datepicker.dates['kr'] = {
        days: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일", "일요일"],
        daysShort: ["일", "월", "화", "수", "목", "금", "토", "일"],
        daysMin: ["일", "월", "화", "수", "목", "금", "토", "일"],
        months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
        monthsShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"]
    };
}(jQuery));


var  SEARCH_TYPE_DB_SOLRAND = 1;
var  SEARCH_TYPE_DB_SOLRAND_SOLROR = 4;


var SET = {
    FIXED_POINT_PART_SIZE: 3,
    INTERVAL: 2,
    CHART: {
        MIN_VALUE: 0,
        X_STEP_SIZE_MIN: 1,
        X_DATA_COUNT_EXPECT: 12,
        X_STEP_SIZE_EXPECT: 2,
        MIN_TICKS_LIMIT_EXPECT: 5,
        MAX_TICKS_LIMIT_EXPECT: 8
    }
}

var ds = SET;
var sc = ds.CHART;

var chartDataSet = {
    labels: [],
    datasets: [
        {
            label: "",
            borderWidth: 0.1,
            backgroundColor: window.chartColors.green,
            strokeColor: window.chartColors.green,
            pointColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            data: []
        },
        {
            label: "",
            borderWidth: 0.5,
            backgroundColor: window.chartColors.red,
            strokeColor: window.chartColors.red,
            pointColor: window.chartColors.red,
            borderColor: window.chartColors.red,
            data: []
        }
    ]
};

var chartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    maintainAspectRatio: true,
    legend: {
        display: false,
        position: 'top',
        labels: {
            fontColor: "#000080"
        }
    },
    datasetFill:false,
    scales: {
        xAxes: [{
            gridLines: {
                display: false,
                color: "black"
            },
            scaleLabel: {
                display: false,
                labelString: "Time in Seconds",
                fontColor: "red"
            }
        }],
        yAxes: [{
            gridLines: {
                display : true,
                color: "black",
                borderDash: [2, 5],
            },
            ticks: {
                min :0,
                max : 5,
                stepSize: 1
            },
            scaleLabel: {
                display: false,
                labelString: "Speed in Miles per Hour",
                fontColor: "green"
            }
        }]
    }
};


var dateFormat = function(date){
    var str = date.getFullYear() +"-"+ zeroDay(date.getMonth()+1) +"-" + zeroDay(date.getDate());
    return str;
}

var zeroDay = function (dateVal){
    var days = '';
    if(dateVal < 10){
        days = '0'+dateVal;
    }else {
        days = dateVal;
    }

    return days;

}


var isEmpty = function(v)  {
    if (this.isNull(v) || !v) {
        return true;
    }
    return (Object.keys(v).length === 0 && v.constructor === Object)
        || (v.length === 0 && v.constructor === Array);
};


var tempid =null;

var loadSpinner = function(){
    var target = $(".wrapper");

    var maskHeight = $(target || window).outerHeight(true);
    var maskWidth = $(target || window).outerWidth(true);
    var obj = target.offset();

    // #div의 현재 위치
    //console.log("left: " + obj.left + "px, top: " + obj.top + "px");
    tempid = 'target_div';
    /*트리그리드는...자식노드 그릴려고..자꾸 반복한다...그래서 생성된 spin 그리드가 존재하면 우선 지운다...*/
    if($("#"+tempid).length > 0){
        $("#"+tempid).remove();
    }

    var bgDiv = $('<div id="'+tempid+'"/>');
    bgDiv.css({
        zIndex:99999999,
        position:'absolute',
        top: obj.top,
        left:obj.left,
        width:'100%',
        height:'100%',
        opacity:0.4
    });


    bgDiv.css("background-color","#DEDADC");

    bgDiv.css("display","none");

    bgDiv.appendTo(target);
    bgDiv.show();
    bgDiv.spin();
}


var  stopspin = function() {
    $("#target_div").spin(false);
    $("#target_div").hide();
}

//체크박스 제어
// 전체 체크박스 event
function chkAllCtl(){
    var isCheckAll = $('#checkAll').prop('checked');

    if(isCheckAll) {

        $('input[name=id]:checkbox').prop('checked', 'checked');
    } else {

        $('input[name=id]:checkbox').prop('checked', '');
    }
}

//체크박스 하나 해제 시 전체 선택 체크박스도 해제
function chkCtl(){
    if($("input[name='id']:checked").length === $('.chk').length){

        $('#checkAll').prop("checked", true);
    }
    else{
        $('#checkAll').prop("checked", false);
    }
}

// start of Sorting 관련 함수
function getSort() {
    var sort = "";

    var asc = $(".sorting_asc").attr("data-id");
    var desc = $(".sorting_desc").attr("data-id");

    if (asc) sort += asc + ",asc";
    else if (desc) sort += desc + ",desc";

    if(sort === '') {
        sort = $('.sorting').first().attr("data-id") + ',asc';
    }

    return sort;
}

$(function () {
    $('.sorting,.sorting_asc,.sorting_desc').click(function(){
        var orginClass = $(this).attr('class');

        $('.sorting_asc,.sorting_desc').attr('class', 'sorting');

        var newClass;
        switch(orginClass) {
            case 'sorting_asc':
                newClass = 'sorting_desc';
                break;
            case 'sorting':
            case 'sorting_desc':
                newClass = 'sorting_asc';
                break;
        }

        $(this).removeClass();
        $(this).addClass(newClass);

        table.ajax.reload();
    });
});
// end of Sorting 관련 함수


function checkValidation(form){
    var result = "";

    var className = "."+ form + "-notNull";

    var isCheck = false;

    $(className).each(function() {
        if((isEmpty($(this).val()) && (!isCheck))){
            result = $(this).parent().find("label").text().trim() + "을 입력해주세요";
            isCheck = true;
        }
    });

    return result;
}

var isEmpty = function(value){
    if( value == null || value == undefined || value.trim()== "" || ( value != null && typeof value == "object" && !Object.keys(value).length ) ){
        return true;
    }else{
        return false
    }
};

String.prototype.replaceAll = function(org, dest) {
    return this.split(org).join(dest);
};

function xssPrevention(data){
    if(!isNaN(data))
        return data;
    if(data != null){
        data = data.replaceAll('&', '&amp;');
        data = data.replaceAll('<', '&lt;');
        data = data.replaceAll('>', '&gt;');
        data = data.replaceAll('\"', '&quot;');
        data = data.replaceAll("'", '&apos;;');
        return data;
    } else{

        return '';
    }
}