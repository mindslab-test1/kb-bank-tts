package ai.mindslab.tts_package.data;

public class Eng2KorEntity {
    private int eng2korId;
    private String bef;
    private String aft;

    public Eng2KorEntity() {
    }

    public int getEng2korId() {
        return this.eng2korId;
    }

    public String getBef() {
        return this.bef;
    }

    public String getAft() {
        return this.aft;
    }

    public void setEng2korId(final int eng2korId) {
        this.eng2korId = eng2korId;
    }

    public void setBef(final String bef) {
        this.bef = bef;
    }

    public void setAft(final String aft) {
        this.aft = aft;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof Eng2KorEntity)) {
            return false;
        } else {
            Eng2KorEntity other = (Eng2KorEntity)o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.getEng2korId() != other.getEng2korId()) {
                return false;
            } else {
                Object this$bef = this.getBef();
                Object other$bef = other.getBef();
                if (this$bef == null) {
                    if (other$bef != null) {
                        return false;
                    }
                } else if (!this$bef.equals(other$bef)) {
                    return false;
                }

                Object this$aft = this.getAft();
                Object other$aft = other.getAft();
                if (this$aft == null) {
                    if (other$aft != null) {
                        return false;
                    }
                } else if (!this$aft.equals(other$aft)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Eng2KorEntity;
    }

    public int hashCode() {
//        int PRIME = true;
        int result = 1;
//        int result = result * 59 + this.getEng2korId();
        Object $bef = this.getBef();
        result = result * 59 + this.getEng2korId();
        result = result * 59 + ($bef == null ? 43 : $bef.hashCode());
        Object $aft = this.getAft();
        result = result * 59 + ($aft == null ? 43 : $aft.hashCode());
        return result;
    }

    public String toString() {
        return "Eng2KorEntity(eng2korId=" + this.getEng2korId() + ", bef=" + this.getBef() + ", aft=" + this.getAft() + ")";
    }
}

