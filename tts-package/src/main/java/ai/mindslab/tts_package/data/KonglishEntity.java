package ai.mindslab.tts_package.data;

public class KonglishEntity {
    private int konglishId;
    private String bef;
    private String aft;

    public KonglishEntity() {
    }

    public int getKonglishId() {
        return this.konglishId;
    }

    public String getBef() {
        return this.bef;
    }

    public String getAft() {
        return this.aft;
    }

    public void setKonglishId(final int konglishId) {
        this.konglishId = konglishId;
    }

    public void setBef(final String bef) {
        this.bef = bef;
    }

    public void setAft(final String aft) {
        this.aft = aft;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof KonglishEntity)) {
            return false;
        } else {
            KonglishEntity other = (KonglishEntity)o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.getKonglishId() != other.getKonglishId()) {
                return false;
            } else {
                Object this$bef = this.getBef();
                Object other$bef = other.getBef();
                if (this$bef == null) {
                    if (other$bef != null) {
                        return false;
                    }
                } else if (!this$bef.equals(other$bef)) {
                    return false;
                }

                Object this$aft = this.getAft();
                Object other$aft = other.getAft();
                if (this$aft == null) {
                    if (other$aft != null) {
                        return false;
                    }
                } else if (!this$aft.equals(other$aft)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof KonglishEntity;
    }

    public int hashCode() {
//        int PRIME = true;
        int result = 1;
//        int result = result * 59 + this.getKonglishId();
        result = result * 59 + this.getKonglishId();
        Object $bef = this.getBef();
        result = result * 59 + ($bef == null ? 43 : $bef.hashCode());
        Object $aft = this.getAft();
        result = result * 59 + ($aft == null ? 43 : $aft.hashCode());
        return result;
    }

    public String toString() {
        return "KonglishEntity(konglishId=" + this.getKonglishId() + ", bef=" + this.getBef() + ", aft=" + this.getAft() + ")";
    }
}
