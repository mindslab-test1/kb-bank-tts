package ai.mindslab.tts_package.data;

import java.io.Serializable;

public class UserInfo implements Serializable {
    private String userId;
    private String userName;
    private int userRoleId;
    private String userRoleName;
    private String userRoleDescript;

    public UserInfo() {
    }

    public String getUserId() {
        return this.userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public int getUserRoleId() {
        return this.userRoleId;
    }

    public String getUserRoleName() {
        return this.userRoleName;
    }

    public String getUserRoleDescript() {
        return this.userRoleDescript;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public void setUserRoleId(final int userRoleId) {
        this.userRoleId = userRoleId;
    }

    public void setUserRoleName(final String userRoleName) {
        this.userRoleName = userRoleName;
    }

    public void setUserRoleDescript(final String userRoleDescript) {
        this.userRoleDescript = userRoleDescript;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof UserInfo)) {
            return false;
        } else {
            UserInfo other = (UserInfo)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label63: {
                    Object this$userId = this.getUserId();
                    Object other$userId = other.getUserId();
                    if (this$userId == null) {
                        if (other$userId == null) {
                            break label63;
                        }
                    } else if (this$userId.equals(other$userId)) {
                        break label63;
                    }

                    return false;
                }

                Object this$userName = this.getUserName();
                Object other$userName = other.getUserName();
                if (this$userName == null) {
                    if (other$userName != null) {
                        return false;
                    }
                } else if (!this$userName.equals(other$userName)) {
                    return false;
                }

                if (this.getUserRoleId() != other.getUserRoleId()) {
                    return false;
                } else {
                    Object this$userRoleName = this.getUserRoleName();
                    Object other$userRoleName = other.getUserRoleName();
                    if (this$userRoleName == null) {
                        if (other$userRoleName != null) {
                            return false;
                        }
                    } else if (!this$userRoleName.equals(other$userRoleName)) {
                        return false;
                    }

                    Object this$userRoleDescript = this.getUserRoleDescript();
                    Object other$userRoleDescript = other.getUserRoleDescript();
                    if (this$userRoleDescript == null) {
                        if (other$userRoleDescript != null) {
                            return false;
                        }
                    } else if (!this$userRoleDescript.equals(other$userRoleDescript)) {
                        return false;
                    }

                    return true;
                }
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof UserInfo;
    }

    public int hashCode() {
//        int PRIME = true;
        int result = 1;
        Object $userId = this.getUserId();
//        int result = result * 59 + ($userId == null ? 43 : $userId.hashCode());
        Object $userName = this.getUserName();
        result = result * 59 + ($userId == null ? 43 : $userId.hashCode());
        result = result * 59 + ($userName == null ? 43 : $userName.hashCode());
        result = result * 59 + this.getUserRoleId();
        Object $userRoleName = this.getUserRoleName();
        result = result * 59 + ($userRoleName == null ? 43 : $userRoleName.hashCode());
        Object $userRoleDescript = this.getUserRoleDescript();
        result = result * 59 + ($userRoleDescript == null ? 43 : $userRoleDescript.hashCode());
        return result;
    }

    public String toString() {
        return "UserInfo(userId=" + this.getUserId() + ", userName=" + this.getUserName() + ", userRoleId=" + this.getUserRoleId() + ", userRoleName=" + this.getUserRoleName() + ", userRoleDescript=" + this.getUserRoleDescript() + ")";
    }
}
