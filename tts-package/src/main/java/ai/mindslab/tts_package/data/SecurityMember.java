package ai.mindslab.tts_package.data;

import ai.mindslab.tts_package.domain.common.CommonRoleEntity;
import ai.mindslab.tts_package.domain.common.CommonUserEntity;
import java.util.ArrayList;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class SecurityMember extends User {
    private static final String ROLE_PREFIX = "ROLE_";
    private static final long serialVersionUID = 1L;

    public SecurityMember(CommonUserEntity member) {
        super(member.getUserId(), member.getPassword(), makeGrantedAuthority(member.getRole()));
    }

    private static List<GrantedAuthority> makeGrantedAuthority(CommonRoleEntity role) {
        List<GrantedAuthority> list = new ArrayList();
        list.add(new SimpleGrantedAuthority("ROLE_" + role.getRoleId()));
        return list;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof SecurityMember)) {
            return false;
        } else {
            SecurityMember other = (SecurityMember)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                return super.equals(o);
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof SecurityMember;
    }

    public int hashCode() {
        int result = super.hashCode();
        return result;
    }

    public String toString() {
        return "SecurityMember()";
    }
}
