package ai.mindslab.tts_package.data;

import java.io.Serializable;

public class UserInfoSVO implements Serializable {
    private String userId;
    private String userKey;

    public UserInfoSVO() {
    }

    public String getUserId() {
        return this.userId;
    }

    public String getUserKey() {
        return this.userKey;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public void setUserKey(final String userKey) {
        this.userKey = userKey;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof UserInfoSVO)) {
            return false;
        } else {
            UserInfoSVO other = (UserInfoSVO)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$userId = this.getUserId();
                Object other$userId = other.getUserId();
                if (this$userId == null) {
                    if (other$userId != null) {
                        return false;
                    }
                } else if (!this$userId.equals(other$userId)) {
                    return false;
                }

                Object this$userKey = this.getUserKey();
                Object other$userKey = other.getUserKey();
                if (this$userKey == null) {
                    if (other$userKey != null) {
                        return false;
                    }
                } else if (!this$userKey.equals(other$userKey)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof UserInfoSVO;
    }

    public int hashCode() {
//        int PRIME = true;
        int result = 1;
        Object $userId = this.getUserId();
//        int result = result * 59 + ($userId == null ? 43 : $userId.hashCode());
        Object $userKey = this.getUserKey();
        result = result * 59 + ($userId == null ? 43 : $userId.hashCode());
        result = result * 59 + ($userKey == null ? 43 : $userKey.hashCode());
        return result;
    }

    public String toString() {
        return "UserInfoSVO(userId=" + this.getUserId() + ", userKey=" + this.getUserKey() + ")";
    }
}
