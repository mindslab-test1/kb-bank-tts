package ai.mindslab.tts_package.config;

import org.apache.catalina.Context;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.boot.web.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

@Component
public class TomcatCustomizer implements WebServerFactoryCustomizer<TomcatServletWebServerFactory> {
    public TomcatCustomizer() {
    }

    public void customize(TomcatServletWebServerFactory factory) {
        factory.addContextCustomizers(new TomcatContextCustomizer[]{new TomcatCustomizer.ContextSecurityCustomizer()});
    }

    private static class ContextSecurityCustomizer implements TomcatContextCustomizer {
        private ContextSecurityCustomizer() {
        }

        public void customize(Context context) {
            SecurityConstraint constraint = new SecurityConstraint();
            SecurityCollection securityCollection = new SecurityCollection();
            securityCollection.setName("Forbidden");
            securityCollection.addPattern("/*");
            securityCollection.addOmittedMethod(HttpMethod.HEAD.toString());
            securityCollection.addOmittedMethod(HttpMethod.POST.toString());
            securityCollection.addOmittedMethod(HttpMethod.GET.toString());
            constraint.addCollection(securityCollection);
            constraint.setAuthConstraint(true);
            context.addConstraint(constraint);
        }
    }
}