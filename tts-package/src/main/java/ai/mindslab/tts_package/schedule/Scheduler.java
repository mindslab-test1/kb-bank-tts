package ai.mindslab.tts_package.schedule;

import ai.mindslab.tts_package.service.monitoring.MonitoringService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Scheduler {
    @Autowired
    MonitoringService monitoringService;
    private Logger logger = LoggerFactory.getLogger(Scheduler.class);

    public Scheduler() {
    }
}
