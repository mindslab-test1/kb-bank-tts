package ai.mindslab.tts_package.repository.monitoring;

import ai.mindslab.tts_package.domain.monitoring.MonitoringProcessEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MonitoringProcessRepository extends JpaRepository<MonitoringProcessEntity, Long>, QuerydslPredicateExecutor<MonitoringProcessEntity> {
    int deleteByProcessNoIn(List<Long> ids);

    MonitoringProcessEntity findByProcessNoNotAndProcessName(Long processNo, String processName);
}