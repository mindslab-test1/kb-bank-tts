package ai.mindslab.tts_package.repository.preprocess;

import ai.mindslab.tts_package.domain.preprocess.PreprocessProfileEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PreprocessProfileRepository extends JpaRepository<PreprocessProfileEntity, Long>, QuerydslPredicateExecutor<PreprocessProfileEntity> {
    int deleteByProfileIdIn(List<Long> ids);

    PreprocessProfileEntity findByProfileIdNotAndProfileName(Long profileId, String profileName);
}

