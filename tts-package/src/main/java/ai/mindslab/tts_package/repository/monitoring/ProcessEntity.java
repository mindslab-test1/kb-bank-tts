package ai.mindslab.tts_package.repository.monitoring;

import java.math.BigInteger;
import java.sql.Timestamp;

public class ProcessEntity {
    private BigInteger processNo;
    private String processName;
    private BigInteger processId;
    private BigInteger historyId;
    private String processStatus;
    private Timestamp createdDtm;
    private Character autoRecovery;
    private Character useYn;

    public ProcessEntity(BigInteger processNo, String processName, BigInteger processId, BigInteger historyId, String processStatus, Timestamp createdDtm, Character autoRecovery, Character useYn) {
        this.processNo = processNo;
        this.processName = processName;
        this.processId = processId;
        this.historyId = historyId;
        this.processStatus = processStatus;
        this.createdDtm = createdDtm;
        this.autoRecovery = autoRecovery;
        this.useYn = useYn;
    }

    public BigInteger getProcessNo() {
        return this.processNo;
    }

    public String getProcessName() {
        return this.processName;
    }

    public BigInteger getProcessId() {
        return this.processId;
    }

    public BigInteger getHistoryId() {
        return this.historyId;
    }

    public String getProcessStatus() {
        return this.processStatus;
    }

    public Timestamp getCreatedDtm() {
        return this.createdDtm;
    }

    public Character getAutoRecovery() {
        return this.autoRecovery;
    }

    public Character getUseYn() {
        return this.useYn;
    }

    public void setProcessNo(final BigInteger processNo) {
        this.processNo = processNo;
    }

    public void setProcessName(final String processName) {
        this.processName = processName;
    }

    public void setProcessId(final BigInteger processId) {
        this.processId = processId;
    }

    public void setHistoryId(final BigInteger historyId) {
        this.historyId = historyId;
    }

    public void setProcessStatus(final String processStatus) {
        this.processStatus = processStatus;
    }

    public void setCreatedDtm(final Timestamp createdDtm) {
        this.createdDtm = createdDtm;
    }

    public void setAutoRecovery(final Character autoRecovery) {
        this.autoRecovery = autoRecovery;
    }

    public void setUseYn(final Character useYn) {
        this.useYn = useYn;
    }
}
