package ai.mindslab.tts_package.repository.monitoring;

import ai.mindslab.tts_package.domain.monitoring.MonitoringResourceInformationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonitoringResourceInformationRepository extends JpaRepository<MonitoringResourceInformationEntity, Long> {
}
