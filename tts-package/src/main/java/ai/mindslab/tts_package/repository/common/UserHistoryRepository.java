package ai.mindslab.tts_package.repository.common;

import ai.mindslab.tts_package.domain.common.UserHistoryEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserHistoryRepository extends JpaRepository<UserHistoryEntity, Integer>, QuerydslPredicateExecutor<UserHistoryEntity> {
    UserHistoryEntity findTopByStatusCdInAndUserIdOrderByCreatedDtmDesc(List<String> statusCds, String userId);
}
