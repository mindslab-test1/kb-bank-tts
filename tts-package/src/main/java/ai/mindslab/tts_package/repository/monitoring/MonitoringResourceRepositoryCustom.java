package ai.mindslab.tts_package.repository.monitoring;

import ai.mindslab.tts_package.domain.monitoring.MonitoringResourceEntity;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.qlrm.mapper.JpaResultMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class MonitoringResourceRepositoryCustom {
    @PersistenceContext
    EntityManager em;
    private static Logger logger = LoggerFactory.getLogger(MonitoringResourceRepositoryCustom.class);

    public MonitoringResourceRepositoryCustom() {
    }

    public List<MonitoringResourceEntity> getList(Integer cnt) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * \n");
        sb.append("from mt_resource_tb \n");
        sb.append("order by created_dtm desc \n");
        sb.append("limit ");
        sb.append(cnt);
        logger.info("getTop60 Query : {}", sb.toString());
        JpaResultMapper jpaResultMapper = new JpaResultMapper();
        Query nativeQuery = this.em.createNativeQuery(sb.toString());
        List<MonitoringResourceEntity> list = jpaResultMapper.list(nativeQuery, MonitoringResourceEntity.class);
        return list;
    }

    public List<MonitoringResourceEntity> getResourceListSearch(String start, String end, String period) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mt_resource_tb\n");
        sb.append("where created_dtm >= '");
        sb.append(start);
        sb.append("'\n");
        sb.append("and created_dtm < '");
        sb.append(end);
        sb.append("'\n");
        sb.append("and MOD(MINUTE(created_dtm), ");
        sb.append(period);
        sb.append(") = 0 \n");
        sb.append("order by created_dtm desc");
        logger.info("getResourceListSearch Query : {}", sb.toString());
        JpaResultMapper jpaResultMapper = new JpaResultMapper();
        Query nativeQuery = this.em.createNativeQuery(sb.toString());
        List<MonitoringResourceEntity> list = jpaResultMapper.list(nativeQuery, MonitoringResourceEntity.class);
        return list;
    }
}
