package ai.mindslab.tts_package.repository.monitoring;

import java.util.List;
import javax.annotation.Nullable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.qlrm.mapper.JpaResultMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.ObjectUtils;

@Repository
public class MonitoringProcessRepositoryCustom {
    @PersistenceContext(
            name = "tts"
    )
    EntityManager em;
    private static Logger logger = LoggerFactory.getLogger(MonitoringProcessRepositoryCustom.class);

    public MonitoringProcessRepositoryCustom() {
    }

    public Page<ProcessEntity> findAllLastHistory(@Nullable String searchCol, @Nullable String searchText, @Nullable Pageable pageable) {
        StringBuilder sb = new StringBuilder();
        int pageNum = pageable.getPageNumber();
        int pageSize = pageable.getPageSize();
        sb.append("select ");
        sb.append("    pro.process_no");
        sb.append("    , pro.process_name");
        sb.append("    , his.process_id");
        sb.append("    , his.history_id");
        sb.append("    , his.process_status");
        sb.append("    , his.created_dtm");
        sb.append("    , pro.auto_recovery");
        sb.append("    , pro.use_yn \n");
        sb.append("from mt_process_tb as pro\n");
        sb.append("    left join pc_history_tb as his\n");
        sb.append("    on pro.process_no = his.process_no\n");
        sb.append("where (");
        sb.append("     his.history_id in (\n");
        sb.append("            select MAX(his2.history_id) \n");
        sb.append("            from pc_history_tb his2 \n");
        sb.append("            where his2.created_dtm \n");
        sb.append("            GROUP BY process_no\n");
        sb.append("     ) \n");
        sb.append("     or ISNULL(his.created_dtm)\n");
        sb.append(")");
        sb.append("order by pro.process_no\n");
        logger.info("findAllLastHistory Query : {}", sb.toString());
        JpaResultMapper jpaResultMapper = new JpaResultMapper();
        Query nativeQuery = this.em.createNativeQuery(sb.toString());
        List<ProcessEntity> list = jpaResultMapper.list(nativeQuery, ProcessEntity.class);
        if (ObjectUtils.isEmpty(pageable)) {
            return new PageImpl(list);
        } else {
            int orgListSize = list.size();
            if ((pageNum + 1) * pageSize < orgListSize - 1) {
                list = list.subList(pageNum * pageSize, (pageNum + 1) * pageSize);
            } else {
                list = list.subList(pageNum * pageSize, orgListSize);
            }

            return new PageImpl(list, pageable, (long)orgListSize);
        }
    }
}
