package ai.mindslab.tts_package.repository.preprocess;

import ai.mindslab.tts_package.domain.preprocess.PreprocessDataTableEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PreprocessTableRepository extends JpaRepository<PreprocessDataTableEntity, Long>, QuerydslPredicateExecutor<PreprocessDataTableEntity> {
    int deleteByDataTableIdIn(List<Long> ids);

    PreprocessDataTableEntity findByDataTableIdNotAndDataTableName(Long dataTableId, String dataTableName);
}
