package ai.mindslab.tts_package.repository.common;

import ai.mindslab.tts_package.domain.common.CommonRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommonRoleRepository extends JpaRepository<CommonRoleEntity, Integer> {
}