package ai.mindslab.tts_package.repository.tts;

import ai.mindslab.tts_package.domain.tts.TtsConfigEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TtsConfigRepository extends JpaRepository<TtsConfigEntity, Long>, QuerydslPredicateExecutor<TtsConfigEntity> {
}
