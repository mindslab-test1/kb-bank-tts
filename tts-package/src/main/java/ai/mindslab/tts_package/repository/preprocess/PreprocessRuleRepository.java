package ai.mindslab.tts_package.repository.preprocess;

import ai.mindslab.tts_package.domain.preprocess.PreprocessRuleEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PreprocessRuleRepository extends JpaRepository<PreprocessRuleEntity, Long>, QuerydslPredicateExecutor<PreprocessRuleEntity> {
    int deleteByRuleIdIn(List<Long> ids);

    PreprocessRuleEntity findByRuleIdNotAndRuleName(Long id, String ruleName);
}
