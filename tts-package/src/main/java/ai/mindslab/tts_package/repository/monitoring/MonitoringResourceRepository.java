package ai.mindslab.tts_package.repository.monitoring;

import ai.mindslab.tts_package.domain.monitoring.MonitoringResourceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MonitoringResourceRepository extends JpaRepository<MonitoringResourceEntity, String>, QuerydslPredicateExecutor<MonitoringResourceEntity> {
}
