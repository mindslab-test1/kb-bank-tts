package ai.mindslab.tts_package.repository.common;

import ai.mindslab.tts_package.domain.common.CommonUserEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CommonUserRepository extends JpaRepository<CommonUserEntity, String>, QuerydslPredicateExecutor<CommonUserEntity> {
    CommonUserEntity findByUserName(String userName);

    CommonUserEntity findByUserId(String userId);

    int deleteByUserIdIn(List<String> ids);
}
