package ai.mindslab.tts_package.repository.monitoring;

import ai.mindslab.tts_package.domain.monitoring.ProcessHistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessHistoryRepository extends JpaRepository<ProcessHistoryEntity, Long>, QuerydslPredicateExecutor<ProcessHistoryEntity> {
    ProcessHistoryEntity findTopByProcessNoOrderByCreatedDtmDesc(Long l);
}
