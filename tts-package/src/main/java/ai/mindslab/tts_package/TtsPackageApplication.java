package ai.mindslab.tts_package;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class TtsPackageApplication {

	public static void main(String[] args) {
		SpringApplication.run(TtsPackageApplication.class, args);
	}

}
