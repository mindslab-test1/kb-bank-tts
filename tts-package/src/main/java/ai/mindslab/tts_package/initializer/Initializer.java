package ai.mindslab.tts_package.initializer;

import ai.mindslab.tts_package.service.monitoring.MonitoringService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile({"prod"})
@Component
public class Initializer implements ApplicationRunner {
    private Logger logger = LoggerFactory.getLogger(Initializer.class);
    @Autowired
    MonitoringService monitoringService;

    public Initializer() {
    }

    public void run(ApplicationArguments args) {
    }
}
