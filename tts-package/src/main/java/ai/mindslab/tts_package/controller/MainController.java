package ai.mindslab.tts_package.controller;

import ai.mindslab.tts_package.common.codes.IRestCodes;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController implements IRestCodes {
    private Logger logger = LoggerFactory.getLogger(MainController.class);

    public MainController() {
    }

    @RequestMapping({"/index"})
    public String index(Model model) {
        return "redirect:/tts/test2";
    }

    @RequestMapping({"/"})
    public String index2(Model model) {
        return "redirect:/tts/test2";
    }

    @RequestMapping({"/sessionOutCall"})
    public String sessionOutCall(HttpServletRequest request, Model model) {
        this.logger.info("sessionOutCall is called");
        String view = "adminLogin";
        if (request.getHeader("x-requested-with") != null) {
            model.addAttribute("code", 9001);
            model.addAttribute("msg", "SESSION TIME OUT. LOGIN AGAIN PLEASE");
            view = "jsonView";
        }

        return view;
    }
}
