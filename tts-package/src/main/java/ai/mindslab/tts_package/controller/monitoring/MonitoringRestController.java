package ai.mindslab.tts_package.controller.monitoring;

import ai.mindslab.tts_package.common.codes.IRestCodes;
import ai.mindslab.tts_package.common.data.BaseResponse;
import ai.mindslab.tts_package.common.exceptions.TtsPackageException;
import ai.mindslab.tts_package.data.UserInfo;
import ai.mindslab.tts_package.domain.monitoring.MonitoringProcessEntity;
import ai.mindslab.tts_package.domain.monitoring.MonitoringResourceEntity;
import ai.mindslab.tts_package.domain.monitoring.MonitoringResourceInformationEntity;
import ai.mindslab.tts_package.domain.monitoring.MonitoringResourceThresholdEntity;
import ai.mindslab.tts_package.repository.monitoring.ProcessEntity;
import ai.mindslab.tts_package.service.monitoring.MonitoringService;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/monitoring"})
public class MonitoringRestController implements IRestCodes {
    private Logger logger = LoggerFactory.getLogger(MonitoringRestController.class);
    @Autowired
    MonitoringService monitoringService;

    public MonitoringRestController() {
    }

    @RequestMapping({"/process/list"})
    public BaseResponse<?> getCustomProcessList(@PageableDefault(size = 2147483647,sort = {"processNo, ASC"}) Pageable pageable, @Nullable @RequestParam(name = "searchCol",required = false) String searchCol, @Nullable @RequestParam(name = "searchText",required = false) String searchText) {
        this.logger.debug("getCustomProcessList params {}, {}, {}", new Object[]{pageable, searchCol, searchText});
        BaseResponse resp = new BaseResponse();

        try {
            Page<ProcessEntity> list = this.monitoringService.getCustomProcessList(pageable, searchCol, searchText);
            resp.setData(list);
        } catch (TtsPackageException var6) {
            resp.setCode(var6.getErrCode());
            resp.setMsg(var6.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/process/get"})
    public BaseResponse<?> getProcess(@RequestParam Long processNo) {
        this.logger.debug("getProcess. {}", processNo);
        BaseResponse resp = new BaseResponse();

        try {
            MonitoringProcessEntity entity = this.monitoringService.getProcess(processNo);
            resp.setData(entity);
        } catch (TtsPackageException var4) {
            resp.setCode(var4.getErrCode());
            resp.setMsg(var4.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/process/save"})
    public BaseResponse<?> saveProcess(HttpServletRequest request, @RequestBody MonitoringProcessEntity param) {
        this.logger.debug("saveProcess params, {}, {}, {}, {}, {}, {}, {}", new Object[]{param.getProcessNo(), param.getProcessName(), param.getStartScriptPath(), param.getEndScriptPath(), param.getStateScriptPath(), param.getAutoRecovery(), param.getUseYn()});
        BaseResponse<MonitoringProcessEntity> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            if (ObjectUtils.isEmpty(param.getProcessName()) || ObjectUtils.isEmpty(param.getStartScriptPath()) || ObjectUtils.isEmpty(param.getEndScriptPath()) || ObjectUtils.isEmpty(param.getStateScriptPath())) {
                throw new TtsPackageException(1001, "INVALID PARAMETERS");
            }

            param = this.monitoringService.saveProcess(param, userInfo.getUserId());
            resp.setData(param);
        } catch (TtsPackageException var6) {
            resp.setCode(var6.getErrCode());
            resp.setMsg(var6.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/process/delete"})
    public BaseResponse<?> deleteProcess(@RequestParam Long[] ids) {
        this.logger.debug("deleteProcess params, {}", Arrays.toString(ids));
        BaseResponse resp = new BaseResponse();

        try {
            int deleteConfirm = this.monitoringService.deleteProcess(ids);
            resp.setData(deleteConfirm);
        } catch (TtsPackageException var4) {
            resp.setCode(var4.getErrCode());
            resp.setMsg(var4.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/process/start"})
    public BaseResponse<?> startProcess(@RequestParam Long processPk) {
        this.logger.debug("startProcess, {}", processPk);
        BaseResponse resp = new BaseResponse();

        try {
            String data = this.monitoringService.startProcess(processPk);
            resp.setData(data);
        } catch (TtsPackageException var4) {
            resp.setCode(var4.getErrCode());
            resp.setMsg(var4.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/process/stop"})
    public BaseResponse<?> stopProcess(@RequestParam Long processPk) {
        this.logger.debug("stopProcess, {}", processPk);
        BaseResponse resp = new BaseResponse();

        try {
            String data = this.monitoringService.stopProcess(processPk);
            resp.setData(data);
        } catch (TtsPackageException var4) {
            resp.setCode(var4.getErrCode());
            resp.setMsg(var4.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/resource/list"})
    public BaseResponse<?> getResourceChart() {
        this.logger.debug("getResourceList");
        BaseResponse resp = new BaseResponse();

        try {
            List<MonitoringResourceEntity> list = this.monitoringService.getResourceList();
            resp.setData(list);
        } catch (TtsPackageException var3) {
            resp.setCode(var3.getErrCode());
            resp.setMsg(var3.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/resource/get"})
    public BaseResponse<?> getResource() {
        this.logger.debug("getResource");
        BaseResponse resp = new BaseResponse();

        try {
            MonitoringResourceEntity entity = this.monitoringService.getResource();
            resp.setData(entity);
        } catch (TtsPackageException var3) {
            resp.setCode(var3.getErrCode());
            resp.setMsg(var3.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/resource/search"})
    public BaseResponse<?> searchResource(@RequestParam(name = "dateTimeRange") String range, @RequestParam(name = "period") String period) {
        this.logger.debug("getResource");
        BaseResponse resp = new BaseResponse();

        try {
            List<MonitoringResourceEntity> list = this.monitoringService.searchResource(range, period);
            resp.setData(list);
        } catch (TtsPackageException var5) {
            resp.setCode(var5.getErrCode());
            resp.setMsg(var5.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/getMax"})
    public BaseResponse<?> getMax() {
        this.logger.debug("getMax");
        BaseResponse resp = new BaseResponse();

        try {
            MonitoringResourceInformationEntity entity = this.monitoringService.getMax();
            resp.setData(entity);
        } catch (TtsPackageException var3) {
            resp.setCode(var3.getErrCode());
            resp.setMsg(var3.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/threshold/get"})
    public BaseResponse<?> getThreshold() {
        this.logger.debug("getThreshold");
        BaseResponse resp = new BaseResponse();

        try {
            List<MonitoringResourceThresholdEntity> entity = this.monitoringService.getThreshold();
            resp.setData(entity);
        } catch (TtsPackageException var3) {
            resp.setCode(var3.getErrCode());
            resp.setMsg(var3.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/threshold/save"})
    public BaseResponse<?> updateThreshold(HttpServletRequest request, @RequestParam("param") String[] param) {
        this.logger.debug("updateThreshold param {}", Arrays.toString(param));
        BaseResponse<List<MonitoringResourceThresholdEntity>> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");
        List list = null;

        try {
            list = this.monitoringService.updateThreshold(param, userInfo.getUserId());
            resp.setData(list);
        } catch (TtsPackageException var7) {
            resp.setCode(var7.getErrCode());
            resp.setMsg(var7.getErrMsg());
        }

        resp.setData(list);
        return resp;
    }
}
