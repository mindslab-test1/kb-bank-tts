package ai.mindslab.tts_package.controller.common;

import ai.mindslab.tts_package.common.codes.IRestCodes;
import ai.mindslab.tts_package.common.data.BaseResponse;
import ai.mindslab.tts_package.common.exceptions.TtsPackageException;
import ai.mindslab.tts_package.data.UserInfo;
import ai.mindslab.tts_package.domain.common.CommonRoleEntity;
import ai.mindslab.tts_package.domain.common.CommonUserEntity;
import ai.mindslab.tts_package.service.common.CommonService;
import java.time.LocalDateTime;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/common"})
public class CommonRestController implements IRestCodes {
    private Logger logger = LoggerFactory.getLogger(CommonRestController.class);
    @Autowired
    CommonService commonService;

    public CommonRestController() {
    }

    @RequestMapping({"/user/list"})
    public BaseResponse<?> getUserList(HttpServletRequest request, @RequestParam(name = "searchCol",required = false) String searchCol, @RequestParam(name = "searchText",required = false) String searchText, @RequestParam(name = "activatedYn",required = false) String activatedYn, @PageableDefault(size = 2147483647) Pageable pageable) {
        this.logger.debug("getUserList params, {}, {}, {}, {}", new Object[]{searchCol, searchText, activatedYn, pageable});
        BaseResponse<Page<CommonUserEntity>> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            if (!StringUtils.equalsIgnoreCase(userInfo.getUserRoleName(), "admin")) {
                throw new TtsPackageException(2001, "PERMISSION DENIED");
            }

            Page<CommonUserEntity> list = this.commonService.getUserList(searchCol, searchText, activatedYn, pageable);
            resp.setData(list);
        } catch (TtsPackageException var9) {
            resp.setCode(var9.getErrCode());
            resp.setMsg(var9.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/user/get"})
    public BaseResponse<?> getCommonUser(HttpServletRequest request, @RequestParam String userId) {
        this.logger.debug("getCommonUser params, {}", userId);
        BaseResponse<CommonUserEntity> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            if (!StringUtils.equalsIgnoreCase(userInfo.getUserRoleName(), "admin") && !StringUtils.equalsIgnoreCase(userInfo.getUserId(), userId)) {
                throw new TtsPackageException(2001, "PERMISSION DENIED");
            }

            CommonUserEntity entity = this.commonService.getCommonUser(userId);
            resp.setData(entity);
        } catch (TtsPackageException var6) {
            resp.setCode(var6.getErrCode());
            resp.setMsg(var6.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/user/save"})
    public BaseResponse<?> saveCommonUser(HttpServletRequest request, @RequestBody CommonUserEntity param) {
        this.logger.debug("saveCommonUser params, {}", param);
        BaseResponse<CommonUserEntity> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            if (!StringUtils.equalsIgnoreCase(userInfo.getUserRoleName(), "admin") && !StringUtils.equalsIgnoreCase(userInfo.getUserId(), param.getUserId())) {
                throw new TtsPackageException(2001, "PERMISSION DENIED");
            }

            param.setPwChgDtm(LocalDateTime.now());
            CommonUserEntity entity = this.commonService.saveCommonUser(param, userInfo.getUserId(), request);
            resp.setData(entity);
        } catch (TtsPackageException var6) {
            resp.setCode(var6.getErrCode());
            resp.setMsg(var6.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/user/delete"})
    public BaseResponse<?> deleteCommonUser(HttpServletRequest request, @RequestParam String[] ids) {
        this.logger.debug("deleteCommonUser params, {}", Arrays.toString(ids));
        BaseResponse<Integer> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            if (!StringUtils.equalsIgnoreCase(userInfo.getUserRoleName(), "admin")) {
                throw new TtsPackageException(2001, "PERMISSION DENIED");
            }

            int deleteConfirm = this.commonService.deleteCommonUser(ids);
            resp.setData(deleteConfirm);
        } catch (TtsPackageException var6) {
            resp.setCode(var6.getErrCode());
            resp.setMsg(var6.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/user/init"})
    public BaseResponse<?> initAdminCommonUser(@PageableDefault(size = 2147483647) Pageable pageable, HttpServletRequest request) {
        this.logger.debug("initAdminCommonUser params");
        BaseResponse resp = new BaseResponse();

        try {
            Page<CommonRoleEntity> list = this.commonService.getRoleList(pageable);
            if (!list.hasContent()) {
                this.commonService.saveInitRole();
            }

            CommonUserEntity initAdmin = new CommonUserEntity();
            initAdmin.setUserId("admin");
            initAdmin.setUserName("시스템 관리자");
            initAdmin.setRoleId(1);
            initAdmin.setActivatedYn("Y");
            CommonUserEntity entity = this.commonService.saveCommonUser(initAdmin, "init", request);
            resp.setData(entity);
        } catch (TtsPackageException var7) {
            resp.setCode(var7.getErrCode());
            resp.setMsg(var7.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/role/list"})
    public BaseResponse<?> getRoleList(@PageableDefault(size = 2147483647) Pageable pageable) {
        this.logger.debug("getRoleList params, {}", pageable);
        BaseResponse resp = new BaseResponse();

        try {
            Page<CommonRoleEntity> list = this.commonService.getRoleList(pageable);
            resp.setData(list);
        } catch (TtsPackageException var4) {
            resp.setCode(var4.getErrCode());
            resp.setMsg(var4.getErrMsg());
        }

        return resp;
    }
}

