package ai.mindslab.tts_package.controller.preprocess;

import ai.mindslab.tts_package.data.UserInfo;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping({"/preprocess"})
public class PreprocessController {
    private Logger logger = LoggerFactory.getLogger(PreprocessController.class);
    private String prefix = "views/preprocess/";

    public PreprocessController() {
    }

    @RequestMapping({"/ruleManage"})
    public String userManage(HttpServletRequest request, Model model) {
        this.logger.info("ruleManage page open");
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");
        String var4 = userInfo.getUserRoleName().toLowerCase();
        byte var5 = -1;
        switch(var4.hashCode()) {
            case -500553564:
                if (var4.equals("operator")) {
                    var5 = 2;
                }
                break;
            case 92668751:
                if (var4.equals("admin")) {
                    var5 = 0;
                }
                break;
            case 835260333:
                if (var4.equals("manager")) {
                    var5 = 1;
                }
        }

        switch(var5) {
            case 0:
            case 1:
                return this.prefix + "ruleManage";
            case 2:
            default:
                this.logger.info("user no permission - redirect to monitoring");
                return "redirect:/tts/test2";
        }
    }

    @RequestMapping({"/dataTableManage"})
    public String dataTableManage(HttpServletRequest request, Model model) {
        this.logger.info("dataTableManage page open");
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");
        String var4 = userInfo.getUserRoleName().toLowerCase();
        byte var5 = -1;
        switch(var4.hashCode()) {
            case -500553564:
                if (var4.equals("operator")) {
                    var5 = 2;
                }
                break;
            case 92668751:
                if (var4.equals("admin")) {
                    var5 = 0;
                }
                break;
            case 835260333:
                if (var4.equals("manager")) {
                    var5 = 1;
                }
        }

        switch(var5) {
            case 0:
            case 1:
                return this.prefix + "dataTableManage";
            case 2:
            default:
                this.logger.info("user no permission - redirect to monitoring");
                return "redirect:/tts/test2";
        }
    }

    @RequestMapping({"/profileManage"})
    public String profileManage(HttpServletRequest request, Model model) {
        this.logger.info("profileManage page open");
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");
        String var4 = userInfo.getUserRoleName().toLowerCase();
        byte var5 = -1;
        switch(var4.hashCode()) {
            case -500553564:
                if (var4.equals("operator")) {
                    var5 = 2;
                }
                break;
            case 92668751:
                if (var4.equals("admin")) {
                    var5 = 0;
                }
                break;
            case 835260333:
                if (var4.equals("manager")) {
                    var5 = 1;
                }
        }

        switch(var5) {
            case 0:
            case 1:
                return this.prefix + "profileManage";
            case 2:
            default:
                this.logger.info("user no permission - redirect to monitoring");
                return "redirect:/tts/test2";
        }
    }

    @RequestMapping({"/eng2korManage"})
    public String eng2korManage(HttpServletRequest request, Model model) {
        this.logger.info("eng2korManage page open");
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");
        String var4 = userInfo.getUserRoleName().toLowerCase();
        byte var5 = -1;
        switch(var4.hashCode()) {
            case -500553564:
                if (var4.equals("operator")) {
                    var5 = 2;
                }
                break;
            case 92668751:
                if (var4.equals("admin")) {
                    var5 = 0;
                }
                break;
            case 835260333:
                if (var4.equals("manager")) {
                    var5 = 1;
                }
        }

        switch(var5) {
            case 0:
            case 1:
                return this.prefix + "eng2korManage";
            case 2:
            default:
                this.logger.info("user no permission - redirect to monitoring");
                return "redirect:/tts/test2";
        }
    }

    @RequestMapping({"/konglishManage"})
    public String konglishManage(HttpServletRequest request, Model model) {
        this.logger.info("konglishManage page open");
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");
        String var4 = userInfo.getUserRoleName().toLowerCase();
        byte var5 = -1;
        switch(var4.hashCode()) {
            case -500553564:
                if (var4.equals("operator")) {
                    var5 = 2;
                }
                break;
            case 92668751:
                if (var4.equals("admin")) {
                    var5 = 0;
                }
                break;
            case 835260333:
                if (var4.equals("manager")) {
                    var5 = 1;
                }
        }

        switch(var5) {
            case 0:
            case 1:
                return this.prefix + "konglishManage";
            case 2:
            default:
                this.logger.info("user no permission - redirect to monitoring");
                return "redirect:/tts/test2";
        }
    }
}
