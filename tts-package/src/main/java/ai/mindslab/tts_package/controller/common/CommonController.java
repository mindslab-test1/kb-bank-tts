package ai.mindslab.tts_package.controller.common;

import ai.mindslab.tts_package.data.UserInfo;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping({"/common"})
public class CommonController {
    private Logger logger = LoggerFactory.getLogger(CommonController.class);
    private String prefix = "views/common/";

    public CommonController() {
    }

    @RequestMapping({"/userManage"})
    public String userManage(HttpServletRequest request, Model model) {
        this.logger.info("userManage page open");
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");
        if (StringUtils.equalsIgnoreCase(userInfo.getUserRoleName(), "admin")) {
            return this.prefix + "userManage";
        } else {
            this.logger.info("user not admin redirect to monitoring");
            return "redirect:/tts/test2";
        }
    }
}
