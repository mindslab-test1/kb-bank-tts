package ai.mindslab.tts_package.controller.tts;

import ai.mindslab.tts_package.common.data.BaseResponse;
import ai.mindslab.tts_package.common.exceptions.TtsPackageException;
import ai.mindslab.tts_package.data.UserInfo;
import ai.mindslab.tts_package.domain.tts.TtsConfigEntity;
import ai.mindslab.tts_package.service.tts.TtsService;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/tts"})
public class TtsRestController {
    private Logger logger = LoggerFactory.getLogger(TtsRestController.class);
    @Autowired
    TtsService ttsService;

    public TtsRestController() {
    }

    @RequestMapping({"/config/get"})
    public BaseResponse<?> getTtsConfig(@RequestParam Long configId) {
        this.logger.debug("getTtsConfig params, {}", configId);
        BaseResponse resp = new BaseResponse();

        try {
            TtsConfigEntity data = this.ttsService.getTtsConfig(configId);
            resp.setData(data);
        } catch (TtsPackageException var4) {
            resp.setCode(var4.getErrCode());
            resp.setMsg(var4.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/config/save"})
    public BaseResponse<?> saveTtsConfig(HttpServletRequest request, @RequestBody TtsConfigEntity param) {
        this.logger.debug("saveTtsConfig params, {}", param);
        BaseResponse<TtsConfigEntity> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            TtsConfigEntity data = this.ttsService.saveTtsConfig(param, userInfo.getUserId());
            resp.setData(data);
        } catch (TtsPackageException var6) {
            resp.setCode(var6.getErrCode());
            resp.setMsg(var6.getErrMsg());
        }

        return resp;
    }
}
