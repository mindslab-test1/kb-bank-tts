package ai.mindslab.tts_package.controller.preprocess;

import ai.mindslab.tts_package.common.codes.IRestCodes;
import ai.mindslab.tts_package.common.data.BaseResponse;
import ai.mindslab.tts_package.common.exceptions.TtsPackageException;
import ai.mindslab.tts_package.data.Eng2KorEntity;
import ai.mindslab.tts_package.data.KonglishEntity;
import ai.mindslab.tts_package.data.UserInfo;
import ai.mindslab.tts_package.domain.preprocess.PreprocessDataTableEntity;
import ai.mindslab.tts_package.domain.preprocess.PreprocessProfileEntity;
import ai.mindslab.tts_package.domain.preprocess.PreprocessRuleEntity;
import ai.mindslab.tts_package.service.preprocess.PreprocessService;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/preprocess"})
public class PreprocessRestController implements IRestCodes {
    private Logger logger = LoggerFactory.getLogger(PreprocessRestController.class);
    @Autowired
    PreprocessService preprocessService;

    public PreprocessRestController() {
    }

    @RequestMapping({"/rule/list"})
    public BaseResponse<?> getPreprocessRuleList(HttpServletRequest request, @RequestParam(name = "searchCol",required = false) String searchCol, @RequestParam(name = "searchText",required = false) String searchText, @PageableDefault(size = 2147483647) Pageable pageable) {
        this.logger.debug("getPreprocessRuleList params, {}, {}, {}", new Object[]{searchCol, searchText, pageable});
        BaseResponse<Page<PreprocessRuleEntity>> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var7 = userInfo.getUserRoleName().toLowerCase();
            byte var8 = -1;
            switch(var7.hashCode()) {
                case -500553564:
                    if (var7.equals("operator")) {
                        var8 = 2;
                    }
                    break;
                case 92668751:
                    if (var7.equals("admin")) {
                        var8 = 0;
                    }
                    break;
                case 835260333:
                    if (var7.equals("manager")) {
                        var8 = 1;
                    }
            }

            switch(var8) {
                case 0:
                case 1:
                    Page<PreprocessRuleEntity> list = this.preprocessService.getPreprocessRuleList(searchCol, searchText, pageable);
                    resp.setData(list);
                    break;
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var10) {
            resp.setCode(var10.getErrCode());
            resp.setMsg(var10.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/rule/get"})
    public BaseResponse<?> getPreprocessRule(HttpServletRequest request, @RequestParam Long ruleId) {
        this.logger.debug("getPreprocessRule params, {}", ruleId);
        BaseResponse<PreprocessRuleEntity> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                case 1:
                    PreprocessRuleEntity data = this.preprocessService.getPreprocessRule(ruleId);
                    resp.setData(data);
                    break;
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var8) {
            resp.setCode(var8.getErrCode());
            resp.setMsg(var8.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/rule/save"})
    public BaseResponse<?> savePreprocessRule(HttpServletRequest request, @RequestBody PreprocessRuleEntity param, @PageableDefault(size = 2147483647) Pageable pageable) {
        this.logger.debug("savePreprocessRule params, {}", param);
        BaseResponse<PreprocessRuleEntity> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var6 = userInfo.getUserRoleName().toLowerCase();
            byte var7 = -1;
            switch(var6.hashCode()) {
                case -500553564:
                    if (var6.equals("operator")) {
                        var7 = 2;
                    }
                    break;
                case 92668751:
                    if (var6.equals("admin")) {
                        var7 = 0;
                    }
                    break;
                case 835260333:
                    if (var6.equals("manager")) {
                        var7 = 1;
                    }
            }

            switch(var7) {
                case 0:
                    PreprocessRuleEntity data = this.preprocessService.savePreprocessRule(param, userInfo.getUserId());
                    resp.setData(data);
                    break;
                case 1:
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var9) {
            resp.setCode(var9.getErrCode());
            resp.setMsg(var9.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/rule/delete"})
    public BaseResponse<?> deletePreprocessRule(HttpServletRequest request, @RequestParam Long[] ids) {
        this.logger.debug("deletePreprocessRule params, {}", Arrays.toString(ids));
        BaseResponse<Integer> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                    int count = this.preprocessService.deletePreprocessRule(ids);
                    resp.setData(count);
                    break;
                case 1:
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var8) {
            resp.setCode(var8.getErrCode());
            resp.setMsg(var8.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/rule/test"})
    public BaseResponse<?> testPreprocessRule(@RequestParam String ruleExpression, @RequestParam String testText) {
        this.logger.debug("testPreprocessRule params, {}, {}", ruleExpression, testText);
        BaseResponse resp = new BaseResponse();

        try {
            String result = this.preprocessService.testPreprocessRule(ruleExpression, testText);
            resp.setData(result);
        } catch (TtsPackageException var5) {
            resp.setCode(var5.getErrCode());
            resp.setMsg(var5.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/ruleManage/txt"})
    public String extractRuleTxt() {
        this.logger.info("extractRuleTxt");
        this.preprocessService.extractRuleTxt();
        return "rule.txt extracted";
    }

    @RequestMapping({"/dataTable/list"})
    public BaseResponse<?> getPreprocessDataTableList(HttpServletRequest request, @RequestParam(name = "searchCol",required = false) String searchCol, @RequestParam(name = "searchText",required = false) String searchText, @PageableDefault(size = 2147483647) Pageable pageable) {
        this.logger.debug("getPreprocessDataTableList params, {}, {}, {}", new Object[]{searchCol, searchText, pageable});
        BaseResponse<Page<PreprocessDataTableEntity>> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var7 = userInfo.getUserRoleName().toLowerCase();
            byte var8 = -1;
            switch(var7.hashCode()) {
                case -500553564:
                    if (var7.equals("operator")) {
                        var8 = 2;
                    }
                    break;
                case 92668751:
                    if (var7.equals("admin")) {
                        var8 = 0;
                    }
                    break;
                case 835260333:
                    if (var7.equals("manager")) {
                        var8 = 1;
                    }
            }

            switch(var8) {
                case 0:
                case 1:
                    Page<PreprocessDataTableEntity> list = this.preprocessService.getPreprocessTableList(searchCol, searchText, pageable);
                    resp.setData(list);
                    break;
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var10) {
            resp.setCode(var10.getErrCode());
            resp.setMsg(var10.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/dataTable/get"})
    public BaseResponse<?> getPreprocessDataTable(HttpServletRequest request, @RequestParam Long dataTableId) {
        this.logger.debug("getPreprocessDataTable params, {}", dataTableId);
        BaseResponse<PreprocessDataTableEntity> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                case 1:
                    PreprocessDataTableEntity data = this.preprocessService.getPreprocessTable(dataTableId);
                    resp.setData(data);
                    break;
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var8) {
            resp.setCode(var8.getErrCode());
            resp.setMsg(var8.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/dataTable/save"})
    public BaseResponse<?> savePreprocessDataTable(HttpServletRequest request, @RequestBody PreprocessDataTableEntity param) {
        this.logger.debug("savePreprocessDataTable params, {}", param);
        BaseResponse<PreprocessDataTableEntity> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                    PreprocessDataTableEntity data = this.preprocessService.savePreprocessTable(param, userInfo.getUserId());
                    resp.setData(data);
                    break;
                case 1:
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var8) {
            resp.setCode(var8.getErrCode());
            resp.setMsg(var8.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/dataTable/delete"})
    public BaseResponse<?> deletePreprocessDataTable(HttpServletRequest request, @RequestParam Long[] ids) {
        this.logger.debug("deletePreprocessDataTable params, {}", Arrays.toString(ids));
        BaseResponse<Integer> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                    int count = this.preprocessService.deletePreprocessTable(ids);
                    resp.setData(count);
                    break;
                case 1:
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var8) {
            resp.setCode(var8.getErrCode());
            resp.setMsg(var8.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/dataTableManage/txt"})
    public String extractDataTableTxt() {
        this.logger.info("extractDataTableTxt");
        this.preprocessService.extractDataTableTxt();
        return "data_table.txt extracted";
    }

    @RequestMapping({"/profile/list"})
    public BaseResponse<?> getPreprocessProfileList(HttpServletRequest request, @RequestParam(name = "searchCol",required = false) String searchCol, @RequestParam(name = "searchText",required = false) String searchText, @PageableDefault(size = 2147483647) Pageable pageable) {
        this.logger.debug("getPreprocessProfileList params, {}, {}, {}", new Object[]{searchCol, searchText, pageable});
        BaseResponse<Page<PreprocessProfileEntity>> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var7 = userInfo.getUserRoleName().toLowerCase();
            byte var8 = -1;
            switch(var7.hashCode()) {
                case -500553564:
                    if (var7.equals("operator")) {
                        var8 = 2;
                    }
                    break;
                case 92668751:
                    if (var7.equals("admin")) {
                        var8 = 0;
                    }
                    break;
                case 835260333:
                    if (var7.equals("manager")) {
                        var8 = 1;
                    }
            }

            switch(var8) {
                case 0:
                case 1:
                case 2:
                    Page<PreprocessProfileEntity> list = this.preprocessService.getPreprocessProfileList(searchCol, searchText, pageable);
                    resp.setData(list);
                    break;
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var10) {
            resp.setCode(var10.getErrCode());
            resp.setMsg(var10.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/profile/get"})
    public BaseResponse<?> getPreprocessProfile(HttpServletRequest request, @RequestParam Long profileId) {
        this.logger.debug("getPreprocessProfile params, {}", profileId);
        BaseResponse<PreprocessProfileEntity> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                case 1:
                    PreprocessProfileEntity data = this.preprocessService.getPreprocessProfile(profileId);
                    resp.setData(data);
                    break;
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var8) {
            resp.setCode(var8.getErrCode());
            resp.setMsg(var8.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/profile/save"})
    public BaseResponse<?> savePreprocessProfile(HttpServletRequest request, @RequestBody PreprocessProfileEntity param) {
        this.logger.debug("savePreprocessProfile params, {}", param);
        BaseResponse<PreprocessProfileEntity> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                    PreprocessProfileEntity data = this.preprocessService.savePreprocessProfile(param, userInfo.getUserId());
                    resp.setData(data);
                    break;
                case 1:
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var8) {
            resp.setCode(var8.getErrCode());
            resp.setMsg(var8.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/profile/delete"})
    public BaseResponse<?> deletePreprocessProfile(HttpServletRequest request, @RequestParam Long[] ids) {
        this.logger.debug("deletePreprocessProfile params, {}", Arrays.toString(ids));
        BaseResponse<Integer> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                    int count = this.preprocessService.deletePreprocessProfile(ids);
                    resp.setData(count);
                    break;
                case 1:
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var8) {
            resp.setCode(var8.getErrCode());
            resp.setMsg(var8.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/profileManage/txt"})
    public String extractProfileTxt() {
        this.logger.info("extractProfileTxt");
        this.preprocessService.extractProfileTxt();
        return "profile.txt extracted";
    }

    @RequestMapping({"/eng2kor/list"})
    public BaseResponse<?> getEng2KorList(HttpServletRequest request, @PageableDefault(size = 2147483647) Pageable pageable) {
        this.logger.debug("getEng2KorList");
        BaseResponse<Page<Eng2KorEntity>> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                case 1:
                    List<Eng2KorEntity> eng2KorEntityList = this.preprocessService.getEng2KorList();
                    int start = (int)pageable.getOffset();
                    int end = Math.min(start + pageable.getPageSize(), eng2KorEntityList.size());
                    Page<Eng2KorEntity> list = new PageImpl(eng2KorEntityList.subList(start, end), pageable, (long)eng2KorEntityList.size());
                    resp.setData(list);
                    break;
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var11) {
            resp.setCode(var11.getErrCode());
            resp.setMsg(var11.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/eng2kor/get"})
    public BaseResponse<?> getEng2Kor(HttpServletRequest request, @RequestParam int eng2korId) {
        this.logger.debug("getEng2Kor params, {}", eng2korId);
        BaseResponse<Eng2KorEntity> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                case 1:
                    Eng2KorEntity data = this.preprocessService.getEng2Kor(eng2korId);
                    resp.setData(data);
                    break;
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var8) {
            resp.setCode(var8.getErrCode());
            resp.setMsg(var8.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/eng2kor/save"})
    public BaseResponse<?> saveEng2Kor(HttpServletRequest request, @RequestBody Eng2KorEntity param) {
        this.logger.debug("saveEng2Kor params, {}", param);
        BaseResponse<List<Eng2KorEntity>> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            int length = this.preprocessService.getEng2KorList().size();
            String var6 = userInfo.getUserRoleName().toLowerCase();
            byte var7 = -1;
            switch(var6.hashCode()) {
                case -500553564:
                    if (var6.equals("operator")) {
                        var7 = 2;
                    }
                    break;
                case 92668751:
                    if (var6.equals("admin")) {
                        var7 = 0;
                    }
                    break;
                case 835260333:
                    if (var6.equals("manager")) {
                        var7 = 1;
                    }
            }

            switch(var7) {
                case 0:
                    List<Eng2KorEntity> data = this.preprocessService.saveEng2Kor(param);
                    if (length == data.size()) {
                        throw new TtsPackageException(1005, "SAME NAME FOUND");
                    }

                    resp.setData(data);
                    break;
                case 1:
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var9) {
            resp.setCode(var9.getErrCode());
            resp.setMsg(var9.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/eng2kor/update"})
    public BaseResponse<?> updateEng2Kor(HttpServletRequest request, @RequestBody Eng2KorEntity param) {
        this.logger.debug("updateEng2Kor params, {}", param);
        BaseResponse<List<Eng2KorEntity>> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                    List<Eng2KorEntity> data = this.preprocessService.updateEng2Kor(param);
                    resp.setData(data);
                    break;
                case 1:
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var8) {
            resp.setCode(var8.getErrCode());
            resp.setMsg(var8.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/eng2kor/delete"})
    public BaseResponse<?> deleteEng2Kor(HttpServletRequest request, @RequestParam Long[] ids) {
        this.logger.debug("deleteEng2Kor params, {}", Arrays.toString(ids));
        BaseResponse<Integer> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                    int count = this.preprocessService.deleteEng2Kor(ids);
                    resp.setData(count);
                    break;
                case 1:
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var8) {
            resp.setCode(var8.getErrCode());
            resp.setMsg(var8.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/konglish/list"})
    public BaseResponse<?> getKonglishList(HttpServletRequest request, @PageableDefault(size = 2147483647) Pageable pageable) {
        this.logger.debug("getKonglishList");
        BaseResponse<Page<KonglishEntity>> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                case 1:
                    List<KonglishEntity> konglishList = this.preprocessService.getKonglishList();
                    int start = (int)pageable.getOffset();
                    int end = Math.min(start + pageable.getPageSize(), konglishList.size());
                    Page<KonglishEntity> list = new PageImpl(konglishList.subList(start, end), pageable, (long)konglishList.size());
                    resp.setData(list);
                    break;
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var11) {
            resp.setCode(var11.getErrCode());
            resp.setMsg(var11.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/konglish/get"})
    public BaseResponse<?> getKonglish(HttpServletRequest request, @RequestParam int konglishId) {
        this.logger.debug("getkonglish params, {}", konglishId);
        BaseResponse<KonglishEntity> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                case 1:
                    KonglishEntity data = this.preprocessService.getKonglish(konglishId);
                    resp.setData(data);
                    break;
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var8) {
            resp.setCode(var8.getErrCode());
            resp.setMsg(var8.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/konglish/save"})
    public BaseResponse<?> saveKonglish(HttpServletRequest request, @RequestBody KonglishEntity param) {
        this.logger.debug("saveKonglish params, {}", param);
        BaseResponse<List<KonglishEntity>> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            int length = this.preprocessService.getKonglishList().size();
            String var6 = userInfo.getUserRoleName().toLowerCase();
            byte var7 = -1;
            switch(var6.hashCode()) {
                case -500553564:
                    if (var6.equals("operator")) {
                        var7 = 2;
                    }
                    break;
                case 92668751:
                    if (var6.equals("admin")) {
                        var7 = 0;
                    }
                    break;
                case 835260333:
                    if (var6.equals("manager")) {
                        var7 = 1;
                    }
            }

            switch(var7) {
                case 0:
                    List<KonglishEntity> data = this.preprocessService.saveKonglish(param);
                    if (length == data.size()) {
                        throw new TtsPackageException(1005, "SAME NAME FOUND");
                    }

                    resp.setData(data);
                    break;
                case 1:
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var9) {
            resp.setCode(var9.getErrCode());
            resp.setMsg(var9.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/konglish/update"})
    public BaseResponse<?> updateKonglish(HttpServletRequest request, @RequestBody KonglishEntity param) {
        this.logger.debug("updateKonglish params, {}", param);
        BaseResponse<List<KonglishEntity>> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                    List<KonglishEntity> data = this.preprocessService.updateKonglish(param);
                    resp.setData(data);
                    break;
                case 1:
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var8) {
            resp.setCode(var8.getErrCode());
            resp.setMsg(var8.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/konglish/delete"})
    public BaseResponse<?> deleteKonglish(HttpServletRequest request, @RequestParam Long[] ids) {
        this.logger.debug("deleteKonglish params, {}", Arrays.toString(ids));
        BaseResponse<Integer> resp = new BaseResponse();
        UserInfo userInfo = (UserInfo)request.getSession().getAttribute("userInfo");

        try {
            String var5 = userInfo.getUserRoleName().toLowerCase();
            byte var6 = -1;
            switch(var5.hashCode()) {
                case -500553564:
                    if (var5.equals("operator")) {
                        var6 = 2;
                    }
                    break;
                case 92668751:
                    if (var5.equals("admin")) {
                        var6 = 0;
                    }
                    break;
                case 835260333:
                    if (var5.equals("manager")) {
                        var6 = 1;
                    }
            }

            switch(var6) {
                case 0:
                    int count = this.preprocessService.deleteKonglish(ids);
                    resp.setData(count);
                    break;
                case 1:
                case 2:
                default:
                    throw new TtsPackageException(2001, "PERMISSION DENIED");
            }
        } catch (TtsPackageException var8) {
            resp.setCode(var8.getErrCode());
            resp.setMsg(var8.getErrMsg());
        }

        return resp;
    }

    @RequestMapping({"/ttsPreproc"})
    public BaseResponse<?> ttsPreproc(HttpServletRequest request, @RequestParam(name = "txt") String txt) {
        this.logger.debug("ttsPreproc params, {}", txt);
        BaseResponse<String> resp = new BaseResponse();
        String processedStr = this.preprocessService.preprocess(txt);
        resp.setData(processedStr);
        return resp;
    }
}
