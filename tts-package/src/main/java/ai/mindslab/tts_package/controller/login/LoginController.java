package ai.mindslab.tts_package.controller.login;

import ai.mindslab.tts_package.common.exceptions.TtsPackageException;
import ai.mindslab.tts_package.data.UserInfo;
import ai.mindslab.tts_package.domain.common.CommonUserEntity;
import ai.mindslab.tts_package.security.ZRsaSecurity;
import ai.mindslab.tts_package.service.common.CommonService;
import ai.mindslab.tts_package.service.user.UserLoggerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URLEncoder;
import java.security.PrivateKey;
import java.time.LocalDateTime;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
    private Logger logger = LoggerFactory.getLogger(LoginController.class);
    @Autowired
    CommonService commonService;
    @Autowired
    UserLoggerService userLoggerService;
    @Autowired
    BCryptPasswordEncoder passwordEncoder;
    @Autowired
    AuthenticationManager authenticationManager;

    public LoginController() {
    }

    @RequestMapping({"/login"})
    public ModelAndView mainView(HttpServletRequest req, HttpServletResponse res) {
        ModelAndView view = new ModelAndView();
        view.setViewName("adminLogin");
        return view;
    }

    @RequestMapping({"getRSAKeyValue"})
    public ModelAndView getRSAKeyValue(HttpServletRequest req, HttpServletResponse res) throws Exception {
        ModelAndView view = new ModelAndView();
        view.setViewName("jsonView");

        try {
            ZRsaSecurity zSecurity = new ZRsaSecurity();
            PrivateKey privateKey = zSecurity.getPrivateKey();
            HttpSession session = req.getSession();
            if (session.getAttribute("_rsaPrivateKey_") != null) {
                session.removeAttribute("_rsaPrivateKey_");
            }

            session.setAttribute("_rsaPrivateKey_", privateKey);
            String publicKeyModulus = zSecurity.getRsaPublicKeyModulus();
            String publicKeyExponent = zSecurity.getRsaPublicKeyExponent();
            view.addObject("publicKeyModulus", publicKeyModulus);
            view.addObject("publicKeyExponent", publicKeyExponent);
        } catch (Exception var9) {
        }

        return view;
    }

    @RequestMapping({"/changePassword"})
    @ResponseBody
    public ModelMap changePw(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") String id, @RequestParam("pw") String pw, @RequestParam("pwCf") String pwConfirm) {
        ModelMap map = new ModelMap();

        try {
            ZRsaSecurity rsa = new ZRsaSecurity();
            HttpSession session = request.getSession();
            PrivateKey privateKey = (PrivateKey)session.getAttribute("_rsaPrivateKey_");
            String userId = rsa.decryptRSA(privateKey, id);
            String userPw = rsa.decryptRSA(privateKey, pw);
            String pwCf = rsa.decryptRSA(privateKey, pwConfirm);
            CommonUserEntity userEntity = this.commonService.getCommonUser(userId);
            if (!userPw.equals(pwCf)) {
                throw new Exception("비밀번호가 서로 다릅니다.");
            }

            userEntity.setPassword(userPw);
            userEntity.setPwChgDtm(LocalDateTime.now());
            this.commonService.saveCommonUser(userEntity, userId, request);
            session.removeAttribute("_rsaPrivateKey_");
            map.put("resultCode", 200);
            map.put("resultMsg", "OK");
            map.put("returnUrl", this.getReturnUrl(request, response));
        } catch (TtsPackageException var14) {
            if (var14.getErrCode() == 1001) {
                map.put("resultCode", 502);
                map.put("resultMsg", var14.getMessage());
            } else {
                map.put("resultCode", 500);
                map.put("resultMsg", var14.getMessage());
            }
        } catch (Exception var15) {
            map.put("resultCode", 500);
            map.put("resultMsg", var15.getMessage());
        }

        return map;
    }

    @RequestMapping(
            value = {"/loginajax"},
            method = {RequestMethod.POST}
    )
    @ResponseBody
    public ModelMap login(HttpServletRequest request, HttpServletResponse response, @RequestParam("userId") String username, @RequestParam("userKey") String password) {
        ModelMap map = new ModelMap();
        String userId = null;

        try {
            ZRsaSecurity rsa = new ZRsaSecurity();
            HttpSession session = request.getSession();
            PrivateKey privateKey = (PrivateKey)session.getAttribute("_rsaPrivateKey_");
            userId = rsa.decryptRSA(privateKey, username);
            String userKey = rsa.decryptRSA(privateKey, password);
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userId, userKey);
            Authentication auth = this.authenticationManager.authenticate(token);
            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(auth);
            CommonUserEntity user = this.commonService.getCommonUser(userId);
            UserInfo info = new UserInfo();
            info.setUserId(userId);
            info.setUserName(user.getUserName());
            info.setUserRoleId(user.getRoleId());
            info.setUserRoleName(user.getRole().getRoleName());
            info.setUserRoleDescript(user.getRole().getDescript());
            if (!userId.equals("admin")) {
                if (!this.commonService.getLastUserHistoryValid(userId)) {
                    map.put("resultCode", 1002);
                    map.put("resultMsg", "로그인한지 1달이 지났습니다. 관리자에게 문의하세요.");
                    return map;
                }

                if (!this.commonService.getUserIdPwValid(user)) {
                    map.put("resultCode", 501);
                    map.put("resultMsg", "password expiration");
                    return map;
                }
            }

            this.commonService.userHistory(userId, this.getUserIp(request), "I");
            this.logger.info("{}, {}, {}", new Object[]{userId, this.getUserIp(request), "I"});
            this.userLoggerService.mkLogFile(userId, this.getUserIp(request), "I");
            session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
            session.setAttribute("userInfo", info);
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = mapper.writeValueAsString(info);
            Cookie userCookie = new Cookie("userInfo", URLEncoder.encode(jsonString, "utf-8"));
            response.addCookie(userCookie);
            session.removeAttribute("_rsaPrivateKey_");
            this.commonService.clearLoginFailCnt(userId);
            map.put("resultCode", 200);
            map.put("resultMsg", "OK");
            map.put("returnUrl", "/login");
        } catch (TtsPackageException var21) {
            map.put("resultCode", var21.getErrCode());
            map.put("resultMsg", var21.getErrMsg());
        } catch (Exception var22) {
            map.put("resultCode", 500);
            map.put("resultMsg", var22.getMessage());

            try {
                int failCnt = this.commonService.addLoginFailCnt(userId);
                this.commonService.userHistory(userId, this.getUserIp(request), "N");
                this.logger.info("{}, {}, {}", new Object[]{userId, this.getUserIp(request), "N"});
                this.userLoggerService.mkLogFile(userId, this.getUserIp(request), "N");
                if (failCnt >= 5) {
                    this.commonService.lockUser(userId);
                    map.put("resultCode", 504);
                    map.put("resultMsg", "비밀번호를 5회이상 잘못 입력하셨습니다. 계정이 비활성화 됩니다.");
                } else {
                    map.put("resultCode", 503);
                    map.put("resultMsg", "비밀번호를 " + failCnt + "회 잘못 입력하셨습니다.");
                }
            } catch (TtsPackageException var19) {
                map.put("resultCode", var19.getErrCode());
                map.put("resultMsg", var19.getErrMsg());
            } catch (Exception var20) {
            }
        }

        return map;
    }

    @RequestMapping({"/defaultPage"})
    public String logTest(Model model) {
        return "index";
    }

    private String getReturnUrl(HttpServletRequest request, HttpServletResponse response) {
        RequestCache requestCache = new HttpSessionRequestCache();
        SavedRequest savedRequest = requestCache.getRequest(request, response);
        return savedRequest == null ? request.getSession().getServletContext().getContextPath() : savedRequest.getRedirectUrl();
    }

    private String getUserIp(HttpServletRequest request) {
        String ip = request.getHeader("X-FORWARDED-FOR");
        if (ip == null) {
            ip = request.getHeader("Proxy-Client-IP");
        }

        if (ip == null) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }

        if (ip == null) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }

        if (ip == null) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }

        if (ip == null) {
            ip = request.getRemoteAddr();
        }

        return ip;
    }
}
