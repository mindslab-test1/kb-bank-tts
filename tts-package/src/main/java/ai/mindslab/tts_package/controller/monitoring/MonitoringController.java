package ai.mindslab.tts_package.controller.monitoring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping({"/monitoring"})
public class MonitoringController {
    private Logger logger = LoggerFactory.getLogger(MonitoringController.class);
    private String prefix = "views/monitoring/";
    @Value("${monitor.timer.process}")
    private String processTimer;
    @Value("${monitor.timer.resource}")
    private String resourceTimer;

    public MonitoringController() {
    }

    @RequestMapping({"/process"})
    public String processMonitoring(Model model) {
        this.logger.info("process page open");
        model.addAttribute("processTimer", this.getTimerSeconds(this.processTimer));
        return this.prefix + "process";
    }

    @RequestMapping({"/resource"})
    public String resourceMonitoring(Model model) {
        this.logger.info("resource page open");
        model.addAttribute("resourceTimer", this.getTimerSeconds(this.resourceTimer));
        return this.prefix + "resource";
    }

    @RequestMapping({"/threshold"})
    public String thresholdMonitoring(Model model) {
        this.logger.info("threshold page open");
        return this.prefix + "threshold";
    }

    private int getTimerSeconds(String sTimer) {
        int timer = 60;
        if (!sTimer.isEmpty()) {
            if (sTimer.endsWith("m")) {
                timer = Integer.parseInt(sTimer.substring(0, sTimer.length() - 1));
                timer *= 60;
            } else if (sTimer.endsWith("s")) {
                timer = Integer.parseInt(sTimer.substring(0, sTimer.length() - 1));
            } else {
                timer = Integer.parseInt(sTimer);
            }
        }

        return timer;
    }
}
