package ai.mindslab.tts_package.controller;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BasicErrorController implements ErrorController {
    private static Logger logger = LoggerFactory.getLogger(BasicErrorController.class);
    private static final String ERROR_PATH = "/error";

    public BasicErrorController() {
    }

    public String getErrorPath() {
        return null;
    }

    @RequestMapping({"/error"})
    public String handelError(HttpServletRequest request, Model model) {
        Object status = request.getAttribute("javax.servlet.error.status_code");
        HttpStatus httpStatus = HttpStatus.valueOf(Integer.valueOf(status.toString()));
        logger.info("httpStatus : " + httpStatus.toString());
        model.addAttribute("code", status.toString());
        model.addAttribute("msg", httpStatus.getReasonPhrase());
        model.addAttribute("timestamp", new Date());
        return "views/error.html";
    }
}
