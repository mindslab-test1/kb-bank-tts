package ai.mindslab.tts_package.controller.profile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ProfileController {
    private Logger logger = LoggerFactory.getLogger(ProfileController.class);
    private String prefix = "views/profile/";

    public ProfileController() {
    }

    @RequestMapping({"/profile"})
    public String profile(Model model) {
        this.logger.info("profile page open");
        return this.prefix + "profile";
    }
}
