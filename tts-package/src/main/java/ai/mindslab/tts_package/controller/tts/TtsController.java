package ai.mindslab.tts_package.controller.tts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping({"/tts"})
public class TtsController {
    private Logger logger = LoggerFactory.getLogger(TtsController.class);
    private String prefix = "views/tts/";
    @Value("${tts.ip}")
    private String ttsIp;
    @Value("${tts.maxSpeakerId}")
    private String maxSpeakerId;

    public TtsController() {
    }

    @RequestMapping({"/config"})
    public String ttsConfig(Model model) {
        this.logger.info("ttsConfig page open");
        model.addAttribute("maxSpeakerId", this.maxSpeakerId);
        return this.prefix + "config";
    }

    @RequestMapping({"/test"})
    public String ttsTest(Model model) {
        this.logger.info("ttsTest page open");
        model.addAttribute("ttsIp", this.ttsIp);
        model.addAttribute("maxSpeakerId", this.maxSpeakerId);
        return this.prefix + "test";
    }

    @RequestMapping({"/test2"})
    public String ttsTest2(Model model) {
        this.logger.info("ttsTest2 page open");
        model.addAttribute("ttsIp", this.ttsIp);
        model.addAttribute("maxSpeakerId", this.maxSpeakerId);
        return this.prefix + "test2";
    }
}
