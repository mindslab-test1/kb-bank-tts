package ai.mindslab.tts_package.domain.common;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(
        name = "user_history_tb"
)
public class UserHistoryEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    @Column(
            name = "history_id"
    )
    private Long historyId;
    @Column(
            updatable = false
    )
    private String userId;
    @Column(
            updatable = false,
            name = "user_ip"
    )
    private String userIP;
    @Column(
            length = 1,
            columnDefinition = "char"
    )
    private String statusCd;
    @Column(
            updatable = false
    )
    @CreationTimestamp
    private LocalDateTime createdDtm;

    public UserHistoryEntity() {
    }

    public Long getHistoryId() {
        return this.historyId;
    }

    public String getUserId() {
        return this.userId;
    }

    public String getUserIP() {
        return this.userIP;
    }

    public String getStatusCd() {
        return this.statusCd;
    }

    public LocalDateTime getCreatedDtm() {
        return this.createdDtm;
    }

    public void setHistoryId(final Long historyId) {
        this.historyId = historyId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public void setUserIP(final String userIP) {
        this.userIP = userIP;
    }

    public void setStatusCd(final String statusCd) {
        this.statusCd = statusCd;
    }

    public void setCreatedDtm(final LocalDateTime createdDtm) {
        this.createdDtm = createdDtm;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof UserHistoryEntity)) {
            return false;
        } else {
            UserHistoryEntity other = (UserHistoryEntity)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label71: {
                    Object this$historyId = this.getHistoryId();
                    Object other$historyId = other.getHistoryId();
                    if (this$historyId == null) {
                        if (other$historyId == null) {
                            break label71;
                        }
                    } else if (this$historyId.equals(other$historyId)) {
                        break label71;
                    }

                    return false;
                }

                Object this$userId = this.getUserId();
                Object other$userId = other.getUserId();
                if (this$userId == null) {
                    if (other$userId != null) {
                        return false;
                    }
                } else if (!this$userId.equals(other$userId)) {
                    return false;
                }

                label57: {
                    Object this$userIP = this.getUserIP();
                    Object other$userIP = other.getUserIP();
                    if (this$userIP == null) {
                        if (other$userIP == null) {
                            break label57;
                        }
                    } else if (this$userIP.equals(other$userIP)) {
                        break label57;
                    }

                    return false;
                }

                Object this$statusCd = this.getStatusCd();
                Object other$statusCd = other.getStatusCd();
                if (this$statusCd == null) {
                    if (other$statusCd != null) {
                        return false;
                    }
                } else if (!this$statusCd.equals(other$statusCd)) {
                    return false;
                }

                Object this$createdDtm = this.getCreatedDtm();
                Object other$createdDtm = other.getCreatedDtm();
                if (this$createdDtm == null) {
                    if (other$createdDtm == null) {
                        return true;
                    }
                } else if (this$createdDtm.equals(other$createdDtm)) {
                    return true;
                }

                return false;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof UserHistoryEntity;
    }

    public int hashCode() {
//        int PRIME = true;
        int result = 1;
        Object $historyId = this.getHistoryId();
//        int result = result * 59 + ($historyId == null ? 43 : $historyId.hashCode());
        result = result * 59 + ($historyId == null ? 43 : $historyId.hashCode());
        Object $userId = this.getUserId();
        result = result * 59 + ($userId == null ? 43 : $userId.hashCode());
        Object $userIP = this.getUserIP();
        result = result * 59 + ($userIP == null ? 43 : $userIP.hashCode());
        Object $statusCd = this.getStatusCd();
        result = result * 59 + ($statusCd == null ? 43 : $statusCd.hashCode());
        Object $createdDtm = this.getCreatedDtm();
        result = result * 59 + ($createdDtm == null ? 43 : $createdDtm.hashCode());
        return result;
    }

    public String toString() {
        return "UserHistoryEntity(historyId=" + this.getHistoryId() + ", userId=" + this.getUserId() + ", userIP=" + this.getUserIP() + ", statusCd=" + this.getStatusCd() + ", createdDtm=" + this.getCreatedDtm() + ")";
    }
}
