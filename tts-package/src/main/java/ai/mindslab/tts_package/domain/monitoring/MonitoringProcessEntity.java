package ai.mindslab.tts_package.domain.monitoring;

import java.time.LocalDateTime;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(
        name = "mt_process_tb"
)
public class MonitoringProcessEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long processNo;
    @Column(
            nullable = false
    )
    private String processName;
    @OneToMany(
            fetch = FetchType.EAGER,
            cascade = {CascadeType.ALL}
    )
    @JoinColumn(
            name = "process_no"
    )
    private Collection<ProcessHistoryEntity> processHistory;
    private String startScriptPath;
    private String endScriptPath;
    private String stateScriptPath;
    @Column(
            length = 1,
            columnDefinition = "char"
    )
    private String autoRecovery;
    @Column(
            length = 1,
            columnDefinition = "char"
    )
    private String useYn;
    @Column(
            updatable = false
    )
    @CreationTimestamp
    private LocalDateTime createdDtm;
    @UpdateTimestamp
    private LocalDateTime updatedDtm;
    @Column(
            updatable = false
    )
    private String creatorId = "unknown";
    private String updaterId;

    public MonitoringProcessEntity() {
    }

    public Long getProcessNo() {
        return this.processNo;
    }

    public String getProcessName() {
        return this.processName;
    }

    public Collection<ProcessHistoryEntity> getProcessHistory() {
        return this.processHistory;
    }

    public String getStartScriptPath() {
        return this.startScriptPath;
    }

    public String getEndScriptPath() {
        return this.endScriptPath;
    }

    public String getStateScriptPath() {
        return this.stateScriptPath;
    }

    public String getAutoRecovery() {
        return this.autoRecovery;
    }

    public String getUseYn() {
        return this.useYn;
    }

    public LocalDateTime getCreatedDtm() {
        return this.createdDtm;
    }

    public LocalDateTime getUpdatedDtm() {
        return this.updatedDtm;
    }

    public String getCreatorId() {
        return this.creatorId;
    }

    public String getUpdaterId() {
        return this.updaterId;
    }

    public void setProcessNo(final Long processNo) {
        this.processNo = processNo;
    }

    public void setProcessName(final String processName) {
        this.processName = processName;
    }

    public void setProcessHistory(final Collection<ProcessHistoryEntity> processHistory) {
        this.processHistory = processHistory;
    }

    public void setStartScriptPath(final String startScriptPath) {
        this.startScriptPath = startScriptPath;
    }

    public void setEndScriptPath(final String endScriptPath) {
        this.endScriptPath = endScriptPath;
    }

    public void setStateScriptPath(final String stateScriptPath) {
        this.stateScriptPath = stateScriptPath;
    }

    public void setAutoRecovery(final String autoRecovery) {
        this.autoRecovery = autoRecovery;
    }

    public void setUseYn(final String useYn) {
        this.useYn = useYn;
    }

    public void setCreatedDtm(final LocalDateTime createdDtm) {
        this.createdDtm = createdDtm;
    }

    public void setUpdatedDtm(final LocalDateTime updatedDtm) {
        this.updatedDtm = updatedDtm;
    }

    public void setCreatorId(final String creatorId) {
        this.creatorId = creatorId;
    }

    public void setUpdaterId(final String updaterId) {
        this.updaterId = updaterId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof MonitoringProcessEntity)) {
            return false;
        } else {
            MonitoringProcessEntity other = (MonitoringProcessEntity)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label155: {
                    Object this$processNo = this.getProcessNo();
                    Object other$processNo = other.getProcessNo();
                    if (this$processNo == null) {
                        if (other$processNo == null) {
                            break label155;
                        }
                    } else if (this$processNo.equals(other$processNo)) {
                        break label155;
                    }

                    return false;
                }

                Object this$processName = this.getProcessName();
                Object other$processName = other.getProcessName();
                if (this$processName == null) {
                    if (other$processName != null) {
                        return false;
                    }
                } else if (!this$processName.equals(other$processName)) {
                    return false;
                }

                Object this$processHistory = this.getProcessHistory();
                Object other$processHistory = other.getProcessHistory();
                if (this$processHistory == null) {
                    if (other$processHistory != null) {
                        return false;
                    }
                } else if (!this$processHistory.equals(other$processHistory)) {
                    return false;
                }

                label134: {
                    Object this$startScriptPath = this.getStartScriptPath();
                    Object other$startScriptPath = other.getStartScriptPath();
                    if (this$startScriptPath == null) {
                        if (other$startScriptPath == null) {
                            break label134;
                        }
                    } else if (this$startScriptPath.equals(other$startScriptPath)) {
                        break label134;
                    }

                    return false;
                }

                label127: {
                    Object this$endScriptPath = this.getEndScriptPath();
                    Object other$endScriptPath = other.getEndScriptPath();
                    if (this$endScriptPath == null) {
                        if (other$endScriptPath == null) {
                            break label127;
                        }
                    } else if (this$endScriptPath.equals(other$endScriptPath)) {
                        break label127;
                    }

                    return false;
                }

                label120: {
                    Object this$stateScriptPath = this.getStateScriptPath();
                    Object other$stateScriptPath = other.getStateScriptPath();
                    if (this$stateScriptPath == null) {
                        if (other$stateScriptPath == null) {
                            break label120;
                        }
                    } else if (this$stateScriptPath.equals(other$stateScriptPath)) {
                        break label120;
                    }

                    return false;
                }

                Object this$autoRecovery = this.getAutoRecovery();
                Object other$autoRecovery = other.getAutoRecovery();
                if (this$autoRecovery == null) {
                    if (other$autoRecovery != null) {
                        return false;
                    }
                } else if (!this$autoRecovery.equals(other$autoRecovery)) {
                    return false;
                }

                label106: {
                    Object this$useYn = this.getUseYn();
                    Object other$useYn = other.getUseYn();
                    if (this$useYn == null) {
                        if (other$useYn == null) {
                            break label106;
                        }
                    } else if (this$useYn.equals(other$useYn)) {
                        break label106;
                    }

                    return false;
                }

                Object this$createdDtm = this.getCreatedDtm();
                Object other$createdDtm = other.getCreatedDtm();
                if (this$createdDtm == null) {
                    if (other$createdDtm != null) {
                        return false;
                    }
                } else if (!this$createdDtm.equals(other$createdDtm)) {
                    return false;
                }

                label92: {
                    Object this$updatedDtm = this.getUpdatedDtm();
                    Object other$updatedDtm = other.getUpdatedDtm();
                    if (this$updatedDtm == null) {
                        if (other$updatedDtm == null) {
                            break label92;
                        }
                    } else if (this$updatedDtm.equals(other$updatedDtm)) {
                        break label92;
                    }

                    return false;
                }

                Object this$creatorId = this.getCreatorId();
                Object other$creatorId = other.getCreatorId();
                if (this$creatorId == null) {
                    if (other$creatorId != null) {
                        return false;
                    }
                } else if (!this$creatorId.equals(other$creatorId)) {
                    return false;
                }

                Object this$updaterId = this.getUpdaterId();
                Object other$updaterId = other.getUpdaterId();
                if (this$updaterId == null) {
                    if (other$updaterId != null) {
                        return false;
                    }
                } else if (!this$updaterId.equals(other$updaterId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof MonitoringProcessEntity;
    }

    public int hashCode() {
//        int PRIME = true;
        int result = 1;
        Object $processNo = this.getProcessNo();
//        int result = result * 59 + ($processNo == null ? 43 : $processNo.hashCode());
        result = result * 59 + ($processNo == null ? 43 : $processNo.hashCode());
        Object $processName = this.getProcessName();
        result = result * 59 + ($processName == null ? 43 : $processName.hashCode());
        Object $processHistory = this.getProcessHistory();
        result = result * 59 + ($processHistory == null ? 43 : $processHistory.hashCode());
        Object $startScriptPath = this.getStartScriptPath();
        result = result * 59 + ($startScriptPath == null ? 43 : $startScriptPath.hashCode());
        Object $endScriptPath = this.getEndScriptPath();
        result = result * 59 + ($endScriptPath == null ? 43 : $endScriptPath.hashCode());
        Object $stateScriptPath = this.getStateScriptPath();
        result = result * 59 + ($stateScriptPath == null ? 43 : $stateScriptPath.hashCode());
        Object $autoRecovery = this.getAutoRecovery();
        result = result * 59 + ($autoRecovery == null ? 43 : $autoRecovery.hashCode());
        Object $useYn = this.getUseYn();
        result = result * 59 + ($useYn == null ? 43 : $useYn.hashCode());
        Object $createdDtm = this.getCreatedDtm();
        result = result * 59 + ($createdDtm == null ? 43 : $createdDtm.hashCode());
        Object $updatedDtm = this.getUpdatedDtm();
        result = result * 59 + ($updatedDtm == null ? 43 : $updatedDtm.hashCode());
        Object $creatorId = this.getCreatorId();
        result = result * 59 + ($creatorId == null ? 43 : $creatorId.hashCode());
        Object $updaterId = this.getUpdaterId();
        result = result * 59 + ($updaterId == null ? 43 : $updaterId.hashCode());
        return result;
    }

    public String toString() {
        return "MonitoringProcessEntity(processNo=" + this.getProcessNo() + ", processName=" + this.getProcessName() + ", processHistory=" + this.getProcessHistory() + ", startScriptPath=" + this.getStartScriptPath() + ", endScriptPath=" + this.getEndScriptPath() + ", stateScriptPath=" + this.getStateScriptPath() + ", autoRecovery=" + this.getAutoRecovery() + ", useYn=" + this.getUseYn() + ", createdDtm=" + this.getCreatedDtm() + ", updatedDtm=" + this.getUpdatedDtm() + ", creatorId=" + this.getCreatorId() + ", updaterId=" + this.getUpdaterId() + ")";
    }
}
