package ai.mindslab.tts_package.domain.preprocess;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators.PropertyGenerator;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@JsonIdentityInfo(
        generator = PropertyGenerator.class,
        property = "profileId"
)
@Table(
        name = "pp_profile_tb"
)
public class PreprocessProfileEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long profileId;
    @Column(
            nullable = false
    )
    private String profileName;
    private String profileType;
    @OneToMany(
            mappedBy = "profile",
            fetch = FetchType.EAGER,
            cascade = {CascadeType.ALL},
            orphanRemoval = true
    )
    private List<PreprocessProcessEntity> processes = new ArrayList();
    @Column(
            updatable = false
    )
    @CreationTimestamp
    private LocalDateTime createdDtm;
    @UpdateTimestamp
    private LocalDateTime updatedDtm;
    @Column(
            updatable = false
    )
    private String creatorId;
    private String updaterId;

    public Long getProfileId() {
        return this.profileId;
    }

    public String getProfileName() {
        return this.profileName;
    }

    public String getProfileType() {
        return this.profileType;
    }

    public List<PreprocessProcessEntity> getProcesses() {
        return this.processes;
    }

    public LocalDateTime getCreatedDtm() {
        return this.createdDtm;
    }

    public LocalDateTime getUpdatedDtm() {
        return this.updatedDtm;
    }

    public String getCreatorId() {
        return this.creatorId;
    }

    public String getUpdaterId() {
        return this.updaterId;
    }

    public void setProfileId(final Long profileId) {
        this.profileId = profileId;
    }

    public void setProfileName(final String profileName) {
        this.profileName = profileName;
    }

    public void setProfileType(final String profileType) {
        this.profileType = profileType;
    }

    public void setProcesses(final List<PreprocessProcessEntity> processes) {
        this.processes = processes;
    }

    public void setCreatedDtm(final LocalDateTime createdDtm) {
        this.createdDtm = createdDtm;
    }

    public void setUpdatedDtm(final LocalDateTime updatedDtm) {
        this.updatedDtm = updatedDtm;
    }

    public void setCreatorId(final String creatorId) {
        this.creatorId = creatorId;
    }

    public void setUpdaterId(final String updaterId) {
        this.updaterId = updaterId;
    }

    public PreprocessProfileEntity() {
    }
}
