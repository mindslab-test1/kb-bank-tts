package ai.mindslab.tts_package.domain.common;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import java.time.LocalDateTime;

public class QCommonRoleEntity extends EntityPathBase<CommonRoleEntity> {
    private static final long serialVersionUID = -271221203L;
    public static final QCommonRoleEntity commonRoleEntity = new QCommonRoleEntity("commonRoleEntity");
    public final DateTimePath<LocalDateTime> createdDtm = this.createDateTime("createdDtm", LocalDateTime.class);
    public final StringPath creatorId = this.createString("creatorId");
    public final StringPath descript = this.createString("descript");
    public final NumberPath<Integer> roleId = this.createNumber("roleId", Integer.class);
    public final StringPath roleName = this.createString("roleName");
    public final DateTimePath<LocalDateTime> updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
    public final StringPath updaterId = this.createString("updaterId");

    public QCommonRoleEntity(String variable) {
        super(CommonRoleEntity.class, PathMetadataFactory.forVariable(variable));
    }

    public QCommonRoleEntity(Path<? extends CommonRoleEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCommonRoleEntity(PathMetadata metadata) {
        super(CommonRoleEntity.class, metadata);
    }
}
