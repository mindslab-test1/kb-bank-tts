package ai.mindslab.tts_package.domain.monitoring;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import java.time.LocalDateTime;

public class QMonitoringResourceEntity extends EntityPathBase<MonitoringResourceEntity> {
    private static final long serialVersionUID = 242655781L;
    public static final QMonitoringResourceEntity monitoringResourceEntity = new QMonitoringResourceEntity("monitoringResourceEntity");
    public final NumberPath<Float> cpuUsageRate = this.createNumber("cpuUsageRate", Float.class);
    public final DateTimePath<LocalDateTime> createdDtm = this.createDateTime("createdDtm", LocalDateTime.class);
    public final NumberPath<Float> gpuUsageRate = this.createNumber("gpuUsageRate", Float.class);
    public final NumberPath<Integer> memoryAvailable = this.createNumber("memoryAvailable", Integer.class);
    public final NumberPath<Long> resourceId = this.createNumber("resourceId", Long.class);
    public final NumberPath<Integer> storageSpace = this.createNumber("storageSpace", Integer.class);

    public QMonitoringResourceEntity(String variable) {
        super(MonitoringResourceEntity.class, PathMetadataFactory.forVariable(variable));
    }

    public QMonitoringResourceEntity(Path<? extends MonitoringResourceEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMonitoringResourceEntity(PathMetadata metadata) {
        super(MonitoringResourceEntity.class, metadata);
    }
}