package ai.mindslab.tts_package.domain.monitoring;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(
        name = "mt_threshold_tb"
)
public class MonitoringResourceThresholdEntity {
    @Id
    private String sortation;
    private Integer thresholdCaution;
    private Integer thresholdWarning;
    private Integer thresholdDanger;
    @CreationTimestamp
    private LocalDateTime createdDtm;
    @UpdateTimestamp
    private LocalDateTime updatedDtm;
    @Column(
            updatable = false
    )
    private String creatorId = "unknown";
    private String updaterId;

    public MonitoringResourceThresholdEntity() {
    }

    public String getSortation() {
        return this.sortation;
    }

    public Integer getThresholdCaution() {
        return this.thresholdCaution;
    }

    public Integer getThresholdWarning() {
        return this.thresholdWarning;
    }

    public Integer getThresholdDanger() {
        return this.thresholdDanger;
    }

    public LocalDateTime getCreatedDtm() {
        return this.createdDtm;
    }

    public LocalDateTime getUpdatedDtm() {
        return this.updatedDtm;
    }

    public String getCreatorId() {
        return this.creatorId;
    }

    public String getUpdaterId() {
        return this.updaterId;
    }

    public void setSortation(final String sortation) {
        this.sortation = sortation;
    }

    public void setThresholdCaution(final Integer thresholdCaution) {
        this.thresholdCaution = thresholdCaution;
    }

    public void setThresholdWarning(final Integer thresholdWarning) {
        this.thresholdWarning = thresholdWarning;
    }

    public void setThresholdDanger(final Integer thresholdDanger) {
        this.thresholdDanger = thresholdDanger;
    }

    public void setCreatedDtm(final LocalDateTime createdDtm) {
        this.createdDtm = createdDtm;
    }

    public void setUpdatedDtm(final LocalDateTime updatedDtm) {
        this.updatedDtm = updatedDtm;
    }

    public void setCreatorId(final String creatorId) {
        this.creatorId = creatorId;
    }

    public void setUpdaterId(final String updaterId) {
        this.updaterId = updaterId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof MonitoringResourceThresholdEntity)) {
            return false;
        } else {
            MonitoringResourceThresholdEntity other = (MonitoringResourceThresholdEntity)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label107: {
                    Object this$sortation = this.getSortation();
                    Object other$sortation = other.getSortation();
                    if (this$sortation == null) {
                        if (other$sortation == null) {
                            break label107;
                        }
                    } else if (this$sortation.equals(other$sortation)) {
                        break label107;
                    }

                    return false;
                }

                Object this$thresholdCaution = this.getThresholdCaution();
                Object other$thresholdCaution = other.getThresholdCaution();
                if (this$thresholdCaution == null) {
                    if (other$thresholdCaution != null) {
                        return false;
                    }
                } else if (!this$thresholdCaution.equals(other$thresholdCaution)) {
                    return false;
                }

                Object this$thresholdWarning = this.getThresholdWarning();
                Object other$thresholdWarning = other.getThresholdWarning();
                if (this$thresholdWarning == null) {
                    if (other$thresholdWarning != null) {
                        return false;
                    }
                } else if (!this$thresholdWarning.equals(other$thresholdWarning)) {
                    return false;
                }

                label86: {
                    Object this$thresholdDanger = this.getThresholdDanger();
                    Object other$thresholdDanger = other.getThresholdDanger();
                    if (this$thresholdDanger == null) {
                        if (other$thresholdDanger == null) {
                            break label86;
                        }
                    } else if (this$thresholdDanger.equals(other$thresholdDanger)) {
                        break label86;
                    }

                    return false;
                }

                label79: {
                    Object this$createdDtm = this.getCreatedDtm();
                    Object other$createdDtm = other.getCreatedDtm();
                    if (this$createdDtm == null) {
                        if (other$createdDtm == null) {
                            break label79;
                        }
                    } else if (this$createdDtm.equals(other$createdDtm)) {
                        break label79;
                    }

                    return false;
                }

                label72: {
                    Object this$updatedDtm = this.getUpdatedDtm();
                    Object other$updatedDtm = other.getUpdatedDtm();
                    if (this$updatedDtm == null) {
                        if (other$updatedDtm == null) {
                            break label72;
                        }
                    } else if (this$updatedDtm.equals(other$updatedDtm)) {
                        break label72;
                    }

                    return false;
                }

                Object this$creatorId = this.getCreatorId();
                Object other$creatorId = other.getCreatorId();
                if (this$creatorId == null) {
                    if (other$creatorId != null) {
                        return false;
                    }
                } else if (!this$creatorId.equals(other$creatorId)) {
                    return false;
                }

                Object this$updaterId = this.getUpdaterId();
                Object other$updaterId = other.getUpdaterId();
                if (this$updaterId == null) {
                    if (other$updaterId != null) {
                        return false;
                    }
                } else if (!this$updaterId.equals(other$updaterId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof MonitoringResourceThresholdEntity;
    }

    public int hashCode() {
//        int PRIME = true;
        int result = 1;
        Object $sortation = this.getSortation();
//        int result = result * 59 + ($sortation == null ? 43 : $sortation.hashCode());
        result = result * 59 + ($sortation == null ? 43 : $sortation.hashCode());
        Object $thresholdCaution = this.getThresholdCaution();
        result = result * 59 + ($thresholdCaution == null ? 43 : $thresholdCaution.hashCode());
        Object $thresholdWarning = this.getThresholdWarning();
        result = result * 59 + ($thresholdWarning == null ? 43 : $thresholdWarning.hashCode());
        Object $thresholdDanger = this.getThresholdDanger();
        result = result * 59 + ($thresholdDanger == null ? 43 : $thresholdDanger.hashCode());
        Object $createdDtm = this.getCreatedDtm();
        result = result * 59 + ($createdDtm == null ? 43 : $createdDtm.hashCode());
        Object $updatedDtm = this.getUpdatedDtm();
        result = result * 59 + ($updatedDtm == null ? 43 : $updatedDtm.hashCode());
        Object $creatorId = this.getCreatorId();
        result = result * 59 + ($creatorId == null ? 43 : $creatorId.hashCode());
        Object $updaterId = this.getUpdaterId();
        result = result * 59 + ($updaterId == null ? 43 : $updaterId.hashCode());
        return result;
    }

    public String toString() {
        return "MonitoringResourceThresholdEntity(sortation=" + this.getSortation() + ", thresholdCaution=" + this.getThresholdCaution() + ", thresholdWarning=" + this.getThresholdWarning() + ", thresholdDanger=" + this.getThresholdDanger() + ", createdDtm=" + this.getCreatedDtm() + ", updatedDtm=" + this.getUpdatedDtm() + ", creatorId=" + this.getCreatorId() + ", updaterId=" + this.getUpdaterId() + ")";
    }
}
