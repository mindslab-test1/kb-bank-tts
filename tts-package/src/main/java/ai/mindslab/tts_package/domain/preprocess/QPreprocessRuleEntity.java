package ai.mindslab.tts_package.domain.preprocess;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import java.time.LocalDateTime;

public class QPreprocessRuleEntity extends EntityPathBase<PreprocessRuleEntity> {
    private static final long serialVersionUID = -2067753453L;
    public static final QPreprocessRuleEntity preprocessRuleEntity = new QPreprocessRuleEntity("preprocessRuleEntity");
    public final DateTimePath<LocalDateTime> createdDtm = this.createDateTime("createdDtm", LocalDateTime.class);
    public final StringPath creatorId = this.createString("creatorId");
    public final StringPath ruleExpression = this.createString("ruleExpression");
    public final NumberPath<Long> ruleId = this.createNumber("ruleId", Long.class);
    public final StringPath ruleName = this.createString("ruleName");
    public final StringPath ruleType = this.createString("ruleType");
    public final DateTimePath<LocalDateTime> updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
    public final StringPath updaterId = this.createString("updaterId");

    public QPreprocessRuleEntity(String variable) {
        super(PreprocessRuleEntity.class, PathMetadataFactory.forVariable(variable));
    }

    public QPreprocessRuleEntity(Path<? extends PreprocessRuleEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPreprocessRuleEntity(PathMetadata metadata) {
        super(PreprocessRuleEntity.class, metadata);
    }
}
