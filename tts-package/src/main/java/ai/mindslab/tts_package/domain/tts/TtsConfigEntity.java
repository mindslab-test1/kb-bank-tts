package ai.mindslab.tts_package.domain.tts;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(
        name = "tts_config_tb"
)
public class TtsConfigEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long configId;
    private String lang;
    private Integer speakerId;
    private String speakerName;
    private String type;
    private Integer speed;
    private Long profileId;
    @Column(
            updatable = false
    )
    @CreationTimestamp
    private LocalDateTime createdDtm;
    @UpdateTimestamp
    private LocalDateTime updatedDtm;
    @Column(
            updatable = false
    )
    private String creatorId;
    private String updaterId;

    public Long getConfigId() {
        return this.configId;
    }

    public String getLang() {
        return this.lang;
    }

    public Integer getSpeakerId() {
        return this.speakerId;
    }

    public String getSpeakerName() {
        return this.speakerName;
    }

    public String getType() {
        return this.type;
    }

    public Integer getSpeed() {
        return this.speed;
    }

    public Long getProfileId() {
        return this.profileId;
    }

    public LocalDateTime getCreatedDtm() {
        return this.createdDtm;
    }

    public LocalDateTime getUpdatedDtm() {
        return this.updatedDtm;
    }

    public String getCreatorId() {
        return this.creatorId;
    }

    public String getUpdaterId() {
        return this.updaterId;
    }

    public void setConfigId(final Long configId) {
        this.configId = configId;
    }

    public void setLang(final String lang) {
        this.lang = lang;
    }

    public void setSpeakerId(final Integer speakerId) {
        this.speakerId = speakerId;
    }

    public void setSpeakerName(final String speakerName) {
        this.speakerName = speakerName;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public void setSpeed(final Integer speed) {
        this.speed = speed;
    }

    public void setProfileId(final Long profileId) {
        this.profileId = profileId;
    }

    public void setCreatedDtm(final LocalDateTime createdDtm) {
        this.createdDtm = createdDtm;
    }

    public void setUpdatedDtm(final LocalDateTime updatedDtm) {
        this.updatedDtm = updatedDtm;
    }

    public void setCreatorId(final String creatorId) {
        this.creatorId = creatorId;
    }

    public void setUpdaterId(final String updaterId) {
        this.updaterId = updaterId;
    }

    public TtsConfigEntity() {
    }
}
