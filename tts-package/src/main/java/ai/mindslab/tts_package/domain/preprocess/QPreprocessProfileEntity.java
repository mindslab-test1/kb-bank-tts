package ai.mindslab.tts_package.domain.preprocess;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.ListPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.StringPath;
import java.time.LocalDateTime;

public class QPreprocessProfileEntity extends EntityPathBase<PreprocessProfileEntity> {
    private static final long serialVersionUID = -118427048L;
    public static final QPreprocessProfileEntity preprocessProfileEntity = new QPreprocessProfileEntity("preprocessProfileEntity");
    public final DateTimePath<LocalDateTime> createdDtm = this.createDateTime("createdDtm", LocalDateTime.class);
    public final StringPath creatorId = this.createString("creatorId");
    public final ListPath<PreprocessProcessEntity, QPreprocessProcessEntity> processes;
    public final NumberPath<Long> profileId;
    public final StringPath profileName;
    public final StringPath profileType;
    public final DateTimePath<LocalDateTime> updatedDtm;
    public final StringPath updaterId;

    public QPreprocessProfileEntity(String variable) {
        super(PreprocessProfileEntity.class, PathMetadataFactory.forVariable(variable));
        this.processes = this.createList("processes", PreprocessProcessEntity.class, QPreprocessProcessEntity.class, PathInits.DIRECT2);
        this.profileId = this.createNumber("profileId", Long.class);
        this.profileName = this.createString("profileName");
        this.profileType = this.createString("profileType");
        this.updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
        this.updaterId = this.createString("updaterId");
    }

    public QPreprocessProfileEntity(Path<? extends PreprocessProfileEntity> path) {
        super(path.getType(), path.getMetadata());
        this.processes = this.createList("processes", PreprocessProcessEntity.class, QPreprocessProcessEntity.class, PathInits.DIRECT2);
        this.profileId = this.createNumber("profileId", Long.class);
        this.profileName = this.createString("profileName");
        this.profileType = this.createString("profileType");
        this.updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
        this.updaterId = this.createString("updaterId");
    }

    public QPreprocessProfileEntity(PathMetadata metadata) {
        super(PreprocessProfileEntity.class, metadata);
        this.processes = this.createList("processes", PreprocessProcessEntity.class, QPreprocessProcessEntity.class, PathInits.DIRECT2);
        this.profileId = this.createNumber("profileId", Long.class);
        this.profileName = this.createString("profileName");
        this.profileType = this.createString("profileType");
        this.updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
        this.updaterId = this.createString("updaterId");
    }
}
