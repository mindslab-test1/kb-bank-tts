package ai.mindslab.tts_package.domain.common;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.JoinColumnOrFormula;
import org.hibernate.annotations.JoinColumnsOrFormulas;
import org.hibernate.annotations.JoinFormula;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;

@Entity
@Table(
        name = "cm_user_tb"
)
public class CommonUserEntity {
    @Id
    private String userId;
    private Integer roleId;
    private String userName;
    private String password;
    @Column(
            length = 1,
            columnDefinition = "char"
    )
    private String activatedYn;
    @Column(
            updatable = false
    )
    @CreationTimestamp
    private LocalDateTime createdDtm;
    @UpdateTimestamp
    private LocalDateTime updatedDtm;
    private LocalDateTime pwChgDtm;
    @Column(
            updatable = false
    )
    private String creatorId;
    private String updaterId;
    @ColumnDefault("0")
    private Integer loginFailCnt;
    @ManyToOne(
            fetch = FetchType.EAGER
    )
    @JoinColumnsOrFormulas({@JoinColumnOrFormula(
            formula = @JoinFormula("role_id")
    )})
    private CommonRoleEntity role;

    public CommonUserEntity() {
    }

    public String getUserId() {
        return this.userId;
    }

    public Integer getRoleId() {
        return this.roleId;
    }

    public String getUserName() {
        return this.userName;
    }

    public String getPassword() {
        return this.password;
    }

    public String getActivatedYn() {
        return this.activatedYn;
    }

    public LocalDateTime getCreatedDtm() {
        return this.createdDtm;
    }

    public LocalDateTime getUpdatedDtm() {
        return this.updatedDtm;
    }

    public LocalDateTime getPwChgDtm() {
        return this.pwChgDtm;
    }

    public String getCreatorId() {
        return this.creatorId;
    }

    public String getUpdaterId() {
        return this.updaterId;
    }

    public Integer getLoginFailCnt() {
        return this.loginFailCnt;
    }

    public CommonRoleEntity getRole() {
        return this.role;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public void setRoleId(final Integer roleId) {
        this.roleId = roleId;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public void setActivatedYn(final String activatedYn) {
        this.activatedYn = activatedYn;
    }

    public void setCreatedDtm(final LocalDateTime createdDtm) {
        this.createdDtm = createdDtm;
    }

    public void setUpdatedDtm(final LocalDateTime updatedDtm) {
        this.updatedDtm = updatedDtm;
    }

    public void setPwChgDtm(final LocalDateTime pwChgDtm) {
        this.pwChgDtm = pwChgDtm;
    }

    public void setCreatorId(final String creatorId) {
        this.creatorId = creatorId;
    }

    public void setUpdaterId(final String updaterId) {
        this.updaterId = updaterId;
    }

    public void setLoginFailCnt(final Integer loginFailCnt) {
        this.loginFailCnt = loginFailCnt;
    }

    public void setRole(final CommonRoleEntity role) {
        this.role = role;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof CommonUserEntity)) {
            return false;
        } else {
            CommonUserEntity other = (CommonUserEntity)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label155: {
                    Object this$userId = this.getUserId();
                    Object other$userId = other.getUserId();
                    if (this$userId == null) {
                        if (other$userId == null) {
                            break label155;
                        }
                    } else if (this$userId.equals(other$userId)) {
                        break label155;
                    }

                    return false;
                }

                Object this$roleId = this.getRoleId();
                Object other$roleId = other.getRoleId();
                if (this$roleId == null) {
                    if (other$roleId != null) {
                        return false;
                    }
                } else if (!this$roleId.equals(other$roleId)) {
                    return false;
                }

                Object this$userName = this.getUserName();
                Object other$userName = other.getUserName();
                if (this$userName == null) {
                    if (other$userName != null) {
                        return false;
                    }
                } else if (!this$userName.equals(other$userName)) {
                    return false;
                }

                label134: {
                    Object this$password = this.getPassword();
                    Object other$password = other.getPassword();
                    if (this$password == null) {
                        if (other$password == null) {
                            break label134;
                        }
                    } else if (this$password.equals(other$password)) {
                        break label134;
                    }

                    return false;
                }

                label127: {
                    Object this$activatedYn = this.getActivatedYn();
                    Object other$activatedYn = other.getActivatedYn();
                    if (this$activatedYn == null) {
                        if (other$activatedYn == null) {
                            break label127;
                        }
                    } else if (this$activatedYn.equals(other$activatedYn)) {
                        break label127;
                    }

                    return false;
                }

                label120: {
                    Object this$createdDtm = this.getCreatedDtm();
                    Object other$createdDtm = other.getCreatedDtm();
                    if (this$createdDtm == null) {
                        if (other$createdDtm == null) {
                            break label120;
                        }
                    } else if (this$createdDtm.equals(other$createdDtm)) {
                        break label120;
                    }

                    return false;
                }

                Object this$updatedDtm = this.getUpdatedDtm();
                Object other$updatedDtm = other.getUpdatedDtm();
                if (this$updatedDtm == null) {
                    if (other$updatedDtm != null) {
                        return false;
                    }
                } else if (!this$updatedDtm.equals(other$updatedDtm)) {
                    return false;
                }

                label106: {
                    Object this$pwChgDtm = this.getPwChgDtm();
                    Object other$pwChgDtm = other.getPwChgDtm();
                    if (this$pwChgDtm == null) {
                        if (other$pwChgDtm == null) {
                            break label106;
                        }
                    } else if (this$pwChgDtm.equals(other$pwChgDtm)) {
                        break label106;
                    }

                    return false;
                }

                Object this$creatorId = this.getCreatorId();
                Object other$creatorId = other.getCreatorId();
                if (this$creatorId == null) {
                    if (other$creatorId != null) {
                        return false;
                    }
                } else if (!this$creatorId.equals(other$creatorId)) {
                    return false;
                }

                label92: {
                    Object this$updaterId = this.getUpdaterId();
                    Object other$updaterId = other.getUpdaterId();
                    if (this$updaterId == null) {
                        if (other$updaterId == null) {
                            break label92;
                        }
                    } else if (this$updaterId.equals(other$updaterId)) {
                        break label92;
                    }

                    return false;
                }

                Object this$loginFailCnt = this.getLoginFailCnt();
                Object other$loginFailCnt = other.getLoginFailCnt();
                if (this$loginFailCnt == null) {
                    if (other$loginFailCnt != null) {
                        return false;
                    }
                } else if (!this$loginFailCnt.equals(other$loginFailCnt)) {
                    return false;
                }

                Object this$role = this.getRole();
                Object other$role = other.getRole();
                if (this$role == null) {
                    if (other$role != null) {
                        return false;
                    }
                } else if (!this$role.equals(other$role)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof CommonUserEntity;
    }

    public int hashCode() {
//        int PRIME = true;
        int result = 1;
        Object $userId = this.getUserId();
//        int result = result * 59 + ($userId == null ? 43 : $userId.hashCode());
        result = result * 59 + ($userId == null ? 43 : $userId.hashCode());
        Object $roleId = this.getRoleId();
        result = result * 59 + ($roleId == null ? 43 : $roleId.hashCode());
        Object $userName = this.getUserName();
        result = result * 59 + ($userName == null ? 43 : $userName.hashCode());
        Object $password = this.getPassword();
        result = result * 59 + ($password == null ? 43 : $password.hashCode());
        Object $activatedYn = this.getActivatedYn();
        result = result * 59 + ($activatedYn == null ? 43 : $activatedYn.hashCode());
        Object $createdDtm = this.getCreatedDtm();
        result = result * 59 + ($createdDtm == null ? 43 : $createdDtm.hashCode());
        Object $updatedDtm = this.getUpdatedDtm();
        result = result * 59 + ($updatedDtm == null ? 43 : $updatedDtm.hashCode());
        Object $pwChgDtm = this.getPwChgDtm();
        result = result * 59 + ($pwChgDtm == null ? 43 : $pwChgDtm.hashCode());
        Object $creatorId = this.getCreatorId();
        result = result * 59 + ($creatorId == null ? 43 : $creatorId.hashCode());
        Object $updaterId = this.getUpdaterId();
        result = result * 59 + ($updaterId == null ? 43 : $updaterId.hashCode());
        Object $loginFailCnt = this.getLoginFailCnt();
        result = result * 59 + ($loginFailCnt == null ? 43 : $loginFailCnt.hashCode());
        Object $role = this.getRole();
        result = result * 59 + ($role == null ? 43 : $role.hashCode());
        return result;
    }

    public String toString() {
        return "CommonUserEntity(userId=" + this.getUserId() + ", roleId=" + this.getRoleId() + ", userName=" + this.getUserName() + ", password=" + this.getPassword() + ", activatedYn=" + this.getActivatedYn() + ", createdDtm=" + this.getCreatedDtm() + ", updatedDtm=" + this.getUpdatedDtm() + ", pwChgDtm=" + this.getPwChgDtm() + ", creatorId=" + this.getCreatorId() + ", updaterId=" + this.getUpdaterId() + ", loginFailCnt=" + this.getLoginFailCnt() + ", role=" + this.getRole() + ")";
    }
}
