package ai.mindslab.tts_package.domain.preprocess;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(
        name = "pp_process_tb"
)
public class PreprocessProcessEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long processId;
    @ManyToOne(
            fetch = FetchType.LAZY
    )
    @JoinColumn(
            name = "profile_id"
    )
    private PreprocessProfileEntity profile;
    private Integer processOrder;
    private Long ruleId;
    private Long dataTableId;
    private String useYn;
    @Column(
            updatable = false
    )
    @CreationTimestamp
    private LocalDateTime createdDtm;
    @UpdateTimestamp
    private LocalDateTime updatedDtm;
    @Column(
            updatable = false
    )
    private String creatorId;
    private String updaterId;

    public void setProfile(PreprocessProfileEntity profile) {
        if (this.profile != null) {
            this.profile.getProcesses().remove(this);
        }

        this.profile = profile;
        this.profile.getProcesses().add(this);
    }

    public Long getProcessId() {
        return this.processId;
    }

    public PreprocessProfileEntity getProfile() {
        return this.profile;
    }

    public Integer getProcessOrder() {
        return this.processOrder;
    }

    public Long getRuleId() {
        return this.ruleId;
    }

    public Long getDataTableId() {
        return this.dataTableId;
    }

    public String getUseYn() {
        return this.useYn;
    }

    public LocalDateTime getCreatedDtm() {
        return this.createdDtm;
    }

    public LocalDateTime getUpdatedDtm() {
        return this.updatedDtm;
    }

    public String getCreatorId() {
        return this.creatorId;
    }

    public String getUpdaterId() {
        return this.updaterId;
    }

    public void setProcessId(final Long processId) {
        this.processId = processId;
    }

    public void setProcessOrder(final Integer processOrder) {
        this.processOrder = processOrder;
    }

    public void setRuleId(final Long ruleId) {
        this.ruleId = ruleId;
    }

    public void setDataTableId(final Long dataTableId) {
        this.dataTableId = dataTableId;
    }

    public void setUseYn(final String useYn) {
        this.useYn = useYn;
    }

    public void setCreatedDtm(final LocalDateTime createdDtm) {
        this.createdDtm = createdDtm;
    }

    public void setUpdatedDtm(final LocalDateTime updatedDtm) {
        this.updatedDtm = updatedDtm;
    }

    public void setCreatorId(final String creatorId) {
        this.creatorId = creatorId;
    }

    public void setUpdaterId(final String updaterId) {
        this.updaterId = updaterId;
    }

    public PreprocessProcessEntity() {
    }
}
