package ai.mindslab.tts_package.domain.tts;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import java.time.LocalDateTime;

public class QTtsConfigEntity extends EntityPathBase<TtsConfigEntity> {
    private static final long serialVersionUID = 1528922211L;
    public static final QTtsConfigEntity ttsConfigEntity = new QTtsConfigEntity("ttsConfigEntity");
    public final NumberPath<Long> configId = this.createNumber("configId", Long.class);
    public final DateTimePath<LocalDateTime> createdDtm = this.createDateTime("createdDtm", LocalDateTime.class);
    public final StringPath creatorId = this.createString("creatorId");
    public final StringPath lang = this.createString("lang");
    public final NumberPath<Long> profileId = this.createNumber("profileId", Long.class);
    public final NumberPath<Integer> speakerId = this.createNumber("speakerId", Integer.class);
    public final StringPath speakerName = this.createString("speakerName");
    public final NumberPath<Integer> speed = this.createNumber("speed", Integer.class);
    public final StringPath type = this.createString("type");
    public final DateTimePath<LocalDateTime> updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
    public final StringPath updaterId = this.createString("updaterId");

    public QTtsConfigEntity(String variable) {
        super(TtsConfigEntity.class, PathMetadataFactory.forVariable(variable));
    }

    public QTtsConfigEntity(Path<? extends TtsConfigEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTtsConfigEntity(PathMetadata metadata) {
        super(TtsConfigEntity.class, metadata);
    }
}
