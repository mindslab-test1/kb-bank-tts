package ai.mindslab.tts_package.domain.monitoring;

import ai.mindslab.tts_package.domain.monitoring.ProcessHistoryEntity.ProcessStatus;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.NumberPath;
import java.time.LocalDateTime;

public class QProcessHistoryEntity extends EntityPathBase<ProcessHistoryEntity> {
    private static final long serialVersionUID = 1609475988L;
    public static final QProcessHistoryEntity processHistoryEntity = new QProcessHistoryEntity("processHistoryEntity");
    public final DateTimePath<LocalDateTime> createdDtm = this.createDateTime("createdDtm", LocalDateTime.class);
    public final NumberPath<Long> historyId = this.createNumber("historyId", Long.class);
    public final NumberPath<Long> processId = this.createNumber("processId", Long.class);
    public final NumberPath<Long> processNo = this.createNumber("processNo", Long.class);
    public final EnumPath<ProcessStatus> processStatus = this.createEnum("processStatus", ProcessStatus.class);

    public QProcessHistoryEntity(String variable) {
        super(ProcessHistoryEntity.class, PathMetadataFactory.forVariable(variable));
    }

    public QProcessHistoryEntity(Path<? extends ProcessHistoryEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProcessHistoryEntity(PathMetadata metadata) {
        super(ProcessHistoryEntity.class, metadata);
    }
}
