package ai.mindslab.tts_package.domain.preprocess;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.ListPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.StringPath;
import java.time.LocalDateTime;

public class QPreprocessDataTableEntity extends EntityPathBase<PreprocessDataTableEntity> {
    private static final long serialVersionUID = 18985907L;
    public static final QPreprocessDataTableEntity preprocessDataTableEntity = new QPreprocessDataTableEntity("preprocessDataTableEntity");
    public final DateTimePath<LocalDateTime> createdDtm = this.createDateTime("createdDtm", LocalDateTime.class);
    public final StringPath creatorId = this.createString("creatorId");
    public final ListPath<PreprocessDataEntity, QPreprocessDataEntity> datas;
    public final NumberPath<Long> dataTableId;
    public final StringPath dataTableName;
    public final StringPath dataTableType;
    public final DateTimePath<LocalDateTime> updatedDtm;
    public final StringPath updaterId;

    public QPreprocessDataTableEntity(String variable) {
        super(PreprocessDataTableEntity.class, PathMetadataFactory.forVariable(variable));
        this.datas = this.createList("datas", PreprocessDataEntity.class, QPreprocessDataEntity.class, PathInits.DIRECT2);
        this.dataTableId = this.createNumber("dataTableId", Long.class);
        this.dataTableName = this.createString("dataTableName");
        this.dataTableType = this.createString("dataTableType");
        this.updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
        this.updaterId = this.createString("updaterId");
    }

    public QPreprocessDataTableEntity(Path<? extends PreprocessDataTableEntity> path) {
        super(path.getType(), path.getMetadata());
        this.datas = this.createList("datas", PreprocessDataEntity.class, QPreprocessDataEntity.class, PathInits.DIRECT2);
        this.dataTableId = this.createNumber("dataTableId", Long.class);
        this.dataTableName = this.createString("dataTableName");
        this.dataTableType = this.createString("dataTableType");
        this.updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
        this.updaterId = this.createString("updaterId");
    }

    public QPreprocessDataTableEntity(PathMetadata metadata) {
        super(PreprocessDataTableEntity.class, metadata);
        this.datas = this.createList("datas", PreprocessDataEntity.class, QPreprocessDataEntity.class, PathInits.DIRECT2);
        this.dataTableId = this.createNumber("dataTableId", Long.class);
        this.dataTableName = this.createString("dataTableName");
        this.dataTableType = this.createString("dataTableType");
        this.updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
        this.updaterId = this.createString("updaterId");
    }
}
