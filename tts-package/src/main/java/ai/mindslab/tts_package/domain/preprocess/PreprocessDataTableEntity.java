package ai.mindslab.tts_package.domain.preprocess;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators.PropertyGenerator;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@JsonIdentityInfo(
        generator = PropertyGenerator.class,
        property = "dataTableId"
)
@Table(
        name = "pp_data_table_tb"
)
public class PreprocessDataTableEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long dataTableId;
    @Column(
            nullable = false
    )
    private String dataTableName;
    private String dataTableType;
    @OneToMany(
            mappedBy = "table",
            fetch = FetchType.EAGER,
            cascade = {CascadeType.ALL},
            orphanRemoval = true
    )
    private List<PreprocessDataEntity> datas = new ArrayList();
    @Column(
            updatable = false
    )
    @CreationTimestamp
    private LocalDateTime createdDtm;
    @UpdateTimestamp
    private LocalDateTime updatedDtm;
    @Column(
            updatable = false
    )
    private String creatorId;
    private String updaterId;

    public Long getDataTableId() {
        return this.dataTableId;
    }

    public String getDataTableName() {
        return this.dataTableName;
    }

    public String getDataTableType() {
        return this.dataTableType;
    }

    public List<PreprocessDataEntity> getDatas() {
        return this.datas;
    }

    public LocalDateTime getCreatedDtm() {
        return this.createdDtm;
    }

    public LocalDateTime getUpdatedDtm() {
        return this.updatedDtm;
    }

    public String getCreatorId() {
        return this.creatorId;
    }

    public String getUpdaterId() {
        return this.updaterId;
    }

    public void setDataTableId(final Long dataTableId) {
        this.dataTableId = dataTableId;
    }

    public void setDataTableName(final String dataTableName) {
        this.dataTableName = dataTableName;
    }

    public void setDataTableType(final String dataTableType) {
        this.dataTableType = dataTableType;
    }

    public void setDatas(final List<PreprocessDataEntity> datas) {
        this.datas = datas;
    }

    public void setCreatedDtm(final LocalDateTime createdDtm) {
        this.createdDtm = createdDtm;
    }

    public void setUpdatedDtm(final LocalDateTime updatedDtm) {
        this.updatedDtm = updatedDtm;
    }

    public void setCreatorId(final String creatorId) {
        this.creatorId = creatorId;
    }

    public void setUpdaterId(final String updaterId) {
        this.updaterId = updaterId;
    }

    public PreprocessDataTableEntity() {
    }
}
