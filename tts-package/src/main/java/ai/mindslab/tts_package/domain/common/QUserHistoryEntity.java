package ai.mindslab.tts_package.domain.common;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import java.time.LocalDateTime;

public class QUserHistoryEntity extends EntityPathBase<UserHistoryEntity> {
    private static final long serialVersionUID = -842844477L;
    public static final QUserHistoryEntity userHistoryEntity = new QUserHistoryEntity("userHistoryEntity");
    public final DateTimePath<LocalDateTime> createdDtm = this.createDateTime("createdDtm", LocalDateTime.class);
    public final NumberPath<Long> historyId = this.createNumber("historyId", Long.class);
    public final StringPath statusCd = this.createString("statusCd");
    public final StringPath userId = this.createString("userId");
    public final StringPath userIP = this.createString("userIP");

    public QUserHistoryEntity(String variable) {
        super(UserHistoryEntity.class, PathMetadataFactory.forVariable(variable));
    }

    public QUserHistoryEntity(Path<? extends UserHistoryEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUserHistoryEntity(PathMetadata metadata) {
        super(UserHistoryEntity.class, metadata);
    }
}