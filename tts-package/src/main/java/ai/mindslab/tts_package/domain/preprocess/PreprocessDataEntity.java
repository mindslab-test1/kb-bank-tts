package ai.mindslab.tts_package.domain.preprocess;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(
        name = "pp_data_tb"
)
public class PreprocessDataEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long dataId;
    @ManyToOne(
            fetch = FetchType.LAZY
    )
    @JoinColumn(
            name = "data_table_id"
    )
    private PreprocessDataTableEntity table;
    private String dataBefore;
    private String dataAfter;
    private String dataNote;
    @Column(
            updatable = false
    )
    @CreationTimestamp
    private LocalDateTime createdDtm;
    @UpdateTimestamp
    private LocalDateTime updatedDtm;
    @Column(
            updatable = false
    )
    private String creatorId;
    private String updaterId;

    public void setTable(PreprocessDataTableEntity table) {
        if (this.table != null) {
            this.table.getDatas().remove(this);
        }

        this.table = table;
        this.table.getDatas().add(this);
    }

    public Long getDataId() {
        return this.dataId;
    }

    public PreprocessDataTableEntity getTable() {
        return this.table;
    }

    public String getDataBefore() {
        return this.dataBefore;
    }

    public String getDataAfter() {
        return this.dataAfter;
    }

    public String getDataNote() {
        return this.dataNote;
    }

    public LocalDateTime getCreatedDtm() {
        return this.createdDtm;
    }

    public LocalDateTime getUpdatedDtm() {
        return this.updatedDtm;
    }

    public String getCreatorId() {
        return this.creatorId;
    }

    public String getUpdaterId() {
        return this.updaterId;
    }

    public void setDataId(final Long dataId) {
        this.dataId = dataId;
    }

    public void setDataBefore(final String dataBefore) {
        this.dataBefore = dataBefore;
    }

    public void setDataAfter(final String dataAfter) {
        this.dataAfter = dataAfter;
    }

    public void setDataNote(final String dataNote) {
        this.dataNote = dataNote;
    }

    public void setCreatedDtm(final LocalDateTime createdDtm) {
        this.createdDtm = createdDtm;
    }

    public void setUpdatedDtm(final LocalDateTime updatedDtm) {
        this.updatedDtm = updatedDtm;
    }

    public void setCreatorId(final String creatorId) {
        this.creatorId = creatorId;
    }

    public void setUpdaterId(final String updaterId) {
        this.updaterId = updaterId;
    }

    public PreprocessDataEntity() {
    }
}
