package ai.mindslab.tts_package.domain.monitoring;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(
        name = "mt_resource_tb"
)
public class MonitoringResourceEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long resourceId;
    private Float cpuUsageRate;
    private Integer memoryAvailable;
    private Float gpuUsageRate;
    private Integer storageSpace;
    @CreationTimestamp
    private LocalDateTime createdDtm;

    public MonitoringResourceEntity() {
    }

    public MonitoringResourceEntity(BigInteger resourceId, Float cpuUsageRate, Timestamp createdDtm, Float gpuUsageRate, Integer memoryAvailable, Integer storageSpace) {
        this.resourceId = Long.valueOf(String.valueOf(resourceId));
        this.cpuUsageRate = cpuUsageRate;
        this.memoryAvailable = memoryAvailable;
        this.gpuUsageRate = gpuUsageRate;
        this.storageSpace = storageSpace;
        this.createdDtm = createdDtm.toLocalDateTime();
    }

    public Long getResourceId() {
        return this.resourceId;
    }

    public Float getCpuUsageRate() {
        return this.cpuUsageRate;
    }

    public Integer getMemoryAvailable() {
        return this.memoryAvailable;
    }

    public Float getGpuUsageRate() {
        return this.gpuUsageRate;
    }

    public Integer getStorageSpace() {
        return this.storageSpace;
    }

    public LocalDateTime getCreatedDtm() {
        return this.createdDtm;
    }

    public void setResourceId(final Long resourceId) {
        this.resourceId = resourceId;
    }

    public void setCpuUsageRate(final Float cpuUsageRate) {
        this.cpuUsageRate = cpuUsageRate;
    }

    public void setMemoryAvailable(final Integer memoryAvailable) {
        this.memoryAvailable = memoryAvailable;
    }

    public void setGpuUsageRate(final Float gpuUsageRate) {
        this.gpuUsageRate = gpuUsageRate;
    }

    public void setStorageSpace(final Integer storageSpace) {
        this.storageSpace = storageSpace;
    }

    public void setCreatedDtm(final LocalDateTime createdDtm) {
        this.createdDtm = createdDtm;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof MonitoringResourceEntity)) {
            return false;
        } else {
            MonitoringResourceEntity other = (MonitoringResourceEntity)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$resourceId = this.getResourceId();
                Object other$resourceId = other.getResourceId();
                if (this$resourceId == null) {
                    if (other$resourceId != null) {
                        return false;
                    }
                } else if (!this$resourceId.equals(other$resourceId)) {
                    return false;
                }

                Object this$cpuUsageRate = this.getCpuUsageRate();
                Object other$cpuUsageRate = other.getCpuUsageRate();
                if (this$cpuUsageRate == null) {
                    if (other$cpuUsageRate != null) {
                        return false;
                    }
                } else if (!this$cpuUsageRate.equals(other$cpuUsageRate)) {
                    return false;
                }

                Object this$memoryAvailable = this.getMemoryAvailable();
                Object other$memoryAvailable = other.getMemoryAvailable();
                if (this$memoryAvailable == null) {
                    if (other$memoryAvailable != null) {
                        return false;
                    }
                } else if (!this$memoryAvailable.equals(other$memoryAvailable)) {
                    return false;
                }

                label62: {
                    Object this$gpuUsageRate = this.getGpuUsageRate();
                    Object other$gpuUsageRate = other.getGpuUsageRate();
                    if (this$gpuUsageRate == null) {
                        if (other$gpuUsageRate == null) {
                            break label62;
                        }
                    } else if (this$gpuUsageRate.equals(other$gpuUsageRate)) {
                        break label62;
                    }

                    return false;
                }

                label55: {
                    Object this$storageSpace = this.getStorageSpace();
                    Object other$storageSpace = other.getStorageSpace();
                    if (this$storageSpace == null) {
                        if (other$storageSpace == null) {
                            break label55;
                        }
                    } else if (this$storageSpace.equals(other$storageSpace)) {
                        break label55;
                    }

                    return false;
                }

                Object this$createdDtm = this.getCreatedDtm();
                Object other$createdDtm = other.getCreatedDtm();
                if (this$createdDtm == null) {
                    if (other$createdDtm != null) {
                        return false;
                    }
                } else if (!this$createdDtm.equals(other$createdDtm)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof MonitoringResourceEntity;
    }

    public int hashCode() {
//        int PRIME = true;
        int result = 1;
        Object $resourceId = this.getResourceId();
//        int result = result * 59 + ($resourceId == null ? 43 : $resourceId.hashCode());
        result = result * 59 + ($resourceId == null ? 43 : $resourceId.hashCode());
        Object $cpuUsageRate = this.getCpuUsageRate();
        result = result * 59 + ($cpuUsageRate == null ? 43 : $cpuUsageRate.hashCode());
        Object $memoryAvailable = this.getMemoryAvailable();
        result = result * 59 + ($memoryAvailable == null ? 43 : $memoryAvailable.hashCode());
        Object $gpuUsageRate = this.getGpuUsageRate();
        result = result * 59 + ($gpuUsageRate == null ? 43 : $gpuUsageRate.hashCode());
        Object $storageSpace = this.getStorageSpace();
        result = result * 59 + ($storageSpace == null ? 43 : $storageSpace.hashCode());
        Object $createdDtm = this.getCreatedDtm();
        result = result * 59 + ($createdDtm == null ? 43 : $createdDtm.hashCode());
        return result;
    }

    public String toString() {
        return "MonitoringResourceEntity(resourceId=" + this.getResourceId() + ", cpuUsageRate=" + this.getCpuUsageRate() + ", memoryAvailable=" + this.getMemoryAvailable() + ", gpuUsageRate=" + this.getGpuUsageRate() + ", storageSpace=" + this.getStorageSpace() + ", createdDtm=" + this.getCreatedDtm() + ")";
    }
}

