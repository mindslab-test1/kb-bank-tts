package ai.mindslab.tts_package.domain.preprocess;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.StringPath;
import java.time.LocalDateTime;

public class QPreprocessProcessEntity extends EntityPathBase<PreprocessProcessEntity> {
    private static final long serialVersionUID = -2034083170L;
    private static final PathInits INITS;
    public static final QPreprocessProcessEntity preprocessProcessEntity;
    public final DateTimePath<LocalDateTime> createdDtm;
    public final StringPath creatorId;
    public final NumberPath<Long> dataTableId;
    public final NumberPath<Long> processId;
    public final NumberPath<Integer> processOrder;
    public final QPreprocessProfileEntity profile;
    public final NumberPath<Long> ruleId;
    public final DateTimePath<LocalDateTime> updatedDtm;
    public final StringPath updaterId;
    public final StringPath useYn;

    public QPreprocessProcessEntity(String variable) {
        this(PreprocessProcessEntity.class, PathMetadataFactory.forVariable(variable), INITS);
    }

    public QPreprocessProcessEntity(Path<? extends PreprocessProcessEntity> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPreprocessProcessEntity(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPreprocessProcessEntity(PathMetadata metadata, PathInits inits) {
        this(PreprocessProcessEntity.class, metadata, inits);
    }

    public QPreprocessProcessEntity(Class<? extends PreprocessProcessEntity> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.createdDtm = this.createDateTime("createdDtm", LocalDateTime.class);
        this.creatorId = this.createString("creatorId");
        this.dataTableId = this.createNumber("dataTableId", Long.class);
        this.processId = this.createNumber("processId", Long.class);
        this.processOrder = this.createNumber("processOrder", Integer.class);
        this.ruleId = this.createNumber("ruleId", Long.class);
        this.updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
        this.updaterId = this.createString("updaterId");
        this.useYn = this.createString("useYn");
        this.profile = inits.isInitialized("profile") ? new QPreprocessProfileEntity(this.forProperty("profile")) : null;
    }

    static {
        INITS = PathInits.DIRECT2;
        preprocessProcessEntity = new QPreprocessProcessEntity("preprocessProcessEntity");
    }
}

