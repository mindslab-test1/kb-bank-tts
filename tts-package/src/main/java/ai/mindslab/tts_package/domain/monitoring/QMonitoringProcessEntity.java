package ai.mindslab.tts_package.domain.monitoring;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.CollectionPath;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.StringPath;
import java.time.LocalDateTime;

public class QMonitoringProcessEntity extends EntityPathBase<MonitoringProcessEntity> {
    private static final long serialVersionUID = -2110067682L;
    public static final QMonitoringProcessEntity monitoringProcessEntity = new QMonitoringProcessEntity("monitoringProcessEntity");
    public final StringPath autoRecovery = this.createString("autoRecovery");
    public final DateTimePath<LocalDateTime> createdDtm = this.createDateTime("createdDtm", LocalDateTime.class);
    public final StringPath creatorId = this.createString("creatorId");
    public final StringPath endScriptPath = this.createString("endScriptPath");
    public final CollectionPath<ProcessHistoryEntity, QProcessHistoryEntity> processHistory;
    public final StringPath processName;
    public final NumberPath<Long> processNo;
    public final StringPath startScriptPath;
    public final StringPath stateScriptPath;
    public final DateTimePath<LocalDateTime> updatedDtm;
    public final StringPath updaterId;
    public final StringPath useYn;

    public QMonitoringProcessEntity(String variable) {
        super(MonitoringProcessEntity.class, PathMetadataFactory.forVariable(variable));
        this.processHistory = this.createCollection("processHistory", ProcessHistoryEntity.class, QProcessHistoryEntity.class, PathInits.DIRECT2);
        this.processName = this.createString("processName");
        this.processNo = this.createNumber("processNo", Long.class);
        this.startScriptPath = this.createString("startScriptPath");
        this.stateScriptPath = this.createString("stateScriptPath");
        this.updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
        this.updaterId = this.createString("updaterId");
        this.useYn = this.createString("useYn");
    }

    public QMonitoringProcessEntity(Path<? extends MonitoringProcessEntity> path) {
        super(path.getType(), path.getMetadata());
        this.processHistory = this.createCollection("processHistory", ProcessHistoryEntity.class, QProcessHistoryEntity.class, PathInits.DIRECT2);
        this.processName = this.createString("processName");
        this.processNo = this.createNumber("processNo", Long.class);
        this.startScriptPath = this.createString("startScriptPath");
        this.stateScriptPath = this.createString("stateScriptPath");
        this.updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
        this.updaterId = this.createString("updaterId");
        this.useYn = this.createString("useYn");
    }

    public QMonitoringProcessEntity(PathMetadata metadata) {
        super(MonitoringProcessEntity.class, metadata);
        this.processHistory = this.createCollection("processHistory", ProcessHistoryEntity.class, QProcessHistoryEntity.class, PathInits.DIRECT2);
        this.processName = this.createString("processName");
        this.processNo = this.createNumber("processNo", Long.class);
        this.startScriptPath = this.createString("startScriptPath");
        this.stateScriptPath = this.createString("stateScriptPath");
        this.updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
        this.updaterId = this.createString("updaterId");
        this.useYn = this.createString("useYn");
    }
}

