package ai.mindslab.tts_package.domain.preprocess;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(
        name = "pp_rule_tb"
)
public class PreprocessRuleEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long ruleId;
    @Column(
            nullable = false
    )
    private String ruleName;
    private String ruleType;
    private String ruleExpression;
    @Column(
            updatable = false
    )
    @CreationTimestamp
    private LocalDateTime createdDtm;
    @UpdateTimestamp
    private LocalDateTime updatedDtm;
    @Column(
            updatable = false
    )
    private String creatorId;
    private String updaterId;

    public Long getRuleId() {
        return this.ruleId;
    }

    public String getRuleName() {
        return this.ruleName;
    }

    public String getRuleType() {
        return this.ruleType;
    }

    public String getRuleExpression() {
        return this.ruleExpression;
    }

    public LocalDateTime getCreatedDtm() {
        return this.createdDtm;
    }

    public LocalDateTime getUpdatedDtm() {
        return this.updatedDtm;
    }

    public String getCreatorId() {
        return this.creatorId;
    }

    public String getUpdaterId() {
        return this.updaterId;
    }

    public void setRuleId(final Long ruleId) {
        this.ruleId = ruleId;
    }

    public void setRuleName(final String ruleName) {
        this.ruleName = ruleName;
    }

    public void setRuleType(final String ruleType) {
        this.ruleType = ruleType;
    }

    public void setRuleExpression(final String ruleExpression) {
        this.ruleExpression = ruleExpression;
    }

    public void setCreatedDtm(final LocalDateTime createdDtm) {
        this.createdDtm = createdDtm;
    }

    public void setUpdatedDtm(final LocalDateTime updatedDtm) {
        this.updatedDtm = updatedDtm;
    }

    public void setCreatorId(final String creatorId) {
        this.creatorId = creatorId;
    }

    public void setUpdaterId(final String updaterId) {
        this.updaterId = updaterId;
    }

    public PreprocessRuleEntity() {
    }
}
