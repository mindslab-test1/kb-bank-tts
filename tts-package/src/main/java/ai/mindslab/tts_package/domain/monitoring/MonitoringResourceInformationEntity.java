package ai.mindslab.tts_package.domain.monitoring;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(
        name = "mt_resource_information_tb"
)
public class MonitoringResourceInformationEntity {
    @Id
    private Long pk = 1L;
    private Long memoryMax;
    private Long storageMax;

    public MonitoringResourceInformationEntity() {
    }

    public Long getPk() {
        return this.pk;
    }

    public Long getMemoryMax() {
        return this.memoryMax;
    }

    public Long getStorageMax() {
        return this.storageMax;
    }

    public void setPk(final Long pk) {
        this.pk = pk;
    }

    public void setMemoryMax(final Long memoryMax) {
        this.memoryMax = memoryMax;
    }

    public void setStorageMax(final Long storageMax) {
        this.storageMax = storageMax;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof MonitoringResourceInformationEntity)) {
            return false;
        } else {
            MonitoringResourceInformationEntity other = (MonitoringResourceInformationEntity)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$pk = this.getPk();
                    Object other$pk = other.getPk();
                    if (this$pk == null) {
                        if (other$pk == null) {
                            break label47;
                        }
                    } else if (this$pk.equals(other$pk)) {
                        break label47;
                    }

                    return false;
                }

                Object this$memoryMax = this.getMemoryMax();
                Object other$memoryMax = other.getMemoryMax();
                if (this$memoryMax == null) {
                    if (other$memoryMax != null) {
                        return false;
                    }
                } else if (!this$memoryMax.equals(other$memoryMax)) {
                    return false;
                }

                Object this$storageMax = this.getStorageMax();
                Object other$storageMax = other.getStorageMax();
                if (this$storageMax == null) {
                    if (other$storageMax != null) {
                        return false;
                    }
                } else if (!this$storageMax.equals(other$storageMax)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof MonitoringResourceInformationEntity;
    }

    public int hashCode() {
//        int PRIME = true;
        int result = 1;
        Object $pk = this.getPk();
//        int result = result * 59 + ($pk == null ? 43 : $pk.hashCode());
        result = result * 59 + ($pk == null ? 43 : $pk.hashCode());
        Object $memoryMax = this.getMemoryMax();
        result = result * 59 + ($memoryMax == null ? 43 : $memoryMax.hashCode());
        Object $storageMax = this.getStorageMax();
        result = result * 59 + ($storageMax == null ? 43 : $storageMax.hashCode());
        return result;
    }

    public String toString() {
        return "MonitoringResourceInformationEntity(pk=" + this.getPk() + ", memoryMax=" + this.getMemoryMax() + ", storageMax=" + this.getStorageMax() + ")";
    }
}
