package ai.mindslab.tts_package.domain.common;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.StringPath;
import java.time.LocalDateTime;

public class QCommonUserEntity extends EntityPathBase<CommonUserEntity> {
    private static final long serialVersionUID = -162769470L;
    private static final PathInits INITS;
    public static final QCommonUserEntity commonUserEntity;
    public final StringPath activatedYn;
    public final DateTimePath<LocalDateTime> createdDtm;
    public final StringPath creatorId;
    public final NumberPath<Integer> loginFailCnt;
    public final StringPath password;
    public final DateTimePath<LocalDateTime> pwChgDtm;
    public final QCommonRoleEntity role;
    public final NumberPath<Integer> roleId;
    public final DateTimePath<LocalDateTime> updatedDtm;
    public final StringPath updaterId;
    public final StringPath userId;
    public final StringPath userName;

    public QCommonUserEntity(String variable) {
        this(CommonUserEntity.class, PathMetadataFactory.forVariable(variable), INITS);
    }

    public QCommonUserEntity(Path<? extends CommonUserEntity> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCommonUserEntity(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCommonUserEntity(PathMetadata metadata, PathInits inits) {
        this(CommonUserEntity.class, metadata, inits);
    }

    public QCommonUserEntity(Class<? extends CommonUserEntity> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.activatedYn = this.createString("activatedYn");
        this.createdDtm = this.createDateTime("createdDtm", LocalDateTime.class);
        this.creatorId = this.createString("creatorId");
        this.loginFailCnt = this.createNumber("loginFailCnt", Integer.class);
        this.password = this.createString("password");
        this.pwChgDtm = this.createDateTime("pwChgDtm", LocalDateTime.class);
        this.roleId = this.createNumber("roleId", Integer.class);
        this.updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
        this.updaterId = this.createString("updaterId");
        this.userId = this.createString("userId");
        this.userName = this.createString("userName");
        this.role = inits.isInitialized("role") ? new QCommonRoleEntity(this.forProperty("role")) : null;
    }

    static {
        INITS = PathInits.DIRECT2;
        commonUserEntity = new QCommonUserEntity("commonUserEntity");
    }
}

