package ai.mindslab.tts_package.domain.preprocess;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.StringPath;
import java.time.LocalDateTime;

public class QPreprocessDataEntity extends EntityPathBase<PreprocessDataEntity> {
    private static final long serialVersionUID = -19647423L;
    private static final PathInits INITS;
    public static final QPreprocessDataEntity preprocessDataEntity;
    public final DateTimePath<LocalDateTime> createdDtm;
    public final StringPath creatorId;
    public final StringPath dataAfter;
    public final StringPath dataBefore;
    public final NumberPath<Long> dataId;
    public final StringPath dataNote;
    public final QPreprocessDataTableEntity table;
    public final DateTimePath<LocalDateTime> updatedDtm;
    public final StringPath updaterId;

    public QPreprocessDataEntity(String variable) {
        this(PreprocessDataEntity.class, PathMetadataFactory.forVariable(variable), INITS);
    }

    public QPreprocessDataEntity(Path<? extends PreprocessDataEntity> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPreprocessDataEntity(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPreprocessDataEntity(PathMetadata metadata, PathInits inits) {
        this(PreprocessDataEntity.class, metadata, inits);
    }

    public QPreprocessDataEntity(Class<? extends PreprocessDataEntity> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.createdDtm = this.createDateTime("createdDtm", LocalDateTime.class);
        this.creatorId = this.createString("creatorId");
        this.dataAfter = this.createString("dataAfter");
        this.dataBefore = this.createString("dataBefore");
        this.dataId = this.createNumber("dataId", Long.class);
        this.dataNote = this.createString("dataNote");
        this.updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
        this.updaterId = this.createString("updaterId");
        this.table = inits.isInitialized("table") ? new QPreprocessDataTableEntity(this.forProperty("table")) : null;
    }

    static {
        INITS = PathInits.DIRECT2;
        preprocessDataEntity = new QPreprocessDataEntity("preprocessDataEntity");
    }
}
