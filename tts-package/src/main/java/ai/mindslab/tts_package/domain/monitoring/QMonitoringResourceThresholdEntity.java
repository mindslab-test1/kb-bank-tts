package ai.mindslab.tts_package.domain.monitoring;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import java.time.LocalDateTime;

public class QMonitoringResourceThresholdEntity extends EntityPathBase<MonitoringResourceThresholdEntity> {
    private static final long serialVersionUID = 271623212L;
    public static final QMonitoringResourceThresholdEntity monitoringResourceThresholdEntity = new QMonitoringResourceThresholdEntity("monitoringResourceThresholdEntity");
    public final DateTimePath<LocalDateTime> createdDtm = this.createDateTime("createdDtm", LocalDateTime.class);
    public final StringPath creatorId = this.createString("creatorId");
    public final StringPath sortation = this.createString("sortation");
    public final NumberPath<Integer> thresholdCaution = this.createNumber("thresholdCaution", Integer.class);
    public final NumberPath<Integer> thresholdDanger = this.createNumber("thresholdDanger", Integer.class);
    public final NumberPath<Integer> thresholdWarning = this.createNumber("thresholdWarning", Integer.class);
    public final DateTimePath<LocalDateTime> updatedDtm = this.createDateTime("updatedDtm", LocalDateTime.class);
    public final StringPath updaterId = this.createString("updaterId");

    public QMonitoringResourceThresholdEntity(String variable) {
        super(MonitoringResourceThresholdEntity.class, PathMetadataFactory.forVariable(variable));
    }

    public QMonitoringResourceThresholdEntity(Path<? extends MonitoringResourceThresholdEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMonitoringResourceThresholdEntity(PathMetadata metadata) {
        super(MonitoringResourceThresholdEntity.class, metadata);
    }
}