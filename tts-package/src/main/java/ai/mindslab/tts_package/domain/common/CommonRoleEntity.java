package ai.mindslab.tts_package.domain.common;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(
        name = "cm_role_tb"
)
public class CommonRoleEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer roleId;
    private String roleName;
    private String descript;
    @Column(
            updatable = false
    )
    @CreationTimestamp
    private LocalDateTime createdDtm;
    @UpdateTimestamp
    private LocalDateTime updatedDtm;
    @Column(
            updatable = false
    )
    private String creatorId;
    private String updaterId;

    public CommonRoleEntity() {
    }

    public Integer getRoleId() {
        return this.roleId;
    }

    public String getRoleName() {
        return this.roleName;
    }

    public String getDescript() {
        return this.descript;
    }

    public LocalDateTime getCreatedDtm() {
        return this.createdDtm;
    }

    public LocalDateTime getUpdatedDtm() {
        return this.updatedDtm;
    }

    public String getCreatorId() {
        return this.creatorId;
    }

    public String getUpdaterId() {
        return this.updaterId;
    }

    public void setRoleId(final Integer roleId) {
        this.roleId = roleId;
    }

    public void setRoleName(final String roleName) {
        this.roleName = roleName;
    }

    public void setDescript(final String descript) {
        this.descript = descript;
    }

    public void setCreatedDtm(final LocalDateTime createdDtm) {
        this.createdDtm = createdDtm;
    }

    public void setUpdatedDtm(final LocalDateTime updatedDtm) {
        this.updatedDtm = updatedDtm;
    }

    public void setCreatorId(final String creatorId) {
        this.creatorId = creatorId;
    }

    public void setUpdaterId(final String updaterId) {
        this.updaterId = updaterId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof CommonRoleEntity)) {
            return false;
        } else {
            CommonRoleEntity other = (CommonRoleEntity)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label95: {
                    Object this$roleId = this.getRoleId();
                    Object other$roleId = other.getRoleId();
                    if (this$roleId == null) {
                        if (other$roleId == null) {
                            break label95;
                        }
                    } else if (this$roleId.equals(other$roleId)) {
                        break label95;
                    }

                    return false;
                }

                Object this$roleName = this.getRoleName();
                Object other$roleName = other.getRoleName();
                if (this$roleName == null) {
                    if (other$roleName != null) {
                        return false;
                    }
                } else if (!this$roleName.equals(other$roleName)) {
                    return false;
                }

                Object this$descript = this.getDescript();
                Object other$descript = other.getDescript();
                if (this$descript == null) {
                    if (other$descript != null) {
                        return false;
                    }
                } else if (!this$descript.equals(other$descript)) {
                    return false;
                }

                label74: {
                    Object this$createdDtm = this.getCreatedDtm();
                    Object other$createdDtm = other.getCreatedDtm();
                    if (this$createdDtm == null) {
                        if (other$createdDtm == null) {
                            break label74;
                        }
                    } else if (this$createdDtm.equals(other$createdDtm)) {
                        break label74;
                    }

                    return false;
                }

                label67: {
                    Object this$updatedDtm = this.getUpdatedDtm();
                    Object other$updatedDtm = other.getUpdatedDtm();
                    if (this$updatedDtm == null) {
                        if (other$updatedDtm == null) {
                            break label67;
                        }
                    } else if (this$updatedDtm.equals(other$updatedDtm)) {
                        break label67;
                    }

                    return false;
                }

                Object this$creatorId = this.getCreatorId();
                Object other$creatorId = other.getCreatorId();
                if (this$creatorId == null) {
                    if (other$creatorId != null) {
                        return false;
                    }
                } else if (!this$creatorId.equals(other$creatorId)) {
                    return false;
                }

                Object this$updaterId = this.getUpdaterId();
                Object other$updaterId = other.getUpdaterId();
                if (this$updaterId == null) {
                    if (other$updaterId != null) {
                        return false;
                    }
                } else if (!this$updaterId.equals(other$updaterId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof CommonRoleEntity;
    }

    public int hashCode() {
//        int PRIME = true;
        int result = 1;
        Object $roleId = this.getRoleId();
//        int result = result * 59 + ($roleId == null ? 43 : $roleId.hashCode());
        result = result * 59 + ($roleId == null ? 43 : $roleId.hashCode());
        Object $roleName = this.getRoleName();
        result = result * 59 + ($roleName == null ? 43 : $roleName.hashCode());
        Object $descript = this.getDescript();
        result = result * 59 + ($descript == null ? 43 : $descript.hashCode());
        Object $createdDtm = this.getCreatedDtm();
        result = result * 59 + ($createdDtm == null ? 43 : $createdDtm.hashCode());
        Object $updatedDtm = this.getUpdatedDtm();
        result = result * 59 + ($updatedDtm == null ? 43 : $updatedDtm.hashCode());
        Object $creatorId = this.getCreatorId();
        result = result * 59 + ($creatorId == null ? 43 : $creatorId.hashCode());
        Object $updaterId = this.getUpdaterId();
        result = result * 59 + ($updaterId == null ? 43 : $updaterId.hashCode());
        return result;
    }

    public String toString() {
        return "CommonRoleEntity(roleId=" + this.getRoleId() + ", roleName=" + this.getRoleName() + ", descript=" + this.getDescript() + ", createdDtm=" + this.getCreatedDtm() + ", updatedDtm=" + this.getUpdatedDtm() + ", creatorId=" + this.getCreatorId() + ", updaterId=" + this.getUpdaterId() + ")";
    }
}

