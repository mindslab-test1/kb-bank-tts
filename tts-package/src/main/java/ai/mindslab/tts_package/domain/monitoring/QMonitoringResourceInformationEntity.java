package ai.mindslab.tts_package.domain.monitoring;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;

public class QMonitoringResourceInformationEntity extends EntityPathBase<MonitoringResourceInformationEntity> {
    private static final long serialVersionUID = -1959733075L;
    public static final QMonitoringResourceInformationEntity monitoringResourceInformationEntity = new QMonitoringResourceInformationEntity("monitoringResourceInformationEntity");
    public final NumberPath<Long> memoryMax = this.createNumber("memoryMax", Long.class);
    public final NumberPath<Long> pk = this.createNumber("pk", Long.class);
    public final NumberPath<Long> storageMax = this.createNumber("storageMax", Long.class);

    public QMonitoringResourceInformationEntity(String variable) {
        super(MonitoringResourceInformationEntity.class, PathMetadataFactory.forVariable(variable));
    }

    public QMonitoringResourceInformationEntity(Path<? extends MonitoringResourceInformationEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMonitoringResourceInformationEntity(PathMetadata metadata) {
        super(MonitoringResourceInformationEntity.class, metadata);
    }
}