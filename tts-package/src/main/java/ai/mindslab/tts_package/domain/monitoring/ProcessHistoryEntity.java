package ai.mindslab.tts_package.domain.monitoring;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(
        name = "pc_history_tb"
)
public class ProcessHistoryEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long historyId;
    @Column(
            name = "process_no"
    )
    private Long processNo;
    private Long processId;
    @Enumerated(EnumType.STRING)
    private ProcessHistoryEntity.ProcessStatus processStatus;
    @CreationTimestamp
    private LocalDateTime createdDtm;

    public ProcessHistoryEntity() {
    }

    public Long getHistoryId() {
        return this.historyId;
    }

    public Long getProcessNo() {
        return this.processNo;
    }

    public Long getProcessId() {
        return this.processId;
    }

    public ProcessHistoryEntity.ProcessStatus getProcessStatus() {
        return this.processStatus;
    }

    public LocalDateTime getCreatedDtm() {
        return this.createdDtm;
    }

    public void setHistoryId(final Long historyId) {
        this.historyId = historyId;
    }

    public void setProcessNo(final Long processNo) {
        this.processNo = processNo;
    }

    public void setProcessId(final Long processId) {
        this.processId = processId;
    }

    public void setProcessStatus(final ProcessHistoryEntity.ProcessStatus processStatus) {
        this.processStatus = processStatus;
    }

    public void setCreatedDtm(final LocalDateTime createdDtm) {
        this.createdDtm = createdDtm;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ProcessHistoryEntity)) {
            return false;
        } else {
            ProcessHistoryEntity other = (ProcessHistoryEntity)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label71: {
                    Object this$historyId = this.getHistoryId();
                    Object other$historyId = other.getHistoryId();
                    if (this$historyId == null) {
                        if (other$historyId == null) {
                            break label71;
                        }
                    } else if (this$historyId.equals(other$historyId)) {
                        break label71;
                    }

                    return false;
                }

                Object this$processNo = this.getProcessNo();
                Object other$processNo = other.getProcessNo();
                if (this$processNo == null) {
                    if (other$processNo != null) {
                        return false;
                    }
                } else if (!this$processNo.equals(other$processNo)) {
                    return false;
                }

                label57: {
                    Object this$processId = this.getProcessId();
                    Object other$processId = other.getProcessId();
                    if (this$processId == null) {
                        if (other$processId == null) {
                            break label57;
                        }
                    } else if (this$processId.equals(other$processId)) {
                        break label57;
                    }

                    return false;
                }

                Object this$processStatus = this.getProcessStatus();
                Object other$processStatus = other.getProcessStatus();
                if (this$processStatus == null) {
                    if (other$processStatus != null) {
                        return false;
                    }
                } else if (!this$processStatus.equals(other$processStatus)) {
                    return false;
                }

                Object this$createdDtm = this.getCreatedDtm();
                Object other$createdDtm = other.getCreatedDtm();
                if (this$createdDtm == null) {
                    if (other$createdDtm == null) {
                        return true;
                    }
                } else if (this$createdDtm.equals(other$createdDtm)) {
                    return true;
                }

                return false;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ProcessHistoryEntity;
    }

    public int hashCode() {
//        int PRIME = true;
        int result = 1;
        Object $historyId = this.getHistoryId();
//        int result = result * 59 + ($historyId == null ? 43 : $historyId.hashCode());
        result = result * 59 + ($historyId == null ? 43 : $historyId.hashCode());
        Object $processNo = this.getProcessNo();
        result = result * 59 + ($processNo == null ? 43 : $processNo.hashCode());
        Object $processId = this.getProcessId();
        result = result * 59 + ($processId == null ? 43 : $processId.hashCode());
        Object $processStatus = this.getProcessStatus();
        result = result * 59 + ($processStatus == null ? 43 : $processStatus.hashCode());
        Object $createdDtm = this.getCreatedDtm();
        result = result * 59 + ($createdDtm == null ? 43 : $createdDtm.hashCode());
        return result;
    }

    public String toString() {
        return "ProcessHistoryEntity(historyId=" + this.getHistoryId() + ", processNo=" + this.getProcessNo() + ", processId=" + this.getProcessId() + ", processStatus=" + this.getProcessStatus() + ", createdDtm=" + this.getCreatedDtm() + ")";
    }

    public static enum ProcessStatus {
        Running,
        Stopped,
        Inactive;

        private ProcessStatus() {
        }
    }
}
