package ai.mindslab.tts_package.common.exceptions;

public class TtsPackageException extends Exception {
    private int errCode;
    private String errMsg;

    public TtsPackageException(int code, String msg, String message) {
        super(message);
        this.errCode = code;
        this.errMsg = msg;
    }

    public TtsPackageException(String message) {
        super(message);
    }

    public TtsPackageException(String message, Throwable cause) {
        super(message, cause);
    }

    public TtsPackageException(Throwable cause) {
        super(cause);
    }

    protected TtsPackageException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public TtsPackageException() {
    }

    public TtsPackageException(final int errCode, final String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    public int getErrCode() {
        return this.errCode;
    }

    public String getErrMsg() {
        return this.errMsg;
    }
}
