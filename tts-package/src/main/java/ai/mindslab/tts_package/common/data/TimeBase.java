package ai.mindslab.tts_package.common.data;

public class TimeBase<T> {
    private long timeStamp;
    private T data;

    public TimeBase() {
        this.timeStamp = System.currentTimeMillis();
    }

    public TimeBase(T data) {
        this.data = data;
        this.timeStamp = System.currentTimeMillis();
    }

    public long getTimeStamp() {
        return this.timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isExpired(long currTimestamp, long timeout) {
        return currTimestamp - this.timeStamp > timeout;
    }

    public boolean isExpired(long timeout) {
        return System.currentTimeMillis() - this.timeStamp > timeout;
    }

    public String toString() {
        return "TimeBase [timeStamp=" + this.timeStamp + ", data=" + this.data + "]";
    }
}
