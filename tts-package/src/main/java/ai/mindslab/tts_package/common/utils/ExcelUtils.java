package ai.mindslab.tts_package.common.utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

public class ExcelUtils {
    private static Logger logger = LoggerFactory.getLogger(ExcelUtils.class);

    public ExcelUtils() {
    }

    public static Workbook getExcelWorkBook(MultipartFile file) {
        Workbook workbook = null;
        String fileName = file.getOriginalFilename();

        try {
            if (fileName.endsWith("xls")) {
                workbook = new HSSFWorkbook(file.getInputStream());
            } else if (fileName.endsWith("xlsx")) {
                workbook = new XSSFWorkbook(file.getInputStream());
            }
        } catch (Exception var4) {
            logger.error(var4.getMessage());
        }

        return (Workbook)workbook;
    }

    public static String getCellValue(Cell cell) {
        String value;
        if (cell == null) {
            value = "";
        } else {
            switch(cell.getCellTypeEnum()) {
                case NUMERIC:
                    value = (int)cell.getNumericCellValue() + "";
                    break;
                case FORMULA:
                    value = cell.getCellFormula();
                    break;
                case STRING:
                    value = cell.getStringCellValue();
                    break;
                default:
                    value = "";
            }
        }

        return value;
    }
}
