package ai.mindslab.tts_package.common.component;


import java.awt.Color;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder.BorderSide;
import org.springframework.stereotype.Component;

@Component
public class CommonExcelComponent {
    private SXSSFWorkbook workbook;
    private Sheet sheet;
    private int rowCount;
    private Cell cell;
    private CellStyle dataStyle;
    private CellStyle columeStyle;
    private CellStyle titleStyle;
    private CellStyle subTitleStyle;

    public CommonExcelComponent() {
    }

    public void init() {
        this.workbook = new SXSSFWorkbook(100);
        this.sheet = this.workbook.createSheet("sheet");
        this.dataStyle = this.getDataStyle();
        this.columeStyle = this.getColumeStyle();
        this.titleStyle = this.getTitleStyle();
        this.subTitleStyle = this.getSubTitleStyle();
        this.rowCount = 0;
    }

    public Row createRow(int rowHeight) {
        ++this.rowCount;
        Row row = this.sheet.createRow(this.rowCount);
        row.setHeightInPoints((float)rowHeight);
        return row;
    }

    public void createTitle(String value, int startCol, int endCol) {
        Row row = this.sheet.createRow(this.rowCount);
        row.setHeightInPoints(25.0F);
        this.createMergedRow(this.rowCount, this.rowCount, startCol, endCol);
        this.cell = row.createCell(1);
        this.cell.setCellValue(value);
        this.cell.setCellStyle(this.titleStyle);
    }

    private void createMergedRow(int startRow, int endRow, int startCol, int endCol) {
        this.sheet.addMergedRegion(new CellRangeAddress(startRow, endRow, startCol, endCol));
    }

    public void createColumnRow(Integer rowCount, String[] columnMap) {
        if (rowCount != null) {
            this.rowCount = rowCount;
        } else {
            ++this.rowCount;
        }

        Row row = this.sheet.createRow(rowCount);
        row.setHeightInPoints(13.5F);

        for(int i = 0; i < columnMap.length; ++i) {
            this.cell = row.createCell(i + 1);
            this.cell.setCellStyle(this.columeStyle);
            this.cell.setCellValue(columnMap[i]);
        }

    }

    public void createDataRow(String[] data) {
        ++this.rowCount;
        Row row = this.sheet.createRow(this.rowCount);

        for(int j = 0; j < data.length; ++j) {
            this.cell = row.createCell(j + 1);
            this.cell.setCellStyle(this.dataStyle);
            this.cell.setCellValue(data[j]);
        }

    }

    public void createCell(Row row, int pos, String value, XSSFCellStyle style) {
        this.cell = row.createCell(pos);
        this.cell.setCellValue(value);
        this.cell.setCellStyle(style);
    }

    public void setSheetPrintSetting() {
        this.sheet.getPrintSetup().setPaperSize((short)9);
        this.sheet.getPrintSetup().setLandscape(false);
        this.sheet.getPrintSetup().setScale((short)100);
        this.sheet.getPrintSetup().setHeaderMargin(0.1D);
        this.sheet.getPrintSetup().setFooterMargin(0.1D);
        this.sheet.setMargin((short)0, 0.2D);
        this.sheet.setMargin((short)1, 0.2D);
        this.sheet.setMargin((short)2, 0.2D);
        this.sheet.setMargin((short)3, 0.2D);
        this.sheet.setPrintGridlines(false);
        this.sheet.setDisplayGridlines(true);
        this.sheet.setAutobreaks(true);
        this.sheet.setHorizontallyCenter(true);
    }

    public void setColumnWidth(int[] columnWiths) {
        for(int i = 0; i < columnWiths.length; ++i) {
            this.sheet.setColumnWidth(i, columnWiths[i]);
        }

    }

    public XSSFCellStyle getColumeStyle() {
        Font columeFont = this.workbook.createFont();
        columeFont.setFontName("굴림");
        columeFont.setFontHeight((short)220);
        columeFont.setBold(true);
        XSSFCellStyle columeCs = (XSSFCellStyle)this.workbook.createCellStyle();
        columeCs.setFont(columeFont);
        columeCs.setAlignment(HorizontalAlignment.CENTER);
        columeCs.setVerticalAlignment(VerticalAlignment.CENTER);
        columeCs.setFillForegroundColor(new XSSFColor(new Color(255, 255, 0)));
        columeCs.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        columeCs.setBorderLeft(BorderStyle.THIN);
        columeCs.setBorderRight(BorderStyle.THIN);
        columeCs.setBorderTop(BorderStyle.THIN);
        columeCs.setBorderBottom(BorderStyle.THIN);
        columeCs.setBorderColor(BorderSide.LEFT, new XSSFColor(Color.BLACK));
        columeCs.setBorderColor(BorderSide.RIGHT, new XSSFColor(Color.BLACK));
        columeCs.setBorderColor(BorderSide.TOP, new XSSFColor(Color.BLACK));
        columeCs.setBorderColor(BorderSide.BOTTOM, new XSSFColor(Color.BLACK));
        return columeCs;
    }

    public XSSFCellStyle getSubTitleStyle() {
        Font subMaiFont = this.workbook.createFont();
        subMaiFont.setFontName("굴림");
        subMaiFont.setFontHeight((short)260);
        subMaiFont.setBold(true);
        XSSFCellStyle subMainCs = (XSSFCellStyle)this.workbook.createCellStyle();
        subMainCs.setFont(subMaiFont);
        subMainCs.setAlignment(HorizontalAlignment.LEFT);
        subMainCs.setVerticalAlignment(VerticalAlignment.CENTER);
        subMainCs.setFillForegroundColor(new XSSFColor(Color.WHITE));
        subMainCs.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        return subMainCs;
    }

    public XSSFCellStyle getTitleStyle() {
        Font mainFont = this.workbook.createFont();
        mainFont.setFontName("굴림");
        mainFont.setFontHeight((short)360);
        mainFont.setBold(true);
        XSSFCellStyle mainCs = (XSSFCellStyle)this.workbook.createCellStyle();
        mainCs.setFont(mainFont);
        mainCs.setAlignment(HorizontalAlignment.CENTER);
        mainCs.setVerticalAlignment(VerticalAlignment.CENTER);
        mainCs.setFillForegroundColor(new XSSFColor(Color.WHITE));
        mainCs.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        return mainCs;
    }

    public XSSFCellStyle getDataStyle() {
        Font dataFont = this.workbook.createFont();
        dataFont.setFontName("굴림");
        dataFont.setFontHeight((short)200);
        XSSFCellStyle dataCs = (XSSFCellStyle)this.workbook.createCellStyle();
        dataCs.setFont(dataFont);
        dataCs.setAlignment(HorizontalAlignment.CENTER);
        dataCs.setVerticalAlignment(VerticalAlignment.CENTER);
        dataCs.setFillForegroundColor(new XSSFColor(Color.WHITE));
        dataCs.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        dataCs.setWrapText(true);
        dataCs.setBorderLeft(BorderStyle.THIN);
        dataCs.setBorderRight(BorderStyle.THIN);
        dataCs.setBorderTop(BorderStyle.THIN);
        dataCs.setBorderBottom(BorderStyle.THIN);
        dataCs.setBorderColor(BorderSide.LEFT, new XSSFColor(Color.BLACK));
        dataCs.setBorderColor(BorderSide.RIGHT, new XSSFColor(Color.BLACK));
        dataCs.setBorderColor(BorderSide.TOP, new XSSFColor(Color.BLACK));
        dataCs.setBorderColor(BorderSide.BOTTOM, new XSSFColor(Color.BLACK));
        return dataCs;
    }

    public Workbook getWorkBook() {
        return this.workbook;
    }

    public int getRowCount() {
        return this.rowCount;
    }

    public void setRowCount(final int rowCount) {
        this.rowCount = rowCount;
    }
}
