package ai.mindslab.tts_package.service.user;

import ai.mindslab.tts_package.data.SecurityMember;
import ai.mindslab.tts_package.domain.common.CommonUserEntity;
import ai.mindslab.tts_package.repository.common.CommonUserRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class SecureUserService implements UserDetailsService {
    @Autowired
    CommonUserRepository dao;

    public SecureUserService() {
    }

    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        CommonUserEntity em = this.dao.findByUserId(email);
        return (UserDetails)Optional.ofNullable(em).filter((m) -> {
            return m != null;
        }).map((m) -> {
            return new SecurityMember(m);
        }).get();
    }
}
