package ai.mindslab.tts_package.service.monitoring;

import ai.mindslab.tts_package.common.codes.IRestCodes;
import ai.mindslab.tts_package.common.exceptions.TtsPackageException;
import ai.mindslab.tts_package.domain.monitoring.MonitoringProcessEntity;
import ai.mindslab.tts_package.domain.monitoring.MonitoringResourceEntity;
import ai.mindslab.tts_package.domain.monitoring.MonitoringResourceInformationEntity;
import ai.mindslab.tts_package.domain.monitoring.MonitoringResourceThresholdEntity;
import ai.mindslab.tts_package.domain.monitoring.ProcessHistoryEntity;
import ai.mindslab.tts_package.domain.monitoring.ProcessHistoryEntity.ProcessStatus;
import ai.mindslab.tts_package.repository.monitoring.MonitoringProcessRepository;
import ai.mindslab.tts_package.repository.monitoring.MonitoringProcessRepositoryCustom;
import ai.mindslab.tts_package.repository.monitoring.MonitoringResourceInformationRepository;
import ai.mindslab.tts_package.repository.monitoring.MonitoringResourceRepository;
import ai.mindslab.tts_package.repository.monitoring.MonitoringResourceRepositoryCustom;
import ai.mindslab.tts_package.repository.monitoring.MonitoringResourceThresholdRepository;
import ai.mindslab.tts_package.repository.monitoring.ProcessEntity;
import ai.mindslab.tts_package.repository.monitoring.ProcessHistoryRepository;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Service
public class MonitoringService implements IRestCodes {
    private Logger logger = LoggerFactory.getLogger(MonitoringService.class);
    @Autowired
    MonitoringProcessRepository monitoringProcessRepository;
    @Autowired
    ProcessHistoryRepository processHistoryRepository;
    @Autowired
    MonitoringProcessRepositoryCustom monitoringProcessRepositoryCustom;
    @Autowired
    MonitoringResourceRepository monitoringResourceRepository;
    @Autowired
    MonitoringResourceRepositoryCustom monitoringResourceRepositoryCustom;
    @Autowired
    MonitoringResourceThresholdRepository monitoringResourceThresholdRepository;
    @Autowired
    MonitoringResourceInformationRepository monitoringResourceInformationRepository;
    @Value("${monitor.resource-status-script-path}/${monitor.resource-status-script-file}")
    private String resStatusPath;
    @Value("${spring.profiles.active}")
    private String profiles;

    public MonitoringService() {
    }

    @Transactional
    public void checkProcess() throws TtsPackageException {
        List<MonitoringProcessEntity> list = this.monitoringProcessRepository.findAll();
        Iterator var2 = list.iterator();

        while(var2.hasNext()) {
            MonitoringProcessEntity entity = (MonitoringProcessEntity)var2.next();
            String output = this.executeScript(entity.getStateScriptPath());
            this.logger.debug("checkProcess output : {}", output);

            try {
                ProcessHistoryEntity historyEntity = this.processHistoryRepository.findTopByProcessNoOrderByCreatedDtmDesc(entity.getProcessNo());
                String flag = output.split(":")[0].trim();
                Long processPk = entity.getProcessNo();
                if (flag.equals("pid")) {
                    Long pid = Long.valueOf(output.split(":")[1].trim());
                    if (ObjectUtils.isEmpty(historyEntity) || !historyEntity.getProcessId().equals(pid)) {
                        ProcessHistoryEntity newHistoryEntity = new ProcessHistoryEntity();
                        newHistoryEntity.setProcessId(pid);
                        newHistoryEntity.setProcessNo(processPk);
                        newHistoryEntity.setProcessStatus(ProcessStatus.Running);
                        this.processHistoryRepository.save(newHistoryEntity);
                    }
                } else {
                    if (ObjectUtils.isEmpty(historyEntity) || !historyEntity.getProcessStatus().equals(ProcessStatus.Stopped)) {
                        ProcessHistoryEntity newHistoryEntity = new ProcessHistoryEntity();
                        newHistoryEntity.setProcessId(0L);
                        newHistoryEntity.setProcessNo(processPk);
                        newHistoryEntity.setProcessStatus(ProcessStatus.Stopped);
                        this.processHistoryRepository.save(newHistoryEntity);
                    }

                    try {
                        if (entity.getAutoRecovery().equalsIgnoreCase("y")) {
                            this.logger.debug("auto_recovery process {}", processPk);
                            this.startProcess(processPk);
                        }
                    } catch (TtsPackageException var10) {
                        this.logger.error("startProcess ERROR, {}", processPk, var10);
                        throw new TtsPackageException(9999, "FAILURE");
                    }
                }
            } catch (TtsPackageException var11) {
                throw new TtsPackageException(var11.getErrCode(), var11.getErrMsg());
            } catch (Exception var12) {
                this.logger.error("checkProcess ERROR ", var12);
                throw new TtsPackageException(1004, "DB CONNECTION ERROR");
            }
        }

    }

    public Page<ProcessEntity> getCustomProcessList(Pageable pageable, String searchCol, String searchText) throws TtsPackageException {
        try {
            Page<ProcessEntity> processList = this.monitoringProcessRepositoryCustom.findAllLastHistory(searchCol, searchText, pageable);
            return processList;
        } catch (Exception var6) {
            this.logger.error("getCustomProcessList ERROR", var6);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    public MonitoringProcessEntity getProcess(Long processNo) throws TtsPackageException {
        MonitoringProcessEntity entity = null;

        try {
            Optional<MonitoringProcessEntity> optional = this.monitoringProcessRepository.findById(processNo);
            if (optional.isPresent()) {
                entity = (MonitoringProcessEntity)optional.get();
            }

            return entity;
        } catch (Exception var4) {
            this.logger.error("getProcess ERROR", var4);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public MonitoringProcessEntity saveProcess(MonitoringProcessEntity param, String userId) throws TtsPackageException {
        MonitoringProcessEntity entity = ObjectUtils.isEmpty(param.getProcessNo()) ? null : this.getProcess(param.getProcessNo());
        if (!ObjectUtils.isEmpty(this.monitoringProcessRepository.findByProcessNoNotAndProcessName(param.getProcessNo(), param.getProcessName()))) {
            throw new TtsPackageException(1005, "SAME NAME FOUND");
        } else if (!param.getProcessName().contains("<") && !param.getProcessName().contains(">") && !param.getProcessName().contains("\"") && !param.getProcessName().contains("'") && !param.getProcessName().contains("&")) {
            if (ObjectUtils.isEmpty(entity)) {
                entity = param;
                param.setCreatorId(userId);
            } else {
                entity.setUpdaterId(userId);
                entity.setProcessName(param.getProcessName());
                entity.setStartScriptPath(param.getStartScriptPath());
                entity.setEndScriptPath(param.getEndScriptPath());
                entity.setStateScriptPath(param.getStateScriptPath());
                entity.setAutoRecovery(param.getAutoRecovery());
                entity.setUseYn(param.getUseYn());
            }

            try {
                entity = (MonitoringProcessEntity)this.monitoringProcessRepository.save(entity);
                return entity;
            } catch (Exception var5) {
                this.logger.error("saveProcessERROR", var5);
                throw new TtsPackageException(1004, "DB CONNECTION ERROR");
            }
        } else {
            throw new TtsPackageException(1003, "CONTENT SAVE ERROR");
        }
    }

    @Transactional
    public int deleteProcess(Long[] ids) throws TtsPackageException {
        boolean var2 = false;

        try {
            int deleteConfirm = this.monitoringProcessRepository.deleteByProcessNoIn(Arrays.asList(ids));
            return deleteConfirm;
        } catch (Exception var4) {
            this.logger.error("deleteProcess ERROR", var4);
            throw new TtsPackageException(1003, "CONTENT SAVE ERROR");
        }
    }

    @Transactional
    public void updateRealTimeChart() {
        try {
            String output = this.executeScript(this.resStatusPath);
            this.logger.debug("updateRealTimeChart script result, {}", output);
            String[] resultArr = output.split("%");
            MonitoringResourceEntity entity = new MonitoringResourceEntity();
            String[] var4 = resultArr;
            int var5 = resultArr.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                String sysInfo = var4[var6];
                String[] sysInfoArr = sysInfo.trim().split(" ");
                String type = sysInfoArr[0];
                byte var11 = -1;
                switch(type.hashCode()) {
                    case 66952:
                        if (type.equals("CPU")) {
                            var11 = 0;
                        }
                        break;
                    case 70796:
                        if (type.equals("GPU")) {
                            var11 = 3;
                        }
                        break;
                    case 71368:
                        if (type.equals("HDD")) {
                            var11 = 2;
                        }
                        break;
                    case 80894:
                        if (type.equals("RAM")) {
                            var11 = 1;
                        }
                }

                switch(var11) {
                    case 0:
                        Float cpuUsageRate = Float.parseFloat(sysInfoArr[1]);
                        entity.setCpuUsageRate(cpuUsageRate);
                        break;
                    case 1:
                        Integer totalMemory = Integer.parseInt(sysInfoArr[1]);
                        Integer useMemory = Integer.parseInt(sysInfoArr[2]);
                        entity.setMemoryAvailable(totalMemory - useMemory);
                        break;
                    case 2:
                        Integer totalStorage = Integer.parseInt(sysInfoArr[1].substring(0, sysInfoArr[1].length() - 1));
                        Integer useStorage = Integer.parseInt(sysInfoArr[2].substring(0, sysInfoArr[2].length() - 1));
                        entity.setStorageSpace(totalStorage - useStorage);
                        break;
                    case 3:
                        Float gpuUsageRate = Float.parseFloat(sysInfoArr[3]);
                        entity.setGpuUsageRate(gpuUsageRate);
                }
            }

            try {
                this.monitoringResourceRepository.save(entity);
            } catch (Exception var18) {
                this.logger.error("updateRealTimeChart ERROR", var18);
                throw new TtsPackageException(1004, "DB CONNECTION ERROR");
            }
        } catch (TtsPackageException var19) {
            var19.printStackTrace();
        }

    }

    public List<MonitoringResourceEntity> getResourceList() throws TtsPackageException {
        try {
            List<MonitoringResourceEntity> list = this.monitoringResourceRepositoryCustom.getList(60);
            return list;
        } catch (Exception var3) {
            this.logger.error("getResourceList ERROR", var3);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    public MonitoringResourceEntity getResource() throws TtsPackageException {
        try {
            MonitoringResourceEntity entity = (MonitoringResourceEntity)this.monitoringResourceRepositoryCustom.getList(1).get(0);
            return entity;
        } catch (Exception var3) {
            this.logger.error("getResource ERROR", var3);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    public List<MonitoringResourceEntity> searchResource(String range, String period) throws TtsPackageException {
        String start = range.split("-")[0].replaceAll("/", "-");
        String end = range.split("-")[1].replaceAll("/", "-");
        this.logger.info("searchResource param, {}, {}, {}", new Object[]{start, end, period});
        start = start.substring(0, 16);
        end = end.substring(0, 16);

        try {
            List<MonitoringResourceEntity> list = this.monitoringResourceRepositoryCustom.getResourceListSearch(start, end, period);
            return list;
        } catch (Exception var7) {
            this.logger.error("getResourceList ERROR", var7);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    public MonitoringResourceInformationEntity getMax() throws TtsPackageException {
        MonitoringResourceInformationEntity entity = null;

        try {
            Optional<MonitoringResourceInformationEntity> optional = this.monitoringResourceInformationRepository.findById(1L);
            if (optional.isPresent()) {
                entity = (MonitoringResourceInformationEntity)optional.get();
            }

            return entity;
        } catch (Exception var3) {
            this.logger.error("getMax", var3);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    public List<MonitoringResourceThresholdEntity> getThreshold() throws TtsPackageException {
        try {
            List<MonitoringResourceThresholdEntity> list = this.monitoringResourceThresholdRepository.findAll();
            return list;
        } catch (Exception var3) {
            this.logger.error("getThreshold ERROR", var3);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public List<MonitoringResourceThresholdEntity> updateThreshold(String[] param, String userId) throws TtsPackageException {
        this.logger.debug("updateThreshold {} {}", param, userId);
        List<MonitoringResourceThresholdEntity> list = new ArrayList();
        String[] sortationArray = new String[]{"cpu", "memory", "gpu", "storage"};

        try {
            for(int i = 0; i < param.length / 3; ++i) {
                MonitoringResourceThresholdEntity entity = new MonitoringResourceThresholdEntity();
                entity.setSortation(sortationArray[i % 4]);
                entity.setThresholdCaution(!ObjectUtils.isEmpty(param[i * 3]) ? Integer.valueOf(param[i * 3]) : null);
                entity.setThresholdWarning(!ObjectUtils.isEmpty(param[i * 3 + 1]) ? Integer.valueOf(param[i * 3 + 1]) : null);
                entity.setThresholdDanger(!ObjectUtils.isEmpty(param[i * 3 + 2]) ? Integer.valueOf(param[i * 3 + 2]) : null);
                entity.setUpdaterId(userId);
                list.add(this.monitoringResourceThresholdRepository.save(entity));
            }

            return list;
        } catch (Exception var7) {
            this.logger.error("updateThreshold ERROR", var7);
            throw new TtsPackageException(1003, "CONTENT SAVE ERROR");
        }
    }

    @Transactional
    public String startProcess(Long processPk) throws TtsPackageException {
        this.logger.debug("startProcess param{}", processPk);

        MonitoringProcessEntity entity;
        try {
            entity = this.getProcess(processPk);
        } catch (TtsPackageException var7) {
            this.logger.error("startProcess ERROR", var7);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }

        String output = this.executeScript(entity.getStartScriptPath());
        if (output.split(" ")[0].equals("success")) {
            output = this.executeScript(entity.getStateScriptPath());
            if (output.split(":")[0].trim().equals("pid")) {
                Long pid = Long.valueOf(output.split(":")[1].trim());
                ProcessHistoryEntity processHistoryEntity = this.processHistoryRepository.findTopByProcessNoOrderByCreatedDtmDesc(processPk);
                if (ObjectUtils.isEmpty(processHistoryEntity) || !processHistoryEntity.getProcessId().equals(pid)) {
                    ProcessHistoryEntity newHistoryEntity = new ProcessHistoryEntity();
                    newHistoryEntity.setProcessId(pid);
                    newHistoryEntity.setProcessNo(processPk);
                    newHistoryEntity.setProcessStatus(ProcessStatus.Running);
                    this.processHistoryRepository.save(newHistoryEntity);
                    output = "success start process";
                }
            }
        }

        if (output.split(" ")[0].equals("error")) {
            throw new TtsPackageException(9999, "FAILURE");
        } else {
            return output;
        }
    }

    @Transactional
    public String stopProcess(Long processPk) throws TtsPackageException {
        this.logger.debug("stopProcess param{}", processPk);

        MonitoringProcessEntity entity;
        try {
            entity = this.getProcess(processPk);
        } catch (TtsPackageException var6) {
            this.logger.error("stopProcessStatus ERROR", var6);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }

        String output = this.executeScript(entity.getEndScriptPath());
        if (output.split(" ")[0].equals("success")) {
            ProcessHistoryEntity processHistoryEntity = this.processHistoryRepository.findTopByProcessNoOrderByCreatedDtmDesc(processPk);
            if (!processHistoryEntity.getProcessStatus().equals(ProcessStatus.Stopped)) {
                ProcessHistoryEntity newHistoryEntity = new ProcessHistoryEntity();
                newHistoryEntity.setProcessId(0L);
                newHistoryEntity.setProcessNo(processPk);
                newHistoryEntity.setProcessStatus(ProcessStatus.Stopped);
                this.processHistoryRepository.save(newHistoryEntity);
                output = "sucess stop process," + entity.getAutoRecovery();
            }
        }

        if (output.split(" ")[0].equals("error")) {
            throw new TtsPackageException(9999, "FAILURE");
        } else {
            return output;
        }
    }

    private String executeScript(String scriptPath) throws TtsPackageException {
        this.logger.info("executeScript param {}", scriptPath);
        if (ObjectUtils.isEmpty(scriptPath)) {
            throw new TtsPackageException(1001, "INVALID PARAMETERS");
        } else {
            String output = "";

            try {
                this.logger.info(scriptPath + " execute");
                Process process = Runtime.getRuntime().exec(scriptPath);

                String line;
                for(BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream())); (line = reader.readLine()) != null; output = line) {
                }

                int exitVal = process.waitFor();
                if (exitVal == 0) {
                    this.logger.debug("executeScript output {}", output);
                }

                return output;
            } catch (InterruptedException | IOException var7) {
                throw new TtsPackageException(1002, "NULL CONTENT");
            }
        }
    }

    public void inputResource() throws TtsPackageException {
        this.logger.info("inputResource");

        String output;
        try {
            output = this.executeScript(this.resStatusPath);
        } catch (TtsPackageException var8) {
            this.logger.error("inputResource EEROR", var8);
            throw new TtsPackageException(1002, "NULL CONTENT");
        }

        int memoryIdx = output.indexOf("RAM");
        int storageIdx = output.indexOf("HDD");
        int gpuIdx = output.indexOf("GPU");
        String[] memory = output.substring(memoryIdx, storageIdx).split(" ");
        String[] storage = output.substring(storageIdx, gpuIdx).split(" ");
        MonitoringResourceInformationEntity entity = new MonitoringResourceInformationEntity();
        entity.setMemoryMax(Long.valueOf(memory[1]));
        entity.setStorageMax(Long.valueOf(storage[1].replace("M", "")));
        entity.setPk(1L);
        this.monitoringResourceInformationRepository.save(entity);
    }
}
