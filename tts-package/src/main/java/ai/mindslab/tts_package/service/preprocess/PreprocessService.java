package ai.mindslab.tts_package.service.preprocess;

import ai.mindslab.tts_package.common.codes.IRestCodes;
import ai.mindslab.tts_package.common.exceptions.TtsPackageException;
import ai.mindslab.tts_package.data.Eng2KorEntity;
import ai.mindslab.tts_package.data.KonglishEntity;
import ai.mindslab.tts_package.domain.preprocess.PreprocessDataEntity;
import ai.mindslab.tts_package.domain.preprocess.PreprocessDataTableEntity;
import ai.mindslab.tts_package.domain.preprocess.PreprocessProcessEntity;
import ai.mindslab.tts_package.domain.preprocess.PreprocessProfileEntity;
import ai.mindslab.tts_package.domain.preprocess.PreprocessRuleEntity;
import ai.mindslab.tts_package.domain.preprocess.QPreprocessDataEntity;
import ai.mindslab.tts_package.domain.preprocess.QPreprocessDataTableEntity;
import ai.mindslab.tts_package.domain.preprocess.QPreprocessProcessEntity;
import ai.mindslab.tts_package.domain.preprocess.QPreprocessProfileEntity;
import ai.mindslab.tts_package.domain.preprocess.QPreprocessRuleEntity;
import ai.mindslab.tts_package.repository.preprocess.PreprocessProfileRepository;
import ai.mindslab.tts_package.repository.preprocess.PreprocessRuleRepository;
import ai.mindslab.tts_package.repository.preprocess.PreprocessTableRepository;
import com.querydsl.core.BooleanBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Service
public class PreprocessService implements IRestCodes {
    private Logger logger = LoggerFactory.getLogger(PreprocessService.class);
    private List<Eng2KorEntity> eng2korList = new ArrayList();
    private List<KonglishEntity> konglishList = new ArrayList();
    @Autowired
    PreprocessRuleRepository preprocessRuleRepository;
    @Autowired
    PreprocessTableRepository preprocessTableRepository;
    @Autowired
    PreprocessProfileRepository preprocessProfileRepository;
    @Value("${tts.txtFilePath}")
    String txtFilePath;
    @Value("${tts.eng2korFilePath}")
    String eng2korFilePath;
    @Value("${tts.konglishFilePath}")
    String konglishFilePath;

    public PreprocessService() {
    }

    @PostConstruct
    public void init() {
        this.initEng2Kor();
        this.initKonglish();
    }

    private void initEng2Kor() {
        this.eng2korList.clear();
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(this.eng2korFilePath));
            int lineNum = 0;

            String line;
            while((line = br.readLine()) != null) {
                System.out.println(line);
                ++lineNum;
                Eng2KorEntity eng2KorEntity = new Eng2KorEntity();
                eng2KorEntity.setEng2korId(lineNum);
                eng2KorEntity.setBef(line.split("=")[0]);
                eng2KorEntity.setAft(line.split("=")[1]);
                this.eng2korList.add(eng2KorEntity);
            }
        } catch (IOException var5) {
            var5.printStackTrace();
        }

    }

    private void initKonglish() {
        this.konglishList.clear();
        BufferedReader br2 = null;

        try {
            br2 = new BufferedReader(new FileReader(this.konglishFilePath));
            int lineNum = 0;

            String line;
            while((line = br2.readLine()) != null) {
                System.out.println(line);
                ++lineNum;
                KonglishEntity konglishEntity = new KonglishEntity();
                konglishEntity.setKonglishId(lineNum);
                konglishEntity.setBef(line.split("=")[0]);
                konglishEntity.setAft(line.split("=")[1]);
                this.konglishList.add(konglishEntity);
            }
        } catch (IOException var5) {
            var5.printStackTrace();
        }

    }

    @Transactional(
            readOnly = true
    )
    public Page<PreprocessRuleEntity> getPreprocessRuleList(String searchCol, String searchText, Pageable pageable) throws TtsPackageException {
        QPreprocessRuleEntity entity = QPreprocessRuleEntity.preprocessRuleEntity;
        BooleanBuilder builder = new BooleanBuilder();
        if (!ObjectUtils.isEmpty(searchCol) && !ObjectUtils.isEmpty(searchText)) {
            byte var7 = -1;
            switch(searchCol.hashCode()) {
                case -1616938316:
                    if (searchCol.equals("ruleExpression")) {
                        var7 = 3;
                    }
                    break;
                case -919875273:
                    if (searchCol.equals("ruleId")) {
                        var7 = 0;
                    }
                    break;
                case 763275175:
                    if (searchCol.equals("ruleName")) {
                        var7 = 1;
                    }
                    break;
                case 763477078:
                    if (searchCol.equals("ruleType")) {
                        var7 = 2;
                    }
            }

            switch(var7) {
                case 0:
                    builder.and(entity.ruleId.eq(Long.parseLong(searchText)));
                    break;
                case 1:
                    builder.and(entity.ruleName.contains(searchText));
                    break;
                case 2:
                    builder.and(entity.ruleType.contains(searchText));
                    break;
                case 3:
                    builder.and(entity.ruleExpression.contains(searchText));
            }
        }

        try {
            Page<PreprocessRuleEntity> list = this.preprocessRuleRepository.findAll(builder, pageable);
            return list;
        } catch (Exception var8) {
            this.logger.error("getPreprocessRuleList ERROR", var8);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public PreprocessRuleEntity getPreprocessRule(Long ruleId) throws TtsPackageException {
        PreprocessRuleEntity entity = null;

        try {
            Optional<PreprocessRuleEntity> optional = this.preprocessRuleRepository.findById(ruleId);
            if (optional.isPresent()) {
                entity = (PreprocessRuleEntity)optional.get();
            }

            return entity;
        } catch (Exception var4) {
            this.logger.error("getPreprocessRule ERROR", var4);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public PreprocessRuleEntity savePreprocessRule(PreprocessRuleEntity param, String userId) throws TtsPackageException {
        PreprocessRuleEntity entity = ObjectUtils.isEmpty(param.getRuleId()) ? null : this.getPreprocessRule(param.getRuleId());
        if (!ObjectUtils.isEmpty(this.preprocessRuleRepository.findByRuleIdNotAndRuleName(param.getRuleId(), param.getRuleName()))) {
            throw new TtsPackageException(1005, "SAME NAME FOUND");
        } else if (!param.getRuleName().contains("<") && !param.getRuleName().contains(">") && !param.getRuleName().contains("\"") && !param.getRuleName().contains("'") && !param.getRuleName().contains("&")) {
            try {
                if (ObjectUtils.isEmpty(entity)) {
                    param.setCreatorId(userId);
                    entity = (PreprocessRuleEntity)this.preprocessRuleRepository.save(param);
                } else {
                    entity.setUpdaterId(userId);
                    entity.setRuleName(param.getRuleName());
                    entity.setRuleType(param.getRuleType());
                    entity.setRuleExpression(param.getRuleExpression());
                }

                return entity;
            } catch (Exception var5) {
                this.logger.error("savePreprocessRule ERROR", var5);
                throw new TtsPackageException(1004, "DB CONNECTION ERROR");
            }
        } else {
            throw new TtsPackageException(1003, "CONTENT SAVE ERROR");
        }
    }

    @Transactional
    public int deletePreprocessRule(Long[] ids) throws TtsPackageException {
        boolean var2 = false;

        try {
            int deleteConfirm = this.preprocessRuleRepository.deleteByRuleIdIn(Arrays.asList(ids));
            return deleteConfirm;
        } catch (Exception var4) {
            this.logger.error("deleteCommonUser ERROR", var4);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    public String testPreprocessRule(String ruleExpression, String testText) throws TtsPackageException {
        String rtnText = testText;
        Pattern p = Pattern.compile("(" + ruleExpression + ")");

        String matchText;
        String replaceText;
        for(Matcher m = p.matcher(testText); m.find(); rtnText = rtnText.replace(matchText, replaceText)) {
            matchText = m.group(1);
            replaceText = "<b style=\"color:blue;\">" + matchText + "</b>";
        }

        return rtnText;
    }

    public void extractRuleTxt() {
        StringBuilder txt = new StringBuilder();
        List<PreprocessRuleEntity> list = this.preprocessRuleRepository.findAll();
        txt.append("id[\\t]\trule_expression[\\t]\trule_name[\\t]\trule_type\n");
        txt.append("----------------------------------------------------\n");
        Iterator var3 = list.iterator();

        while(var3.hasNext()) {
            PreprocessRuleEntity entity = (PreprocessRuleEntity)var3.next();
            txt.append(entity.getRuleId()).append("\t");
            txt.append(entity.getRuleExpression()).append("\t");
            txt.append(entity.getRuleName()).append("\t");
            txt.append(entity.getRuleType()).append("\t").append("\n");
        }

        try {
            File file = new File(this.txtFilePath + "rule.txt");
            FileWriter fw = new FileWriter(file, false);
            fw.write(txt.toString());
            fw.flush();
        } catch (IOException var5) {
            var5.printStackTrace();
        }

    }

    @Transactional(
            readOnly = true
    )
    public Page<PreprocessDataTableEntity> getPreprocessTableList(String searchCol, String searchText, Pageable pageable) throws TtsPackageException {
        QPreprocessDataTableEntity entity = QPreprocessDataTableEntity.preprocessDataTableEntity;
        BooleanBuilder builder = new BooleanBuilder();
        if (!ObjectUtils.isEmpty(searchCol) && !ObjectUtils.isEmpty(searchText)) {
            byte var7 = -1;
            switch(searchCol.hashCode()) {
                case 500941551:
                    if (searchCol.equals("dataTableName")) {
                        var7 = 1;
                    }
                    break;
                case 501143454:
                    if (searchCol.equals("dataTableType")) {
                        var7 = 2;
                    }
                    break;
                case 1260854911:
                    if (searchCol.equals("dataTableId")) {
                        var7 = 0;
                    }
                    break;
                case 1706957847:
                    if (searchCol.equals("inputText")) {
                        var7 = 3;
                    }
            }

            switch(var7) {
                case 0:
                    builder.and(entity.dataTableId.eq(Long.parseLong(searchText)));
                    break;
                case 1:
                    builder.and(entity.dataTableName.contains(searchText));
                    break;
                case 2:
                    builder.and(entity.dataTableType.contains(searchText));
                    break;
                case 3:
                    builder.and(((QPreprocessDataEntity)entity.datas.any()).dataBefore.contains(searchText));
            }
        }

        try {
            Page<PreprocessDataTableEntity> list = this.preprocessTableRepository.findAll(builder, pageable);
            return list;
        } catch (Exception var8) {
            this.logger.error("getPreprocessTableList ERROR", var8);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public PreprocessDataTableEntity getPreprocessTable(Long dataTableId) throws TtsPackageException {
        PreprocessDataTableEntity entity = null;

        try {
            Optional<PreprocessDataTableEntity> optional = this.preprocessTableRepository.findById(dataTableId);
            if (optional.isPresent()) {
                entity = (PreprocessDataTableEntity)optional.get();
                entity.getDatas().size();
            }

            return entity;
        } catch (Exception var4) {
            this.logger.error("getPreprocessRule ERROR", var4);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public PreprocessDataTableEntity savePreprocessTable(PreprocessDataTableEntity param, String userId) throws TtsPackageException {
        if (!ObjectUtils.isEmpty(this.preprocessTableRepository.findByDataTableIdNotAndDataTableName(param.getDataTableId(), param.getDataTableName()))) {
            throw new TtsPackageException(1005, "SAME NAME FOUND");
        } else if (!param.getDataTableName().contains("<") && !param.getDataTableName().contains(">") && !param.getDataTableName().contains("\"") && !param.getDataTableName().contains("'") && !param.getDataTableName().contains("&")) {
            PreprocessDataTableEntity entity = null;
            if (param.getDataTableId() != null && param.getDataTableId() > 0L) {
                entity = this.getPreprocessTable(param.getDataTableId());
            } else {
                entity = new PreprocessDataTableEntity();
            }

            try {
                entity.setDataTableName(param.getDataTableName());
                entity.setDataTableType(param.getDataTableType());
                entity.setCreatorId(userId);
                entity.setUpdaterId(userId);
                entity.getDatas().clear();
                this.preprocessTableRepository.save(entity);
                Iterator var4 = param.getDatas().iterator();

                while(var4.hasNext()) {
                    PreprocessDataEntity data = (PreprocessDataEntity)var4.next();
                    PreprocessDataEntity newEntity = new PreprocessDataEntity();
                    newEntity.setDataBefore(data.getDataBefore());
                    newEntity.setDataAfter(data.getDataAfter());
                    newEntity.setDataNote(data.getDataNote());
                    newEntity.setCreatorId(userId);
                    newEntity.setUpdaterId(userId);
                    newEntity.setTable(entity);
                }

                entity = (PreprocessDataTableEntity)this.preprocessTableRepository.save(entity);
                return entity;
            } catch (Exception var7) {
                this.logger.error("savePreprocessTable ERROR", var7);
                throw new TtsPackageException(1004, "DB CONNECTION ERROR");
            }
        } else {
            throw new TtsPackageException(1003, "CONTENT SAVE ERROR");
        }
    }

    @Transactional
    public int deletePreprocessTable(Long[] ids) throws TtsPackageException {
        boolean var2 = false;

        try {
            int deleteConfirm = this.preprocessTableRepository.deleteByDataTableIdIn(Arrays.asList(ids));
            return deleteConfirm;
        } catch (Exception var4) {
            this.logger.error("deletePreprocessTable ERROR", var4);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    public void extractDataTableTxt() {
        StringBuilder txt = new StringBuilder();
        List<PreprocessDataTableEntity> list = this.preprocessTableRepository.findAll();
        txt.append("id[\\t]\tdata_table_name[\\t]\tdata_table_type[\\t]\tdata_id[\\t]\tdata_bef[\\t]\tdata_aft[\\t]\tdata_note\n");
        txt.append("--------------------------------------------------------------------------------------------------------\n");
        Iterator var3 = list.iterator();

        while(var3.hasNext()) {
            PreprocessDataTableEntity entity = (PreprocessDataTableEntity)var3.next();
            StringBuilder dataTable = new StringBuilder();
            dataTable.append(entity.getDataTableId()).append("\t");
            dataTable.append(entity.getDataTableName()).append("\t");
            dataTable.append(entity.getDataTableType()).append("\t");
            StringBuilder dataStr = new StringBuilder();
            Iterator var7 = entity.getDatas().iterator();

            while(var7.hasNext()) {
                PreprocessDataEntity dataEntity = (PreprocessDataEntity)var7.next();
                dataStr.append(dataTable);
                dataStr.append(dataEntity.getDataId()).append("\t");
                dataStr.append(dataEntity.getDataBefore()).append("\t");
                dataStr.append(dataEntity.getDataAfter()).append("\t");
                dataStr.append(dataEntity.getDataNote()).append("\t").append("\n");
            }

            txt.append(dataStr);
        }

        try {
            File file = new File(this.txtFilePath + "data_table.txt");
            FileWriter fw = new FileWriter(file, false);
            fw.write(txt.toString());
            fw.flush();
        } catch (IOException var9) {
            var9.printStackTrace();
        }

    }

    @Transactional(
            readOnly = true
    )
    public Page<PreprocessProfileEntity> getPreprocessProfileList(String searchCol, String searchText, Pageable pageable) throws TtsPackageException {
        QPreprocessProfileEntity entity = QPreprocessProfileEntity.preprocessProfileEntity;
        BooleanBuilder builder = new BooleanBuilder();
        if (!ObjectUtils.isEmpty(searchCol) && !ObjectUtils.isEmpty(searchText)) {
            byte var7 = -1;
            switch(searchCol.hashCode()) {
                case -1005400924:
                    if (searchCol.equals("profileId")) {
                        var7 = 0;
                    }
                    break;
                case -919875273:
                    if (searchCol.equals("ruleId")) {
                        var7 = 3;
                    }
                    break;
                case 177503188:
                    if (searchCol.equals("profileName")) {
                        var7 = 1;
                    }
                    break;
                case 177705091:
                    if (searchCol.equals("profileType")) {
                        var7 = 2;
                    }
                    break;
                case 1260854911:
                    if (searchCol.equals("dataTableId")) {
                        var7 = 4;
                    }
            }

            switch(var7) {
                case 0:
                    builder.and(entity.profileId.eq(Long.parseLong(searchText)));
                    break;
                case 1:
                    builder.and(entity.profileName.contains(searchText));
                    break;
                case 2:
                    builder.and(entity.profileType.contains(searchText));
                    break;
                case 3:
                    builder.and(((QPreprocessProcessEntity)entity.processes.any()).ruleId.eq(Long.parseLong(searchText)));
                    break;
                case 4:
                    builder.and(((QPreprocessProcessEntity)entity.processes.any()).dataTableId.eq(Long.parseLong(searchText)));
            }
        }

        try {
            Page<PreprocessProfileEntity> list = this.preprocessProfileRepository.findAll(builder, pageable);
            return list;
        } catch (Exception var8) {
            this.logger.error("getPreprocessProfileList ERROR", var8);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public PreprocessProfileEntity getPreprocessProfile(Long profileId) throws TtsPackageException {
        PreprocessProfileEntity entity = null;

        try {
            Optional<PreprocessProfileEntity> optional = this.preprocessProfileRepository.findById(profileId);
            if (optional.isPresent()) {
                entity = (PreprocessProfileEntity)optional.get();
                entity.getProcesses().size();
            }

            return entity;
        } catch (Exception var4) {
            this.logger.error("getPreprocessProfile ERROR", var4);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public PreprocessProfileEntity savePreprocessProfile(PreprocessProfileEntity param, String userId) throws TtsPackageException {
        if (!ObjectUtils.isEmpty(this.preprocessProfileRepository.findByProfileIdNotAndProfileName(param.getProfileId(), param.getProfileName()))) {
            throw new TtsPackageException(1005, "SAME NAME FOUND");
        } else if (!param.getProfileName().contains("<") && !param.getProfileName().contains(">") && !param.getProfileName().contains("\"") && !param.getProfileName().contains("'") && !param.getProfileName().contains("&")) {
            PreprocessProfileEntity entity = null;
            if (param.getProfileId() != null && param.getProfileId() > 0L) {
                entity = this.getPreprocessProfile(param.getProfileId());
            } else {
                entity = new PreprocessProfileEntity();
            }

            try {
                entity.setProfileName(param.getProfileName());
                entity.setProfileType(param.getProfileType());
                entity.setCreatorId(userId);
                entity.setUpdaterId(userId);
                entity.getProcesses().clear();
                this.preprocessProfileRepository.save(entity);
                int flag = 0;
                Iterator var5 = param.getProcesses().iterator();

                while(var5.hasNext()) {
                    PreprocessProcessEntity process = (PreprocessProcessEntity)var5.next();
                    PreprocessProcessEntity newEntity = new PreprocessProcessEntity();
                    if (process.getRuleId() == null) {
                        ++flag;
                    } else {
                        newEntity.setProcessOrder(process.getProcessOrder() - flag);
                        newEntity.setRuleId(process.getRuleId());
                        newEntity.setDataTableId(process.getDataTableId());
                        newEntity.setUseYn(process.getUseYn());
                        newEntity.setCreatorId(userId);
                        newEntity.setUpdaterId(userId);
                        newEntity.setProfile(entity);
                    }
                }

                entity = (PreprocessProfileEntity)this.preprocessProfileRepository.save(entity);
                return entity;
            } catch (Exception var8) {
                this.logger.error("savePreprocessTable ERROR", var8);
                throw new TtsPackageException(1004, "DB CONNECTION ERROR");
            }
        } else {
            throw new TtsPackageException(1003, "CONTENT SAVE ERROR");
        }
    }

    @Transactional
    public int deletePreprocessProfile(Long[] ids) throws TtsPackageException {
        boolean var2 = false;

        try {
            int deleteConfirm = this.preprocessProfileRepository.deleteByProfileIdIn(Arrays.asList(ids));
            return deleteConfirm;
        } catch (Exception var4) {
            this.logger.error("deletePreprocessProfile ERROR", var4);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    public void extractProfileTxt() {
        StringBuilder txt = new StringBuilder();
        List<PreprocessProfileEntity> list = this.preprocessProfileRepository.findAll();
        txt.append("id[\\t]\tprofile_name[\\t]\tprofile_type[\\t]\tprocess_id[\\t]\tprocess_order[\\t]\trule_id[\\t]\tdata_table_id[\\t]\tuse_yn\n");
        txt.append("--------------------------------------------------------------------------------------------------------\n");
        Iterator var3 = list.iterator();

        while(var3.hasNext()) {
            PreprocessProfileEntity entity = (PreprocessProfileEntity)var3.next();
            StringBuilder profile = new StringBuilder();
            profile.append(entity.getProfileId()).append("\t");
            profile.append(entity.getProfileName()).append("\t");
            profile.append(entity.getProfileType()).append("\t");
            StringBuilder process = new StringBuilder();
            Iterator var7 = entity.getProcesses().iterator();

            while(var7.hasNext()) {
                PreprocessProcessEntity dataEntity = (PreprocessProcessEntity)var7.next();
                process.append(profile);
                process.append(dataEntity.getProcessId()).append("\t");
                process.append(dataEntity.getProcessOrder()).append("\t");
                process.append(dataEntity.getRuleId()).append("\t");
                process.append(dataEntity.getDataTableId()).append("\t");
                process.append(dataEntity.getUseYn()).append("\t").append("\n");
            }

            txt.append(process);
        }

        try {
            File file = new File(this.txtFilePath + "profile.txt");
            FileWriter fw = new FileWriter(file, false);
            fw.write(txt.toString());
            fw.flush();
        } catch (IOException var9) {
            var9.printStackTrace();
        }

    }

    @Transactional(
            readOnly = true
    )
    public List<Eng2KorEntity> getEng2KorList() {
        this.initEng2Kor();
        return this.eng2korList;
    }

    @Transactional
    public Eng2KorEntity getEng2Kor(int eng2korId) {
        return (Eng2KorEntity)this.eng2korList.get(eng2korId - 1);
    }

    @Transactional
    public List<Eng2KorEntity> saveEng2Kor(Eng2KorEntity eng2KorEntity) {
        int flag = 0;
        int lineNum = 0;

        for(Iterator var4 = this.eng2korList.iterator(); var4.hasNext(); ++lineNum) {
            Eng2KorEntity entity = (Eng2KorEntity)var4.next();
            if (entity.getBef().trim().equals(eng2KorEntity.getBef().trim())) {
                ++flag;
                break;
            }
        }

        if (flag == 0) {
            eng2KorEntity.setEng2korId(lineNum + 1);
            this.eng2korList.add(eng2KorEntity);
        }

        this.eng2KorListToFile();
        return this.eng2korList;
    }

    @Transactional
    public List<Eng2KorEntity> updateEng2Kor(Eng2KorEntity eng2KorEntity) throws TtsPackageException {
        int lineNum = 0;
        int entityId = eng2KorEntity.getEng2korId();
        Iterator var4 = this.eng2korList.iterator();

        Eng2KorEntity entity;
        do {
            if (!var4.hasNext()) {
//                Eng2KorEntity entity = (Eng2KorEntity)this.eng2korList.get(entityId - 1);
                entity = (Eng2KorEntity)this.eng2korList.get(entityId - 1);
                entity.setBef(eng2KorEntity.getBef());
                entity.setAft(eng2KorEntity.getAft());
                this.eng2korList.remove(entityId - 1);
                this.eng2korList.add(entityId - 1, entity);
                this.eng2KorListToFile();
                return this.eng2korList;
            }

            entity = (Eng2KorEntity)var4.next();
            ++lineNum;
        } while(lineNum == entityId || !entity.getBef().trim().equals(eng2KorEntity.getBef().trim()));

        throw new TtsPackageException(1005, "SAME NAME FOUND");
    }

    @Transactional
    public int deleteEng2Kor(Long[] ids) throws TtsPackageException {
        byte deleteConfirm = 0;

        try {
            ArrayUtils.reverse(ids);
            Long[] var3 = ids;
            int var4 = ids.length;

            for(int var5 = 0; var5 < var4; ++var5) {
                Long id = var3[var5];
                this.eng2korList.remove(Integer.parseInt(String.valueOf(id)) - 1);
            }

            this.eng2KorListToFile();
            return deleteConfirm;
        } catch (Exception var7) {
            this.logger.error("deleteEng2Kor ERROR", var7);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    void eng2KorListToFile() {
        StringBuilder txt = new StringBuilder();
        Iterator var2 = this.eng2korList.iterator();

        while(var2.hasNext()) {
            Eng2KorEntity entity = (Eng2KorEntity)var2.next();
            txt.append(entity.getBef());
            txt.append("=");
            txt.append(entity.getAft());
            txt.append("\n");
        }

        try {
            File file = new File(this.eng2korFilePath);
            FileWriter fw = new FileWriter(file, false);
            fw.write(txt.toString());
            fw.flush();
        } catch (IOException var4) {
            var4.printStackTrace();
        }

        this.initEng2Kor();
    }

    @Transactional(
            readOnly = true
    )
    public List<KonglishEntity> getKonglishList() {
        this.initKonglish();
        return this.konglishList;
    }

    @Transactional
    public KonglishEntity getKonglish(int konglishId) {
        return (KonglishEntity)this.konglishList.get(konglishId - 1);
    }

    @Transactional
    public List<KonglishEntity> saveKonglish(KonglishEntity konglishEntity) throws TtsPackageException {
        int flag = 0;
        int lineNum = 0;

        for(Iterator var4 = this.konglishList.iterator(); var4.hasNext(); ++lineNum) {
            KonglishEntity entity = (KonglishEntity)var4.next();
            if (entity.getBef().trim().equals(konglishEntity.getBef().trim())) {
                ++flag;
                break;
            }
        }

        if (flag == 0) {
            konglishEntity.setKonglishId(lineNum + 1);
            this.konglishList.add(konglishEntity);
        }

        this.konglishListToFile();
        return this.konglishList;
    }

    @Transactional
    public List<KonglishEntity> updateKonglish(KonglishEntity konglishEntity) throws TtsPackageException {
        int lineNum = 0;
        int entityId = konglishEntity.getKonglishId();
        Iterator var4 = this.konglishList.iterator();

        while(var4.hasNext()) {
            KonglishEntity entity = (KonglishEntity)var4.next();
            ++lineNum;
            if (lineNum != entityId) {
                if (entity.getBef().trim().equals(konglishEntity.getBef().trim())) {
                    throw new TtsPackageException(1005, "SAME NAME FOUND");
                }

                ++lineNum;
            }
        }

        KonglishEntity entity = (KonglishEntity)this.konglishList.get(entityId - 1);
        entity.setBef(konglishEntity.getBef());
        entity.setAft(konglishEntity.getAft());
        this.konglishList.remove(entityId - 1);
        this.konglishList.add(entityId - 1, entity);
        this.konglishListToFile();
        return this.konglishList;
    }

    @Transactional
    public int deleteKonglish(Long[] ids) throws TtsPackageException {
        byte deleteConfirm = 0;

        try {
            ArrayUtils.reverse(ids);
            Long[] var3 = ids;
            int var4 = ids.length;

            for(int var5 = 0; var5 < var4; ++var5) {
                Long id = var3[var5];
                this.konglishList.remove(Integer.parseInt(String.valueOf(id)) - 1);
            }

            this.konglishListToFile();
            return deleteConfirm;
        } catch (Exception var7) {
            this.logger.error("deleteKonglish ERROR", var7);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    void konglishListToFile() {
        StringBuilder txt = new StringBuilder();
        Iterator var2 = this.konglishList.iterator();

        while(var2.hasNext()) {
            KonglishEntity entity = (KonglishEntity)var2.next();
            txt.append(entity.getBef());
            txt.append("=");
            txt.append(entity.getAft());
            txt.append("\n");
        }

        try {
            File file = new File(this.konglishFilePath);
            FileWriter fw = new FileWriter(file, false);
            fw.write(txt.toString());
            fw.flush();
        } catch (IOException var4) {
            var4.printStackTrace();
        }

        this.initKonglish();
    }

    public String preprocess(String txt) {
        Iterator var2;
        KonglishEntity eachKonglish;
        for(var2 = this.konglishList.iterator(); var2.hasNext(); txt = txt.replaceAll(eachKonglish.getBef(), eachKonglish.getAft())) {
            eachKonglish = (KonglishEntity)var2.next();
        }

        Eng2KorEntity eachEng2KorList;
        for(var2 = this.eng2korList.iterator(); var2.hasNext(); txt = txt.replaceAll(eachEng2KorList.getBef(), eachEng2KorList.getAft())) {
            eachEng2KorList = (Eng2KorEntity)var2.next();
        }

        return txt;
    }
}
