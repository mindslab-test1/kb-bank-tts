package ai.mindslab.tts_package.service.common;

import ai.mindslab.tts_package.common.codes.IRestCodes;
import ai.mindslab.tts_package.common.exceptions.TtsPackageException;
import ai.mindslab.tts_package.domain.common.CommonRoleEntity;
import ai.mindslab.tts_package.domain.common.CommonUserEntity;
import ai.mindslab.tts_package.domain.common.QCommonUserEntity;
import ai.mindslab.tts_package.domain.common.UserHistoryEntity;
import ai.mindslab.tts_package.repository.common.CommonRoleRepository;
import ai.mindslab.tts_package.repository.common.CommonUserRepository;
import ai.mindslab.tts_package.repository.common.UserHistoryRepository;
import ai.mindslab.tts_package.service.user.UserLoggerService;
import com.querydsl.core.BooleanBuilder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Service
public class CommonService implements IRestCodes {
    private Logger logger = LoggerFactory.getLogger(CommonService.class);
    @Autowired
    UserLoggerService userLoggerService;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    CommonUserRepository commonUserRepository;
    @Autowired
    CommonRoleRepository commonRoleRepository;
    @Autowired
    UserHistoryRepository userHistoryRepository;

    public CommonService() {
    }

    @Transactional(
            readOnly = true
    )
    public Page<CommonUserEntity> getUserList(String searchCol, String searchText, String activatedYn, Pageable pageable) throws TtsPackageException {
        QCommonUserEntity qCommonUserEntity = QCommonUserEntity.commonUserEntity;
        BooleanBuilder builder = new BooleanBuilder();
        if (!ObjectUtils.isEmpty(searchCol) && !ObjectUtils.isEmpty(searchText)) {
            byte var8 = -1;
            switch(searchCol.hashCode()) {
                case -836030906:
                    if (searchCol.equals("userId")) {
                        var8 = 0;
                    }
                    break;
                case -266666762:
                    if (searchCol.equals("userName")) {
                        var8 = 2;
                    }
                    break;
                case 3506294:
                    if (searchCol.equals("role")) {
                        var8 = 1;
                    }
            }

            switch(var8) {
                case 0:
                    builder.and(qCommonUserEntity.userId.contains(searchText));
                    break;
                case 1:
                    builder.and(qCommonUserEntity.role.descript.contains(searchText));
                    break;
                case 2:
                    builder.and(qCommonUserEntity.userName.contains(searchText));
            }
        }

        if (!ObjectUtils.isEmpty(activatedYn)) {
            builder.and(qCommonUserEntity.activatedYn.eq(activatedYn));
        }

        try {
            Page<CommonUserEntity> list = this.commonUserRepository.findAll(builder, pageable);
            return list;
        } catch (Exception var9) {
            this.logger.error("getUserList ERROR", var9);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public CommonUserEntity getCommonUser(String userId) throws TtsPackageException {
        CommonUserEntity entity = null;

        try {
            Optional<CommonUserEntity> optional = this.commonUserRepository.findById(userId);
            if (optional.isPresent()) {
                entity = (CommonUserEntity)optional.get();
            }

            return entity;
        } catch (Exception var4) {
            this.logger.error("getCommonUser ERROR", var4);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public CommonUserEntity saveCommonUser(CommonUserEntity param, String userId, HttpServletRequest request) throws TtsPackageException {
        CommonUserEntity entity = ObjectUtils.isEmpty(param.getUserId()) ? null : this.getCommonUser(param.getUserId());

        try {
            if (!param.getUserId().contains("<") && !param.getUserName().contains("<") && !param.getUserId().contains(">") && !param.getUserName().contains(">") && !param.getUserId().contains("\"") && !param.getUserName().contains("\"") && !param.getUserId().contains("'") && !param.getUserName().contains("'") && !param.getUserId().contains("&") && !param.getUserName().contains("&")) {
                if (ObjectUtils.isEmpty(entity)) {
                    if (param.getRoleId() == null || param.getUserId().equals("") || param.getUserName().equals("") || param.getRoleId() == 0) {
                        throw new TtsPackageException(1001, "INVALID PARAMETERS");
                    }

                    param.setCreatorId(userId);
                    param.setPassword(this.bCryptPasswordEncoder.encode("1234"));
                    param.setPwChgDtm(LocalDateTime.now());
                    param.setLoginFailCnt(0);
                    this.userHistory(param.getUserId(), this.getUserIp(request), "A");
                    this.logger.info("{}, {}, {}", new Object[]{param.getUserId(), this.getUserIp(request), "Y"});
                    this.userLoggerService.mkLogFile(param.getUserId(), this.getUserIp(request), "Y");
                    entity = (CommonUserEntity)this.commonUserRepository.save(param);
                } else {
                    entity.setUpdaterId(userId);
                    entity.setUserName(param.getUserName());
                    String befActivateYn = entity.getActivatedYn();
                    entity.setActivatedYn(param.getActivatedYn());
                    if (befActivateYn.equals("N") && param.getActivatedYn().equals("Y")) {
                        entity.setLoginFailCnt(0);
                        this.userHistory(param.getUserId(), this.getUserIp(request), "Y");
                        entity.setPassword(this.bCryptPasswordEncoder.encode("1234"));
                        this.logger.info("{}, {}, {}", new Object[]{param.getUserId(), this.getUserIp(request), "Y"});
                        this.userLoggerService.mkLogFile(param.getUserId(), this.getUserIp(request), "Y");
                    }

                    if (!ObjectUtils.isEmpty(param.getRoleId())) {
                        entity.setRoleId(param.getRoleId());
                    }

                    if (!ObjectUtils.isEmpty(param.getPassword())) {
                        if (this.chkPassword(param)) {
                            throw new TtsPackageException(1001, "INVALID PARAMETERS");
                        }

                        entity.setPassword(this.bCryptPasswordEncoder.encode(param.getPassword()));
                        entity.setPwChgDtm(param.getPwChgDtm());
                    }
                }

                return entity;
            } else {
                throw new TtsPackageException(1003, "CONTENT SAVE ERROR");
            }
        } catch (TtsPackageException var6) {
            throw new TtsPackageException(var6.getErrCode(), var6.getErrMsg());
        } catch (Exception var7) {
            this.logger.error("saveCommonUser ERROR", var7);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    public boolean chkPassword(CommonUserEntity entity) {
        boolean flag1 = true;
        boolean flag2 = true;
        boolean flag3 = true;
        boolean flag4 = true;
        String userId = entity.getUserId();
        String password = entity.getPassword();
        if (ObjectUtils.isEmpty(password)) {
            return false;
        } else {
            String chk1 = "^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,}$";
            flag1 = Pattern.matches(chk1, password);
            String chk2 = "(\\w)\\1\\1/";
            flag2 = !Pattern.matches(chk2, password);
            int cnt = 0;

            String val_con1;
            for(int i = 0; i < userId.length() - 3; ++i) {
                val_con1 = null;
                val_con1 = String.valueOf(userId.charAt(i)) + userId.charAt(i + 1) + userId.charAt(i + 2) + userId.charAt(i + 3);
                if (password.contains(val_con1)) {
                    ++cnt;
                }
            }

            if (cnt > 0) {
                flag3 = false;
            }

            String val_con0 = "~!@#$%^&*()_+";
            val_con1 = "`1234567890-=";
            String val_con2 = "QWERTYUIOP{}|";
            String val_con3 = "ASDFGHJKL:\"";
            String val_con4 = "ZXCVBNM<>?";
            String val_con5 = "qwertyuiop[]\\";
            String val_con6 = "asdfghjkl;'";
            String val_con7 = "zxcvbnm,./";
            String[] ifPw = new String[]{val_con0, (new StringBuffer(val_con0)).reverse().toString(), val_con1, (new StringBuffer(val_con1)).reverse().toString(), val_con2, (new StringBuffer(val_con2)).reverse().toString(), val_con3, (new StringBuffer(val_con3)).reverse().toString(), val_con4, (new StringBuffer(val_con4)).reverse().toString(), val_con5, (new StringBuffer(val_con5)).reverse().toString(), val_con6, (new StringBuffer(val_con6)).reverse().toString(), val_con7, (new StringBuffer(val_con7)).reverse().toString()};

            for(int i = 0; i < password.length() - 2; ++i) {
                String tmp = String.valueOf(password.charAt(i)) + password.charAt(i + 1) + password.charAt(i + 2);
                String[] var22 = ifPw;
                int var23 = ifPw.length;

                for(int var24 = 0; var24 < var23; ++var24) {
                    String s = var22[var24];
                    if (s.contains(tmp)) {
                        flag4 = false;
                        break;
                    }
                }
            }

            return !flag1 | !flag2 | !flag3 | !flag4;
        }
    }

    public void userHistory(String userId, String userIP, String statusCd) throws TtsPackageException {
        UserHistoryEntity entity = new UserHistoryEntity();
        entity.setUserId(userId);
        entity.setUserIP(userIP);
        entity.setStatusCd(statusCd);

        try {
            this.userHistoryRepository.save(entity);
        } catch (Exception var6) {
            this.logger.error("userHisotry ERROR", var6);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    public boolean getUserIdPwValid(CommonUserEntity userEntity) throws Exception {
        this.logger.debug("User Valid Check");
        if (this.bCryptPasswordEncoder.matches("1234", userEntity.getPassword())) {
            return false;
        } else if (userEntity.getActivatedYn().equals("N")) {
            throw new TtsPackageException(2001, "PERMISSION DENIED");
        } else {
            this.logger.debug("Password Valid Check");
            DateFormat f = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
            String time = String.valueOf(userEntity.getPwChgDtm());
            Date updatedDtm = f.parse(time);
            Date currentDtm = f.parse(f.format(new Date()));
            long diff = currentDtm.getTime() - updatedDtm.getTime();
            return diff / 2592000000L < 3L;
        }
    }

    @Transactional
    public int addLoginFailCnt(String userId) throws TtsPackageException {
        boolean var2 = false;

        try {
            CommonUserEntity userEntity = this.getCommonUser(userId);
            int cnt = userEntity.getLoginFailCnt() + 1;
            userEntity.setLoginFailCnt(cnt);
            return cnt;
        } catch (TtsPackageException var4) {
            this.logger.error("addLoginFailCnt ERROR", var4);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public void clearLoginFailCnt(String userId) throws TtsPackageException {
        byte cnt = 0;

        try {
            CommonUserEntity userEntity = this.getCommonUser(userId);
            userEntity.setLoginFailCnt(Integer.valueOf(cnt));
        } catch (TtsPackageException var4) {
            this.logger.error("addLoginFailCnt ERROR", var4);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public void lockUser(String userId) throws TtsPackageException {
        try {
            if (!userId.equalsIgnoreCase("admin")) {
                CommonUserEntity userEntity = this.getCommonUser(userId);
                userEntity.setActivatedYn("N");
                this.saveCommonUser(userEntity, userId, (HttpServletRequest)null);
            }

        } catch (TtsPackageException var3) {
            this.logger.error("lockUser ERROR ", var3);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public boolean getLastUserHistoryValid(String userId) throws ParseException, TtsPackageException {
        this.logger.debug("Password History Valid Check");
        List<String> statusCds = new ArrayList();
        statusCds.add("A");
        statusCds.add("I");
        statusCds.add("Y");
        UserHistoryEntity entity = this.userHistoryRepository.findTopByStatusCdInAndUserIdOrderByCreatedDtmDesc(statusCds, userId);
        if (!ObjectUtils.isEmpty(entity)) {
            DateFormat f = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
            String time = String.valueOf(entity.getCreatedDtm());
            Date updatedDtm = f.parse(time);
            Date currentDtm = f.parse(f.format(new Date()));
            long diff = currentDtm.getTime() - updatedDtm.getTime();
            if (diff / 2592000000L >= 1L) {
                this.lockUser(userId);
            }

            return diff / 2592000000L < 1L;
        } else {
            this.logger.error("getLastUserHistory ERROR");
            throw new TtsPackageException(1002, "NULL CONTENT");
        }
    }

    @Transactional
    public int deleteCommonUser(String[] ids) throws TtsPackageException {
        boolean var2 = false;

        try {
            int deleteConfirm = this.commonUserRepository.deleteByUserIdIn(Arrays.asList(ids));
            return deleteConfirm;
        } catch (Exception var4) {
            this.logger.error("deleteCommonUser ERROR", var4);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional(
            readOnly = true
    )
    public Page<CommonRoleEntity> getRoleList(Pageable pageable) throws TtsPackageException {
        Page list = null;

        try {
            list = this.commonRoleRepository.findAll(pageable);
            return list;
        } catch (Exception var4) {
            this.logger.error("getRoleList ERROR", var4);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public CommonRoleEntity saveInitRole() throws TtsPackageException {
        CommonRoleEntity entity = null;

        try {
            Optional<CommonRoleEntity> optional = this.commonRoleRepository.findById(1);
            if (optional.isPresent()) {
                entity = (CommonRoleEntity)optional.get();
            }

            if (ObjectUtils.isEmpty(entity)) {
                CommonRoleEntity param = new CommonRoleEntity();
                param.setRoleName("ADMIN");
                param.setDescript("시스템 관리자");
                param.setCreatorId("init");
                entity = (CommonRoleEntity)this.commonRoleRepository.save(param);
            }

            return entity;
        } catch (Exception var4) {
            this.logger.error("saveInitRole ERROR", var4);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    private String getUserIp(HttpServletRequest request) {
        String ip = request.getHeader("X-FORWARDED-FOR");
        if (ip == null) {
            ip = request.getHeader("Proxy-Client-IP");
        }

        if (ip == null) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }

        if (ip == null) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }

        if (ip == null) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }

        if (ip == null) {
            ip = request.getRemoteAddr();
        }

        return ip;
    }
}
