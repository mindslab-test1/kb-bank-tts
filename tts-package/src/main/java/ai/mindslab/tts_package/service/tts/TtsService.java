package ai.mindslab.tts_package.service.tts;

import ai.mindslab.tts_package.common.codes.IRestCodes;
import ai.mindslab.tts_package.common.exceptions.TtsPackageException;
import ai.mindslab.tts_package.domain.tts.TtsConfigEntity;
import ai.mindslab.tts_package.repository.tts.TtsConfigRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Service
public class TtsService implements IRestCodes {
    private Logger logger = LoggerFactory.getLogger(TtsService.class);
    @Autowired
    TtsConfigRepository ttsConfigRepository;

    public TtsService() {
    }

    @Transactional
    public TtsConfigEntity getTtsConfig(Long configId) throws TtsPackageException {
        TtsConfigEntity entity = null;

        try {
            Optional<TtsConfigEntity> optional = this.ttsConfigRepository.findById(configId);
            if (optional.isPresent()) {
                entity = (TtsConfigEntity)optional.get();
            }

            return entity;
        } catch (Exception var4) {
            this.logger.error("getTtsConfig ERROR", var4);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }

    @Transactional
    public TtsConfigEntity saveTtsConfig(TtsConfigEntity param, String userId) throws TtsPackageException {
        TtsConfigEntity entity = ObjectUtils.isEmpty(param.getConfigId()) ? null : this.getTtsConfig(param.getConfigId());

        try {
            if (ObjectUtils.isEmpty(entity)) {
                param.setCreatorId(userId);
                entity = (TtsConfigEntity)this.ttsConfigRepository.save(param);
            } else {
                entity.setUpdaterId(userId);
                entity.setLang(param.getLang());
                entity.setType(param.getType());
                entity.setSpeakerId(param.getSpeakerId());
                entity.setProfileId(param.getProfileId());
            }

            return entity;
        } catch (Exception var5) {
            this.logger.error("savePreprocessRule ERROR", var5);
            throw new TtsPackageException(1004, "DB CONNECTION ERROR");
        }
    }
}
