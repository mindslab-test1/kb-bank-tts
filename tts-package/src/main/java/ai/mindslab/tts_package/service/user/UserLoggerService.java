package ai.mindslab.tts_package.service.user;

import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UserLoggerService {
    @Value("${logging.path}")
    private String path;

    public UserLoggerService() {
    }

    public void mkLogFile(String userId, String ip, String statusCd) throws Exception {
        String txt = "";
        String fileName = "";

        try {
            DateFormat f = new SimpleDateFormat("yyyyMMddHHms", Locale.KOREA);
            DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd-HH-m-s", Locale.KOREA);
            Date now = new Date();
            String currentDtm = f.format(now);
            String currentDtm2 = f2.format(now);
            byte var13 = -1;
            switch(statusCd.hashCode()) {
                case 65:
                    if (statusCd.equals("A")) {
                        var13 = 3;
                    }
                    break;
                case 73:
                    if (statusCd.equals("I")) {
                        var13 = 1;
                    }
                    break;
                case 78:
                    if (statusCd.equals("N")) {
                        var13 = 2;
                    }
                    break;
                case 89:
                    if (statusCd.equals("Y")) {
                        var13 = 0;
                    }
            }

            String state;
            switch(var13) {
                case 0:
                    state = "관리자 승인";
                    break;
                case 1:
                    state = "로그인 성공";
                    break;
                case 2:
                    state = "로그인 실패";
                    break;
                case 3:
                    state = "계정 추가";
                    break;
                default:
                    state = "";
            }

            txt = currentDtm2 + "__" + userId + "__" + ip + "__" + state;
            fileName = this.path + "/" + userId + "/" + currentDtm + ".txt";
            File file = new File(fileName);
            file.getParentFile().mkdir();
            FileWriter fw;
            if (file.createNewFile()) {
                fw = new FileWriter(file);
            } else {
                fw = new FileWriter(file, true);
            }

            fw.write(txt);
            fw.flush();
            fw.close();
            file.setReadOnly();
        } catch (Exception var14) {
            var14.printStackTrace();
        }

    }
}