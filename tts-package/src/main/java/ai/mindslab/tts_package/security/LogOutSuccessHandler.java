package ai.mindslab.tts_package.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class LogOutSuccessHandler implements LogoutSuccessHandler {
    Logger logger = LoggerFactory.getLogger(LogOutSuccessHandler.class);

    public LogOutSuccessHandler() {
    }

    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        HttpSession session = request.getSession();
        this.logger.info("user logOut");
        if (session.getAttribute("userInfo") != null) {
            session.removeAttribute("userInfo");
        }

        Cookie[] cookies = request.getCookies();
        Cookie[] var6 = cookies;
        int var7 = cookies.length;

        for(int var8 = 0; var8 < var7; ++var8) {
            Cookie cookie = var6[var8];
            if (StringUtils.equals(cookie.getName(), "userInfo")) {
                cookie.setMaxAge(0);
                response.addCookie(cookie);
            }
        }

        response.setStatus(200);
        response.sendRedirect("/login");
    }
}
