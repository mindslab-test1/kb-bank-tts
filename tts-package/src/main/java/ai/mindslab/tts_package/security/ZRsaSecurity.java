package ai.mindslab.tts_package.security;

import java.nio.charset.Charset;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import javax.crypto.Cipher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZRsaSecurity {
    private static final Logger log = LoggerFactory.getLogger(ZRsaSecurity.class);
    private final int KEY_SIZE = 1024;
    private final String Alg_NM = "RSA";
    private PublicKey publicKey;
    private PrivateKey privateKey;
    private KeyFactory keyFactory = null;
    private RSAPublicKeySpec rsaPksc = null;

    public ZRsaSecurity() throws Exception {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(1024);
        KeyPair keyPair = generator.genKeyPair();
        this.keyFactory = KeyFactory.getInstance("RSA");
        this.publicKey = keyPair.getPublic();
        this.privateKey = keyPair.getPrivate();
        this.rsaPksc = (RSAPublicKeySpec)this.keyFactory.getKeySpec(this.publicKey, RSAPublicKeySpec.class);
    }

    public String getRsaPublicKeyModulus() throws Exception {
        if (null != this.rsaPksc) {
            return this.rsaPksc.getModulus().toString(16);
        } else {
            throw new Exception();
        }
    }

    public String getRsaPublicKeyExponent() throws Exception {
        if (null != this.rsaPksc) {
            return this.rsaPksc.getPublicExponent().toString(16);
        } else {
            throw new Exception();
        }
    }

    public PublicKey getPublicKey() {
        return this.publicKey;
    }

    public PrivateKey getPrivateKey() {
        return this.privateKey;
    }

    public String encryptRSA(PublicKey publicKey, String input) {
        String s = "";

        try {
            try {
                Cipher cipher = Cipher.getInstance("RSA");
                cipher.init(1, publicKey);
                byte[] ba = cipher.doFinal(input.getBytes(Charset.forName("utf-8")));
                StringBuilder sb = new StringBuilder();
                byte[] var7 = ba;
                int var8 = ba.length;

                for(int var9 = 0; var9 < var8; ++var9) {
                    byte b = var7[var9];
                    sb.append(String.format("%02X", b));
                }

                s = sb.toString();
            } catch (Exception var14) {
                var14.printStackTrace();
            }

            return s;
        } finally {
            ;
        }
    }

    public String decryptRSA(PrivateKey privateKey, String securedValue) {
        String decryptedValue = "";

        try {
            Cipher cipher = Cipher.getInstance("RSA");
            byte[] encryptedBytes = this.hexToByteArray(securedValue);
            cipher.init(2, privateKey);
            byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
            decryptedValue = new String(decryptedBytes, "utf-8");
        } catch (Exception var7) {
            decryptedValue = "";
        }

        return decryptedValue;
    }

    public byte[] hexToByteArray(String hex) {
        if (hex != null && hex.length() % 2 == 0) {
            byte[] bytes = new byte[hex.length() / 2];

            for(int i = 0; i < hex.length(); i += 2) {
                byte value = (byte)Integer.parseInt(hex.substring(i, i + 2), 16);
                bytes[(int)Math.floor((double)(i / 2))] = value;
            }

            return bytes;
        } else {
            return new byte[0];
        }
    }
}
