package maum.brain.tts.mybatis.model;

import java.util.List;
import maum.brain.tts.vo.TtsDataVO;
import maum.brain.tts.vo.TtsProfileVO;
import maum.brain.tts.vo.TtsRuleVO;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

public class TtsSQLQuery {
    private SqlSessionFactory sqlSessionFactory = null;

    public TtsSQLQuery(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    public List<TtsDataVO> selectData(String table_id) {
        SqlSession session = this.sqlSessionFactory.openSession();

        List var4;
        try {
            List<TtsDataVO> output = session.selectList("TtsMapper.selectData", table_id);
            var4 = output;
        } finally {
            session.close();
        }

        return var4;
    }

    public List<TtsProfileVO> selectProfile(String table_id) {
        SqlSession session = this.sqlSessionFactory.openSession(false);

        List var4;
        try {
            List<TtsProfileVO> output = session.selectList("TtsMapper.selectProfile", table_id);
            var4 = output;
        } finally {
            session.close();
        }

        return var4;
    }

    public TtsRuleVO selectRegex(String rule_id) {
        SqlSession session = this.sqlSessionFactory.openSession(false);

        TtsRuleVO var4;
        try {
            TtsRuleVO output = (TtsRuleVO)session.selectOne("TtsMapper.selectRegex", rule_id);
            output.setRule_id(rule_id);
            var4 = output;
        } finally {
            session.close();
        }

        return var4;
    }

    public List<TtsDataVO> selectDataId(String table_id) {
        SqlSession session = this.sqlSessionFactory.openSession(false);

        List var4;
        try {
            List<TtsDataVO> output = session.selectList("TtsMapper.selectDataId", table_id);
            var4 = output;
        } finally {
            session.close();
        }

        return var4;
    }
}
