package maum.brain.tts;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Configuration
@ComponentScan(
        basePackages = {"maum.brain.tts"}
)
@EnableScheduling
public class CustomTTSConfig {
    private static Logger logger = LoggerFactory.getLogger(CustomTTSConfig.class);
    private final int corePoolSize = 50;
    private final int maxPoolSize = 50;
    private final int queueCapacity = 50;
    private final int keepAliveSeconds = 60;

    public CustomTTSConfig() {
    }

    @Bean
    public TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        logger.info("taskScheduler/PoolSize={}", 50);
        scheduler.setPoolSize(50);
        scheduler.setWaitForTasksToCompleteOnShutdown(Boolean.TRUE);
        scheduler.initialize();
        return scheduler;
    }

    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        logger.info("taskExecutor/maxPoolSize={},corePoolSize={}", 50, 50);
        executor.setCorePoolSize(50);
        executor.setMaxPoolSize(50);
        executor.setQueueCapacity(50);
        executor.setKeepAliveSeconds(60);
        executor.setWaitForTasksToCompleteOnShutdown(Boolean.TRUE);
        executor.setThreadNamePrefix("tts-async-");
        executor.setRejectedExecutionHandler(new CallerRunsPolicy());
        executor.initialize();
        return executor;
    }
}
