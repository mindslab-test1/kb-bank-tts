package maum.brain.tts.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import maum.brain.tts.AudioEncoding;
import maum.brain.tts.NgTtsServiceGrpc;
import maum.brain.tts.Sentence;
import maum.brain.tts.TtsCustomRequest;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.tts.NgTtsServiceGrpc.NgTtsServiceBlockingStub;

public class EnglishCoreClient {
    private ManagedChannel channel;

    public EnglishCoreClient() {
    }

    public void callCoreTTSAfterG2P(String ttsIp, int ttsPort, int timeout, int speaker, List<Sentence> sentenceList, AudioEncoding audioEncoding, StreamObserver<TtsMediaResponse> observer) {
        if (this.channel == null) {
            this.channel = ManagedChannelBuilder.forAddress(ttsIp, ttsPort).usePlaintext().build();
        }

        NgTtsServiceBlockingStub ttsStub = NgTtsServiceGrpc.newBlockingStub(this.channel);
        TtsCustomRequest ttsCustomRequest = TtsCustomRequest.newBuilder().setSpeaker(speaker).setAudioEncoding(audioEncoding).addAllSentences(sentenceList).build();
        Iterator<TtsMediaResponse> iter = ((NgTtsServiceBlockingStub)ttsStub.withDeadlineAfter((long)timeout, TimeUnit.SECONDS)).speakWavCustom(ttsCustomRequest);
        TtsMediaResponse response = null;

        while(iter.hasNext()) {
            response = (TtsMediaResponse)iter.next();
            observer.onNext(response);
        }

    }
}
