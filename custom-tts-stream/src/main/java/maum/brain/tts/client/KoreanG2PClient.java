package maum.brain.tts.client;

import com.google.protobuf.ProtocolStringList;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.List;
import maum.brain.g2p.G2PGrpc;
import maum.brain.g2p.G2PGrpc.G2PBlockingStub;
import maum.brain.g2p.G2POuterClass.Grapheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KoreanG2PClient {
    private static Logger logger = LoggerFactory.getLogger(KoreanG2PClient.class);

    public KoreanG2PClient() {
    }

    public List<String> callG2p(List<String> sentenceList) {
        String g2pAddress = System.getProperty("GRPC_ADDR_G2P");
        if (g2pAddress == null || g2pAddress.isEmpty()) {
            g2pAddress = "g2p.mlab:19002";
        }

        if (g2pAddress != null && !g2pAddress.isEmpty()) {
            ManagedChannel g2pChannel = ManagedChannelBuilder.forTarget(g2pAddress).usePlaintext().build();

            List var5;
            try {
                Grapheme inputText = Grapheme.newBuilder().addAllSentences(sentenceList).build();
                G2PBlockingStub g2pStub = G2PGrpc.newBlockingStub(g2pChannel);
                ProtocolStringList var6 = g2pStub.transliterate(inputText).getSentencesList();
                return var6;
            } catch (Exception var10) {
                logger.error(var10.getMessage());
                var5 = sentenceList;
            } finally {
                if (g2pChannel != null && !g2pChannel.isShutdown()) {
                    g2pChannel.shutdown();
                }

            }

            return var5;
        } else {
            return sentenceList;
        }
    }
}
