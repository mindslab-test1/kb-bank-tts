package maum.brain.tts.client;

import com.google.protobuf.ProtocolStringList;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.List;
import maum.brain.konglish.KonglishGrpc;
import maum.brain.konglish.KonglishGrpc.KonglishBlockingStub;
import maum.brain.konglish.KonglishOuterClass.English;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class KonglishClient {
    @Value("${tts.ko_KR.konglish.address}")
    private String konglishAddress;

    public KonglishClient() {
    }

    public KonglishClient(String konglishAddress) {
        this.konglishAddress = konglishAddress;
    }

//    public List<String> callKonglish(List<String> wordList) {
    public Object callKonglish(List<String> wordList) {
        if (this.konglishAddress.isEmpty()) {
            return wordList;
        } else {
            ManagedChannel konglishChannel = null;

            KonglishBlockingStub kongStub;
            try {
                English inputText = English.newBuilder().addAllWords(wordList).build();
                konglishChannel = ManagedChannelBuilder.forTarget(this.konglishAddress).usePlaintext().build();
                kongStub = KonglishGrpc.newBlockingStub(konglishChannel);
                ProtocolStringList var5 = kongStub.transliterate(inputText).getWordsList();
                return var5;
            } catch (Exception var9) {
                kongStub = null;
            } finally {
                konglishChannel.shutdown();
            }

            return kongStub;
        }
    }
}
