package maum.brain.tts.client;

import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Iterator;
import maum.brain.tts.NgTtsServiceGrpc;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.tts.TtsRequest;
import maum.brain.tts.NgTtsServiceGrpc.NgTtsServiceBlockingStub;
import maum.brain.tts.TtsRequest.Builder;
import maum.common.LangOuterClass.Lang;

public class TestCoreClient {
    public TestCoreClient() {
    }

    public static void main(String[] args) {
        System.out.println("TestClinet started.");
        System.out.println("speakWav test...");
        String target = "10.122.65.229:19999";
        String ip = "221.168.33.192";
//        int port = true;
        ManagedChannel channel = ManagedChannelBuilder.forTarget(target).usePlaintext().build();
        NgTtsServiceBlockingStub stub = NgTtsServiceGrpc.newBlockingStub(channel);
        Builder req = TtsRequest.newBuilder();
        req.setLang(Lang.ko_KR);
        req.setSampleRate(22050);
        req.setSpeaker(0);
        req.setText("안녕하세요.");
        Iterator iter = stub.speakWav(req.build());

        try {
            FileOutputStream fos = new FileOutputStream("C:/Temp/aaa1.wav");
            Throwable var9 = null;

            try {
                FileChannel fc = fos.getChannel();
                Throwable var11 = null;

                try {
                    while(iter.hasNext()) {
                        TtsMediaResponse resp = (TtsMediaResponse)iter.next();
                        ByteString data = resp.getMediaData();
                        ByteBuffer buff = data.asReadOnlyByteBuffer();
                        fc.write(buff);
                    }
                } catch (Throwable var38) {
                    var11 = var38;
                    throw var38;
                } finally {
                    if (fc != null) {
                        if (var11 != null) {
                            try {
                                fc.close();
                            } catch (Throwable var37) {
                                var11.addSuppressed(var37);
                            }
                        } else {
                            fc.close();
                        }
                    }

                }
            } catch (Throwable var40) {
                var9 = var40;
                throw var40;
            } finally {
                if (fos != null) {
                    if (var9 != null) {
                        try {
                            fos.close();
                        } catch (Throwable var36) {
                            var9.addSuppressed(var36);
                        }
                    } else {
                        fos.close();
                    }
                }

            }
        } catch (IOException var42) {
            var42.printStackTrace();
        }

    }
}
