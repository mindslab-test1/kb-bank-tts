package maum.brain.tts.client;

import java.io.File;
import java.io.FilenameFilter;
import java.io.SequenceInputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.UUID;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.AudioFileFormat.Type;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AudioClient {
    private static final int MAX_DIGIT_LENGTH = 11;
    private static Logger logger = LoggerFactory.getLogger(AudioClient.class);

    public AudioClient() {
    }

    public static void main(String[] args) {
        String test = "0";
        String wavDir = "C:/Temp";
        int speaker = 0;
        String fileName = makePhoneWavFile(wavDir.concat("/"), speaker, test);
        System.out.println(fileName);
    }

    public static void deleteFileList(String dir, final String startFilter, int exceptLength) {
        File sDir = new File(dir);
        if (sDir.exists() && sDir.isDirectory()) {
            File[] fileNameList = sDir.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.startsWith(startFilter);
                }
            });
            File[] var5 = fileNameList;
            int var6 = fileNameList.length;

            for(int var7 = 0; var7 < var6; ++var7) {
                File curFile = var5[var7];
                if (curFile.getName().length() != exceptLength) {
                    curFile.delete();
                }
            }
        }

    }

    public static ArrayList<String> getDeleteFileList(String dir, final String filter) {
        File sDir = new File(dir);
        File[] fileNameList = sDir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(filter);
            }
        });
        ArrayList<String> retFileList = new ArrayList();
        File[] var5 = fileNameList;
        int var6 = fileNameList.length;

        for(int var7 = 0; var7 < var6; ++var7) {
            File curFile = var5[var7];
            if (curFile.getName().length() != 12) {
                retFileList.add(curFile.getPath());
            }
        }

        return retFileList;
    }

    public static File[] getFileList(String dir) {
        File sDir = new File(dir);
        File[] fileNameList = sDir.listFiles();
        return fileNameList;
    }

    public static File[] getEndsFilterFileList(String dir, final String filter) {
        File sDir = new File(dir);
        File[] fileNameList = sDir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(filter);
            }
        });
        return fileNameList;
    }

    public static File[] getStartsFilterFileList(String dir, final String filter) {
        File sDir = new File(dir);
        File[] fileNameList = sDir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(filter);
            }
        });
        return fileNameList;
    }

    private static String byteArrayToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        byte[] var2 = bytes;
        int var3 = bytes.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            byte b = var2[var4];
            sb.append(String.format("%02X", b & 255));
        }

        return sb.toString();
    }

    public static String makeWavFileName(String wavDir, int speaker) throws Exception {
        String model = "model";
        if (speaker == 0) {
            model = model.concat("1/");
        } else if (speaker == 1) {
            model = model.concat("2/");
        } else if (speaker == 2) {
            model = model.concat("3/");
        } else {
            model = model.concat("1/");
        }

        MessageDigest salt = MessageDigest.getInstance("SHA-256");
        salt.update(UUID.randomUUID().toString().getBytes("UTF-8"));
        String digest = byteArrayToHexString(salt.digest());
        String fDir = wavDir + model;
        String fName = fDir + digest + ".wav";
        File f = new File(fDir);
        if (!f.exists()) {
            f.mkdir();
        }

        return fName;
    }

    public static String makeDigitWavFile(String wavDir, int speaker, String digitNumber) {
        if (digitNumber == null) {
            digitNumber = "0000";
        }

        if (digitNumber.length() > 11) {
            digitNumber = digitNumber.substring(0, 11);
        }

        String model = "model";
        if (speaker == 0) {
            model = model.concat("1_");
        } else if (speaker == 1) {
            model = model.concat("2_");
        } else if (speaker == 2) {
            model = model.concat("3_");
        } else {
            model = model.concat("1_");
        }

        String fName = wavDir + model + digitNumber + ".wav";
        File f = new File(fName);
        if (f.exists()) {
            return fName;
        } else {
            String[] wav = new String[11];
            String[] newFile = new String[11];

            for(int i = 0; i < digitNumber.length(); ++i) {
                wav[i] = wavDir + model + digitNumber.charAt(i) + ".wav";
                if (i > 0) {
                    if (i == 1) {
                        newFile[i] = wavDir + model + makeAudioFileName(wav[i - 1], wav[i]);
                        appendWavFile(wav[i - 1], wav[i], newFile[i]);
                    } else {
                        newFile[i] = wavDir + model + makeAudioFileName(newFile[i - 1], wav[i]);
                        appendWavFile(newFile[i - 1], wav[i], newFile[i]);
                    }
                }
            }

            return newFile[digitNumber.length() - 1];
        }
    }

    public static String makePhoneWavFile(String wavDir, int speaker, String phoneNumber) {
        if (phoneNumber == null) {
            phoneNumber = "01000000000";
        }

        if (phoneNumber.length() > 11) {
            phoneNumber = phoneNumber.substring(0, 11);
        }

        String model = "model";
        if (speaker == 0) {
            model = model.concat("1_");
        } else if (speaker == 1) {
            model = model.concat("2_");
        } else if (speaker == 2) {
            model = model.concat("3_");
        } else {
            model = model.concat("1_");
        }

        String fName = wavDir + model + phoneNumber + ".wav";
        File f = new File(fName);
        if (f.exists()) {
            return fName;
        } else {
            String wav1 = wavDir + model + phoneNumber.charAt(0) + ".wav";
            String wav2 = wavDir + model + phoneNumber.charAt(1) + ".wav";
            String newFile = wavDir + model + makeAudioFileName(wav1, wav2);
            appendWavFile(wav1, wav2, newFile);
            if (phoneNumber.length() == 2) {
                return newFile;
            } else {
                String wav3 = wavDir + model + phoneNumber.charAt(2) + ".wav";
                String newFile2 = wavDir + model + makeAudioFileName(newFile, wav3);
                appendWavFile(newFile, wav3, newFile2);
                if (phoneNumber.length() == 3) {
                    return newFile2;
                } else {
                    String wav4 = wavDir + model + phoneNumber.charAt(3) + ".wav";
                    String newFile3 = wavDir + model + makeAudioFileName(newFile2, wav4);
                    appendWavFile(newFile2, wav4, newFile3);
                    if (phoneNumber.length() == 4) {
                        return newFile3;
                    } else {
                        String wav5 = wavDir + model + phoneNumber.charAt(4) + ".wav";
                        String newFile4 = wavDir + model + makeAudioFileName(newFile3, wav5);
                        appendWavFile(newFile3, wav5, newFile4);
                        if (phoneNumber.length() == 5) {
                            return newFile4;
                        } else {
                            String wav6 = wavDir + model + phoneNumber.charAt(5) + ".wav";
                            String newFile5 = wavDir + model + makeAudioFileName(newFile4, wav6);
                            appendWavFile(newFile4, wav6, newFile5);
                            if (phoneNumber.length() == 6) {
                                return newFile5;
                            } else {
                                String wav7 = wavDir + model + phoneNumber.charAt(6) + ".wav";
                                String newFile6 = wavDir + model + makeAudioFileName(newFile5, wav7);
                                appendWavFile(newFile5, wav7, newFile6);
                                if (phoneNumber.length() == 7) {
                                    return newFile6;
                                } else {
                                    String wav8 = wavDir + model + phoneNumber.charAt(7) + ".wav";
                                    String newFile7 = wavDir + model + makeAudioFileName(newFile6, wav8);
                                    appendWavFile(newFile6, wav8, newFile7);
                                    if (phoneNumber.length() == 8) {
                                        return newFile7;
                                    } else {
                                        String wav9 = wavDir + model + phoneNumber.charAt(8) + ".wav";
                                        String newFile8 = wavDir + model + makeAudioFileName(newFile7, wav9);
                                        appendWavFile(newFile7, wav9, newFile8);
                                        if (phoneNumber.length() == 9) {
                                            return newFile8;
                                        } else {
                                            String wav10 = wavDir + model + phoneNumber.charAt(9) + ".wav";
                                            String newFile9 = wavDir + model + makeAudioFileName(newFile8, wav10);
                                            appendWavFile(newFile8, wav10, newFile9);
                                            if (phoneNumber.length() == 10) {
                                                return newFile9;
                                            } else {
                                                String wav11 = wavDir + model + phoneNumber.charAt(10) + ".wav";
                                                String newFile10 = wavDir + model + makeAudioFileName(newFile9, wav11);
                                                appendWavFile(newFile9, wav11, newFile10);
                                                return newFile10;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static String mergeTwoWavFile(String originalFile, String appenfFile, String wavDir, int speaker) {
        String outFile = new String();

        try {
            AudioInputStream clip1 = AudioSystem.getAudioInputStream(new File(originalFile));
            AudioInputStream clip2 = AudioSystem.getAudioInputStream(new File(appenfFile));
            AudioInputStream appendedFiles = new AudioInputStream(new SequenceInputStream(clip1, clip2), clip1.getFormat(), clip1.getFrameLength() + clip2.getFrameLength());
            outFile = makeWavFileName(wavDir, speaker);
            AudioSystem.write(appendedFiles, Type.WAVE, new File(outFile));
        } catch (Exception var8) {
            logger.error(var8.getMessage());
        }

        return outFile;
    }

    private static void appendWavFile(String originalFile, String appenfFile, String outFile) {
        try {
            AudioInputStream currentWav = AudioSystem.getAudioInputStream(new File(originalFile));
            AudioInputStream toAppend = AudioSystem.getAudioInputStream(new File(appenfFile));
            currentWav = new AudioInputStream(new SequenceInputStream(currentWav, toAppend), currentWav.getFormat(), currentWav.getFrameLength() + toAppend.getFrameLength());
            AudioSystem.write(currentWav, Type.WAVE, new File(outFile));
        } catch (Exception var5) {
            logger.error(var5.getMessage());
        }

    }

    private static String makeAudioFileName(String wavFile1, String wavFile2) {
        String name1 = FilenameUtils.removeExtension(FilenameUtils.getName(wavFile1));
        String name2 = FilenameUtils.removeExtension(FilenameUtils.getName(wavFile2));
        name1 = name1.replaceAll("model1_", "").replaceAll("model2_", "").replaceAll("model3_", "");
        name2 = name2.replaceAll("model1_", "").replaceAll("model2_", "").replaceAll("model3_", "");
        return name1.concat(name2).concat(".wav");
    }
}
