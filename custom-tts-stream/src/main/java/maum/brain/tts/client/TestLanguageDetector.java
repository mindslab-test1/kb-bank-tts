package maum.brain.tts.client;

import java.io.File;
import java.io.IOException;
import opennlp.tools.langdetect.Language;
import opennlp.tools.langdetect.LanguageDetector;
import opennlp.tools.langdetect.LanguageDetectorME;
import opennlp.tools.langdetect.LanguageDetectorModel;

public class TestLanguageDetector {
    private static String LangDetectFile = "src/main/resources/langdetect-183.bin";

    public TestLanguageDetector() {
    }

    public static void main(String[] main) {
        File modelFile = new File(LangDetectFile);
        LanguageDetectorModel trainedModel = null;

        try {
            trainedModel = new LanguageDetectorModel(modelFile);
        } catch (IOException var9) {
        }

        LanguageDetector languageDetector = new LanguageDetectorME(trainedModel);
        Language[] languages = languageDetector.predictLanguages("Puedo darte ejemplos de los métodos");
        System.out.println("Predicted language: " + languages[0].getLang());
        languages = languageDetector.predictLanguages("반갑습니다. 여기는 서울입니다.");
        System.out.println("Predicted language: " + languages[0].getLang());
        languages = languageDetector.predictLanguages("I know I’m not perfect.");
        System.out.println("Predicted language: " + languages[0].getLang());
        languages = languageDetector.predictLanguages("<digit>1234</digit>");
        System.out.println("Predicted language: " + languages[0].getLang());
        languages = languageDetector.predictLanguages("1234");
        System.out.println("Predicted language: " + languages[0].getLang());
        languages = languageDetector.predictLanguages("난 뚱뚱하지 않아/ I am not fat.");
        Language[] var5 = languages;
        int var6 = languages.length;

        for(int var7 = 0; var7 < var6; ++var7) {
            Language language = var5[var7];
            System.out.println(language.getLang() + "  confidence:" + language.getConfidence());
        }

    }
}
