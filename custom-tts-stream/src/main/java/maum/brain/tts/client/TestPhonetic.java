package maum.brain.tts.client;

public class TestPhonetic {
    private static final String cmuPhonetic = "AA AE AH AO AW AY B CH D DH EH ER EY F G HH IH IY JH K L M N NG OW OY P R S SH T TH UH UW V W Y Z ZH";

    public TestPhonetic() {
    }

    public static void main(String[] args) {
        String text = "{HH EH L OW}.{T W EH L V} {TH ER D IY} {F AO R}.";
        String chkText = text.replaceAll("\\{", " ").replaceAll("\\}", " ").replaceAll("[.]", " ").replaceAll("\\s+", " ");
        boolean b = isPhonetic(chkText);
        System.out.println(b);
    }

    public static boolean isPhonetic(String sentence) {
        String[] split = sentence.split(" ");

        for(int i = 0; i < split.length; ++i) {
            String token = split[i];
            if (!token.isEmpty()) {
                System.out.println("token-->" + token);
                if (!"AA AE AH AO AW AY B CH D DH EH ER EY F G HH IH IY JH K L M N NG OW OY P R S SH T TH UH UW V W Y Z ZH".contains(token)) {
                    return false;
                }
            }
        }

        return true;
    }
}
