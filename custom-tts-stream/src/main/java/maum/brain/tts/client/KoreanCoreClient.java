package maum.brain.tts.client;

import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import maum.brain.tts.AudioEncoding;
import maum.brain.tts.NgTtsServiceGrpc;
import maum.brain.tts.Sentence;
import maum.brain.tts.TtsCustomRequest;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.tts.NgTtsServiceGrpc.NgTtsServiceBlockingStub;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KoreanCoreClient {
    private static Logger logger = LoggerFactory.getLogger(KoreanCoreClient.class);
    private ManagedChannel channel;
    private KoreanG2PClient g2p;

    public KoreanCoreClient() {
    }

    public void streamCoreTTSAfterG2P(String lastFileName, String ttsIp, int ttsPort, int timeout, int speaker, ArrayList<String> sentenceList, AudioEncoding audioEncoding, StreamObserver<TtsMediaResponse> observer) {
        TtsMediaResponse response = null;

        try {
            Path path = Paths.get(lastFileName);
            byte[] data = Files.readAllBytes(path);
            ByteString bs = ByteString.copyFrom(data);
            response = TtsMediaResponse.newBuilder().setMediaData(bs).build();
            observer.onNext(response);
        } catch (Exception var16) {
            logger.error(var16.getMessage());
        }

        if (this.g2p == null) {
            this.g2p = new KoreanG2PClient();
        }

        List<String> g2pSentenceList = this.g2p.callG2p(sentenceList);
        List<Sentence> newSentenceList = new ArrayList();
        new String();

        for(int i = 0; i < g2pSentenceList.size(); ++i) {
            String sentence = ((String)g2pSentenceList.get(i)).trim();
            logger.info("streamCoreTTSAfterG2P:speaker:" + speaker + ":sentence:" + sentence);
            newSentenceList.add(Sentence.newBuilder().setText(sentence).build());
        }

        if (this.channel == null) {
            this.channel = ManagedChannelBuilder.forAddress(ttsIp, ttsPort).usePlaintext().build();
        }

        NgTtsServiceBlockingStub ttsStub = NgTtsServiceGrpc.newBlockingStub(this.channel);
        TtsCustomRequest ttsCustomRequest = TtsCustomRequest.newBuilder().setSpeaker(speaker).setAudioEncoding(audioEncoding).addAllSentences(newSentenceList).build();
        Iterator iter = ((NgTtsServiceBlockingStub)ttsStub.withDeadlineAfter((long)timeout, TimeUnit.SECONDS)).speakWavCustom(ttsCustomRequest);

        while(iter.hasNext()) {
            response = (TtsMediaResponse)iter.next();
            observer.onNext(response);
        }

    }

    public void callCoreTTSAfterG2P(String ttsIp, int ttsPort, int timeout, int speaker, ArrayList<String> sentenceList, AudioEncoding audioEncoding, StreamObserver<TtsMediaResponse> observer) {
        if (this.g2p == null) {
            this.g2p = new KoreanG2PClient();
        }

        List<String> g2pSentenceList = this.g2p.callG2p(sentenceList);
        List<Sentence> newSentenceList = new ArrayList();
        new String();

        for(int i = 0; i < g2pSentenceList.size(); ++i) {
            String sentence = ((String)g2pSentenceList.get(i)).trim();
            logger.info("callCoreTTSAfterG2P:speaker:" + speaker + ":sentence:" + sentence);
            newSentenceList.add(Sentence.newBuilder().setText(sentence).build());
        }

        if (this.channel == null) {
            this.channel = ManagedChannelBuilder.forAddress(ttsIp, ttsPort).usePlaintext().build();
        }

        NgTtsServiceBlockingStub ttsStub = NgTtsServiceGrpc.newBlockingStub(this.channel);
        TtsCustomRequest ttsCustomRequest = TtsCustomRequest.newBuilder().setSpeaker(speaker).setAudioEncoding(audioEncoding).addAllSentences(newSentenceList).build();
        Iterator<TtsMediaResponse> iter = ((NgTtsServiceBlockingStub)ttsStub.withDeadlineAfter((long)timeout, TimeUnit.SECONDS)).speakWavCustom(ttsCustomRequest);
        TtsMediaResponse response = null;

        while(iter.hasNext()) {
            response = (TtsMediaResponse)iter.next();
            observer.onNext(response);
        }

    }

    public String saveCoreTTSAfterG2P(String dictRoot, String ttsIp, int ttsPort, int timeout, int speaker, ArrayList<String> sentenceList, AudioEncoding audioEncoding) {
        if (this.g2p == null) {
            this.g2p = new KoreanG2PClient();
        }

        List<String> g2pSentenceList = this.g2p.callG2p(sentenceList);
        List<Sentence> newSentenceList = new ArrayList();
        new String();

        for(int i = 0; i < g2pSentenceList.size(); ++i) {
            String sentence = ((String)g2pSentenceList.get(i)).trim();
            logger.info("saveCoreTTSAfterG2P:speaker:" + speaker + ":sentence:" + sentence);
            newSentenceList.add(Sentence.newBuilder().setText(sentence).build());
        }

        if (this.channel == null) {
            this.channel = ManagedChannelBuilder.forAddress(ttsIp, ttsPort).usePlaintext().build();
        }

        NgTtsServiceBlockingStub ttsStub = NgTtsServiceGrpc.newBlockingStub(this.channel);
        TtsCustomRequest ttsCustomRequest = TtsCustomRequest.newBuilder().setSpeaker(speaker).setAudioEncoding(audioEncoding).addAllSentences(newSentenceList).build();
        Iterator<TtsMediaResponse> iter = ((NgTtsServiceBlockingStub)ttsStub.withDeadlineAfter((long)timeout, TimeUnit.SECONDS)).speakWavCustom(ttsCustomRequest);
        String fileName = new String();
        if (!dictRoot.endsWith("/")) {
            dictRoot = dictRoot.concat("/");
        }

        try {
            fileName = AudioClient.makeWavFileName(dictRoot, speaker);
            FileOutputStream fos = new FileOutputStream(fileName);
            FileChannel fc = fos.getChannel();
            TtsMediaResponse response = null;

            while(iter.hasNext()) {
                response = (TtsMediaResponse)iter.next();
                ByteString data = response.getMediaData();
                ByteBuffer buff = data.asReadOnlyByteBuffer();
                fc.write(buff);
            }

            fos.close();
            fc.close();
        } catch (Exception var20) {
            logger.error(var20.getMessage());
        }

        return fileName;
    }

    public void callPhoneNumber(String dictRoot, int speaker, String text, StreamObserver<TtsMediaResponse> observer) {
        TtsMediaResponse response = null;

        try {
            String[] arrPhone = StringUtils.substringsBetween(text, "<phone>", "</phone>");
            int nLen = arrPhone.length;
            String[] strPhoneFileName = new String[nLen];
            String[] mergeFile = new String[nLen];
            new String();

            for(int i = 0; i < nLen; ++i) {
                String phone = arrPhone[i];
                phone = phone.replaceAll("[^0-9]", "");
                if (dictRoot.endsWith("/")) {
                    strPhoneFileName[i] = AudioClient.makePhoneWavFile(dictRoot.concat("phone/"), speaker, phone);
                } else {
                    strPhoneFileName[i] = AudioClient.makePhoneWavFile(dictRoot.concat("/phone/"), speaker, phone);
                }

                logger.info("callPhoneNumber:phoneFileName:" + strPhoneFileName[i]);
                if (i == 1) {
                    mergeFile[i] = AudioClient.mergeTwoWavFile(strPhoneFileName[i - 1], strPhoneFileName[i], dictRoot, speaker);
                    logger.info("callPhoneNumber:mergeFile:" + mergeFile[i]);
                }

                if (nLen > 2) {
                    break;
                }
            }

            byte[] data;
            ByteString bs;
            Path path;
            if (nLen == 1) {
                path = Paths.get(strPhoneFileName[0]);
                data = Files.readAllBytes(path);
                bs = ByteString.copyFrom(data);
                response = TtsMediaResponse.newBuilder().setMediaData(bs).build();
                observer.onNext(response);
            }

            if (nLen == 2) {
                path = Paths.get(mergeFile[1]);
                data = Files.readAllBytes(path);
                bs = ByteString.copyFrom(data);
                response = TtsMediaResponse.newBuilder().setMediaData(bs).build();
                observer.onNext(response);
            }
        } catch (Exception var14) {
            logger.error(var14.getMessage());
        }

    }

    public void callPhone2NumberAppend(String dictRoot, int speaker, String text, String bName, StreamObserver<TtsMediaResponse> observer) {
        try {
            String[] arrPhone = StringUtils.substringsBetween(text, "<phone_2>", "</phone_2>");
            String[] var7 = arrPhone;
            int var8 = arrPhone.length;

            for(int var9 = 0; var9 < var8; ++var9) {
                String phone = var7[var9];
                phone = phone.replaceAll("[^0-9]", "");
                new String();
                String fileName;
                if (dictRoot.endsWith("/")) {
                    fileName = AudioClient.makePhoneWavFile(dictRoot.concat("phone/"), speaker, phone);
                } else {
                    fileName = AudioClient.makePhoneWavFile(dictRoot.concat("/phone/"), speaker, phone);
                }

                logger.info("callPhone2NumberAppend:fileName:" + fileName);
                String mergeFile = AudioClient.mergeTwoWavFile(bName, fileName, dictRoot, speaker);
                Path path = Paths.get(mergeFile);
                byte[] data = Files.readAllBytes(path);
                ByteString bs = ByteString.copyFrom(data);
                TtsMediaResponse response = TtsMediaResponse.newBuilder().setMediaData(bs).build();
                observer.onNext(response);
            }
        } catch (Exception var17) {
            logger.error(var17.getMessage());
        }

    }

    public String savePhone2NumberAppend(String dictRoot, int speaker, String text, String bName) {
        String mergeFile = new String();

        try {
            String[] arrPhone = StringUtils.substringsBetween(text, "<phone_2>", "</phone_2>");
            String[] var7 = arrPhone;
            int var8 = arrPhone.length;

            for(int var9 = 0; var9 < var8; ++var9) {
                String phone = var7[var9];
                phone = phone.replaceAll("[^0-9]", "");
                new String();
                String fileName;
                if (dictRoot.endsWith("/")) {
                    fileName = AudioClient.makePhoneWavFile(dictRoot.concat("phone/"), speaker, phone);
                } else {
                    fileName = AudioClient.makePhoneWavFile(dictRoot.concat("/phone/"), speaker, phone);
                }

                logger.info("savePhone2NumberAppend:fileName:" + fileName);
                mergeFile = AudioClient.mergeTwoWavFile(bName, fileName, dictRoot, speaker);
            }
        } catch (Exception var12) {
            logger.error(var12.getMessage());
        }

        return mergeFile;
    }

    public String savePhone2Number(String dictRoot, int speaker, String text) {
        String fileName = new String();

        try {
            String[] arrPhone = StringUtils.substringsBetween(text, "<phone_2>", "</phone_2>");
            String[] var6 = arrPhone;
            int var7 = arrPhone.length;

            for(int var8 = 0; var8 < var7; ++var8) {
                String phone = var6[var8];
                phone = phone.replaceAll("[^0-9]", "");
                if (dictRoot.endsWith("/")) {
                    fileName = AudioClient.makePhoneWavFile(dictRoot.concat("phone/"), speaker, phone);
                } else {
                    fileName = AudioClient.makePhoneWavFile(dictRoot.concat("/phone/"), speaker, phone);
                }

                logger.info("savePhone2Number:fileName:" + fileName);
            }
        } catch (Exception var10) {
            logger.error(var10.getMessage());
        }

        return fileName;
    }

    public void callDigitNumber(String dictRoot, int speaker, String text, StreamObserver<TtsMediaResponse> observer) {
        try {
            String[] arrDigit = StringUtils.substringsBetween(text, "<digit>", "</digit>");
            String[] var6 = arrDigit;
            int var7 = arrDigit.length;

            for(int var8 = 0; var8 < var7; ++var8) {
                String digit = var6[var8];
                digit = digit.replaceAll("[^0-9]", "");
                new String();
                String fileName;
                if (dictRoot.endsWith("/")) {
                    fileName = AudioClient.makeDigitWavFile(dictRoot.concat("digit/"), speaker, digit);
                } else {
                    fileName = AudioClient.makeDigitWavFile(dictRoot.concat("/digit/"), speaker, digit);
                }

                logger.info("callDigitNumber:fileName:" + fileName);
                Path path = Paths.get(fileName);
                byte[] data = Files.readAllBytes(path);
                ByteString bs = ByteString.copyFrom(data);
                TtsMediaResponse response = TtsMediaResponse.newBuilder().setMediaData(bs).build();
                observer.onNext(response);
            }
        } catch (Exception var15) {
            logger.error(var15.getMessage());
        }

    }

    public void streamWaveFile(String fileName, StreamObserver<TtsMediaResponse> observer) {
        try {
            Path path = Paths.get(fileName);
            byte[] data = Files.readAllBytes(path);
            ByteString bs = ByteString.copyFrom(data);
            TtsMediaResponse response = TtsMediaResponse.newBuilder().setMediaData(bs).build();
            observer.onNext(response);
        } catch (Exception var7) {
            logger.error(var7.getMessage());
        }

    }

    public void callCoreTTS(String ttsIp, int ttsPort, int timeout, int speaker, ArrayList<String> sentenceList, AudioEncoding audioEncoding, StreamObserver<TtsMediaResponse> observer) {
        List<Sentence> newSentenceList = new ArrayList();
        new String();

        for(int i = 0; i < sentenceList.size(); ++i) {
            String sentence = ((String)sentenceList.get(i)).trim();
            logger.info("callCoreTTS:speaker:" + speaker + "sentence:" + sentence);
            newSentenceList.add(Sentence.newBuilder().setText(sentence).build());
        }

        if (this.channel == null) {
            this.channel = ManagedChannelBuilder.forAddress(ttsIp, ttsPort).usePlaintext().build();
        }

        NgTtsServiceBlockingStub ttsStub = NgTtsServiceGrpc.newBlockingStub(this.channel);
        TtsCustomRequest ttsCustomRequest = TtsCustomRequest.newBuilder().setSpeaker(speaker).setAudioEncoding(audioEncoding).addAllSentences(newSentenceList).build();
        Iterator<TtsMediaResponse> iter = ((NgTtsServiceBlockingStub)ttsStub.withDeadlineAfter((long)timeout, TimeUnit.SECONDS)).speakWavCustom(ttsCustomRequest);
        TtsMediaResponse response = null;

        while(iter.hasNext()) {
            response = (TtsMediaResponse)iter.next();
            observer.onNext(response);
        }

    }
}
