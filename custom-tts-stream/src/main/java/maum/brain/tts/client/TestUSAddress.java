package maum.brain.tts.client;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestUSAddress {
    public TestUSAddress() {
    }

    public static void main(String[] args) {
        String[] addresses = new String[]{"701 FIFTH AVE", "11318 Lazarro Ln", "465 Columbus Avenue", "2157 Henderson Highway", "1940 DUKE STREET", "1 Almaden Boulevard", "2040 MAIN STREET", "465 Columbus Avenue", "28 STATE STREET", "1441 SEAMIST DR.", "2200 CLARENDON BLVD.", "999 Peachtree Street NE", "300 North Meridian Street", "840 NORTH PLANKINTON AVENUE", "111 MONUMENT CIRCLE, SUITE 3700", "2000 PENNSYLVANIA AVENUE, N.W.", "1025 Connecticut Avenue, NW", "799 Ninth Street, NW", "1130 Connecticut Ave., NW, Suite 420", "BANNER & WITCOFF, LTD", "CHIP LAW GROUP", "HAMMER & ASSOCIATES, P.C.", "MH2 TECHNOLOGY LAW GROUP, LLP", "HOLLYWOOD, FL 33022-2480", "Attn: Patent Docketing", "c/o Armstrong Teasdale LLP", "C/O Ballard Spahr LLP", "P.O. BOX 2903", "P.O, Drawer 800889."};
        String[] patterns = new String[]{"C/O [\\w ]{2,}", "P.O[.,] [\\w ]{2,}", "[A-Z]+(,)? [A-Z]+ (\\d+)-(\\d+)", "[\\w]{2,5} BOX [\\d]{2,8}", "^[#\\d]{1,7} ([\\w]+)?(,)? ([\\w.]+)?([,|.])?[\\s]?([A-Z.]+)?([,|.])?[\\s]?([\\d\\w]+)?[\\s]?([\\d]+)?", "ATTN: [A-Z]+ [A-Z]+", "ATTENTION: [\\w]{2,}"};
        Pattern p = null;
        Matcher m = null;

        for(int i = 0; i < addresses.length; ++i) {
            String address = addresses[i];

            for(int j = 0; j < patterns.length; ++j) {
                p = Pattern.compile(patterns[j], 2);
                m = p.matcher(address);

                while(m.find()) {
                    System.out.println("[" + i + "][" + j + "]" + m.group(0));
                }
            }
        }

    }
}
