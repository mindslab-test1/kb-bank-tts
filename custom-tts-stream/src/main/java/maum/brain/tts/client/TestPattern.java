package maum.brain.tts.client;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestPattern {
    private static final Pattern PATTERN_BRACKET = Pattern.compile("[\\(]([^\\(|\\)])*[\\)]");
    private static final String VOID = "";

    public TestPattern() {
    }

    public static void main(String[] args) {
        String case1 = "(주)신한은행입니다. 일시상환방식으로 고객님의 프리미엄론 가능금액은 최고 12000000원이고 대출기간은 36 개월로 신청가능합니다. 고객님의 자격유효기간은 2020년 07월 31일까지이고 이자율은 2020년 07월 31일까지 정상 이자율인 16.05%에서 20% 할인되어 실제 적용 이자율은 12.84%입니다.";
        System.out.println(deleteBracketText(case1));
        System.out.println(findBracketText(case1));
    }

    private static String deleteBracketText(String text) {
        Matcher m = PATTERN_BRACKET.matcher(text);
        String pureText = text;
        new String();

        while(m.find()) {
            int start = m.start();
            int end = m.end();
            String removeTextArea = pureText.substring(start, end);
            pureText = pureText.replace(removeTextArea, "");
            m = PATTERN_BRACKET.matcher(pureText);
        }

        return pureText;
    }

    private static List<String> findBracketText(String text) {
        ArrayList<String> bracketList = new ArrayList();
        Matcher m = PATTERN_BRACKET.matcher(text);
        String pureText = text;
        new String();

        while(m.find()) {
            int start = m.start();
            int end = m.end();
            String findText = pureText.substring(start, end);
            pureText = pureText.replace(findText, "");
            m = PATTERN_BRACKET.matcher(pureText);
            bracketList.add(findText);
        }

        return bracketList;
    }
}
