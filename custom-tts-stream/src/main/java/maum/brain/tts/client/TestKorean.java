package maum.brain.tts.client;

import java.lang.Character.UnicodeBlock;
import maum.brain.tts.utils.PPUtils;

public class TestKorean {
    public TestKorean() {
    }

    public static void main(String[] args) {
        char[] chars = "ㅋㅋㅋㅠㅠ한engABC123-~!@#$%^&*()_".toCharArray();

        for(int i = 0; i < chars.length; ++i) {
            char ch = chars[i];
            UnicodeBlock cb = UnicodeBlock.of(ch);
            System.out.println(i + " : " + ch + " : " + cb);
        }

        System.out.println(PPUtils.removeKoreanConsonantAndVowel(new String(chars)));
    }
}
