package maum.brain.tts.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;

public class TestRestClient {

//    @Value("${tts.ip}")
//    private static String ttsIp;
    private static String ttsIp = "http://10.122.65.229:9998";

    public TestRestClient() {
    }

    public static void main(String[] args) {
        downloadWav();
    }

    private static void downloadWav() {
        try {
            String tempname = Long.valueOf((new Date()).getTime()).toString();
            JSONObject json = new JSONObject();
            json.put("fileName", tempname);
            json.put("lang", "ko_KR");
            json.put("samplerate", "22050");
            json.put("profile", "1");
            json.put("text", "안녕하세요.");
            String body = json.toString();
            System.out.println("Reuqest to Rest API Server");
            System.out.println(body);
            URL postUrl = new URL(ttsIp+"/tts-rest/tts/block/speak");
            HttpURLConnection con = (HttpURLConnection)postUrl.openConnection();
            con.setDoOutput(true);
            con.setInstanceFollowRedirects(false);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            OutputStream os = con.getOutputStream();
            os.write(body.getBytes());
            os.flush();
            os.close();
            int responseCode = con.getResponseCode();
            if (responseCode == 200) {
                System.out.println("Response from Rest API Server");
                InputStream is = con.getInputStream();
//                int read = false;
                byte[] bytes = new byte[1024];
                File f = new File("C:/Temp/" + tempname + ".wav");
                f.createNewFile();
                FileOutputStream ops = new FileOutputStream(f);

                while(true) {
                    int read;
                    if ((read = is.read(bytes)) == -1) {
                        is.close();
                        ops.close();
                        System.out.println("file=" + f.getAbsolutePath());
                        break;
                    }

                    ops.write(bytes, 0, read);
                }
            }

            con.disconnect();
        } catch (Exception var12) {
            var12.printStackTrace();
        }

    }
}
