package maum.brain.tts.client;

import maum.brain.tts.preprocess.kor.ConvertRange;

public class TestReplace {
    public TestReplace() {
    }

    public static void main(String[] agrs) {
        String sentence = "<range>07:00~13:30</range>";
        String range = "07:00~13:30";
        String newRange = range.replace("~", "-");
        String newToken = ConvertRange.convertRangeFormat(newRange);
        String oriToken = "<range>".concat(range).concat("</range>");
        System.out.println(sentence);
        System.out.println(oriToken);
        System.out.println(newToken);
        sentence = sentence.replace(oriToken, newToken);
        System.out.println(sentence);
    }
}