package maum.brain.tts.client;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MakeEng2Kor {
    public static Hashtable<String, String> dict = new Hashtable();
    private static String eng2hanFile = "C:/eclipse/dict/eng2kor.txt";

    public MakeEng2Kor() {
    }

    public static void main(String[] args) {
    }

    public static String convertEnglish(String token) {
        String value = new String();

        try {
            if (dict.isEmpty()) {
                readTextFile(eng2hanFile);
            }

            value = (String)dict.get(token);
            if (value == null || value.isEmpty()) {
                return token;
            }
        } catch (Exception var3) {
            var3.getMessage();
        }

        return value;
    }

    public static void readTextFile(String txtFile) throws Exception {
        eng2hanFile = txtFile;
        FileReader fr = new FileReader(txtFile);
        BufferedReader br = new BufferedReader(fr);
        new String();
        String[] st = null;

        String line;
        while((line = br.readLine()) != null) {
            st = line.split("=");
            dict.put(st[0].toLowerCase(), st[1]);
        }

        br.close();
        fr.close();
    }

    public static List<String> extractHangle(String text) {
        List<String> list = new ArrayList();
        Pattern pattern = Pattern.compile("([a-zA-Z]+)(\\s[ㄱ-ㅎ가-힣]+)");
        Matcher m = pattern.matcher(text);
        String[] var10000 = new String[]{"", ""};

        while(m.find()) {
            String[] s = m.group(0).replaceAll("\\s+", "=").split("=");
            list.add(m.group(0).replaceAll("\\s+", "="));
            dict.put(s[0].toLowerCase(), s[1].trim());
        }

        return list;
    }
}
