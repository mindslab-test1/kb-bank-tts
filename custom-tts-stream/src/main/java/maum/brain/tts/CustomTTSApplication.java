package maum.brain.tts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({CustomTTSConfig.class})
public class CustomTTSApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomTTSApplication.class, args);
	}

}
