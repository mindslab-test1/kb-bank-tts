package maum.brain.tts.server;

import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import maum.brain.tts.AudioEncoding;
import maum.brain.tts.PreprocessingRequest;
import maum.brain.tts.PreprocessingResponse;
import maum.brain.tts.Sentence;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.tts.TtsRequest;
import maum.brain.tts.NgTtsServiceGrpc.NgTtsServiceImplBase;
import maum.brain.tts.client.AudioClient;
import maum.brain.tts.client.EnglishCoreClient;
import maum.brain.tts.client.EnglishG2PClient;
import maum.brain.tts.client.KoreanCoreClient;
import maum.brain.tts.client.KoreanG2PClient;
import maum.brain.tts.exception.TtsException;
import maum.brain.tts.preprocess.eng.ConvertAbbreviation;
import maum.brain.tts.preprocess.eng.ConvertAcronym;
import maum.brain.tts.preprocess.eng.ConvertContraction;
import maum.brain.tts.preprocess.eng.EnglishTokenizer;
import maum.brain.tts.preprocess.eng.PreProcessEng;
import maum.brain.tts.preprocess.kor.ConvertEnglish;
import maum.brain.tts.preprocess.kor.ConvertHanja;
import maum.brain.tts.preprocess.kor.ConvertReplaceWord;
import maum.brain.tts.preprocess.kor.PreProcess;
import maum.common.LangOuterClass.Lang;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class CustomTTSService extends NgTtsServiceImplBase {
    private static Logger logger = LoggerFactory.getLogger(CustomTTSService.class);
    @Autowired
    private Environment env;
    @Value("${tts.resources.preprocess.root}")
    private String dictRoot;
    private static KoreanCoreClient _ko_coreClient = null;
    private static EnglishCoreClient _en_coreClient = null;
    private static String dict_eng2kor = "tts.kor.dict.eng2kor.properties";
    private static String dict_replaceKor = "tts.kor.dict.replace.properties";
    private static String dict_hanja2kor = "tts.kor.dict.hanja2kor.xml";
    private static String dict_AbbreviationEng = "tts.eng.dict.abbreviation.properties";
    private static String dict_AcronymEng = "tts.eng.dict.acronym.properties";
    private static String dict_ContractionEng = "tts.eng.dict.contraction.properties";
    public static String site = "KBCard";
    private static KoreanG2PClient g2p_kor;
    private static EnglishG2PClient g2p_eng;
    private static final String cmuPhonetic = "AA AE AH AO AW AY B CH D DH EH ER EY F G HH IH IY JH K L M N NG OW OY P R S SH T TH UH UW V W Y Z ZH";

    public CustomTTSService() {
    }

    @PostConstruct
    public void init() {
        try {
            site = System.getProperty("site");
            if (site == null || "Mindslab".equals(site) || "KBCard".equals(site) || "Shinhan".equals(site)) {
                if (!this.dictRoot.endsWith("/")) {
                    this.dictRoot = this.dictRoot.concat("/");
                }

                logger.info("Invoke English Dictionary(Korean):" + this.dictRoot + dict_eng2kor);
                ConvertEnglish.readTextFile(this.dictRoot + dict_eng2kor);
                logger.info("Invoke Replce Words Dictionary(Korean):" + this.dictRoot + dict_replaceKor);
                ConvertReplaceWord.readTextFile(this.dictRoot + dict_replaceKor);
                logger.info("Invoke Hanja Dictionary(Korean):" + this.dictRoot + dict_hanja2kor);
                ConvertHanja.loadXMLData(this.dictRoot + dict_hanja2kor);
                logger.info("Initiate Phone Directory(Korean):" + this.dictRoot + "phone/");
                AudioClient.deleteFileList(this.dictRoot + "phone/", "model", 12);
                logger.info("Initiate Digit Directory(Korean):" + this.dictRoot + "digit/");
                AudioClient.deleteFileList(this.dictRoot + "digit/", "model", 12);
                g2p_kor = new KoreanG2PClient();
            }

            if ("KBCard".equals(site)) {
                if (!this.dictRoot.endsWith("/")) {
                    this.dictRoot = this.dictRoot.concat("/");
                }

                logger.info("Allocate English Sentence:" + this.dictRoot + "en-sent.bin");
                EnglishTokenizer.sentenceFile = this.dictRoot + "en-sent.bin";
                logger.info("Allocate English token:" + this.dictRoot + "en-token.bin");
                EnglishTokenizer.tokenFile = this.dictRoot + "en-token.bin";
                logger.info("Invoke Abbreviation Dictionary(English):" + this.dictRoot + dict_AbbreviationEng);
                ConvertAbbreviation.allocateDictionary(this.dictRoot + dict_AbbreviationEng);
                logger.info("Invoke Acronym Dictionary(English):" + this.dictRoot + dict_AcronymEng);
                ConvertAcronym.allocateDictionary(this.dictRoot + dict_AcronymEng);
                logger.info("Invoke Contraction Dictionary(English):" + this.dictRoot + dict_ContractionEng);
                ConvertContraction.allocateDictionary(this.dictRoot + dict_ContractionEng);
                g2p_eng = new EnglishG2PClient();
            }
        } catch (Exception var2) {
            logger.error("init:" + var2.getMessage());
        }

    }

    public void preprocess(PreprocessingRequest request, StreamObserver<PreprocessingResponse> responseObserver) {
        String text = request.getText();
        Lang lang = request.getLang();
        logger.info("[preprocess]Lang=" + request.getLang().toString() + ":text=" + text);
        new ArrayList();

        try {
            ArrayList sentenceList;
            List g2pSentenceList;
            StringBuilder sb;
            int i;
            if (lang == Lang.en_US) {
                sentenceList = PreProcessEng.getTTSSentence(text);
                if (g2p_eng == null) {
                    g2p_eng = new EnglishG2PClient();
                }

                g2pSentenceList = g2p_eng.callG2p(sentenceList);
                sb = new StringBuilder();

                for(i = 0; i < g2pSentenceList.size(); ++i) {
                    sb.append((String)g2pSentenceList.get(i));
                }

                responseObserver.onNext(PreprocessingResponse.newBuilder().setText(sb.toString()).build());
                responseObserver.onCompleted();
            }

            if (lang == Lang.ko_KR) {
                sentenceList = PreProcess.getTTSSentence(text);
//                StringBuilder sb;
//                int i;
                if ("Mindslab".equals(site)) {
                    sb = new StringBuilder();

                    for(i = 0; i < sentenceList.size(); ++i) {
                        sb.append((String)sentenceList.get(i));
                    }

                    responseObserver.onNext(PreprocessingResponse.newBuilder().setText(sb.toString()).build());
                    responseObserver.onCompleted();
                } else if ("KBCard".equals(site)) {
                    if (g2p_kor == null) {
                        g2p_kor = new KoreanG2PClient();
                    }

                    g2pSentenceList = g2p_kor.callG2p(sentenceList);
                    sb = new StringBuilder();

                    for(i = 0; i < g2pSentenceList.size(); ++i) {
                        sb.append((String)g2pSentenceList.get(i));
                    }

                    responseObserver.onNext(PreprocessingResponse.newBuilder().setText(sb.toString()).build());
                    responseObserver.onCompleted();
                } else if ("Shinhan".equals(site)) {
                    sb = new StringBuilder();

                    for(i = 0; i < sentenceList.size(); ++i) {
                        sb.append((String)sentenceList.get(i));
                    }

                    responseObserver.onNext(PreprocessingResponse.newBuilder().setText(sb.toString()).build());
                    responseObserver.onCompleted();
                } else {
                    if (g2p_kor == null) {
                        g2p_kor = new KoreanG2PClient();
                    }

                    g2pSentenceList = g2p_kor.callG2p(sentenceList);
                    sb = new StringBuilder();

                    for(i = 0; i < g2pSentenceList.size(); ++i) {
                        sb.append((String)g2pSentenceList.get(i));
                    }

                    responseObserver.onNext(PreprocessingResponse.newBuilder().setText(sb.toString()).build());
                    responseObserver.onCompleted();
                }
            }
        } catch (Exception var9) {
            logger.error("[preprocess]" + var9.getMessage());
            responseObserver.onError(getGrpcException(99999, var9.getMessage()));
        }

    }

    public void speakWav(TtsRequest request, StreamObserver<TtsMediaResponse> responseObserver) {
        String text = request.getText().trim();
        int speakerId = request.getSpeaker();
        AudioEncoding audioEncoding = request.getAudioEncoding();
        Lang lang = request.getLang();
        logger.info("[speakWav]lang=" + request.getLang().toString() + ":text=" + text);
        boolean bPhone = Boolean.FALSE;
        boolean bPhone2 = Boolean.FALSE;
        boolean bDigit = Boolean.FALSE;

        try {
            if (StringUtils.isEmpty(text)) {
                logger.error("[speakWav]text is blank.");
                responseObserver.onError(getGrpcException(99999, "Empty requested."));
                return;
            }

            int timeout = Integer.parseInt(this.env.getProperty("tts.job.timeout"));
            int maxlength = Integer.parseInt(this.env.getProperty("tts.maxlength"));
            if (text.length() > maxlength) {
                logger.error("[speakWav]exceeded maxlength");
                responseObserver.onError(getGrpcException(60001, "exceeded sentence maximum length."));
                return;
            }

            new ArrayList();
            ArrayList sentenceList;
            String site;
            if (lang == Lang.en_US) {
                List<Sentence> newSentenceList = new ArrayList();
                String chkText = text.replaceAll("\\{", " ").replaceAll("}", " ").replaceAll("[.]", " ");
                if (isPhonetic(chkText)) {
                    newSentenceList.add(Sentence.newBuilder().setText(text).build());
                } else {
                    if (g2p_eng == null) {
                        g2p_eng = new EnglishG2PClient();
                    }

                    sentenceList = PreProcessEng.getTTSSentence(text);
                    List<String> g2pSentenceList = g2p_eng.callG2p(sentenceList);
                    new String();

                    for(int i = 0; i < g2pSentenceList.size(); ++i) {
                        String sentence = ((String)g2pSentenceList.get(i)).trim();
                        logger.info("[speakWav]sentence:" + sentence);
                        newSentenceList.add(Sentence.newBuilder().setText(sentence).build());
                    }
                }

                site = this.env.getProperty("tts.en_US.tts.ip");
                int corePort = Integer.parseInt(this.env.getProperty("tts.en_US.tts.port"));
                if (_en_coreClient == null) {
                    _en_coreClient = new EnglishCoreClient();
                }

                logger.info("[speakWav]speaker={}:port={}", speakerId, corePort);
                _en_coreClient.callCoreTTSAfterG2P(site, corePort, timeout, 0, newSentenceList, audioEncoding, responseObserver);
                responseObserver.onCompleted();
            }

            if (lang == Lang.ko_KR) {
                if (text.startsWith("<phone>") && text.endsWith("</phone>")) {
                    bPhone = true;
                }

                if (text.startsWith("<digit>") && text.endsWith("</digit>")) {
                    bDigit = true;
                }

                if (text.contains("<phone_2>") && text.contains("</phone_2>")) {
                    bPhone2 = true;
                }

                sentenceList = PreProcess.getTTSSentence(text);
                String coreIp = this.env.getProperty("tts.ko_KR.tts.ip");
                int corePort = Integer.parseInt(this.env.getProperty("tts.ko_KR.tts.port"));
                if (_ko_coreClient == null) {
                    _ko_coreClient = new KoreanCoreClient();
                }

                logger.info("[speakWav]speaker={}:port={}", speakerId, corePort);
                site = System.getProperty("site");
                if (site == null || site.isEmpty()) {
                    site = "KBCard";
                }

                if ("Mindslab".equals(site)) {
                    _ko_coreClient.callCoreTTS(coreIp, corePort, timeout, 0, sentenceList, audioEncoding, responseObserver);
                } else if (!"KBCard".equals(site)) {
                    if ("Shinhan".equals(site)) {
                        _ko_coreClient.callCoreTTS(coreIp, corePort, timeout, 0, sentenceList, audioEncoding, responseObserver);
                    } else {
                        _ko_coreClient.callCoreTTSAfterG2P(coreIp, corePort, timeout, 0, sentenceList, audioEncoding, responseObserver);
                    }
                } else {
                    if (bPhone) {
                        _ko_coreClient.callPhoneNumber(this.dictRoot, speakerId, text, responseObserver);
                    }

                    if (!bPhone && bDigit) {
                        _ko_coreClient.callDigitNumber(this.dictRoot, speakerId, text, responseObserver);
                    }

                    if (!bPhone && !bDigit && bPhone2) {
                        ArrayList<String> newSentenceList = new ArrayList();
                        String lastFileName = new String();

                        String sentence;
                        for(int i = 0; i < sentenceList.size(); ++i) {
                            sentence = ((String)sentenceList.get(i)).trim();
                            logger.info("[speakWav]sentence={}", sentence);
                            if (sentence.startsWith("<phone_2>") && sentence.endsWith("</phone_2>")) {
                                String fileName = new String();
                                if (newSentenceList.size() > 0) {
                                    fileName = _ko_coreClient.saveCoreTTSAfterG2P(this.dictRoot, coreIp, corePort, timeout, 0, newSentenceList, audioEncoding);
                                    logger.info("[speakWav]fileName={}", fileName);
                                    newSentenceList.clear();
                                }

                                if (fileName.length() > 0) {
                                    lastFileName = _ko_coreClient.savePhone2NumberAppend(this.dictRoot, speakerId, sentence, fileName);
                                } else {
                                    lastFileName = _ko_coreClient.savePhone2Number(this.dictRoot, speakerId, sentence);
                                }
                            } else {
                                newSentenceList.add(sentence);
                            }
                        }

//                        new String();
//                        new String();
                        if (newSentenceList.size() > 0) {
                            String afterFile = _ko_coreClient.saveCoreTTSAfterG2P(this.dictRoot, coreIp, corePort, timeout, 0, newSentenceList, audioEncoding);
                            if (lastFileName.length() > 0) {
                                sentence = AudioClient.mergeTwoWavFile(lastFileName, afterFile, this.dictRoot, speakerId);
                                _ko_coreClient.streamWaveFile(sentence, responseObserver);
                            } else {
                                _ko_coreClient.streamWaveFile(afterFile, responseObserver);
                            }
                        } else {
                            _ko_coreClient.streamWaveFile(lastFileName, responseObserver);
                        }
                    }

                    if (!bPhone && !bDigit && !bPhone2) {
                        _ko_coreClient.callCoreTTSAfterG2P(coreIp, corePort, timeout, 0, sentenceList, audioEncoding, responseObserver);
                    }
                }

                responseObserver.onCompleted();
            }
        } catch (Exception var21) {
            logger.error("[speakWav]core gRPC Server Error. {}", var21.getMessage());
            responseObserver.onError(getGrpcException(99999, var21.getMessage()));
        }

    }

    private static boolean isPhonetic(String sentence) {
        String[] split = sentence.split(" ");

        for(int i = 0; i < split.length; ++i) {
            String token = split[i];
            if (!token.isEmpty() && !"AA AE AH AO AW AY B CH D DH EH ER EY F G HH IH IY JH K L M N NG OW OY P R S SH T TH UH UW V W Y Z ZH".contains(token)) {
                return false;
            }
        }

        return true;
    }

    private static StatusRuntimeException getGrpcException(int errCode, String description) {
        return Status.INTERNAL.withDescription(description).withCause(new TtsException(errCode, description)).asRuntimeException();
    }
}
