package maum.brain.tts;

public class CommonCode {
    public static final int SERVER_NOT_FOUND = 40002;
    public static final int NOT_SUPPORTED_LANG = 40003;
    public static final String MSG_SERVER_NOT_FOUND = "not found server.";
    public static final String MSG_NOT_SUPPORTED_LANG = "not supported language.";
    public static final int EXCEEDED_TEXT_LENGTH = 60001;
    public static final int EXCEEDED_SPEAKER = 60002;
    public static final String MSG_EXCEEDED_TEXT_LENGTH = "exceeded sentence maximum length.";
    public static final String MSG_EXCEEDED_SPEAKER = "exceeded speaker number.";
    public static final int PP_ERROR = 99998;
    public static final int SERVICE_ERROR = 99999;
    public static final String MSG_PP_ERROR = "Pre-Processing error.";
    public static final String MSG_SERVICE_ERROR = "Service error.";
    public static final String MSG_EMPTY_ERROR = "Empty requested.";

    public CommonCode() {
    }

    public static enum Lang {
        ko_KR,
        en_US;

        private Lang() {
        }
    }

    public static enum JobStaus {
        notexist,
        started,
        running,
        completed,
        error;

        private JobStaus() {
        }
    }
}