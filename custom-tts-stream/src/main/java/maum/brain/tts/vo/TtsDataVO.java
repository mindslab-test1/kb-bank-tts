package maum.brain.tts.vo;

import org.springframework.stereotype.Component;

@Component
public class TtsDataVO {
    private String table_id;
    private String table_name;
    private String data_before;
    private String data_after;

    public TtsDataVO() {
    }

    public String getData_before() {
        return this.data_before;
    }

    public void setData_before(String data_before) {
        this.data_before = data_before;
    }

    public String getData_after() {
        return this.data_after;
    }

    public void setData_after(String data_after) {
        this.data_after = data_after;
    }

    public String getTable_id() {
        return this.table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getTable_name() {
        return this.table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }
}
