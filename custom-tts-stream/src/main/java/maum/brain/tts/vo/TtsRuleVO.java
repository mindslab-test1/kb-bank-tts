package maum.brain.tts.vo;

import org.springframework.stereotype.Component;

@Component
public class TtsRuleVO {
    private String rule_expression;
    private String rule_id;
    private String rule_name;

    public TtsRuleVO() {
    }

    public String getRule_name() {
        return this.rule_name;
    }

    public void setRule_name(String rule_name) {
        this.rule_name = rule_name;
    }

    public String getRule_expression() {
        return this.rule_expression;
    }

    public void setRule_expression(String rule_expression) {
        this.rule_expression = rule_expression;
    }

    public String getRule_id() {
        return this.rule_id;
    }

    public void setRule_id(String rule_id) {
        this.rule_id = rule_id;
    }
}
