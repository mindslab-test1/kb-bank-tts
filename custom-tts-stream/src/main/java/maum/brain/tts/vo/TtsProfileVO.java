package maum.brain.tts.vo;

import org.springframework.stereotype.Component;

@Component
public class TtsProfileVO {
    private String process_order;
    private String rule_id;
    private String data_table_id;

    public TtsProfileVO() {
    }

    public String getProcess_order() {
        return this.process_order;
    }

    public void setProcess_order(String process_order) {
        this.process_order = process_order;
    }

    public String getRule_id() {
        return this.rule_id;
    }

    public void setRule_id(String rule_id) {
        this.rule_id = rule_id;
    }

    public String getData_table_id() {
        return this.data_table_id;
    }

    public void setData_table_id(String data_table_id) {
        this.data_table_id = data_table_id;
    }
}
