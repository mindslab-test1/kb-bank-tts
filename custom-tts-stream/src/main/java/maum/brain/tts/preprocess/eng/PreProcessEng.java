package maum.brain.tts.preprocess.eng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import maum.brain.tts.preprocess.type.EngPatternType;
import maum.brain.tts.utils.PPUtils;
import org.apache.commons.lang3.StringUtils;

public class PreProcessEng {
    private static boolean bLog;

    static {
        bLog = Boolean.FALSE;
    }

    public PreProcessEng() {
    }

    public static void main(String[] args) {
        String paragraph = "His barber kept his word. \nBut keeping such a huge secret to himself was driving him crazy. \nFinally, the barber went up a mountain and almost to the edge of a cliff. \nHe dug a hole in the midst of some reeds. He looked about, to make sure no one was near. \n";
        paragraph = "10,000 will be pronounced ten thousand.";
        paragraph = "256 will be pronounced two hundred (and) fifty six.";
        paragraph = "4358 will be pronounced four thousand three hundred (and) fifty eight.";
        paragraph = "1,000 will be pronounced one thousand.";
        paragraph = "+5 will be pronounced plus five.";
        paragraph = "−3,000 will be pronounced minus three thousand.";
        paragraph = "4.5 will be pronounced four point five.";
        paragraph = "-3.1 will be pronounced minus three point one.";
        paragraph = "1,000.12 will be pronounced one thousand point one two.";
        paragraph = "21st will be pronounced twenty first.";
        paragraph = "42nd will be pronounced forty second.";
        paragraph = "6th will be pronounced sixth.";
        paragraph = "1,000,000th will be pronounced one millionth.";
        paragraph = "60s will be pronounced sixties.";
        paragraph = "100s will be pronounced one hundreds.";
        paragraph = "bc 52's will be pronounced b c fifty twos.";
        paragraph = "B.C. 52's will be pronounced b c fifty twos.";
        paragraph = "MDCCCLXXXVIII";
        paragraph = "LIst will be pronounced fifty first.";
        paragraph = "MMXIth will be pronounced two thousand eleventh.";
        paragraph = "Queen Elizabeth II will be pronounced queen elizabeth the second.";
        paragraph = "Henry III of England will be pronounced henry the third of england.";
        paragraph = "Chapter XIX will be pronounced chapter nineteen.";
        paragraph = "World War II will be pronounced world war two.";
        paragraph = "xxiii will be pronounced twenty three.";
        paragraph = "15⁄5678 will be pronounced fifteen five thousand six hundred seventy eighths.";
        paragraph = "1/2 will be pronounced one half.";
        paragraph = "1/4 will be pronounced one quarter.";
        paragraph = "1/3 will be pronounced one third.";
        paragraph = "3/4 will be pronounced three quarters.";
        paragraph = "1/8 will be pronounced one eighth.";
        paragraph = "2/3 will be pronounced two thirds.";
        paragraph = "6/8 will be pronounced six eighths.";
        paragraph = "fl oz will be pronounced fluid ounce.";
        paragraph = "14'5\" will be pronounced fourteen feet five inches.";
        paragraph = "10' will be pronounced ten feet.";
        paragraph = "10\" will be pronounced ten inches.";
        paragraph = "01:02:30 will be pronounced one hour two minutes thirty seconds.";
        paragraph = "1h2m30s will be pronounced one hour two minutes thirty seconds.";
        paragraph = "1 tbsp will be pronounced one tablespoon.";
        paragraph = "5 tsp will be pronounced five teaspoons.";
        paragraph = "2.6 GHz will be pronounced two point six gigahertz.";
        paragraph = "25 MPH will be pronounced twenty five miles per hour.";
        paragraph = "8 nmi will be pronounced eight nautical miles.";
        paragraph = "-0.01% will be pronounced minus zero point zero one percent.";
        paragraph = "90° will be pronounced ninety degrees.";
        paragraph = "50¢ will be pronounced fifty cents.";
        paragraph = "40 km/h will be pronounced forty kilometers per hour.";
        paragraph = "1㎝ 10cm";
        paragraph = "1㎞ 10km";
        paragraph = "$10 will be pronounced ten dollars.";
        paragraph = "USD5.27 will be pronounced five u s dollars and twenty seven cents.";
        paragraph = "£5.27 will be pronounced five pounds and twenty seven pence.";
        paragraph = "GBP1,000 will be pronounced one thousand pounds.";
        paragraph = "€20000 will be pronounced twenty thousand euros.";
        paragraph = "EUR20,000 will be pronounced twenty thousand euros.";
        paragraph = "1:59 will be pronounced one fifty nine.";
        paragraph = "2:00 will be pronounced two a m.";
        paragraph = "01:59am will be pronounced one fifty nine a m.";
        paragraph = "2 AM will be pronounced two a m.";
        paragraph = "13:00 will be pronounced one p m.";
        paragraph = "10:25:30 will be pronounced ten hours twenty five minutes thirty seconds.";
        paragraph = "07:53:10 A.M. will be pronounced seven hours fifty three minutes ten seconds a m.";
        paragraph = "Today is 01/01/2021.  Today is 1.1.2021. Today is 01-01-2021.";
        paragraph = "12/31/1999 will be pronounced december thirty first nineteen ninety nine.";
        paragraph = "10-25-99 will be pronounced october twenty fifth nineteen ninety nine.";
        paragraph = "Dec/31/1999 will be pronounced december thirty first nineteen ninety nine.";
        paragraph = "April-25-1999 will be pronounced april twenty fifth nineteen ninety nine.";
        paragraph = "12/may/1995 will be pronounced may twelfth nineteen ninety five.";
        paragraph = "12-Apr-2007 will be pronounced april twelfth two thousand seven.";
        paragraph = "20.3.2011 will be pronounced march twentieth twenty eleven.";
        paragraph = "20-03-2011 will be pronounced march twentieth twenty eleven.";
        paragraph = "2007/01/01 will be pronounced january first two thousand seven.";
        paragraph = "2007-Jan-01 will be pronounced january first two thousand seven.";
        paragraph = "2007-January-01 will be pronounced january first two thousand seven.";
        paragraph = "June 2 will be pronounced june second.";
        paragraph = "Aug. 5, 1921 will be pronounced august fifth nineteen twenty one.";
        paragraph = "which matches dates of format 'january 31, 1976', '9/18/2013', and '11-20-1988'. This works fine for those formats, but I'm now encountering dates in the European '26th May, 2020' format.";
        paragraph = "1063 A.D. will be pronounced ten sixty three a d";
        paragraph = "1063A.D. will be pronounced ten sixty three a d";
        paragraph = "ages 3–5 will be pronounced ages three to five.";
        paragraph = "June 15-20 will be pronounced june fifteenth to twentieth.";
        paragraph = "1939-1945 will be pronounced nineteen thirty nine to nineteen forty five.";
        paragraph = "40Hz-20KHz will be pronounced forty hertz to twenty kilohertz.";
        paragraph = "20.6KHz will be pronounced twenty point six kilohertz.";
        paragraph = "i.e.";
        paragraph = "Mr vs bros Inc. will be interpreted as that is mister versus brothers incorporated.";
        paragraph = "<address>159 W. Popplar Av., Ste. 5, St. George, CA 12345</address> will be pronounced one fifty nine west popplar avenue, suite five, saint george california one two three four five.";
        paragraph = "<address>P.O. Box 778, Dover, DE 19903</address>";
        paragraph = "<phone>(978) 555-2345</phone> will be pronounced as nine seven eight five five five two three four five.";
        paragraph = "<phone>1-800-555-1234 ex. 10</phone> will be pronounced as one eight hundred five five five one two three four extension one zero.";
        paragraph = "<url>http://www.mindslab.ai</url> will be pronounced h t t p colon slash slash w w w dot mindslab dot ai.";
        paragraph = "sungsik@mindslab.ai will be pronounced sungsik at mindslab dot ai";
        paragraph = "<digit>1234</digit>";
        paragraph = "<alpha>speed</alpha>";
        paragraph = "<cardinal>2021</cardinal>";
        paragraph = "<ordinal>2021</ordinal>";
        paragraph = "<date>01/01/2021</date>";
        paragraph = "<time>12:59</time>";
        paragraph = "<range>12-120</range>";
        paragraph = "<duration>12:39:59:10</duration>";
        paragraph = "<money>$5000.12</money>";
        paragraph = "<money>$123,456.78</money>";
        paragraph = "<money>€123.456,78</money>";
        paragraph = "<money>EUR 3.321.234,56</money>";
        paragraph = "<money>EURO 3.321.234</money>";
        getTTSSentence(paragraph);
    }

    public static ArrayList<String> getTTSSentence(String paragraph) {
        ArrayList<String> sentenceList = new ArrayList();
        ArrayList cleanSentenceList = new ArrayList();

        try {
            new String();
            new String();
            StringBuilder sb = new StringBuilder();
            String[] arrFraction;
            String sentence;
            int i;
            int j;
            String[] parts;
            String spoonType;
            if (paragraph.contains("<address>") && paragraph.contains("</address>")) {
                arrFraction = StringUtils.substringsBetween(paragraph, "<address>", "</address>");
                if (arrFraction != null) {
                    parts = arrFraction;
                    j = arrFraction.length;

                    for(i = 0; i < j; ++i) {
                        sentence = parts[i];
                        spoonType = ConvertAddress.convertUSAddress(sentence);
                        paragraph = paragraph.replace("<address>".concat(sentence).concat("</address>"), spoonType);
                    }
                }
            }

            if (paragraph.contains("<phone>") && paragraph.contains("</phone>")) {
                arrFraction = StringUtils.substringsBetween(paragraph, "<phone>", "</phone>");
                if (arrFraction != null) {
                    parts = arrFraction;
                    j = arrFraction.length;

                    for(i = 0; i < j; ++i) {
                        sentence = parts[i];
                        spoonType = ConvertPhone.convertUSPhone(sentence);
                        paragraph = paragraph.replace("<phone>".concat(sentence).concat("</phone>"), spoonType);
                    }
                }
            }

            if (paragraph.contains("<url>") && paragraph.contains("</url>")) {
                arrFraction = StringUtils.substringsBetween(paragraph, "<url>", "</url>");
                if (arrFraction != null) {
                    parts = arrFraction;
                    j = arrFraction.length;

                    for(i = 0; i < j; ++i) {
                        sentence = parts[i];
                        spoonType = ConvertToken.convertURL(sentence);
                        paragraph = paragraph.replace("<url>".concat(sentence).concat("</url>"), spoonType);
                    }
                }
            }

            if (paragraph.contains("<digit>") && paragraph.contains("</digit>")) {
                arrFraction = StringUtils.substringsBetween(paragraph, "<digit>", "</digit>");
                if (arrFraction != null) {
                    parts = arrFraction;
                    j = arrFraction.length;

                    for(i = 0; i < j; ++i) {
                        sentence = parts[i];
                        spoonType = ConvertDigit.convertDigit(sentence);
                        paragraph = paragraph.replace("<digit>".concat(sentence).concat("</digit>"), spoonType);
                    }
                }
            }

            if (paragraph.contains("<cardinal>") && paragraph.contains("</cardinal>")) {
                arrFraction = StringUtils.substringsBetween(paragraph, "<cardinal>", "</cardinal>");
                if (arrFraction != null) {
                    parts = arrFraction;
                    j = arrFraction.length;

                    for(i = 0; i < j; ++i) {
                        sentence = parts[i];
                        if (PPUtils.isDigit(sentence)) {
                            spoonType = ConvertToken.convertRealNumber(sentence);
                            paragraph = paragraph.replace("<cardinal>".concat(sentence).concat("</cardinal>"), spoonType);
                        }
                    }
                }
            }

            if (paragraph.contains("<ordinal>") && paragraph.contains("</ordinal>")) {
                arrFraction = StringUtils.substringsBetween(paragraph, "<ordinal>", "</ordinal>");
                if (arrFraction != null) {
                    parts = arrFraction;
                    j = arrFraction.length;

                    for(i = 0; i < j; ++i) {
                        sentence = parts[i];
                        if (PPUtils.isDigit(sentence)) {
                            spoonType = ConvertToken.convertOrdinal(sentence);
                            paragraph = paragraph.replace("<ordinal>".concat(sentence).concat("</ordinal>"), spoonType);
                        }
                    }
                }
            }

            if (paragraph.contains("<alpha>") && paragraph.contains("</alpha>")) {
                arrFraction = StringUtils.substringsBetween(paragraph, "<alpha>", "</alpha>");
                if (arrFraction != null) {
                    parts = arrFraction;
                    j = arrFraction.length;

                    for(i = 0; i < j; ++i) {
                        sentence = parts[i];
                        spoonType = ConvertDigit.convertAlpha(sentence);
                        paragraph = paragraph.replace("<alpha>".concat(sentence).concat("</alpha>"), spoonType);
                    }
                }
            }

            if (paragraph.contains("<date>") && paragraph.contains("</date>")) {
                arrFraction = StringUtils.substringsBetween(paragraph, "<date>", "</date>");
                if (arrFraction != null) {
                    parts = arrFraction;
                    j = arrFraction.length;

                    for(i = 0; i < j; ++i) {
                        sentence = parts[i];
                        spoonType = ConvertToken.convertDate(sentence);
                        paragraph = paragraph.replace("<date>".concat(sentence).concat("</date>"), spoonType);
                    }
                }
            }

            if (paragraph.contains("<time>") && paragraph.contains("</time>")) {
                arrFraction = StringUtils.substringsBetween(paragraph, "<time>", "</time>");
                if (arrFraction != null) {
                    parts = arrFraction;
                    j = arrFraction.length;

                    for(i = 0; i < j; ++i) {
                        sentence = parts[i];
                        spoonType = ConvertToken.convertTime(sentence);
                        paragraph = paragraph.replace("<time>".concat(sentence).concat("</time>"), spoonType);
                    }
                }
            }

            if (paragraph.contains("<range>") && paragraph.contains("</range>")) {
                arrFraction = StringUtils.substringsBetween(paragraph, "<range>", "</range>");
                if (arrFraction != null) {
                    parts = arrFraction;
                    j = arrFraction.length;

                    for(i = 0; i < j; ++i) {
                        sentence = parts[i];
                        spoonType = ConvertToken.convertRange(sentence);
                        paragraph = paragraph.replace("<range>".concat(sentence).concat("</range>"), spoonType);
                    }
                }
            }

            if (paragraph.contains("<duration>") && paragraph.contains("</duration>")) {
                arrFraction = StringUtils.substringsBetween(paragraph, "<duration>", "</duration>");
                if (arrFraction != null) {
                    parts = arrFraction;
                    j = arrFraction.length;

                    for(i = 0; i < j; ++i) {
                        sentence = parts[i];
                        spoonType = ConvertToken.convertDuration(sentence);
                        paragraph = paragraph.replace("<duration>".concat(sentence).concat("</duration>"), spoonType);
                    }
                }
            }

            if (paragraph.contains("<money>") && paragraph.contains("</money>")) {
                arrFraction = StringUtils.substringsBetween(paragraph, "<money>", "</money>");
                if (arrFraction != null) {
                    parts = arrFraction;
                    j = arrFraction.length;

                    for(i = 0; i < j; ++i) {
                        sentence = parts[i];
                        spoonType = ConvertToken.convertMoney(sentence);
                        paragraph = paragraph.replace("<money>".concat(sentence).concat("</money>"), spoonType);
                    }
                }
            }

            if (paragraph.contains("<fraction>") && paragraph.contains("</fraction>")) {
                arrFraction = StringUtils.substringsBetween(paragraph, "<fraction>", "</fraction>");
                if (arrFraction != null) {
                    parts = arrFraction;
                    j = arrFraction.length;

                    for(i = 0; i < j; ++i) {
                        sentence = parts[i];
                        if (sentence.contains("⁄") || sentence.contains("/")) {
                            spoonType = ConvertFraction.convertFraction(sentence);
                            paragraph = paragraph.replace("<fraction>".concat(sentence).concat("</fraction>"), spoonType);
                        }
                    }
                }
            }

            sentenceList = new ArrayList(EnglishTokenizer.anlyseWithSentenceDetectorME(paragraph));
            new ArrayList();
            new String();

            String preToken;
            for(i = 0; sentenceList != null && i < sentenceList.size(); ++i) {
                sentence = ((String)sentenceList.get(i)).trim();
                if (!sentence.isEmpty()) {
                    Matcher m;
                    for(m = EngPatternType.P_TIME_EX.matcher(sentence); m.find(); sentence = sentence.replace(m.group(0), ConvertToken.convertNumber(preToken) + " " + ConvertToken.addSpace(spoonType))) {
                        preToken = m.group(0).substring(0, m.group(0).length() - 2).trim();
                        spoonType = m.group(0).substring(m.group(0).length() - 2).trim();
                    }

                    for(m = EngPatternType.P_ROMAN_ORDINAL.matcher(sentence); m.find(); sentence = sentence.replace(m.group(0), ConvertToken.convertRoman(preToken.toLowerCase(), Boolean.TRUE) + spoonType)) {
                        preToken = m.group(0).substring(0, m.group(0).length() - 2);
                        spoonType = m.group(0).substring(m.group(0).length() - 2);
                    }

                    m = EngPatternType.P_ROMAN_MONARCHS.matcher(sentence);

                    while(m.find()) {
                        parts = m.group(0).split(" ");
                        if (parts.length == 2 && !"Chapter".equals(parts[0]) && !"War".equals(parts[0])) {
                            spoonType = parts[0].toLowerCase() + " " + ConvertToken.convertOrdinalRoman(parts[1].toLowerCase());
                            sentence = sentence.replace(m.group(0), spoonType);
                        }
                    }

                    sentence = sentence.toLowerCase();
                    m = EngPatternType.P_SPOON.matcher(sentence);

                    while(m.find()) {
                        preToken = m.group(0).substring(0, m.group(0).indexOf(" "));
                        spoonType = m.group(0).substring(m.group(0).indexOf(" ") + 1);
                        if ("tbsp".equals(spoonType)) {
                            if (preToken.equals("1")) {
                                sentence = sentence.replace(m.group(0), ConvertToken.convertNumber(preToken) + " " + "tablespoon");
                            } else {
                                sentence = sentence.replace(m.group(0), ConvertToken.convertNumber(preToken) + " " + "tablespoons");
                            }
                        } else if ("tsp".equals(spoonType)) {
                            if (preToken.equals("1")) {
                                sentence = sentence.replace(m.group(0), ConvertToken.convertNumber(preToken) + " " + "teaspoon");
                            } else {
                                sentence = sentence.replace(m.group(0), ConvertToken.convertNumber(preToken) + " " + "teaspoons");
                            }
                        }
                    }

                    for(m = EngPatternType.P_US_MONTH_DATE_EX.matcher(sentence); m.find(); sentence = sentence.replace(m.group(0), m.group(0).replaceAll("'", "").replaceAll(",", "").replaceAll(" ", "/"))) {
                    }

                    for(m = EngPatternType.P_US_MONTH_DATE_EX2.matcher(sentence); m.find(); sentence = sentence.replace(m.group(0), m.group(0).replaceAll("'", "").replaceAll(",", "").replaceAll("th", "").replaceAll("rd", "").replaceAll("st", "").replaceAll("nd", "").replaceAll(" ", "/"))) {
                    }

                    m = EngPatternType.P_US_MONTH_DATE_EX3.matcher(sentence);

                    while(m.find()) {
                        preToken = ConvertToken.convertUSDate(m.group(0));
                        if (!m.group(0).equals(preToken)) {
                            sentence = sentence.replace(m.group(0), preToken);
                        }
                    }

                    m = EngPatternType.P_US_MONTH_ABBR_DATE_EX.matcher(sentence);

                    while(m.find()) {
                        preToken = ConvertToken.convertUSDateEx(m.group(0));
                        if (!m.group(0).equals(preToken)) {
                            sentence = sentence.replace(m.group(0), preToken);
                        }
                    }

                    m = EngPatternType.P_YEAR_EX.matcher(sentence);

                    while(m.find()) {
                        preToken = ConvertToken.convertYearBCADWithSpace(m.group(0));
                        if (!m.group(0).equals(preToken)) {
                            sentence = sentence.replace(m.group(0), preToken);
                        }
                    }

                    m = EngPatternType.P_YRAE_NUMBERS.matcher(sentence);

                    while(m.find()) {
                        preToken = ConvertToken.convertShortYearBCADWithSpace(m.group(0));
                        if (!m.group(0).equals(preToken)) {
                            sentence = sentence.replace(m.group(0), preToken);
                        }
                    }

                    m = EngPatternType.P_MONTH_RANGE.matcher(sentence);

                    while(m.find()) {
                        preToken = m.group(1) + " " + ConvertToken.convertOrdinal(m.group(2)) + " to " + ConvertToken.convertOrdinal(m.group(3)) + " ";
                        if (!m.group(0).equals(preToken)) {
                            sentence = sentence.replace(m.group(0), preToken);
                        }
                    }

                    if (sentence.contains("fl oz")) {
                        sentence = sentence.replaceAll("fl oz", "fluid ounce");
                    }

                    cleanSentenceList.add(sentence);
                }
            }

            sentenceList.clear();

            for(i = 0; cleanSentenceList != null && i < cleanSentenceList.size(); ++i) {
                sentence = ((String)cleanSentenceList.get(i)).trim();
                printLog("Cleasing[" + i + "]" + sentence);
                if (!sentence.isEmpty()) {
                    sentence = sentence.toLowerCase();
                    sb.setLength(0);
                    ArrayList<String> tokenList = new ArrayList(EnglishTokenizer.anlyseWithWhitespaceTokenizer(sentence));

                    for(j = 0; tokenList != null && j < tokenList.size(); ++j) {
                        String token = (String)tokenList.get(j);
                        String newToken = ConvertToken.convertRoman(token);
                        if (!token.equals(newToken)) {
                            sb.append(newToken).append(" ");
                        } else {
                            if (!token.equals("as") && !token.equals("hi") && !token.equals("id") && !token.equals("in") && !token.equals("oh") && !token.equals("ok") && !token.equals("or") && !token.equals("dc") && !token.equals("ms") && !token.equals("w") && !token.equals("s") && !token.equals("e") && !token.equals("n")) {
                                newToken = ConvertAbbreviation.getAbbreviation(token);
                                if (!token.equals(newToken)) {
                                    sb.append(newToken).append(" ");
                                    continue;
                                }
                            }

                            newToken = ConvertContraction.getContraction(token);
                            if (!token.equals(newToken)) {
                                sb.append(newToken).append(" ");
                            } else {
                                newToken = ConvertAcronym.getAcronym(token);
                                if (!token.equals(newToken)) {
                                    sb.append(newToken).append(" ");
                                } else {
                                    newToken = ConvertMetrics.convertHertz(token);
                                    if (!token.equals(newToken)) {
                                        sb.append(newToken).append(" ");
                                    } else {
                                        newToken = ConvertMetrics.convertMPH(token);
                                        if (!token.equals(newToken)) {
                                            sb.append(newToken).append(" ");
                                        } else {
                                            newToken = ConvertMetrics.convertNMI(token);
                                            if (!token.equals(newToken)) {
                                                sb.append(newToken).append(" ");
                                            } else {
                                                newToken = ConvertMetrics.convertDegrees(token);
                                                if (!token.equals(newToken)) {
                                                    sb.append(newToken).append(" ");
                                                } else {
                                                    newToken = ConvertMetrics.convertCents(token);
                                                    if (!token.equals(newToken)) {
                                                        sb.append(newToken).append(" ");
                                                    } else {
                                                        newToken = ConvertMetrics.convertKmPerHour(token);
                                                        if (!token.equals(newToken)) {
                                                            sb.append(newToken).append(" ");
                                                        } else {
                                                            newToken = ConvertMetrics.convertMeters(token);
                                                            if (!token.equals(newToken)) {
                                                                sb.append(newToken).append(" ");
                                                            } else if (!token.equals("a.m.") && !token.equals("a.m")) {
                                                                if (!token.equals("p.m.") && !token.equals("p.m")) {
                                                                    if (PPUtils.isAlphabetic(token)) {
                                                                        sb.append(token).append(" ");
                                                                    } else {
                                                                        if (token.endsWith(".")) {
                                                                            preToken = token.substring(0, token.length() - 1);
                                                                            if (PPUtils.isAlphabetic(preToken)) {
                                                                                sb.append(token).append(" ");
                                                                                continue;
                                                                            }
                                                                        }

                                                                        if ("a".equals(token.toLowerCase())) {
                                                                            sb.append(token).append(" ");
                                                                        } else {
                                                                            if (token.endsWith("st") || token.endsWith("nd") || token.endsWith("rd") || token.endsWith("th")) {
                                                                                preToken = token.substring(0, token.length() - 2);
                                                                                if (preToken.contains(",")) {
                                                                                    preToken = preToken.replaceAll(",", "");
                                                                                }

                                                                                Boolean b = PPUtils.isDigit(preToken);
                                                                                if (b) {
                                                                                    newToken = ConvertToken.convertOrdinal(preToken);
                                                                                    sb.append(newToken).append(" ");
                                                                                    continue;
                                                                                }
                                                                            }

                                                                            newToken = ConvertToken.convertDate(token);
                                                                            if (!token.equals(newToken)) {
                                                                                sb.append(newToken).append(" ");
                                                                            } else {
                                                                                newToken = ConvertToken.convertNumberS(token);
                                                                                if (!token.equals(newToken)) {
                                                                                    sb.append(newToken).append(" ");
                                                                                } else {
                                                                                    newToken = ConvertToken.convertYearBCAD(token);
                                                                                    if (!token.equals(newToken)) {
                                                                                        sb.append(newToken).append(" ");
                                                                                    } else {
                                                                                        newToken = ConvertToken.convertMoney(token);
                                                                                        if (!token.equals(newToken)) {
                                                                                            sb.append(newToken).append(" ");
                                                                                        } else {
                                                                                            newToken = ConvertToken.convertTime(token);
                                                                                            if (!token.equals(newToken)) {
                                                                                                sb.append(newToken).append(" ");
                                                                                            } else {
                                                                                                newToken = ConvertToken.convertYear(token);
                                                                                                if (!token.equals(newToken)) {
                                                                                                    sb.append(newToken).append(" ");
                                                                                                } else {
                                                                                                    newToken = ConvertToken.convertDurationEx(token);
                                                                                                    if (!token.equals(newToken)) {
                                                                                                        sb.append(newToken).append(" ");
                                                                                                    } else {
                                                                                                        newToken = ConvertToken.convertNumberWord(token);
                                                                                                        if (!token.equals(newToken)) {
                                                                                                            sb.append(newToken).append(" ");
                                                                                                        } else {
                                                                                                            newToken = ConvertToken.convertDuration(token);
                                                                                                            if (!token.equals(newToken)) {
                                                                                                                sb.append(newToken).append(" ");
                                                                                                            } else {
                                                                                                                newToken = ConvertToken.convertHashTag(token);
                                                                                                                if (!token.equals(newToken)) {
                                                                                                                    sb.append(newToken).append(" ");
                                                                                                                } else {
                                                                                                                    newToken = ConvertToken.convertURL(token);
                                                                                                                    if (!token.equals(newToken)) {
                                                                                                                        sb.append(newToken).append(" ");
                                                                                                                    } else {
                                                                                                                        newToken = ConvertToken.convertRange(token);
                                                                                                                        if (!token.equals(newToken)) {
                                                                                                                            sb.append(newToken).append(" ");
                                                                                                                        } else {
                                                                                                                            if (token.contains("⁄") || token.contains("/")) {
                                                                                                                                newToken = ConvertFraction.convertFraction(token);
                                                                                                                                if (!token.equals(newToken)) {
                                                                                                                                    sb.append(newToken).append(" ");
                                                                                                                                    continue;
                                                                                                                                }
                                                                                                                            }

                                                                                                                            newToken = ConvertToken.convertUnderscoreAndDash(token);
                                                                                                                            if (!token.equals(newToken)) {
                                                                                                                                sb.append(newToken).append(" ");
                                                                                                                            } else {
                                                                                                                                newToken = ConvertToken.convertConsonant(token);
                                                                                                                                if (!token.equals(newToken)) {
                                                                                                                                    sb.append(newToken).append(" ");
                                                                                                                                } else {
                                                                                                                                    newToken = ConvertToken.convertFeetInch(token);
                                                                                                                                    if (!token.equals(newToken)) {
                                                                                                                                        sb.append(newToken).append(" ");
                                                                                                                                    } else {
                                                                                                                                        newToken = ConvertPuncts.removeSpecialCharacters(token);
                                                                                                                                        if (!token.equals(newToken)) {
                                                                                                                                            sb.append(newToken).append(" ");
                                                                                                                                        } else {
                                                                                                                                            newToken = ConvertToken.convertRealNumber(token);
                                                                                                                                            if (!token.equals(newToken)) {
                                                                                                                                                sb.append(newToken).append(" ");
                                                                                                                                            } else {
                                                                                                                                                sb.append(token).append(" ");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                    sb.append("p m").append(" ");
                                                                }
                                                            } else {
                                                                sb.append("a m").append(" ");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    sb.append("\n");
                    printLog("Result[" + i + "]" + sb.toString());
                    sentenceList.add(sb.toString());
                }
            }
        } catch (IOException var12) {
            printLog(var12.getMessage());
        }

        return sentenceList;
    }

    private static void printLog(String text) {
        if (bLog) {
            System.out.println(text);
        }

    }
}

