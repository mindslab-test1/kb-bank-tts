package maum.brain.tts.preprocess.type;

public class TagType {
    public static final String TAG_START = "<";
    public static final String TAG_END = "</";
    public static final String URL_START = "<url>";
    public static final String URL_END = "</url>";
    public static final String NAME_START = "<name>";
    public static final String NAME_END = "</name>";
    public static final String PHONE_START = "<phone>";
    public static final String PHONE_END = "</phone>";
    public static final String PHONE2_START = "<phone_2>";
    public static final String PHONE2_END = "</phone_2>";
    public static final String DATE_START = "<date>";
    public static final String DATE_END = "</date>";
    public static final String DIGIT_START = "<digit>";
    public static final String DIGIT_END = "</digit>";
    public static final String CARDINAL_START = "<cardinal>";
    public static final String CARDINAL_END = "</cardinal>";
    public static final String ORDINAL_START = "<ordinal>";
    public static final String ORDINAL_END = "</ordinal>";
    public static final String TIME_START = "<time>";
    public static final String TIME_END = "</time>";
    public static final String DURATION_START = "<duration>";
    public static final String DURATION_END = "</duration>";
    public static final String RANGE_START = "<range>";
    public static final String RANGE_END = "</range>";
    public static final String MONEY_START = "<money>";
    public static final String MONEY_END = "</money>";
    public static final String ALPHA_START = "<alpha>";
    public static final String ALPHA_END = "</alpha>";
    public static final String ADDRESS_START = "<address>";
    public static final String ADDRESS_END = "</address>";
    public static final String PAUSE_START = "<pause>";
    public static final String PAUSE_END = "</pause>";
    public static final String PAUSE2_START = "<pause_2>";
    public static final String PAUSE2_END = "</pause_2>";
    public static final String PAUSE3_START = "<pause_3>";
    public static final String PAUSE3_END = "</pause_3>";
    public static final String FRACTION_START = "<fraction>";
    public static final String FRACTION_END = "</fraction>";
    public static final String PHONE_DIR = "phone/";
    public static final String PHONE_DIR_EX = "/phone/";
    public static final String DIGIT_DIR = "digit/";
    public static final String DIGIT_DIR_EX = "/digit/";

    public TagType() {
    }
}
