package maum.brain.tts.preprocess.eng;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Hashtable;

public class ConvertAcronym {
    private static boolean bLog;
    public static Hashtable<String, String> dict;
    private static String acronymFile;

    public ConvertAcronym() {
    }

    public static void main(String[] args) {
        String token = "asap";
        String value = getAcronym(token);
        System.out.println(value);
    }

    public static String getAcronym(String token) {
        String value = new String();

        try {
            if (dict.isEmpty()) {
                allocateDictionary(acronymFile);
            }

            value = (String)dict.get(token.toLowerCase());
            if (value == null || value.isEmpty()) {
                return token;
            }
        } catch (Exception var3) {
            printLog(var3.getMessage());
        }

        return value;
    }

    public static void allocateDictionary(String txtFile) throws Exception {
        acronymFile = txtFile;
        FileReader fr = new FileReader(txtFile);
        BufferedReader br = new BufferedReader(fr);
        new String();
        String[] st = null;

        String line;
        while((line = br.readLine()) != null) {
            st = line.split("=");
            if (!st[0].trim().startsWith("#")) {
                dict.put(st[0].toLowerCase(), st[1]);
            }
        }

        br.close();
        fr.close();
    }

    private static void printLog(String text) {
        if (bLog) {
            System.out.println(text);
        }

    }

    static {
        bLog = Boolean.FALSE;
        dict = new Hashtable();
        acronymFile = "src/main/resources/tts.eng.dict.acronym.properties";
    }
}
