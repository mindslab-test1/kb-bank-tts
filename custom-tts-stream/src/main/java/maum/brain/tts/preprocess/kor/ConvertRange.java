package maum.brain.tts.preprocess.kor;

import java.util.List;
import maum.brain.tts.preprocess.type.KorPatternType;
import maum.brain.tts.utils.NumberUtils;
import maum.brain.tts.utils.PPUtils;

public class ConvertRange {
    public ConvertRange() {
    }

    public static String convertRangeFormat(String token) {
        int idx = token.indexOf("-");
        String be = token.substring(0, idx);
        String af = token.substring(idx + 1, token.length());
        String from = getKoreanFromHHMM(be);
        String to = getKoreanFromHHMM(af);
        return from.concat("부터").concat(" ") + to.concat("까지");
    }

    private static String getKoreanFromHHMM(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_HHMI_SEP, token);
        if (!aList.isEmpty()) {
            String HH_MI = (String)aList.get(0);
            String HH = HH_MI.substring(0, 2);
            String MI = HH_MI.substring(3, 5);
            if ("00".equals(MI)) {
                token = NumberUtils.convertHour2Hangle(HH).concat("시");
            } else {
                token = NumberUtils.convertHour2Hangle(HH).concat("시").concat(" ").concat(NumberUtils.convertDecimal2Hangle(MI, false)).concat("분");
            }
        }

        return token;
    }
}
