package maum.brain.tts.preprocess.eng.address;

import maum.brain.tts.preprocess.eng.address.Utils.NamedGroupPattern;

class AddressRegexLibrary {
    public static final String NUMBER;
    public static final String FRACTION = "\\d+\\/\\d+";
    public static final String LINE1A;
    public static final String LINE1B;
    public static final String LINE1A2;
    public static final String LINE1B2;
    public static final String LINE1;
    public static final String UNIT_NUMBER = "(?:\\b\\p{Alpha}{1}\\s+|\\p{Alpha}*[-/]?)?(?:\\d+|\\b\\p{Alpha}\\b(?=\\s|$))(?:[ ]*\\p{Alpha}\\b|-\\w+)?";
    public static final String ZIP = "\\d{5}(?:[- ]\\d{3,4})?";
    public static final String NOT_STATE_OR_ZIP;
    public static final String LINE2A;
    public static final String LINE2A_GROUPED;
    public static final String LINE2B;
    public static final String LINE2;
    public static final String LASTLINE;
    public static final String ADDR_NAME = "(?:(?P<name>[^,]+)\\W+)??";
    public static final String STREET_ADDRESS;
    public static final String CORNER = "(?:\\band\\b|\\bat\\b|&|\\@)";
    public static final String INTERSECTION;
    public static final NamedGroupPattern P_CSZ;
    public static final NamedGroupPattern P_STREET_ADDRESS;
    public static final NamedGroupPattern P_INTERSECTION;
    public static final NamedGroupPattern P_CORNER;

    AddressRegexLibrary() {
    }

    static {
        NUMBER = "(?:\\p{Alpha})?\\d+(?:[- ][\\p{Alpha}&&[^NSEW]](?!\\s+(?:st|street|ave|aven|avenu|avenue|blvd|boulv|boulevard|boulv|plz|plaza|plza)))?|\\d+-?\\d*(?:-?\\p{Alpha})?|" + RegexLibrary.TXT_NUM_0_9 + "|" + RegexLibrary.TXT_NUM_10_19;
        LINE1A = "(?P<street>" + RegexLibrary.DIRECTIONS + ")\\W+(?P<type>" + RegexLibrary.STREET_DESIGNATOR + ")\\b";
        LINE1B = "(?:(?P<predir>" + RegexLibrary.DIRECTIONS + ")\\W+)?(?:(?P<street>[^,]+)(?:[^\\w,]+(?P<type>" + RegexLibrary.STREET_DESIGNATOR + ")\\b)(?:[^\\w,]+(?P<postdir>" + RegexLibrary.DIRECTIONS + ")\\b)?|(?P<street>[^,]*\\d)(?:(?P<postdir>" + RegexLibrary.DIRECTIONS + ")\\b)|(?P<street>[^,]+?)(?:[^\\w,]+(?P<type>" + RegexLibrary.STREET_DESIGNATOR + ")\\b)?(?:[^\\w,]+(?P<postdir>" + RegexLibrary.DIRECTIONS + ")\\b)?)";
        LINE1A2 = "(?P<street2>" + RegexLibrary.DIRECTIONS + ")\\W+(?P<type2>" + RegexLibrary.STREET_DESIGNATOR + ")\\b";
        LINE1B2 = "(?:(?P<predir2>" + RegexLibrary.DIRECTIONS + ")\\W+)?(?:(?P<street2>[^,]+)(?:[^\\w,]+(?P<type2>" + RegexLibrary.STREET_DESIGNATOR + ")\\b)(?:[^\\w,]+(?P<postdir2>" + RegexLibrary.DIRECTIONS + ")\\b)?|(?P<street2>[^,]*\\d)(?:(?P<postdir2>" + RegexLibrary.DIRECTIONS + ")\\b)|(?P<street2>[^,]+?)(?:[^\\w,]+(?P<type2>" + RegexLibrary.STREET_DESIGNATOR + ")\\b)?(?:[^\\w,]+(?P<postdir2>" + RegexLibrary.DIRECTIONS + ")\\b)?)";
        LINE1 = "(?P<number>(?:" + NUMBER + ")(?:\\W+" + "\\d+\\/\\d+" + ")?)\\W+(?:" + LINE1B + "|" + LINE1A + ")";
        NOT_STATE_OR_ZIP = "(?![^,]*\\W+(?:\\b(?:" + RegexLibrary.US_STATES + ")\\b(?:\\W*$|(?:" + "\\d{5}(?:[- ]\\d{3,4})?" + ")\\W*$))|(?:\\b(?:" + "\\d{5}(?:[- ]\\d{3,4})?" + ")\\b\\W*$))";
        LINE2A = "(?:" + RegexLibrary.ADDR_UNIT + ")[s]?\\W*?(?:" + "(?:\\b\\p{Alpha}{1}\\s+|\\p{Alpha}*[-/]?)?(?:\\d+|\\b\\p{Alpha}\\b(?=\\s|$))(?:[ ]*\\p{Alpha}\\b|-\\w+)?" + ")";
        LINE2A_GROUPED = "(" + RegexLibrary.ADDR_UNIT + ")[s]?\\W*?(" + "(?:\\b\\p{Alpha}{1}\\s+|\\p{Alpha}*[-/]?)?(?:\\d+|\\b\\p{Alpha}\\b(?=\\s|$))(?:[ ]*\\p{Alpha}\\b|-\\w+)?" + ")";
        LINE2B = "(?:(?:zeroth|(?:first|second|third|fourth|forth|fifth|sixth|seventh|eighth|ninth|nineth)|(?:tenth|eleventh|twelfth|twelveth|twelvth|thirteenth|fourteenth|fifteenth|sixteenth|seventeenth|enghteenth|nineteenth)|(?:[0-9]*(?:0[-]?th|1[-]?st|2[-]?nd|3[-]?rd|[0[4-9]][-]?th|1[0-9][-]?th)))\\W*(?:" + RegexLibrary.ADDR_UNIT + ")[s]?)";
        LINE2 = "(?:(?P<line2>" + LINE2A + "|" + LINE2B + "|[^,]*?" + NOT_STATE_OR_ZIP + ")\\W+)??";
        LASTLINE = "(?:(?P<city>[^\\d,]+?)\\W+\\b(?P<state>(?:" + RegexLibrary.US_STATES + ")\\b)?\\W*)?(?P<zip>" + "\\d{5}(?:[- ]\\d{3,4})?" + ")?";
        STREET_ADDRESS = "(?:(?P<name>[^,]+)\\W+)??" + LINE1 + "(?P<tlid>\\W+)" + LINE2 + LASTLINE + "\\W*";
        INTERSECTION = "(?:(?P<name>[^,]+)\\W+)??(?:" + LINE1A + "|" + LINE1B + ")\\W*\\s+" + "(?:\\band\\b|\\bat\\b|&|\\@)" + "\\s+(?:" + LINE1A2 + "|" + LINE1B2 + ")\\W+" + LASTLINE + "\\W*";
        P_CSZ = Utils.compile("(?i:" + LASTLINE + ")");
        P_STREET_ADDRESS = Utils.compile("(?i:" + STREET_ADDRESS + ")");
        P_INTERSECTION = Utils.compile("(?i:" + INTERSECTION + ")");
        P_CORNER = Utils.compile("(?i:(?:\\band\\b|\\bat\\b|&|\\@))");
    }
}
