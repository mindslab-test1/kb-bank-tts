package maum.brain.tts.preprocess.kor;

import maum.brain.tts.utils.NumberUtils;

public class ConvertDigit {
    public ConvertDigit() {
    }

    public static String tagDigitFormat(String token) {
        if (token != null && !token.isEmpty()) {
            StringBuilder sb = new StringBuilder();

            for(int i = 0; i < token.length(); ++i) {
                char digit = token.charAt(i);
                if (digit == '0') {
                    sb.append("영::");
                }

                if (digit == '1') {
                    sb.append("일::");
                }

                if (digit == '2') {
                    sb.append("이::");
                }

                if (digit == '3') {
                    sb.append("삼::");
                }

                if (digit == '4') {
                    sb.append("사::");
                }

                if (digit == '5') {
                    sb.append("오::");
                }

                if (digit == '6') {
                    sb.append("륙::");
                }

                if (digit == '7') {
                    sb.append("칠::");
                }

                if (digit == '8') {
                    sb.append("팔::");
                }

                if (digit == '9') {
                    sb.append("구::");
                }
            }

            return sb.toString();
        } else {
            return "";
        }
    }

    public static String convertAlphaDigitFormat(String token) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < token.length(); ++i) {
            if ("0".equals(String.valueOf(token.charAt(i)))) {
                sb.append("영");
            } else if ("1".equals(String.valueOf(token.charAt(i)))) {
                sb.append("일");
            } else if ("2".equals(String.valueOf(token.charAt(i)))) {
                sb.append("이");
            } else if ("3".equals(String.valueOf(token.charAt(i)))) {
                sb.append("삼");
            } else if ("4".equals(String.valueOf(token.charAt(i)))) {
                sb.append("사");
            } else if ("5".equals(String.valueOf(token.charAt(i)))) {
                sb.append("오");
            } else if ("6".equals(String.valueOf(token.charAt(i)))) {
                sb.append("륙");
            } else if ("7".equals(String.valueOf(token.charAt(i)))) {
                sb.append("칠");
            } else if ("8".equals(String.valueOf(token.charAt(i)))) {
                sb.append("팔");
            } else if ("9".equals(String.valueOf(token.charAt(i)))) {
                sb.append("구");
            } else if ("a".equals(String.valueOf(token.charAt(i)))) {
                sb.append("에이");
            } else if ("b".equals(String.valueOf(token.charAt(i)))) {
                sb.append("비");
            } else if ("c".equals(String.valueOf(token.charAt(i)))) {
                sb.append("씨");
            } else if ("d".equals(String.valueOf(token.charAt(i)))) {
                sb.append("디");
            } else if ("e".equals(String.valueOf(token.charAt(i)))) {
                sb.append("이");
            } else if ("f".equals(String.valueOf(token.charAt(i)))) {
                sb.append("에프");
            } else if ("g".equals(String.valueOf(token.charAt(i)))) {
                sb.append("쥐");
            } else if ("h".equals(String.valueOf(token.charAt(i)))) {
                sb.append("에이치");
            } else if ("i".equals(String.valueOf(token.charAt(i)))) {
                sb.append("아이");
            } else if ("j".equals(String.valueOf(token.charAt(i)))) {
                sb.append("제이");
            } else if ("k".equals(String.valueOf(token.charAt(i)))) {
                sb.append("케이");
            } else if ("l".equals(String.valueOf(token.charAt(i)))) {
                sb.append("엘");
            } else if ("m".equals(String.valueOf(token.charAt(i)))) {
                sb.append("엠");
            } else if ("n".equals(String.valueOf(token.charAt(i)))) {
                sb.append("엔");
            } else if ("o".equals(String.valueOf(token.charAt(i)))) {
                sb.append("오");
            } else if ("p".equals(String.valueOf(token.charAt(i)))) {
                sb.append("피");
            } else if ("q".equals(String.valueOf(token.charAt(i)))) {
                sb.append("큐");
            } else if ("r".equals(String.valueOf(token.charAt(i)))) {
                sb.append("알");
            } else if ("s".equals(String.valueOf(token.charAt(i)))) {
                sb.append("에스");
            } else if ("t".equals(String.valueOf(token.charAt(i)))) {
                sb.append("티");
            } else if ("u".equals(String.valueOf(token.charAt(i)))) {
                sb.append("유");
            } else if ("v".equals(String.valueOf(token.charAt(i)))) {
                sb.append("브이");
            } else if ("w".equals(String.valueOf(token.charAt(i)))) {
                sb.append("더블유");
            } else if ("x".equals(String.valueOf(token.charAt(i)))) {
                sb.append("엑스");
            } else if ("y".equals(String.valueOf(token.charAt(i)))) {
                sb.append("와이");
            } else if ("z".equals(String.valueOf(token.charAt(i)))) {
                sb.append("지");
            } else {
                sb.append(String.valueOf(token.charAt(i)));
            }
        }

        return sb.toString();
    }

    public static String convertDigitFormat(String token) {
        return token.startsWith("0") ? NumberUtils.convertDigit2HangleEx(token) : NumberUtils.convertDecimal2HangleEx(token, false);
    }
}
