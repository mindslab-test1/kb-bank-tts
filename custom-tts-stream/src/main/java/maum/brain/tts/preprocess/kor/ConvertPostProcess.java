package maum.brain.tts.preprocess.kor;

public class ConvertPostProcess {
    public ConvertPostProcess() {
    }

    public static String convertNumber2Hangle(String text) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < text.length(); ++i) {
            if ("0".equals(String.valueOf(text.charAt(i)))) {
                sb.append("영");
            } else if ("1".equals(String.valueOf(text.charAt(i)))) {
                sb.append("일");
            } else if ("2".equals(String.valueOf(text.charAt(i)))) {
                sb.append("이");
            } else if ("3".equals(String.valueOf(text.charAt(i)))) {
                sb.append("삼");
            } else if ("4".equals(String.valueOf(text.charAt(i)))) {
                sb.append("사");
            } else if ("5".equals(String.valueOf(text.charAt(i)))) {
                sb.append("오");
            } else if ("6".equals(String.valueOf(text.charAt(i)))) {
                sb.append("육");
            } else if ("7".equals(String.valueOf(text.charAt(i)))) {
                sb.append("칠");
            } else if ("8".equals(String.valueOf(text.charAt(i)))) {
                sb.append("팔");
            } else if ("9".equals(String.valueOf(text.charAt(i)))) {
                sb.append("구");
            } else if ("a".equals(String.valueOf(text.charAt(i)))) {
                sb.append("에이").append(" ");
            } else if ("b".equals(String.valueOf(text.charAt(i)))) {
                sb.append("비").append(" ");
            } else if ("c".equals(String.valueOf(text.charAt(i)))) {
                sb.append("씨").append(" ");
            } else if ("d".equals(String.valueOf(text.charAt(i)))) {
                sb.append("디").append(" ");
            } else if ("e".equals(String.valueOf(text.charAt(i)))) {
                sb.append("이").append(" ");
            } else if ("f".equals(String.valueOf(text.charAt(i)))) {
                sb.append("에프").append(" ");
            } else if ("g".equals(String.valueOf(text.charAt(i)))) {
                sb.append("쥐").append(" ");
            } else if ("h".equals(String.valueOf(text.charAt(i)))) {
                sb.append("에이치").append(" ");
            } else if ("i".equals(String.valueOf(text.charAt(i)))) {
                sb.append("아이").append(" ");
            } else if ("j".equals(String.valueOf(text.charAt(i)))) {
                sb.append("제이").append(" ");
            } else if ("k".equals(String.valueOf(text.charAt(i)))) {
                sb.append("케이").append(" ");
            } else if ("l".equals(String.valueOf(text.charAt(i)))) {
                sb.append("엘").append(" ");
            } else if ("m".equals(String.valueOf(text.charAt(i)))) {
                sb.append("엠").append(" ");
            } else if ("n".equals(String.valueOf(text.charAt(i)))) {
                sb.append("엔").append(" ");
            } else if ("o".equals(String.valueOf(text.charAt(i)))) {
                sb.append("오").append(" ");
            } else if ("p".equals(String.valueOf(text.charAt(i)))) {
                sb.append("피").append(" ");
            } else if ("q".equals(String.valueOf(text.charAt(i)))) {
                sb.append("큐").append(" ");
            } else if ("r".equals(String.valueOf(text.charAt(i)))) {
                sb.append("알").append(" ");
            } else if ("s".equals(String.valueOf(text.charAt(i)))) {
                sb.append("에스").append(" ");
            } else if ("t".equals(String.valueOf(text.charAt(i)))) {
                sb.append("티").append(" ");
            } else if ("u".equals(String.valueOf(text.charAt(i)))) {
                sb.append("유").append(" ");
            } else if ("v".equals(String.valueOf(text.charAt(i)))) {
                sb.append("브이").append(" ");
            } else if ("w".equals(String.valueOf(text.charAt(i)))) {
                sb.append("더블유").append(" ");
            } else if ("x".equals(String.valueOf(text.charAt(i)))) {
                sb.append("엑스").append(" ");
            } else if ("y".equals(String.valueOf(text.charAt(i)))) {
                sb.append("와이").append(" ");
            } else if ("z".equals(String.valueOf(text.charAt(i)))) {
                sb.append("지").append(" ");
            } else if ("(".equals(String.valueOf(text.charAt(i)))) {
                sb.append("괄호 열고").append(" ");
            } else if (")".equals(String.valueOf(text.charAt(i)))) {
                sb.append("괄호 닫고").append(" ");
            } else {
                sb.append(String.valueOf(text.charAt(i)));
            }
        }

        return sb.toString();
    }
}
