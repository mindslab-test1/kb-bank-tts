package maum.brain.tts.preprocess.kor;

import java.util.List;
import maum.brain.tts.preprocess.type.KorPatternType;
import maum.brain.tts.utils.NumberUtils;
import maum.brain.tts.utils.PPUtils;

public class ConvertTidle {
    public ConvertTidle() {
    }

    public static String convertTidleFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_TIDLE, token);
        if (aList != null && !aList.isEmpty()) {
            int idx = token.indexOf("~");
            String be = token.substring(0, idx);
            String af = token.substring(idx + 1, token.length());
            StringBuilder sb = new StringBuilder();
            sb.append(convertDigit2Hangle(be, false)).append("에서").append(" ").append(convertDigit2Hangle(af, false));
            return sb.toString();
        } else {
            return token;
        }
    }

    public static String convertFractionFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_FRACTION, token);
        int cnt = aList.size();
        if (cnt == 0) {
            return token;
        } else {
            int idx = token.indexOf("/");
            String be = token.substring(0, idx);
            String af = token.substring(idx + 1, token.length());
            StringBuilder sb = new StringBuilder();
            sb.append(convertDigit2Hangle(af, false)).append("분의").append(" ").append(convertDigit2Hangle(be, false));
            return sb.toString();
        }
    }

    private static String convertDigit2Hangle(String token, boolean official) {
        token = NumberUtils.convertDecimal2Hangle(token, false);
        return token.trim();
    }
}
