package maum.brain.tts.preprocess.eng;

public class Roman2Arab {
    private String roma;
    private int alab;

    public Roman2Arab(String roma, int alab) {
        this.roma = roma;
        this.alab = alab;
    }

    public String getRoma() {
        return this.roma;
    }

    public void setRoma(String roma) {
        this.roma = roma;
    }

    public int getAlab() {
        return this.alab;
    }

    public void setAlab(int alab) {
        this.alab = alab;
    }
}
