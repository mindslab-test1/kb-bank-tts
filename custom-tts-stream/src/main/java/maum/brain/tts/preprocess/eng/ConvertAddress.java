package maum.brain.tts.preprocess.eng;

import java.util.Map;
import maum.brain.tts.preprocess.eng.address.AddressComponent;
import maum.brain.tts.preprocess.eng.address.AddressParser;
import maum.brain.tts.utils.PPUtils;

public class ConvertAddress {
    private static boolean bLog;

    static {
        bLog = Boolean.TRUE;
    }

    public ConvertAddress() {
    }

    public static String convertUSAddress(String address) {
        printLog("---------->" + address);
        Map<AddressComponent, String> parsedAddr = AddressParser.parseAddress(address);
        printLog(parsedAddr);
        String zip = (String)parsedAddr.get(AddressComponent.ZIP);
        String number = (String)parsedAddr.get(AddressComponent.NUMBER);
        String line2 = (String)parsedAddr.get(AddressComponent.LINE2);
        String newLine2;
        if (!number.isEmpty()) {
            if (!number.equalsIgnoreCase("P.O") && !number.equalsIgnoreCase("PO") && !number.equalsIgnoreCase("P.O.") && !number.equalsIgnoreCase("P.O,")) {
                if (PPUtils.isDigit(number)) {
                    newLine2 = replaceNumber(number);
                    address = address.replace(number, newLine2);
                }
            } else {
                newLine2 = ConvertAbbreviation.getAbbreviation(number.toLowerCase());
                address = address.replace(number, newLine2);
            }
        }

        if (zip != null && !zip.isEmpty() && PPUtils.isDigit(zip)) {
            newLine2 = replaceNumber(zip);
            address = address.replace(zip, newLine2);
        }

        if (line2 != null && !line2.isEmpty() && PPUtils.isDigit(line2)) {
            newLine2 = replaceNumber(line2);
            address = address.replace(line2, newLine2.trim());
        }

        return address;
    }

    private static String replaceNumber(String number) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; number != null && i < number.length(); ++i) {
            if ("0".equals(String.valueOf(number.charAt(i)))) {
                sb.append("zero").append(" ");
            } else if ("1".equals(String.valueOf(number.charAt(i)))) {
                sb.append("one").append(" ");
            } else if ("2".equals(String.valueOf(number.charAt(i)))) {
                sb.append("two").append(" ");
            } else if ("3".equals(String.valueOf(number.charAt(i)))) {
                sb.append("three").append(" ");
            } else if ("4".equals(String.valueOf(number.charAt(i)))) {
                sb.append("four").append(" ");
            } else if ("5".equals(String.valueOf(number.charAt(i)))) {
                sb.append("five").append(" ");
            } else if ("6".equals(String.valueOf(number.charAt(i)))) {
                sb.append("six").append(" ");
            } else if ("7".equals(String.valueOf(number.charAt(i)))) {
                sb.append("seven").append(" ");
            } else if ("8".equals(String.valueOf(number.charAt(i)))) {
                sb.append("eight").append(" ");
            } else if ("9".equals(String.valueOf(number.charAt(i)))) {
                sb.append("nine").append(" ");
            } else {
                sb.append(String.valueOf(number.charAt(i)));
            }
        }

        return sb.toString();
    }

    private static void printLog(Map<AddressComponent, String> text) {
        if (bLog) {
            System.out.println(text);
        }

    }

    private static void printLog(String text) {
        if (bLog) {
            System.out.println(text);
        }

    }
}
