package maum.brain.tts.preprocess.type;

public class MailType {
    public static final String GMAIL = "gmail.com";
    public static final String MINDSLAB = "mindslab.ai";
    public static final String NAVER = "naver.com";
    public static final String KR_GMAIL = " 지메일 쩜 컴";
    public static final String KR_MINDSLAB = " 마인즈랩 쩜 에이 아이";
    public static final String KR_NAVER = " 네이버 쩜 컴";

    public MailType() {
    }
}
