package maum.brain.tts.preprocess.kor;

import java.util.List;
import maum.brain.tts.preprocess.type.KorPatternType;
import maum.brain.tts.utils.NumberUtils;
import maum.brain.tts.utils.PPUtils;

public class ConvertOrdinalNumber {
    public ConvertOrdinalNumber() {
    }

    public static String convertOrdinalNumberFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_BASIC_UNIT, token);
        if (aList != null && !aList.isEmpty()) {
            String extractDigit = PPUtils.getDigit(token);

            try {
                token = token.replace(extractDigit, NumberUtils.convertOrdinalNumber2Hangle(extractDigit));
                return token;
            } catch (Exception var4) {
                return token;
            }
        } else {
            return token;
        }
    }
}
