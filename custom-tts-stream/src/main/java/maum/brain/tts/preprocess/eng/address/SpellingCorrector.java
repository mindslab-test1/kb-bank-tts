package maum.brain.tts.preprocess.eng.address;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

public class SpellingCorrector {
    private static final Map<Integer, Set<String>> STATE_TOKENS = new HashMap();
    private static final Pattern DIGIT = Pattern.compile("^\\d+$");

    public SpellingCorrector() {
    }

    public static String correctStateSpelling(String rawAddress) {
        String[] originalTokens = rawAddress.split("\\s+");
        String[] tokens = rawAddress.toUpperCase().split("\\s+");
        int end = tokens.length - 1;

        int i;
        for(i = end; i > 0 && DIGIT.matcher(tokens[i]).matches(); --i) {
            --end;
        }

        if (tokens[end].length() <= 2) {
            return rawAddress;
        } else {
            for(i = 1; i <= 4; ++i) {
                if (end >= i - 1) {
                    Iterator var5 = ((Set)STATE_TOKENS.get(i)).iterator();

                    while(var5.hasNext()) {
                        String s = (String)var5.next();
                        StringBuilder sb = new StringBuilder();
                        int newEnd = end - i + 1;

                        for(int j = 0; j < i; ++j) {
                            sb.append(tokens[newEnd + j]).append(" ");
                        }

                        float metrics = getNormalizedSimilarity(s, sb.toString().trim());
                        if (metrics == 1.0F) {
                            return rawAddress;
                        }

                        if (metrics >= 0.75F) {
                            if (i != 1) {
                                for(int j = 0; j < i - 1; ++j) {
                                    originalTokens[newEnd + j] = "";
                                }
                            }

                            originalTokens[end] = s;
                            return StringUtils.join(originalTokens, " ");
                        }
                    }
                }
            }

            return rawAddress;
        }
    }

    private static float getNormalizedSimilarity(String s, String t) {
        return 1.0F - (float)StringUtils.getLevenshteinDistance(s, t) / (float)Math.max(s.length(), t.length());
    }

    static {
        String s;
        int size;
        for(Iterator var0 = Data.getSTATE_CODE_MAP().keySet().iterator(); var0.hasNext(); ((Set)STATE_TOKENS.get(size)).add(s)) {
            s = (String)var0.next();
            size = s.split("\\s+").length;
            Set<String> set = (Set)STATE_TOKENS.get(size);
            if (set == null) {
                STATE_TOKENS.put(size, new HashSet());
            }
        }

    }
}
