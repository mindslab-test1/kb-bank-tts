package maum.brain.tts.preprocess.kor;

import java.util.List;
import maum.brain.tts.preprocess.type.KorPatternType;
import maum.brain.tts.utils.PPUtils;

public class ConvertRegidentNumber {
    public ConvertRegidentNumber() {
    }

    public static String convertRegidentNumberFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_REGIDENT_NUMBER, token);

        for(int i = 0; i < aList.size(); ++i) {
            token = convertRegidentNumber2Hangle((String)aList.get(i));
        }

        return token;
    }

    private static String convertRegidentNumber2Hangle(String text) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < text.length(); ++i) {
            if ("0".equals(String.valueOf(text.charAt(i)))) {
                sb.append("공");
            }

            if ("1".equals(String.valueOf(text.charAt(i)))) {
                sb.append("일");
            }

            if ("2".equals(String.valueOf(text.charAt(i)))) {
                sb.append("이");
            }

            if ("3".equals(String.valueOf(text.charAt(i)))) {
                sb.append("삼");
            }

            if ("4".equals(String.valueOf(text.charAt(i)))) {
                sb.append("사");
            }

            if ("5".equals(String.valueOf(text.charAt(i)))) {
                sb.append("오");
            }

            if ("6".equals(String.valueOf(text.charAt(i)))) {
                sb.append("육");
            }

            if ("7".equals(String.valueOf(text.charAt(i)))) {
                sb.append("칠");
            }

            if ("8".equals(String.valueOf(text.charAt(i)))) {
                sb.append("팔");
            }

            if ("9".equals(String.valueOf(text.charAt(i)))) {
                sb.append("구");
            }

            if ("-".equals(String.valueOf(text.charAt(i)))) {
                sb.append(" ").append("다시").append(" ");
            }

            if ("~".equals(String.valueOf(text.charAt(i)))) {
                sb.append(" ").append("다시").append(" ");
            }
        }

        return sb.toString();
    }
}
