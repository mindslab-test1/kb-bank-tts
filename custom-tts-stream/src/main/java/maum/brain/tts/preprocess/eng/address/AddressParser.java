package maum.brain.tts.preprocess.eng.address;

import java.util.EnumMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

public class AddressParser {
    private static final Pattern CORNER;
    private static final Pattern STREET_ADDRESS;
    private static final Pattern CSZ;
    private static final Pattern INTERSECTION;
    private static final Pattern CLEANUP;
    private static final Pattern STREET_TYPES;
    private static final Pattern STATES;
    private static Pattern STREET_DESIGNATOR_CHECK;

    public AddressParser() {
    }

    private static String getCleanSttring(String rawAddr) {
        return CLEANUP.matcher(rawAddr).replaceAll(" ").replaceAll("\\s+", " ").trim();
    }

    public static Map<AddressComponent, String> parseAddress(String rawAddr, boolean autoCorrectStateSpelling) {
        rawAddr = getCleanSttring(rawAddr);
        if (autoCorrectStateSpelling) {
            rawAddr = SpellingCorrector.correctStateSpelling(rawAddr);
        }

        Matcher m = STREET_ADDRESS.matcher(rawAddr);
        Map<AddressComponent, String> ret = null;
        if (m.matches()) {
            ret = getAddrMap(m, AddressRegexLibrary.P_STREET_ADDRESS.getNamedGroupMap());
            postProcess(ret);
            String splitRawAddr = null;
            String line12sep = (String)ret.get(AddressComponent.TLID);
            if (!line12sep.contains(",") && (splitRawAddr = designatorConfusingCitiesCorrection(ret, rawAddr)) != null) {
                m = STREET_ADDRESS.matcher(splitRawAddr);
                if (m.matches()) {
                    ret = getAddrMap(m, AddressRegexLibrary.P_STREET_ADDRESS.getNamedGroupMap());
                    ret.remove(AddressComponent.TLID);
                    return ret;
                }
            }

            ret.remove(AddressComponent.TLID);
        }

        m = CORNER.matcher(rawAddr);
        if (ret == null && m.find()) {
            m = INTERSECTION.matcher(rawAddr);
            if (m.matches()) {
                ret = getAddrMap(m, AddressRegexLibrary.P_INTERSECTION.getNamedGroupMap());
            }
        }

        if (ret == null) {
            m = CSZ.matcher(rawAddr);
            if (m.matches()) {
                ret = getAddrMap(m, AddressRegexLibrary.P_CSZ.getNamedGroupMap());
            }
        }

        return ret;
    }

    public static Map<AddressComponent, String> parseAddress(String rawAddr) {
        return parseAddress(rawAddr, true);
    }

    private static void postProcess(Map<AddressComponent, String> m) {
        if (m.get(AddressComponent.TYPE) == null && m.get(AddressComponent.STREET) != null && STREET_TYPES.matcher(((String)m.get(AddressComponent.STREET)).toUpperCase()).matches()) {
            m.put(AddressComponent.TYPE, m.get(AddressComponent.STREET));
            m.put(AddressComponent.STREET, m.get(AddressComponent.PREDIR));
            m.put(AddressComponent.PREDIR, (String) null);
        }

        if (m.get(AddressComponent.STATE) == null && m.get(AddressComponent.LINE2) != null && STATES.matcher(((String)m.get(AddressComponent.LINE2)).toUpperCase()).matches()) {
            m.put(AddressComponent.STATE, m.get(AddressComponent.LINE2));
            m.put(AddressComponent.LINE2, (String) null);
        }

    }

    private static Map<AddressComponent, String> getAddrMap(Matcher m, Map<Integer, String> groupMap) {
        Map<AddressComponent, String> ret = new EnumMap(AddressComponent.class);

        for(int i = 1; i <= m.groupCount(); ++i) {
            String name = (String)groupMap.get(i);
            AddressComponent comp = AddressComponent.valueOf(name);
            if (ret.get(comp) == null) {
                putIfNotNull(ret, comp, m.group(i));
            }
        }

        return ret;
    }

    private static void putIfNotNull(Map<AddressComponent, String> m, AddressComponent ac, String v) {
        if (v != null) {
            m.put(ac, v);
        }

    }

    private static String designatorConfusingCitiesCorrection(Map<AddressComponent, String> parsedLocation, String input) {
        String street = (String)parsedLocation.get(AddressComponent.STREET);
        String type = (String)parsedLocation.get(AddressComponent.TYPE);
        String line2 = (String)parsedLocation.get(AddressComponent.LINE2);
        if (street != null && type != null && line2 == null && street.split(" ").length >= 2) {
            Matcher m = STREET_DESIGNATOR_CHECK.matcher(street);
            if (!m.find()) {
                return null;
            } else {
                String parsedstate = (String)parsedLocation.get(AddressComponent.STATE);
                String normalizedState;
                if (parsedstate == null) {
                    normalizedState = (String)parsedLocation.get(AddressComponent.CITY);
                    if (normalizedState != null && normalizedState.length() == 2) {
                        parsedstate = normalizedState;
                    }
                }

                normalizedState = AddressStandardizer.normalizeState(StringUtils.upperCase(parsedstate));
                String inputUpper = input.toUpperCase();
                String ret = null;
                Set<String> stateSet = new HashSet();
                if (normalizedState != null) {
                    stateSet.add(normalizedState);
                } else {
                    stateSet.addAll(SpecialData.dict.keySet());
                }

                int stateIdx = parsedstate == null ? input.length() : input.lastIndexOf(parsedstate);
                Iterator var12 = stateSet.iterator();

                while(var12.hasNext()) {
                    String state = (String)var12.next();
                    Iterator var14 = ((List)SpecialData.dict.get(state)).iterator();

                    while(var14.hasNext()) {
                        String s = (String)var14.next();
//                        int idx = true;
                        int idx;
                        if ((idx = inputUpper.lastIndexOf(s)) != -1 && idx + s.length() >= stateIdx - 2) {
                            return input.substring(0, idx) + "," + input.substring(idx);
                        }
                    }
                }

                return (String)ret;
            }
        } else {
            return null;
        }
    }

    static {
        CORNER = Pattern.compile(AddressRegexLibrary.P_CORNER.getRegex());
        STREET_ADDRESS = Pattern.compile(AddressRegexLibrary.P_STREET_ADDRESS.getRegex());
        CSZ = Pattern.compile(AddressRegexLibrary.P_CSZ.getRegex());
        INTERSECTION = Pattern.compile(AddressRegexLibrary.P_INTERSECTION.getRegex());
        CLEANUP = Pattern.compile("^\\W+|\\W+$|[\\s\\p{Punct}&&[^\\)\\(#&,:;@'`-]]");
        STREET_TYPES = Pattern.compile(RegexLibrary.STREET_DESIGNATOR);
        STATES = Pattern.compile(RegexLibrary.US_STATES);
        STREET_DESIGNATOR_CHECK = Pattern.compile("\\b(?i:(?:" + RegexLibrary.STREET_DESIGNATOR + "))\\b");
    }
}
