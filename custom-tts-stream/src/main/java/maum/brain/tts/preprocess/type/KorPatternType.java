package maum.brain.tts.preprocess.type;

import java.util.regex.Pattern;

public class KorPatternType {
    public static final Pattern P_ADDRESS = Pattern.compile("<address>.*</address>");
    public static final Pattern P_ADDRESS_LOAD = Pattern.compile("[가-힣A-Za-z·\\d~\\-\\.]+(로|길)");
    public static final Pattern P_ENGLISHDIGIT = Pattern.compile("([a-z]{2})([0-9]\\d{4,10})", 2);
    public static final Pattern P_NOT_KOREAN = Pattern.compile("([^가-힣]+)", 2);
    public static final Pattern P_BRACKET = Pattern.compile("[\\(]([^\\(|\\)])*[\\)]");
    public static final Pattern P_DIGIT = Pattern.compile("\\s([0-9]+)\\s");
    public static final Pattern P_CURRENCY = Pattern.compile("([0-9]+원)");
    public static final Pattern P_CURRENCY_DIGIT = Pattern.compile("([0-9,]+[만천백십]*원[가-힣]*)");
    public static final Pattern P_CURRENCY_UNIT = Pattern.compile("\\p{Sc}([0-9]*)", 2);
    public static final Pattern P_CURRENCY_UNIT_DIGIT = Pattern.compile("\\p{Sc}(\\d+(,\\d{3})*(\\.\\d+)?)", 2);
    public static final Pattern P_YYYYMMDD_EX = Pattern.compile("(19|20)\\d{2}[- /.]*(0[1-9]|1[012])[- /.]*(0[1-9]|[12][0-9]|3[01])", 10);
    public static final Pattern P_YYYYMMDD = Pattern.compile("(19|20|21)\\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])");
    public static final Pattern P_YYYYMMDDHH = Pattern.compile("(19|20|21)\\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(0[0-9]|1[0-9]|2[0-3])");
    public static final Pattern P_YYYYMMDDHHMI = Pattern.compile("(19|20|21)\\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(0[0-9]|1[0-9]|2[0-3])([0-5][0-9])");
    public static final Pattern P_YYYYMMDDHHMISS = Pattern.compile("(19|20|21)\\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(0[0-9]|1[0-9]|2[0-3])([0-5][0-9])([0-5][0-9])", 10);
    public static final Pattern P_MMDD = Pattern.compile("(0[1-9]|1[012])(0[1-9]|1[012])");
    public static final Pattern P_HHMI = Pattern.compile("(0[0-9]|1[0-9]|2[0-3])([0-5][0-9])");
    public static final Pattern P_HHMI_SEP = Pattern.compile("(0[0-9]|1[0-9]|2[0-3])\\:([0-5][0-9])");
    public static final Pattern P_MM = Pattern.compile("(0[1-9]|1[012])");
    public static final Pattern P_DD = Pattern.compile("(0[1-9]|[12][0-9]|3[01])");
    public static final Pattern P_HH = Pattern.compile("(0[0-9]|1[0-9]|2[0-3])");
    public static final Pattern P_MI = Pattern.compile("([0-5][0-9])");
    public static final Pattern P_SS = Pattern.compile("([0-5][0-9])");
    public static final Pattern P_YEAR = Pattern.compile("([0-9]+년)");
    public static final Pattern P_YEAR_EX = Pattern.compile("([0-9,]+년[가-힣]+)");
    public static final Pattern P_MONTH = Pattern.compile("([0-9]+월)");
    public static final Pattern P_MONTH_EX = Pattern.compile("([0-9,]+월[가-힣]+)");
    public static final Pattern P_DAY = Pattern.compile("([0-9]+일)");
    public static final Pattern P_DAY_EX = Pattern.compile("([0-9,]+일[가-힣]+)");
    public static final Pattern P_HOUR = Pattern.compile("((\\d{2}|\\d{1})시)");
    public static final Pattern P_MINUTE = Pattern.compile("([0-9]+분)");
    public static final Pattern P_SEC = Pattern.compile("([0-9]+초)");
    public static final Pattern P_HOUR_EX = Pattern.compile("([0-9,]+시간[가-힣]+)");
    public static final Pattern P_MINUTE_EX = Pattern.compile("([0-9,]+분[가-힣]+)");
    public static final Pattern P_SEC_EX = Pattern.compile("([0-9,]+초[가-힣]+)");
    public static final Pattern P_BETWEEN_MONTH = Pattern.compile("([0-9]+개월)");
    public static final Pattern P_BETWEEN_MONTH_EX = Pattern.compile("([0-9]+개월로)");
    public static final Pattern P_PHONE_NUMBER = Pattern.compile("(01(?:0|1|[6-9])-(?:\\d{3}|\\d{4})-\\d{4})");
    public static final Pattern P_LANDLINE_NUMBER = Pattern.compile("(0(?:2|[3-9])-\\d{3,4}-\\d{4})");
    public static final Pattern P_CSLINE_NUMBER = Pattern.compile("\\d{4}[(\\*)|-]\\d{4}");
    public static final Pattern P_REGIDENT_NUMBER = Pattern.compile("\\d{2}(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|[3][01])[-~]*([1-4][0-9]{6})");
    public static final Pattern P_EMAIL = Pattern.compile("([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.[a-zA-Z0-9._-]+)");
    private static final String regNumCheck = "([+-]?\\d[\\d,]*)([\\.\\d]*)";
    public static final Pattern P_TIDLE = Pattern.compile("([+-]?\\d[\\d,]*)([\\.\\d]*)(\\~| \\~|\\~ | \\~ )([+-]?\\d[\\d,]*)([\\.\\d]*)");
    public static final Pattern P_FRACTION = Pattern.compile("([+-]?\\d[\\d,]*)([\\.\\d]*)\\/([+-]?\\d[\\d,]*)([\\.\\d]*)");
    public static final Pattern P_PERCENT = Pattern.compile("\\d+\\.?\\d*%[가-힣]*");
    public static final Pattern P_PERCENT_POINT = Pattern.compile("\\d+\\.?\\d*%p[가-힣]*");
    public static final Pattern P_LEN_CM = Pattern.compile("\\d+\\.?\\d*㎝");
    public static final Pattern P_LEN_KM = Pattern.compile("\\d+\\.?\\d*㎞");
    public static final Pattern P_LEN_MM = Pattern.compile("\\d+\\.?\\d*㎜");
    public static final Pattern P_KG = Pattern.compile("\\d+\\.?\\d*㎏");
    public static final Pattern P_TEMP_C = Pattern.compile("\\d+\\.?\\d*℃");
    public static final Pattern P_TEMP_F = Pattern.compile("\\d+\\.?\\d*℉");
    public static final Pattern P_SQUARE = Pattern.compile("\\d+\\.?\\d*㎡");
    public static final Pattern P_DECIBEL = Pattern.compile("\\d+\\.?\\d*㏈");
    public static final Pattern P_CALORIE = Pattern.compile("\\d+\\.?\\d*㎈");
    public static final Pattern P_VELOCITY = Pattern.compile("\\d+\\.?\\d*㎧");
    public static final Pattern P_HERTZ = Pattern.compile("\\d+\\.?\\d*㎐");
    public static final Pattern P_BASIC_UNIT = Pattern.compile("(\\d+)(시간|대|문|명|가지|살|마리|포기|송이|수|톨|통|개|벌|척|채|다발|그루|자루|줄|켤레|그릇|잔|마디|상자|사람|곡|병|판|달|자리|건|돈)");
    public static final Pattern P_CHINA_UNIT = Pattern.compile("(\\d+)(매|포인트)");
    public static final Pattern P_SILENCE = Pattern.compile("(\\s\\d\\d)|(\\s\\d.\\d)|(\\s\\d)");
    public static final Pattern P_SILENCE_LEN = Pattern.compile("(\\d\\d)|(\\d.\\d)|\\d");

    public KorPatternType() {
    }
}
