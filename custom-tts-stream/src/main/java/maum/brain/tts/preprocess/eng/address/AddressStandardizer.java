package maum.brain.tts.preprocess.eng.address;

import java.util.EnumMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

public class AddressStandardizer {
    private static final Pattern TXT_NUM;
    private static final Pattern DIGIT;
    private static final Pattern LINE2A;

    public AddressStandardizer() {
    }

    public static String toSingleLine(Map<AddressComponent, String> parsedAddr) {
        if (parsedAddr == null) {
            return null;
        } else {
            StringBuilder sb = new StringBuilder();
            appendIfNotNull(sb, (String)parsedAddr.get(AddressComponent.NAME), ", ");
            appendIfNotNull(sb, (String)parsedAddr.get(AddressComponent.NUMBER), " ");
            appendIfNotNull(sb, (String)parsedAddr.get(AddressComponent.PREDIR), " ");
            appendIfNotNull(sb, (String)parsedAddr.get(AddressComponent.STREET), " ");
            if (parsedAddr.get(AddressComponent.STREET2) != null) {
                appendIfNotNull(sb, (String)parsedAddr.get(AddressComponent.TYPE2), " ");
                appendIfNotNull(sb, (String)parsedAddr.get(AddressComponent.POSTDIR2), " ");
                sb.append("& ");
                appendIfNotNull(sb, (String)parsedAddr.get(AddressComponent.PREDIR2), " ");
                appendIfNotNull(sb, (String)parsedAddr.get(AddressComponent.STREET2), " ");
            }

            appendIfNotNull(sb, (String)parsedAddr.get(AddressComponent.TYPE), " ");
            appendIfNotNull(sb, (String)parsedAddr.get(AddressComponent.POSTDIR), " ");
            if (StringUtils.isNotBlank(sb.toString())) {
                sb.append(", ");
            }

            appendIfNotNull(sb, (String)parsedAddr.get(AddressComponent.LINE2), ", ");
            appendIfNotNull(sb, (String)parsedAddr.get(AddressComponent.CITY), ", ");
            appendIfNotNull(sb, (String)parsedAddr.get(AddressComponent.STATE), " ");
            appendIfNotNull(sb, (String)parsedAddr.get(AddressComponent.ZIP), " ");
            return sb.toString().replaceAll(" ,", ",");
        }
    }

    private static void appendIfNotNull(StringBuilder sb, String s, String suffix) {
        if (s != null) {
            sb.append(s).append(suffix);
        }

    }

    public static Map<AddressComponent, String> normalizeParsedAddress(Map<AddressComponent, String> parsedAddr) {
        Map<AddressComponent, String> ret = new EnumMap(AddressComponent.class);
        Iterator var2 = parsedAddr.entrySet().iterator();

        while(var2.hasNext()) {
            Entry<AddressComponent, String> e = (Entry)var2.next();
            String v = StringUtils.upperCase((String)e.getValue());
            switch((AddressComponent)e.getKey()) {
                case PREDIR:
                    ret.put(AddressComponent.PREDIR, normalizeDir(v));
                    break;
                case POSTDIR:
                    ret.put(AddressComponent.POSTDIR, normalizeDir(v));
                    break;
                case TYPE:
                    ret.put(AddressComponent.TYPE, normalizeStreetType(v));
                    break;
                case PREDIR2:
                    ret.put(AddressComponent.PREDIR2, normalizeDir(v));
                    break;
                case POSTDIR2:
                    ret.put(AddressComponent.POSTDIR2, normalizeDir(v));
                    break;
                case TYPE2:
                    ret.put(AddressComponent.TYPE2, normalizeStreetType(v));
                    break;
                case NUMBER:
                    ret.put(AddressComponent.NUMBER, normalizeNum(v));
                    break;
                case STATE:
                    ret.put(AddressComponent.STATE, normalizeState(v));
                    break;
                case ZIP:
                    ret.put(AddressComponent.ZIP, normalizeZip(v));
                    break;
                case LINE2:
                    ret.put(AddressComponent.LINE2, normalizeLine2(v));
                    break;
                case CITY:
                    ret.put(AddressComponent.CITY, saintAbbrExpansion(v));
                    break;
                case STREET:
                    ret.put(AddressComponent.STREET, normalizeOrdinal(saintAbbrExpansion(v)));
                    break;
                case STREET2:
                    ret.put(AddressComponent.STREET2, normalizeOrdinal(saintAbbrExpansion(v)));
                    break;
                default:
                    ret.put(e.getKey(), v);
            }
        }

        ret.put(AddressComponent.CITY, resolveCityAlias((String)ret.get(AddressComponent.CITY), (String)ret.get(AddressComponent.STATE)));
        return ret;
    }

    private static String normalizeNum(String num) {
        if (num == null) {
            return null;
        } else {
            Matcher m = TXT_NUM.matcher(num);
            String ret = null;
            if (m.matches()) {
                ret = m.group(1);
                if (!ret.contains("-") && !ret.contains(" ")) {
                    ret = (String)Data.getNUMBER_MAP().get(ret);
                } else {
                    String[] pair = ret.split("[ -]");
                    String pre = ((String)Data.getNUMBER_MAP().get(pair[0])).substring(0, 1);
                    ret = pre + (String)Data.getNUMBER_MAP().get(pair[1]);
                }
            } else {
                m = DIGIT.matcher(num);
                if (m.matches()) {
                    ret = m.group(2) == null ? m.group(1) : m.group(1) + "-" + m.group(2);
                }
            }

            return (String)Utils.nvl(ret, num);
        }
    }

    private static String normalizeDir(String dir) {
        if (dir == null) {
            return null;
        } else {
            dir = dir.replace(" ", "");
            return dir.length() > 2 ? (String)Data.getDIRECTIONAL_MAP().get(dir) : dir;
        }
    }

    private static String normalizeStreetType(String type) {
        return (String)Utils.nvl(Data.getSTREET_TYPE_MAP().get(type), type);
    }

    public static String normalizeState(String state) {
        return (String)Utils.nvl(Data.getSTATE_CODE_MAP().get(state), state);
    }

    private static String normalizeLine2(String line2) {
        if (line2 == null) {
            return null;
        } else {
            Matcher m = LINE2A.matcher(line2);
            if (m.matches()) {
                Iterator var2 = Data.getUNIT_MAP().entrySet().iterator();

                while(var2.hasNext()) {
                    Entry<String, String> e = (Entry)var2.next();
                    if (line2.startsWith((String)e.getKey() + " ")) {
                        line2 = line2.replaceFirst((String)e.getKey() + " ", (String)e.getValue() + " ");
                        break;
                    }
                }
            }

            return line2;
        }
    }

    private static String normalizeZip(String zip) {
        return StringUtils.length(zip) > 5 ? zip.substring(0, 5) : zip;
    }

    private static String resolveCityAlias(String city, String state) {
        return AliasResolver.resolveCityAlias(city, state);
    }

    private static String saintAbbrExpansion(String city) {
        String exp = null;
        return (exp = (String)Data.getSAINT_NAME_MAP().get(city)) != null ? exp : city;
    }

    private static String normalizeOrdinal(String street) {
        String ordinal = null;
        return (ordinal = (String)Data.getORDINAL_MAP().get(street)) != null ? ordinal : street;
    }

    static {
        TXT_NUM = Pattern.compile("^\\W*(" + RegexLibrary.TXT_NUM_0_19 + ")\\W*");
        DIGIT = Pattern.compile("(.*?\\d+)\\W*(.+)?");
        LINE2A = Pattern.compile("\\W*(?:" + AddressRegexLibrary.LINE2A_GROUPED + ")\\W*");
    }
}
