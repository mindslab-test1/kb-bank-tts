package maum.brain.tts.preprocess.type;

public class SiteType {
    public static final String MINDSLAB = "Mindslab";
    public static final String KBCARD = "KBCard";
    public static final String SHINHAN = "Shinhan";
    public static final String KBCARD_G2P = "g2p.mlab:19002";
    public static final String G2P_EN = "127.0.0.1:19001";

    public SiteType() {
    }
}
