package maum.brain.tts.preprocess.eng;

import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.RuleBasedNumberFormat;
import com.ibm.icu.util.ULocale;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import maum.brain.tts.preprocess.type.EngPatternType;
import maum.brain.tts.utils.PPUtils;

public class ConvertToken {
    private static boolean bLog;
    private static final RuleBasedNumberFormat rbnf;
    private static final String cardinalRule = "%spellout-numbering";
    private static final String ordinalRule;
    private static final String yearRule;
    private static final DateFormat df;

    static {
        bLog = Boolean.TRUE;
        rbnf = new RuleBasedNumberFormat(ULocale.ENGLISH, 1);
        ordinalRule = getOrdinalRuleName(rbnf);
        yearRule = getYearRuleName(rbnf);
        df = DateFormat.getDateInstance(1, ULocale.ENGLISH);
    }

    public ConvertToken() {
    }

    public static void main(String[] args) {
        String token = "$5000.12";
        String ret = convertMoney(token);
        printLog(ret);
    }

    public static String convertDate(String token) {
        try {
            String tmptoken = token.toLowerCase().replaceAll("'", "");
            Boolean isComma = Boolean.FALSE;
            Boolean isDot = Boolean.FALSE;
            if (tmptoken.endsWith(",")) {
                tmptoken = tmptoken.substring(0, tmptoken.length() - 1);
                isComma = true;
            }

            if (tmptoken.endsWith(".")) {
                tmptoken = tmptoken.substring(0, tmptoken.length() - 1);
                isDot = true;
            }

            String[] dateParts;
            Date humanDate;
            String newToken;
            if (EngPatternType.P_US_DATE.matcher(tmptoken).matches()) {
                printLog("convertDate:P_US_DATE:" + tmptoken);
                tmptoken = tmptoken.replace(".", "/");
                tmptoken = tmptoken.replace("-", "/");
                dateParts = tmptoken.split("/");
                if (Integer.parseInt(dateParts[0]) > 12) {
                    humanDate = DateFormat.getPatternInstance("dd.MM.yyyy", ULocale.UK).parse(tmptoken);
                } else {
                    humanDate = DateFormat.getPatternInstance("dd.MM.yyyy", ULocale.US).parse(tmptoken);
                }

                dateParts = df.format(humanDate).replaceAll(",", "").split("\\s");
                dateParts[1] = convertOrdinal(dateParts[1]);
                dateParts[2] = convertYear(Double.parseDouble(dateParts[2]));
                newToken = dateParts[0] + " " + dateParts[1] + " " + dateParts[2];
                newToken = newToken.replaceAll("-", " ").toLowerCase();
                if (isComma) {
                    newToken = newToken.concat(",");
                }

                if (isDot) {
                    newToken = newToken.concat(".");
                }

                return newToken;
            }

            if (EngPatternType.P_US_DATE_EX.matcher(tmptoken).matches()) {
                printLog("convertDate:P_US_DATE_EX:" + tmptoken);
                tmptoken = tmptoken.replace(".", "/");
                tmptoken = tmptoken.replace("-", "/");
                dateParts = tmptoken.split("/");
                if (Integer.parseInt(dateParts[0]) > 12) {
                    humanDate = DateFormat.getPatternInstance("dd.MM.yyyy", ULocale.UK).parse(tmptoken);
                } else {
                    humanDate = DateFormat.getPatternInstance("dd.MM.yyyy", ULocale.US).parse(tmptoken);
                }

                dateParts = df.format(humanDate).replaceAll(",", "").split("\\s");
                dateParts[1] = convertOrdinal(dateParts[1]);
                dateParts[2] = "nineteen".concat(" ") + convertYear(Double.parseDouble(dateParts[2]));
                newToken = dateParts[0] + " " + dateParts[1] + " " + dateParts[2];
                newToken = newToken.replaceAll("-", " ").toLowerCase();
                if (isComma) {
                    newToken = newToken.concat(",");
                }

                if (isDot) {
                    newToken = newToken.concat(".");
                }

                return newToken;
            }

//            String newToken;
            if (EngPatternType.P_US_MONTH_DATE.matcher(tmptoken).matches()) {
                label411: {
                    printLog("convertDate:P_US_MONTH_DATE:" + tmptoken);
                    tmptoken = tmptoken.replace(".", "/");
                    tmptoken = tmptoken.replace("-", "/");
                    dateParts = tmptoken.split("/");
                    switch((newToken = dateParts[0]).hashCode()) {
                        case -2029849391:
                            if (newToken.equals("september")) {
                                tmptoken = tmptoken.replace("september", "09");
                                break label411;
                            }
                            break;
                        case -1826660246:
                            if (newToken.equals("january")) {
                                tmptoken = tmptoken.replace("january", "01");
                                break label411;
                            }
                            break;
                        case -1621487904:
                            if (newToken.equals("october")) {
                                tmptoken = tmptoken.replace("october", "10");
                                break label411;
                            }
                            break;
                        case -1406703101:
                            if (newToken.equals("august")) {
                                tmptoken = tmptoken.replace("august", "08");
                                break label411;
                            }
                            break;
                        case -263893086:
                            if (newToken.equals("february")) {
                                tmptoken = tmptoken.replace("february", "02");
                                break label411;
                            }
                            break;
                        case 107877:
                            if (newToken.equals("may")) {
                                tmptoken = tmptoken.replace("may", "05");
                                break label411;
                            }
                            break;
                        case 3273752:
                            if (newToken.equals("july")) {
                                tmptoken = tmptoken.replace("july", "07");
                                break label411;
                            }
                            break;
                        case 3273794:
                            if (newToken.equals("june")) {
                                tmptoken = tmptoken.replace("june", "06");
                                break label411;
                            }
                            break;
                        case 93031046:
                            if (newToken.equals("april")) {
                                tmptoken = tmptoken.replace("april", "04");
                                break label411;
                            }
                            break;
                        case 103666243:
                            if (newToken.equals("march")) {
                                tmptoken = tmptoken.replace("march", "03");
                                break label411;
                            }
                            break;
                        case 561839141:
                            if (newToken.equals("december")) {
                                tmptoken = tmptoken.replace("december", "12");
                                break label411;
                            }
                            break;
                        case 1639129394:
                            if (newToken.equals("november")) {
                                tmptoken = tmptoken.replace("november", "11");
                                break label411;
                            }
                    }

                    tmptoken = tmptoken.replace("december", "12");
                }

                humanDate = DateFormat.getPatternInstance("dd.MM.yyyy", ULocale.US).parse(tmptoken);
                dateParts = df.format(humanDate).replaceAll(",", "").split("\\s");
                dateParts[1] = convertOrdinal(dateParts[1]);
                dateParts[2] = convertYear(Double.parseDouble(dateParts[2]));
                newToken = dateParts[0] + " " + dateParts[1] + " " + dateParts[2];
                newToken = newToken.replaceAll("-", " ").toLowerCase();
                if (isComma) {
                    newToken = newToken.concat(",");
                }

                if (isDot) {
                    newToken = newToken.concat(".");
                }

                return newToken;
            }

            if (EngPatternType.P_US_MONTH_ABBR_DATE.matcher(tmptoken).matches()) {
                label391: {
                    printLog("convertDate:P_US_MONTH_ABBR_DATE:" + tmptoken);
                    tmptoken = tmptoken.replace(".", "/");
                    tmptoken = tmptoken.replace("-", "/");
                    dateParts = tmptoken.split("/");
                    switch((newToken = dateParts[0]).hashCode()) {
                        case 96803:
                            if (newToken.equals("apr")) {
                                tmptoken = tmptoken.replace("apr", "04");
                                break label391;
                            }
                            break;
                        case 96947:
                            if (newToken.equals("aug")) {
                                tmptoken = tmptoken.replace("aug", "08");
                                break label391;
                            }
                            break;
                        case 99330:
                            if (newToken.equals("dec")) {
                                tmptoken = tmptoken.replace("dec", "12");
                                break label391;
                            }
                            break;
                        case 101251:
                            if (newToken.equals("feb")) {
                                tmptoken = tmptoken.replace("feb", "02");
                                break label391;
                            }
                            break;
                        case 104983:
                            if (newToken.equals("jan")) {
                                tmptoken = tmptoken.replace("jan", "01");
                                break label391;
                            }
                            break;
                        case 105601:
                            if (newToken.equals("jul")) {
                                tmptoken = tmptoken.replace("jul", "07");
                                break label391;
                            }
                            break;
                        case 105603:
                            if (newToken.equals("jun")) {
                                tmptoken = tmptoken.replace("jun", "06");
                                break label391;
                            }
                            break;
                        case 107870:
                            if (newToken.equals("mar")) {
                                tmptoken = tmptoken.replace("mar", "03");
                                break label391;
                            }
                            break;
                        case 107877:
                            if (newToken.equals("may")) {
                                tmptoken = tmptoken.replace("may", "05");
                                break label391;
                            }
                            break;
                        case 109269:
                            if (newToken.equals("nov")) {
                                tmptoken = tmptoken.replace("nov", "11");
                                break label391;
                            }
                            break;
                        case 109856:
                            if (newToken.equals("oct")) {
                                tmptoken = tmptoken.replace("oct", "10");
                                break label391;
                            }
                            break;
                        case 113758:
                            if (newToken.equals("sep")) {
                                tmptoken = tmptoken.replace("sep", "09");
                                break label391;
                            }
                    }

                    tmptoken = tmptoken.replace("dec", "12");
                }

                humanDate = DateFormat.getPatternInstance("dd.MM.yyyy", ULocale.US).parse(tmptoken);
                dateParts = df.format(humanDate).replaceAll(",", "").split("\\s");
                dateParts[1] = convertOrdinal(dateParts[1]);
                dateParts[2] = convertYear(Double.parseDouble(dateParts[2]));
                newToken = dateParts[0] + " " + dateParts[1] + " " + dateParts[2];
                newToken = newToken.replaceAll("-", " ").toLowerCase();
                if (isComma) {
                    newToken = newToken.concat(",");
                }

                if (isDot) {
                    newToken = newToken.concat(".");
                }

                return newToken;
            }

            if (EngPatternType.P_UK_MONTH_ABBR_DATE.matcher(tmptoken).matches()) {
                label371: {
                    printLog("convertDate:P_UK_MONTH_ABBR_DATE:" + tmptoken);
                    tmptoken = tmptoken.replace(".", "/");
                    tmptoken = tmptoken.replace("-", "/");
                    dateParts = tmptoken.split("/");
                    switch((newToken = dateParts[1]).hashCode()) {
                        case 96803:
                            if (newToken.equals("apr")) {
                                tmptoken = tmptoken.replace("apr", "04");
                                break label371;
                            }
                            break;
                        case 96947:
                            if (newToken.equals("aug")) {
                                tmptoken = tmptoken.replace("aug", "08");
                                break label371;
                            }
                            break;
                        case 99330:
                            if (newToken.equals("dec")) {
                                tmptoken = tmptoken.replace("dec", "12");
                                break label371;
                            }
                            break;
                        case 101251:
                            if (newToken.equals("feb")) {
                                tmptoken = tmptoken.replace("feb", "02");
                                break label371;
                            }
                            break;
                        case 104983:
                            if (newToken.equals("jan")) {
                                tmptoken = tmptoken.replace("jan", "01");
                                break label371;
                            }
                            break;
                        case 105601:
                            if (newToken.equals("jul")) {
                                tmptoken = tmptoken.replace("jul", "07");
                                break label371;
                            }
                            break;
                        case 105603:
                            if (newToken.equals("jun")) {
                                tmptoken = tmptoken.replace("jun", "06");
                                break label371;
                            }
                            break;
                        case 107870:
                            if (newToken.equals("mar")) {
                                tmptoken = tmptoken.replace("mar", "03");
                                break label371;
                            }
                            break;
                        case 107877:
                            if (newToken.equals("may")) {
                                tmptoken = tmptoken.replace("may", "05");
                                break label371;
                            }
                            break;
                        case 109269:
                            if (newToken.equals("nov")) {
                                tmptoken = tmptoken.replace("nov", "11");
                                break label371;
                            }
                            break;
                        case 109856:
                            if (newToken.equals("oct")) {
                                tmptoken = tmptoken.replace("oct", "10");
                                break label371;
                            }
                            break;
                        case 113758:
                            if (newToken.equals("sep")) {
                                tmptoken = tmptoken.replace("sep", "09");
                                break label371;
                            }
                    }

                    tmptoken = tmptoken.replace("dec", "12");
                }

                humanDate = DateFormat.getPatternInstance("dd.MM.yyyy", ULocale.UK).parse(tmptoken);
                dateParts = df.format(humanDate).replaceAll(",", "").split("\\s");
                dateParts[1] = convertOrdinal(dateParts[1]);
                dateParts[2] = convertYear(Double.parseDouble(dateParts[2]));
                newToken = dateParts[0] + " " + dateParts[1] + " " + dateParts[2];
                newToken = newToken.replaceAll("-", " ").toLowerCase();
                if (isComma) {
                    newToken = newToken.concat(",");
                }

                if (isDot) {
                    newToken = newToken.concat(".");
                }

                return newToken;
            }

            if (EngPatternType.P_ISO_MONTH_ABBR_DATE.matcher(tmptoken).matches()) {
                label351: {
                    printLog("convertDate:P_ISO_MONTH_ABBR_DATE:" + tmptoken);
                    tmptoken = tmptoken.replace(".", "/");
                    tmptoken = tmptoken.replace("-", "/");
                    dateParts = tmptoken.split("/");
                    switch((newToken = dateParts[1]).hashCode()) {
                        case 96803:
                            if (newToken.equals("apr")) {
                                dateParts[1] = "04";
                                break label351;
                            }
                            break;
                        case 96947:
                            if (newToken.equals("aug")) {
                                dateParts[1] = "08";
                                break label351;
                            }
                            break;
                        case 99330:
                            if (newToken.equals("dec")) {
                                dateParts[1] = "12";
                                break label351;
                            }
                            break;
                        case 101251:
                            if (newToken.equals("feb")) {
                                dateParts[1] = "02";
                                break label351;
                            }
                            break;
                        case 104983:
                            if (newToken.equals("jan")) {
                                dateParts[1] = "01";
                                break label351;
                            }
                            break;
                        case 105601:
                            if (newToken.equals("jul")) {
                                dateParts[1] = "07";
                                break label351;
                            }
                            break;
                        case 105603:
                            if (newToken.equals("jun")) {
                                dateParts[1] = "06";
                                break label351;
                            }
                            break;
                        case 107870:
                            if (newToken.equals("mar")) {
                                dateParts[1] = "03";
                                break label351;
                            }
                            break;
                        case 107877:
                            if (newToken.equals("may")) {
                                dateParts[1] = "05";
                                break label351;
                            }
                            break;
                        case 109269:
                            if (newToken.equals("nov")) {
                                dateParts[1] = "11";
                                break label351;
                            }
                            break;
                        case 109856:
                            if (newToken.equals("oct")) {
                                dateParts[1] = "10";
                                break label351;
                            }
                            break;
                        case 113758:
                            if (newToken.equals("sep")) {
                                dateParts[1] = "09";
                                break label351;
                            }
                    }

                    dateParts[1] = "12";
                }

                newToken = dateParts[1] + "/" + dateParts[2] + "/" + dateParts[0];
                humanDate = DateFormat.getPatternInstance("dd.MM.yyyy", ULocale.US).parse(newToken);
                dateParts = df.format(humanDate).replaceAll(",", "").split("\\s");
                dateParts[1] = convertOrdinal(dateParts[1]);
                dateParts[2] = convertYear(Double.parseDouble(dateParts[2]));
                newToken = dateParts[0] + " " + dateParts[1] + " " + dateParts[2];
                newToken = newToken.replaceAll("-", " ").toLowerCase();
                if (isComma) {
                    newToken = newToken.concat(",");
                }

                if (isDot) {
                    newToken = newToken.concat(".");
                }

                return newToken;
            }

            if (EngPatternType.P_ISO_MONTH_DATE.matcher(tmptoken).matches()) {
                label331: {
                    printLog("convertDate:P_ISO_MONTH_DATE:" + tmptoken);
                    tmptoken = tmptoken.replace(".", "/");
                    tmptoken = tmptoken.replace("-", "/");
                    dateParts = tmptoken.split("/");
                    switch((newToken = dateParts[1]).hashCode()) {
                        case -2029849391:
                            if (newToken.equals("september")) {
                                dateParts[1] = "09";
                                break label331;
                            }
                            break;
                        case -1826660246:
                            if (newToken.equals("january")) {
                                dateParts[1] = "01";
                                break label331;
                            }
                            break;
                        case -1621487904:
                            if (newToken.equals("october")) {
                                dateParts[1] = "10";
                                break label331;
                            }
                            break;
                        case -1406703101:
                            if (newToken.equals("august")) {
                                dateParts[1] = "08";
                                break label331;
                            }
                            break;
                        case -263893086:
                            if (newToken.equals("february")) {
                                dateParts[1] = "02";
                                break label331;
                            }
                            break;
                        case 107877:
                            if (newToken.equals("may")) {
                                dateParts[1] = "05";
                                break label331;
                            }
                            break;
                        case 3273752:
                            if (newToken.equals("july")) {
                                dateParts[1] = "07";
                                break label331;
                            }
                            break;
                        case 3273794:
                            if (newToken.equals("june")) {
                                dateParts[1] = "06";
                                break label331;
                            }
                            break;
                        case 93031046:
                            if (newToken.equals("april")) {
                                dateParts[1] = "04";
                                break label331;
                            }
                            break;
                        case 103666243:
                            if (newToken.equals("march")) {
                                dateParts[1] = "03";
                                break label331;
                            }
                            break;
                        case 561839141:
                            if (newToken.equals("december")) {
                                dateParts[1] = "12";
                                break label331;
                            }
                            break;
                        case 1639129394:
                            if (newToken.equals("november")) {
                                dateParts[1] = "11";
                                break label331;
                            }
                    }

                    dateParts[1] = "12";
                }

                newToken = dateParts[1] + "/" + dateParts[2] + "/" + dateParts[0];
                humanDate = DateFormat.getPatternInstance("dd.MM.yyyy", ULocale.US).parse(newToken);
                dateParts = df.format(humanDate).replaceAll(",", "").split("\\s");
                dateParts[1] = convertOrdinal(dateParts[1]);
                dateParts[2] = convertYear(Double.parseDouble(dateParts[2]));
                newToken = dateParts[0] + " " + dateParts[1] + " " + dateParts[2];
                newToken = newToken.replaceAll("-", " ").toLowerCase();
                if (isComma) {
                    newToken = newToken.concat(",");
                }

                if (isDot) {
                    newToken = newToken.concat(".");
                }

                return newToken;
            }

            if (EngPatternType.P_ISO_DATE.matcher(tmptoken).matches()) {
                printLog("convertDate:P_ISO_DATE:" + tmptoken);
                tmptoken = tmptoken.replace(".", "/");
                tmptoken = tmptoken.replace("-", "/");
                dateParts = tmptoken.split("/");
                new String();
                if (Integer.parseInt(dateParts[1]) > 12) {
                    newToken = dateParts[1] + "/" + dateParts[2] + "/" + dateParts[0];
                    humanDate = DateFormat.getPatternInstance("dd.MM.yyyy", ULocale.UK).parse(newToken);
                } else {
                    newToken = dateParts[1] + "/" + dateParts[2] + "/" + dateParts[0];
                    humanDate = DateFormat.getPatternInstance("dd.MM.yyyy", ULocale.US).parse(newToken);
                }

                dateParts = df.format(humanDate).replaceAll(",", "").split("\\s");
                dateParts[1] = convertOrdinal(dateParts[1]);
                dateParts[2] = convertYear(Double.parseDouble(dateParts[2]));
                newToken = dateParts[0] + " " + dateParts[1] + " " + dateParts[2];
                newToken = newToken.replaceAll("-", " ").toLowerCase();
                if (isComma) {
                    newToken = newToken.concat(",");
                }

                if (isDot) {
                    newToken = newToken.concat(".");
                }

                return newToken;
            }
        } catch (Exception var8) {
            printLog(var8.getMessage());
        }

        return token;
    }

    public static String convertFeetInch(String token) {
        if (EngPatternType.P_FEET_INCH.matcher(token).find()) {
            printLog("convertFeetInch:" + token);
            String[] puncTokens = token.split("'");
            StringBuilder sb = new StringBuilder();
            String feet;
            if (puncTokens.length == 1) {
                feet = puncTokens[0];
                if (feet.endsWith("\"")) {
                    feet = feet.replace("\"", "");
                    sb.append(convertNumber(feet)).append(" ").append("inches");
                } else {
                    sb.append(convertNumber(feet)).append(" ").append("feet");
                }
            }

            if (puncTokens.length == 2) {
                feet = puncTokens[0];
                String inch = puncTokens[1].replace("\"", "");
                sb.append(convertNumber(feet)).append(" ").append("feet").append(" ").append(convertNumber(inch)).append(" ").append("inches");
            }

            return sb.toString();
        } else {
            return token;
        }
    }

    public static String convertPunctuation(String token) {
        if (EngPatternType.P_PUNCTUATION.matcher(token).find() && token.length() > 1) {
            printLog("convertPunctuation:" + token);
            String[] puncTokens = token.split("((?<=\\p{Punct})|(?=\\p{Punct}))");
            StringBuilder sb = new StringBuilder();
            String[] var6 = puncTokens;
            int var5 = puncTokens.length;

            for(int var4 = 0; var4 < var5; ++var4) {
                String tok = var6[var4];
                tok = tok.replaceAll("[,\\]\\[]", "");
                sb.append(tok).append(" ");
            }

            return sb.toString();
        } else {
            return token;
        }
    }

    public static String convertConsonant(String token) {
        if (EngPatternType.P_CONSONANT.matcher(token).matches()) {
            printLog("convertConsonant:" + token);
            StringBuilder sb = new StringBuilder();

            for(int i = 0; i < token.length(); ++i) {
                if (i > 0) {
                    sb.append(' ');
                }

                sb.append(token.charAt(i));
            }

            return sb.toString();
        } else {
            return token;
        }
    }

    public static String addSpace(String token) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < token.length(); ++i) {
            if (i > 0) {
                sb.append(' ');
            }

            sb.append(token.charAt(i));
        }

        return sb.toString();
    }

    public static String convertUnderscoreAndDash(String token) {
        int var5;
        int var9;
        if (token.contains("-")) {
            printLog("convertUnderscoreAndDash:" + token);
            StringBuilder sb = new StringBuilder();
            String[] tokens = token.split("-");
            int len = tokens.length;
            if (len == 2) {
                if (!tokens[0].endsWith("Hz".toLowerCase()) && !tokens[0].endsWith("KHz".toLowerCase()) && !tokens[0].endsWith("MHz".toLowerCase()) && !tokens[0].endsWith("GHz".toLowerCase())) {
                    sb.append(tokens[0]);
                } else {
                    sb.append(convertNumberWord(tokens[0]));
                }

                sb.append(" to ");
                if (!tokens[1].endsWith("Hz".toLowerCase()) && !tokens[1].endsWith("KHz".toLowerCase()) && !tokens[1].endsWith("MHz".toLowerCase()) && !tokens[1].endsWith("GHz".toLowerCase())) {
                    sb.append(tokens[1]);
                } else {
                    sb.append(convertNumberWord(tokens[1]));
                }

                return sb.toString();
            } else {
                sb.setLength(0);
                String[] var17 = tokens;
                int var16 = tokens.length;

                for(var5 = 0; var5 < var16; ++var5) {
                    String tok = var17[var5];
                    if (tok.matches("\\d+")) {
                        char[] var11;
                        int var19 = (var11 = tok.toCharArray()).length;

                        for(var9 = 0; var9 < var19; ++var9) {
                            char c = var11[var9];
                            sb.append(convertNumber(String.valueOf(c))).append(" ");
                        }
                    } else {
                        sb.append(tok).append(" ");
                    }
                }

                return sb.toString();
            }
        } else if (!token.contains("_")) {
            return token;
        } else {
            printLog("convertUnderscoreAndDash:" + token);
            String[] tokens = token.split("[-_]");
            StringBuilder sb = new StringBuilder();
            String[] var6 = tokens;
            var5 = tokens.length;

            for(int var4 = 0; var4 < var5; ++var4) {
                String tok = var6[var4];
                if (tok.matches("\\d+")) {
                    char[] var10;
                    var9 = (var10 = tok.toCharArray()).length;

                    for(int var8 = 0; var8 < var9; ++var8) {
                        char c = var10[var8];
                        sb.append(convertNumber(String.valueOf(c))).append(" ");
                    }
                } else {
                    sb.append(tok).append(" ");
                }
            }

            return sb.toString();
        }
    }

    public static String convertHashTag(String token) {
        Matcher m = EngPatternType.P_HASHTAG.matcher(token);
        if (!m.find()) {
            return token;
        } else {
            printLog("convertHashTag:" + token);
            StringBuilder sb = new StringBuilder("hashtag");
            sb.append(" ");
            String tag = m.group(2);
            if (tag.matches("[a-z]+") && tag.matches("[A-Z]+")) {
                sb.append(tag);
            } else {
                char lastChar = ' ';

                for(int ii = 0; ii < tag.length(); ++ii) {
                    char cc = tag.charAt(ii);
                    if (Character.isDigit(cc)) {
                        if (lastChar != ' ') {
                            sb.append(' ');
                        }

                        int number;
                        for(number = 0; ii < tag.length() && Character.isDigit(cc = tag.charAt(ii)); ++ii) {
                            number *= 10;
                            lastChar = cc;
                            number += Integer.parseInt(Character.toString(cc));
                        }

                        --ii;
                        sb.append(convertNumber((double)number));
                    } else {
                        if (Character.isUpperCase(cc) || Character.isDigit(lastChar)) {
                            sb.append(' ');
                        }

                        lastChar = cc;
                        sb.append(cc);
                    }
                }
            }

            return sb.toString();
        }
    }

    public static String convertURL(String token) {
        Matcher m = EngPatternType.P_URL.matcher(token);
        if (m.find()) {
            printLog("convertURL:" + token);
            String email;
            String[] tokens;
            String tmpToken;
            if (token.startsWith("http://")) {
                email = m.group(2);
                tokens = email.split("((?<=[.@/])|(?=[.@/]))");
                tmpToken = Arrays.toString(tokens).replaceAll("[,\\]\\[]", "").replaceAll("[.]", "dot").replaceAll("@", "at").replaceAll("www", "w w w");
                return "h t t p colon slash slash " + tmpToken;
            } else {
                email = m.group(2);
                tokens = email.split("((?<=[.@/])|(?=[.@/]))");
                tmpToken = Arrays.toString(tokens).replaceAll("[,\\]\\[]", "").replaceAll("[.]", "dot").replaceAll("@", "at");
                return tmpToken;
            }
        } else {
            return token;
        }
    }

    public static String convertNumberWord(String token) {
        if (!EngPatternType.P_NUMBERWORD.matcher(token).matches()) {
            return token;
        } else {
            printLog("convertNumberWord:" + token);
            StringBuilder sb = new StringBuilder();
            String[] groups = token.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
            int len = groups.length;
            if (len == 2) {
                if (groups[1].equalsIgnoreCase("Hz")) {
                    sb.append(convertNumber(groups[0])).append(" ").append("hertz");
                    return sb.toString();
                }

                if (groups[1].equalsIgnoreCase("KHz")) {
                    sb.append(convertNumber(groups[0])).append(" ").append("kilohertz");
                    return sb.toString();
                }

                if (groups[1].equalsIgnoreCase("MHz")) {
                    sb.append(convertNumber(groups[0])).append(" ").append("megahertz");
                    return sb.toString();
                }

                if (groups[1].equalsIgnoreCase("GHz")) {
                    sb.append(convertNumber(groups[0])).append(" ").append("gigahertz");
                    return sb.toString();
                }
            }

            String[] var7 = groups;
            int var6 = groups.length;

            for(int var5 = 0; var5 < var6; ++var5) {
                String group = var7[var5];
                if (sb.length() > 0) {
                    sb.append(' ');
                }

                if (group.matches("\\d+")) {
                    for(int ii = 0; ii < group.length(); ++ii) {
                        if (ii != 0) {
                            sb.append(' ');
                        }

                        sb.append(convertNumber(Character.toString(group.charAt(ii))));
                    }
                } else {
                    sb.append(group);
                }
            }

            return sb.toString();
        }
    }

    public static String convertDurationEx(String token) {
        Matcher m = EngPatternType.P_DURATION_EX.matcher(token);
        if (m.find()) {
            printLog("convertDurationEx:" + token);
            String hrs = convertNumber(m.group(2)) + " hours ";
            String mins = convertNumber(m.group(4)) + " minutes ";
            String secs = convertNumber(m.group(6)) + " seconds";
            return hrs + mins + secs;
        } else {
            return token;
        }
    }

    public static String convertDuration(String token) {
        Matcher m = EngPatternType.P_DURATION.matcher(token);
        if (m.find()) {
            printLog("convertDuration:" + token);
            String hrs = convertNumber(m.group(1)) + " hours ";
            String mins = convertNumber(m.group(2)).replace("-", " ") + " minutes ";
            String secs = convertNumber(m.group(3)) + " seconds";
            if (m.group(4) != null) {
                new String();
                String ms = " and " + convertNumber(m.group(5)) + " milliseconds ";
                return hrs + mins + secs + ms;
            } else {
                return hrs + mins + secs;
            }
        } else {
            return token;
        }
    }

    public static String convertTime(String token) {
        String[] splitToken = token.split(":");
        if (splitToken.length > 2) {
            return token;
        } else {
            StringBuilder sb = new StringBuilder();
            Matcher m = EngPatternType.P_TIME.matcher(token);
            if (m.find()) {
                printLog("convertTime:" + token);
                String hour = m.group(1);
                int hourValue = Integer.parseInt(hour);
                boolean pastNoon = hourValue >= 12;
                if (hourValue > 12) {
                    hourValue -= 12;
                } else if (hourValue == 0) {
                    hourValue = 12;
                    pastNoon = true;
                }

                sb.append(convertNumber((double)hourValue));
                if (!m.group(6).equals("00")) {
                    if (m.group(6).matches("0\\d")) {
                        sb.append(" ").append("oh").append(" ").append(convertNumber(Double.parseDouble(m.group(6))));
                    } else {
                        sb.append(" ").append(convertNumber(Double.parseDouble(m.group(6))));
                    }
                }

                if (m.group(7) != null) {
                    char[] var10;
                    int var9 = (var10 = m.group(7).replaceAll("\\.", "").toCharArray()).length;

                    for(int var8 = 0; var8 < var9; ++var8) {
                        char c = var10[var8];
                        sb.append(" ").append(c);
                    }
                } else {
                    sb.append(!pastNoon ? " a m" : " p m");
                }

                return sb.toString().replace("-", " ");
            } else {
                return token;
            }
        }
    }

    public static String convertRange(String token) {
        String tmpToken = token.replace("–", "-");
        Matcher m = EngPatternType.P_RANGE.matcher(tmpToken);
        if (m.find()) {
            printLog("convertRange:" + tmpToken);
            return m.group(1).length() == 4 && m.group(2).length() == 4 ? convertYear(m.group(1)) + " to " + convertYear(m.group(2)) : convertNumber(m.group(1)) + " to " + convertNumber(m.group(2));
        } else {
            return token;
        }
    }

    public static String convertYearBCAD(String token) {
        Matcher yearMatcher = EngPatternType.P_YEAR.matcher(token);
        if (yearMatcher.find()) {
            printLog("convertYearBCAD:" + token);
            String abbrev;
            if (yearMatcher.group(2).contains(".")) {
                String[] abbrevAr = yearMatcher.group(2).split("\\.");
                abbrev = Arrays.toString(abbrevAr).replaceAll("[,\\]\\[]", "");
            } else {
                abbrev = convertConsonants(yearMatcher.group(2));
            }

            return convertYear(Double.parseDouble(yearMatcher.group(1))) + " " + abbrev;
        } else {
            return token;
        }
    }

    public static String convertYearBCADWithSpace(String token) {
        String[] tmpToken = token.split(" ");
        if (tmpToken.length == 2) {
            printLog("convertYearBCADWithSpace:" + token);
            new String();
            String abbrev;
            if (tmpToken[1].contains(".")) {
                String[] abbrevAr = tmpToken[1].split("\\.");
                abbrev = Arrays.toString(abbrevAr).replaceAll("[,\\]\\[]", "");
            } else {
                abbrev = convertConsonants(tmpToken[1]);
            }

            return convertYear(tmpToken[0]) + " " + abbrev;
        } else {
            return token;
        }
    }

    public static String convertShortYearBCADWithSpace(String token) {
        String[] tmpToken = token.split(" ");
        if (tmpToken.length == 2) {
            printLog("convertShortYearBCADWithSpace:" + token);
            new String();
            String abbrev;
            if (tmpToken[0].contains(".")) {
                abbrev = convertConsonants(tmpToken[0].replaceAll("[.]", " "));
            } else {
                abbrev = convertConsonants(tmpToken[0]);
            }

            return abbrev + " " + convertNumberS(tmpToken[1]) + " ";
        } else {
            return token;
        }
    }

    public static String convertOrdinalRoman(String token) {
        if (EngPatternType.P_ROMAN.matcher(token).matches()) {
            printLog("convertOrdinalRoman:" + token);
            int num = 0;
            String regex = null;
            ArrayList<Roman2Arab> list = RomanDictionary.romaDictionary;
            Roman2Arab r2a = null;
//            int _alab = false;
            new String();
            Matcher m = null;

            for(int i = 0; i < list.size(); ++i) {
                r2a = (Roman2Arab)list.get(i);
                int _alab = r2a.getAlab();
                String _roma = r2a.getRoma();
                regex = "^(" + _roma + ")";
                Pattern p = Pattern.compile(regex, 2);
                m = p.matcher(token);
                if (m.find()) {
                    num += _alab;
                    token = token.substring(_roma.length());
                }
            }

            return "the " + convertOrdinal(String.valueOf(num)).replaceAll("-", " ");
        } else {
            return token;
        }
    }

    public static String convertRoman(String token) {
        if (token.length() < 2) {
            return token;
        } else if (EngPatternType.P_ROMAN.matcher(token).matches()) {
            printLog("convertRoman:" + token);
            int num = 0;
            String regex = null;
            ArrayList<Roman2Arab> list = RomanDictionary.romaDictionary;
            Roman2Arab r2a = null;
//            int _alab = false;
            new String();
            Matcher m = null;

            for(int i = 0; i < list.size(); ++i) {
                r2a = (Roman2Arab)list.get(i);
                int _alab = r2a.getAlab();
                String _roma = r2a.getRoma();
                regex = "^(" + _roma + ")";
                Pattern p = Pattern.compile(regex, 2);
                m = p.matcher(token);
                if (m.find()) {
                    num += _alab;
                    token = token.substring(_roma.length());
                }
            }

            return convertNumber(String.valueOf(num)).replaceAll("-", " ");
        } else {
            return token;
        }
    }

    public static int romanToInt(String s) {
        boolean isIUsed = false;
        boolean isVUsed = false;
        boolean isXUsed = false;
        boolean isLUsed = false;
        boolean isDUsed = false;
        int result = 0;
        char previous = 'A';

        for(int i = 0; i < s.length(); ++i) {
            char current = s.charAt(i);
            switch(current) {
                case 'C':
                    if (isLUsed || isVUsed || isIUsed) {
                        throw new IllegalArgumentException("Invalid Roman Numeral Input");
                    }

                    if (previous == 'X') {
                        result -= 10;
                        result += 90;
                    } else {
                        result += 100;
                    }
                    break;
                case 'D':
                    if (isLUsed || isXUsed || isVUsed || isIUsed) {
                        throw new IllegalArgumentException("Invalid Roman Numeral Input");
                    }

                    if (previous == 'C') {
                        result -= 100;
                        result += 400;
                    } else {
                        result += 500;
                    }

                    isDUsed = true;
                    break;
                case 'I':
                    ++result;
                    isIUsed = true;
                    break;
                case 'L':
                    if (isVUsed || isIUsed) {
                        throw new IllegalArgumentException("Invalid Roman Numeral Input");
                    }

                    if (previous == 'X') {
                        result -= 10;
                        result += 40;
                    } else {
                        result += 50;
                    }

                    isLUsed = true;
                    break;
                case 'M':
                    if (isDUsed || isLUsed || isXUsed || isVUsed || isIUsed) {
                        throw new IllegalArgumentException("Invalid Roman Numeral Input");
                    }

                    if (previous == 'C') {
                        result -= 100;
                        result += 900;
                    } else {
                        result += 1000;
                    }
                    break;
                case 'V':
                    if (previous == 'I') {
                        --result;
                        result += 4;
                    } else {
                        result += 5;
                    }

                    isVUsed = true;
                    break;
                case 'X':
                    if (previous == 'I') {
                        --result;
                        result += 9;
                    } else {
                        result += 10;
                    }

                    isXUsed = true;
            }

            previous = current;
        }

        return result;
    }

    public static String convertRoman(String token, boolean bNumber) {
        if (!bNumber) {
            return convertRoman(token);
        } else if (!EngPatternType.P_ROMAN.matcher(token).matches()) {
            return token;
        } else {
            int num = 0;
            String regex = null;
            ArrayList<Roman2Arab> list = RomanDictionary.romaDictionary;
            Roman2Arab r2a = null;
//            int _alab = false;
            new String();
            Matcher m = null;
            Boolean bNotRoman = false;

            for(int i = 0; i < list.size(); ++i) {
                r2a = (Roman2Arab)list.get(i);
                int _alab = r2a.getAlab();
                String _roma = r2a.getRoma();
                regex = "^(" + _roma + ")";
                Pattern p = Pattern.compile(regex, 2);
                m = p.matcher(token);
                if (!m.find()) {
                    bNotRoman = true;
                    break;
                }

                num += _alab;
                token = token.substring(_roma.length());
            }

            if (bNotRoman) {
                return token;
            } else {
                printLog("convertRoman(true):" + token + "->" + num);
                return String.valueOf(num);
            }
        }
    }

    private static String convertConsonants(String token) {
        printLog("convertConsonants:" + token);
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < token.length(); ++i) {
            if (i > 0) {
                sb.append(' ');
            }

            sb.append(token.charAt(i));
        }

        return sb.toString();
    }

    public static String convertYear(String token) {
        try {
            if (token.length() == 4 && PPUtils.isDigit(token)) {
                printLog("convertYear:" + token);
                return convertYear(Double.parseDouble(token));
            } else {
                return token;
            }
        } catch (Exception var2) {
            printLog(var2.getMessage());
            return token;
        }
    }

    private static String convertYear(double token) {
        rbnf.setDefaultRuleSet(yearRule);
        String result = rbnf.format(token);
        result = result.replaceAll("-", " ");
        return result;
    }

    private static String getYearRuleName(RuleBasedNumberFormat rbnf) {
        List<String> l = Arrays.asList(rbnf.getRuleSetNames());
        if (l.contains("%spellout-numbering-year")) {
            return "%spellout-numbering-year";
        } else {
            Iterator var3 = l.iterator();

            while(var3.hasNext()) {
                String string = (String)var3.next();
                if (string.startsWith("%spellout-numbering-year")) {
                    return string;
                }
            }

            throw new UnsupportedOperationException("The locale " + rbnf.getLocale(ULocale.ACTUAL_LOCALE) + " doesn't support year spelling.");
        }
    }

    private static String getOrdinalRuleName(RuleBasedNumberFormat rbnf) {
        List<String> l = Arrays.asList(rbnf.getRuleSetNames());
        if (l.contains("%spellout-ordinal")) {
            return "%spellout-ordinal";
        } else if (l.contains("%spellout-ordinal-masculine")) {
            return "%spellout-ordinal-masculine";
        } else {
            Iterator var3 = l.iterator();

            while(var3.hasNext()) {
                String string = (String)var3.next();
                if (string.startsWith("%spellout-ordinal")) {
                    return string;
                }
            }

            throw new UnsupportedOperationException("The locale " + rbnf.getLocale(ULocale.ACTUAL_LOCALE) + " doesn't support ordinal spelling.");
        }
    }

    public static String convertOrdinal(String token) {
        printLog("convertOrdinal:" + token);

        try {
            token = token.replaceAll(",", "");
            return convertOrdinal(Double.parseDouble(token));
        } catch (Exception var2) {
            printLog(var2.getMessage());
            return token;
        }
    }

    private static String convertOrdinal(double token) {
        rbnf.setDefaultRuleSet(ordinalRule);
        return rbnf.format(token).replaceAll("-", " ");
    }

    public static String convertMoney(String token) {
        if (token.contains(" ")) {
            token = token.replaceAll(" ", "");
        }

        if (token.toLowerCase().startsWith("euro")) {
            token = token.toLowerCase();
            token = token.replace("euro", "€");
        }

        int commaIdx = token.indexOf(",");
        int dotIdx = token.indexOf(".");
        if (commaIdx > dotIdx) {
            token = token.replaceAll("[.]", "");
            token = token.replace(",", ".");
        }

        if (!token.contains(",")) {
            String[] tokens = token.split("[.]");
            if (tokens.length > 1) {
                token = token.replaceAll("[.]", ",");
            }
        }

        String tmpToken = token.replaceAll(",", "");
        Matcher m = EngPatternType.P_MONEY.matcher(tmpToken);
        if (m.find()) {
            printLog("convertMoney:" + tmpToken);
            new String();
            String currency;
            if (tmpToken.toUpperCase().startsWith("USD")) {
                currency = "USD";
            } else if (tmpToken.toUpperCase().startsWith("GBP")) {
                currency = "GBP";
            } else if (tmpToken.toUpperCase().startsWith("EUR")) {
                currency = "EUR";
            } else {
                currency = m.group(1);
            }

            String[] parts = m.group(2).split("\\.");
            StringBuilder sb = new StringBuilder();
            double whole = Double.parseDouble(parts[0]);
            boolean hasFraction = false;
            int fracValue = 0;
            if (parts.length > 1) {
                fracValue = Integer.valueOf(parts[1]);
                hasFraction = true;
            }

            switch(currency.hashCode()) {
                case 36:
                    if (currency.equals("$")) {
                        sb.append(convertNumber(whole)).append(whole > 1.0D ? " dollars" : " dollar");
                        if (hasFraction) {
                            sb.append(" ").append("and").append(" ").append(convertNumber((double)fracValue)).append(" cents");
                        }
                    }
                    break;
                case 163:
                    if (currency.equals("£")) {
                        sb.append(convertNumber(whole)).append(whole > 1.0D ? " pounds" : " pound");
                        if (hasFraction) {
                            sb.append(" ").append("and").append(" ").append(convertNumber((double)fracValue)).append(" pence");
                        }
                    }
                    break;
                case 8364:
                    if (currency.equals("€")) {
                        sb.append(convertNumber(whole)).append(whole > 1.0D ? " euros" : " euro");
                        if (hasFraction) {
                            sb.append(" ").append("and").append(" ").append(convertNumber((double)fracValue)).append(" cents");
                        }
                    }
                    break;
                case 69026:
                    if (currency.equals("EUR")) {
                        sb.append(convertNumber(whole)).append(whole > 1.0D ? " euros" : " euro");
                        if (hasFraction) {
                            sb.append(" ").append("and").append(" ").append(convertNumber((double)fracValue)).append(" cents");
                        }
                    }
                    break;
                case 70357:
                    if (currency.equals("GBP")) {
                        sb.append(convertNumber(whole)).append(whole > 1.0D ? " pounds" : " pound");
                        if (hasFraction) {
                            sb.append(" ").append("and").append(" ").append(convertNumber((double)fracValue)).append(" pence");
                        }
                    }
                    break;
                case 84326:
                    if (currency.equals("USD")) {
                        sb.append(convertNumber(whole)).append(whole > 1.0D ? " u s dollars" : " u s dollar");
                        if (hasFraction) {
                            sb.append(" ").append("and").append(" ").append(convertNumber((double)fracValue)).append(" cents");
                        }
                    }
            }

            return sb.toString().replaceAll("-", " ");
        } else {
            return token;
        }
    }

    public static String convertRealNumber(String token) {
        if (!PPUtils.hasDigit(token)) {
            return token;
        } else {
            String unit = new String();
            if (token.endsWith("Hz".toLowerCase())) {
                unit = "hertz";
            }

            if (token.endsWith("KHz".toLowerCase())) {
                unit = "kilohertz";
            }

            if (token.endsWith("MHz".toLowerCase())) {
                unit = "megahertz";
            }

            if (token.endsWith("GHz".toLowerCase())) {
                unit = "gigahertz";
            }

            String tmpToken = token.replace("−", "-").replaceAll(",", "");
            Matcher m = EngPatternType.P_REALNUM.matcher(tmpToken);
            if (!m.find()) {
                return token;
            } else {
                StringBuilder newTok = new StringBuilder();
                if (m.group(1) != null) {
                    if ("-".equals(m.group(1))) {
                        newTok.append("minus").append(" ");
                    } else if ("+".equals(m.group(1))) {
                        newTok.append("plus").append(" ");
                    }
                }

                if (m.group(2) != null) {
                    newTok.append(convertNumber(Double.parseDouble(m.group(2)))).append(" ");
                }

                if (m.group(3) != null) {
                    newTok.append("point").append(" ");
                    char[] var8;
                    int var7 = (var8 = m.group(4).toCharArray()).length;

                    for(int var6 = 0; var6 < var7; ++var6) {
                        char c = var8[var6];
                        newTok.append(convertNumber(Double.parseDouble(String.valueOf(c)))).append(" ");
                    }

                    if (m.group(5) != null) {
                        if ("%".equals(m.group(5))) {
                            newTok.append("percent");
                        }

                        if ("P".toLowerCase().equals(m.group(5).toLowerCase())) {
                            newTok.append("point");
                        }
                    }
                }

                String result;
                if (unit.isEmpty()) {
                    result = newTok.toString().trim().replaceAll("-", " ");
                    printLog("convertRealNumber:" + token + "->" + result);
                    return newTok.toString().trim().replaceAll("-", " ");
                } else {
                    result = newTok.toString().trim().replaceAll("-", " ") + " " + unit;
                    printLog("convertRealNumber:" + token + "->" + result);
                    return result;
                }
            }
        }
    }

    public static String convertNumberS(String token) {
        String tmpToken = token.replaceAll(",", "");
        Matcher m = EngPatternType.P_NUMBERS.matcher(tmpToken);
        if (m.find()) {
            printLog("convertNumberS:" + tmpToken);
            String number = convertNumber(m.group(1));
            if (number.endsWith("x")) {
                number = number + "es";
            } else if (number.endsWith("y")) {
                number = number.replace("y", "ies");
            } else {
                number = number + "s";
            }

            return number.replaceAll("-", " ");
        } else {
            return token;
        }
    }

    public static String convertNumber(String token) {
        token = token.replaceAll(",", "");
        return convertNumber(Double.parseDouble(token));
    }

    private static String convertNumber(double token) {
        rbnf.setDefaultRuleSet("%spellout-numbering");
        return rbnf.format(token);
    }

    public static String convertUSDate(String date) {
        String[] dateParts;
        label64: {
            printLog("convertUSDate:" + date);
            date = date.replaceAll(" ", "/");
            dateParts = date.split("/");
            String var3;
            switch((var3 = dateParts[0]).hashCode()) {
                case -2029849391:
                    if (var3.equals("september")) {
                        date = date.replace("september", "09");
                        break label64;
                    }
                    break;
                case -1826660246:
                    if (var3.equals("january")) {
                        date = date.replace("january", "01");
                        break label64;
                    }
                    break;
                case -1621487904:
                    if (var3.equals("october")) {
                        date = date.replace("october", "10");
                        break label64;
                    }
                    break;
                case -1406703101:
                    if (var3.equals("august")) {
                        date = date.replace("august", "08");
                        break label64;
                    }
                    break;
                case -263893086:
                    if (var3.equals("february")) {
                        date = date.replace("february", "02");
                        break label64;
                    }
                    break;
                case 107877:
                    if (var3.equals("may")) {
                        date = date.replace("may", "05");
                        break label64;
                    }
                    break;
                case 3273752:
                    if (var3.equals("july")) {
                        date = date.replace("july", "07");
                        break label64;
                    }
                    break;
                case 3273794:
                    if (var3.equals("june")) {
                        date = date.replace("june", "06");
                        break label64;
                    }
                    break;
                case 93031046:
                    if (var3.equals("april")) {
                        date = date.replace("april", "04");
                        break label64;
                    }
                    break;
                case 103666243:
                    if (var3.equals("march")) {
                        date = date.replace("march", "03");
                        break label64;
                    }
                    break;
                case 561839141:
                    if (var3.equals("december")) {
                        date = date.replace("december", "12");
                        break label64;
                    }
                    break;
                case 1639129394:
                    if (var3.equals("november")) {
                        date = date.replace("november", "11");
                        break label64;
                    }
            }

            date = date.replace("december", "12");
        }

        try {
            Date humanDate = DateFormat.getPatternInstance("dd.MM.yyyy", ULocale.US).parse(date.concat("1999"));
            dateParts = df.format(humanDate).replaceAll(",", "").split("\\s");
            dateParts[1] = convertOrdinal(dateParts[1]);
            String newToken = dateParts[0] + " " + dateParts[1] + " ";
            newToken = newToken.replaceAll("-", " ").toLowerCase();
            return newToken;
        } catch (ParseException var5) {
            printLog(var5.getMessage());
            return date;
        }
    }

    public static String convertUSDateEx(String date) {
        String[] dateParts;
        label64: {
            printLog("convertUSDateEx:" + date);
            date = date.replaceAll("[.]", "");
            date = date.replaceAll(",", "");
            date = date.replaceAll(" ", "/");
            dateParts = date.split("/");
            String var3;
            switch((var3 = dateParts[0]).hashCode()) {
                case 96803:
                    if (var3.equals("apr")) {
                        date = date.replace("apr", "04");
                        break label64;
                    }
                    break;
                case 96947:
                    if (var3.equals("aug")) {
                        date = date.replace("aug", "08");
                        break label64;
                    }
                    break;
                case 99330:
                    if (var3.equals("dec")) {
                        date = date.replace("dec", "12");
                        break label64;
                    }
                    break;
                case 101251:
                    if (var3.equals("feb")) {
                        date = date.replace("feb", "02");
                        break label64;
                    }
                    break;
                case 104983:
                    if (var3.equals("jan")) {
                        date = date.replace("jan", "01");
                        break label64;
                    }
                    break;
                case 105601:
                    if (var3.equals("jul")) {
                        date = date.replace("jul", "07");
                        break label64;
                    }
                    break;
                case 105603:
                    if (var3.equals("jun")) {
                        date = date.replace("jun", "06");
                        break label64;
                    }
                    break;
                case 107870:
                    if (var3.equals("mar")) {
                        date = date.replace("mar", "03");
                        break label64;
                    }
                    break;
                case 107877:
                    if (var3.equals("may")) {
                        date = date.replace("may", "05");
                        break label64;
                    }
                    break;
                case 109269:
                    if (var3.equals("nov")) {
                        date = date.replace("nov", "11");
                        break label64;
                    }
                    break;
                case 109856:
                    if (var3.equals("oct")) {
                        date = date.replace("oct", "10");
                        break label64;
                    }
                    break;
                case 113758:
                    if (var3.equals("sep")) {
                        date = date.replace("sep", "09");
                        break label64;
                    }
            }

            date = date.replace("dec", "12");
        }

        try {
            Date humanDate = DateFormat.getPatternInstance("dd.MM.yyyy", ULocale.US).parse(date);
            dateParts = df.format(humanDate).replaceAll(",", "").split("\\s");
            dateParts[1] = convertOrdinal(dateParts[1]);
            dateParts[2] = convertYear(Double.parseDouble(dateParts[2]));
            String newToken = dateParts[0] + " " + dateParts[1] + " " + dateParts[2] + " ";
            newToken = newToken.replaceAll("-", " ").toLowerCase();
            return newToken;
        } catch (ParseException var5) {
            printLog(var5.getMessage());
            return date;
        }
    }

    private static void printLog(String text) {
        if (bLog) {
            System.out.println(text);
        }

    }
}
