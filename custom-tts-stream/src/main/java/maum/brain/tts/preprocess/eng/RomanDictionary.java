package maum.brain.tts.preprocess.eng;

import java.util.ArrayList;

public class RomanDictionary {
    private static final String romans = "'',0,I,1,II,2,III,3,IV,4,V,5,VI,6,VII,7,VIII,8,IX,9,X,10,XX,20,XXX,30,XL,40,L,50,LX,60,LXX,70,LXXX,80,XC,90,C,100,CC,200,CCC,300,CD,400,D,500,DC,600,DCC,700,DCCC,800,CM,900,M,1000,MM,2000";
    public static final ArrayList<Roman2Arab> romaDictionary = new ArrayList();

    public RomanDictionary() {
    }

    static {
        if (romaDictionary == null || romaDictionary.isEmpty()) {
            String[] c = "'',0,I,1,II,2,III,3,IV,4,V,5,VI,6,VII,7,VIII,8,IX,9,X,10,XX,20,XXX,30,XL,40,L,50,LX,60,LXX,70,LXXX,80,XC,90,C,100,CC,200,CCC,300,CD,400,D,500,DC,600,DCC,700,DCCC,800,CM,900,M,1000,MM,2000".split(",");

            for(int t = c.length - 2; t > 0; t -= 2) {
                romaDictionary.add(new Roman2Arab(c[t], Integer.parseInt(c[t + 1])));
            }
        }

    }
}
