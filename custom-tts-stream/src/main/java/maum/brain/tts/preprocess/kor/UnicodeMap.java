package maum.brain.tts.preprocess.kor;

import javax.xml.bind.annotation.XmlElement;

class UnicodeMap {
    @XmlElement
    String code;

    UnicodeMap() {
    }
}
