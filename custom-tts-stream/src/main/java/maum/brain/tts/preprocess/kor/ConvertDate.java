package maum.brain.tts.preprocess.kor;

import java.util.List;
import java.util.StringTokenizer;
import maum.brain.tts.preprocess.type.KorPatternType;
import maum.brain.tts.utils.NumberUtils;
import maum.brain.tts.utils.PPUtils;

public class ConvertDate {
    public ConvertDate() {
    }

    public static String convertDateFormat(String token) {
        return convertDateFormat(token, "");
    }

    public static String convertDateFormat(String token, String notKorToken) {
        if (token.equals(notKorToken)) {
            if (token.contains("-")) {
                token = changeDateFormat(token, "-");
            }

            if (token.contains("/")) {
                token = changeDateFormat(token, "/");
            }

            if (token.contains(".")) {
                token = changeDateFormat(token, ".");
            }
        }

        if (token != null && !token.isEmpty() && PPUtils.hasDigit(token)) {
            boolean bHandle = Boolean.FALSE;
            String curNum;
            String remainedWord;
            String onlyNum;
            String dd;
            String HH;
//            String HH;
            if (token.length() == 16 && (token.endsWith("부터") || token.endsWith("까지"))) {
//                new String();
//                new String();
//                new String();
//                new String();
//                new String();
//                new String();
                curNum = token.substring(0, 4);
                remainedWord = token.substring(4, 6);
                onlyNum = token.substring(6, 8);
                dd = token.substring(8, 10);
                HH = token.substring(10, 12);
                HH = token.substring(12, 14);
                token = NumberUtils.convertDecimal2Hangle(curNum, false).concat("년").concat(" ").concat(NumberUtils.convertMonth2Hangle(remainedWord)).concat("월").concat(" ").concat(NumberUtils.convertDecimal2Hangle(onlyNum, false)).concat("일").concat(" ").concat(NumberUtils.convertHour2Hangle(dd)).concat("시").concat(" ").concat(NumberUtils.convertDecimal2Hangle(HH, false)).concat("분").concat(" ").concat(NumberUtils.convertDecimal2Hangle(HH, false)).concat("초").concat(token.substring(14, 16));
                return token;
            } else {
                String SS;
                if (token.length() == 14) {
                    if (PPUtils.isDigitEx(token)) {
//                        new String();
//                        new String();
//                        new String();
//                        new String();
//                        new String();
//                        new String();
//                        new String();
                        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_YYYYMMDDHHMISS, token);
                        if (!aList.isEmpty()) {
                            curNum = (String)aList.get(0);
                            remainedWord = curNum.substring(0, 4);
                            onlyNum = curNum.substring(4, 6);
                            dd = curNum.substring(6, 8);
                            HH = curNum.substring(8, 10);
                            HH = curNum.substring(10, 12);
                            SS = curNum.substring(12, 14);
                            token = NumberUtils.convertDecimal2Hangle(remainedWord, false).concat("년").concat(" ").concat(NumberUtils.convertMonth2Hangle(onlyNum)).concat("월").concat(" ").concat(NumberUtils.convertDecimal2Hangle(dd, false)).concat("일").concat(" ").concat(NumberUtils.convertHour2Hangle(HH)).concat("시").concat(" ").concat(NumberUtils.convertDecimal2Hangle(HH, false)).concat("분").concat(" ").concat(NumberUtils.convertDecimal2Hangle(SS, false)).concat("초").concat(" ");
                            return token;
                        }

                        bHandle = Boolean.FALSE;
                    } else {
//                        new String();
//                        new String();
//                        new String();
//                        new String();
//                        new String();
                        if (token.endsWith("부터") || token.endsWith("까지")) {
                            curNum = token.substring(0, 4);
                            remainedWord = token.substring(4, 6);
                            onlyNum = token.substring(6, 8);
                            dd = token.substring(8, 10);
                            HH = token.substring(10, 12);
                            token = NumberUtils.convertDecimal2Hangle(curNum, false).concat("년").concat(" ").concat(NumberUtils.convertMonth2Hangle(remainedWord)).concat("월").concat(" ").concat(NumberUtils.convertDecimal2Hangle(onlyNum, false)).concat("일").concat(" ").concat(NumberUtils.convertHour2Hangle(dd)).concat("시").concat(" ").concat(NumberUtils.convertDecimal2Hangle(HH, false)).concat("분").concat(token.substring(12, 14));
                            return token;
                        }
                    }
                }

                if (!bHandle && token.length() == 12) {
                    if (PPUtils.isDigitEx(token)) {
//                        new String();
//                        new String();
//                        new String();
//                        new String();
//                        new String();
//                        new String();
                        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_YYYYMMDDHHMI, token);
                        if (!aList.isEmpty()) {
                            curNum = (String)aList.get(0);
                            remainedWord = curNum.substring(0, 4);
                            onlyNum = curNum.substring(4, 6);
                            dd = curNum.substring(6, 8);
                            HH = curNum.substring(8, 10);
                            HH = curNum.substring(10, 12);
                            token = NumberUtils.convertDecimal2Hangle(remainedWord, false).concat("년").concat(" ").concat(NumberUtils.convertMonth2Hangle(onlyNum)).concat("월").concat(" ").concat(NumberUtils.convertDecimal2Hangle(dd, false)).concat("일").concat(" ").concat(NumberUtils.convertHour2Hangle(HH)).concat("시").concat(" ").concat(NumberUtils.convertDecimal2Hangle(HH, false)).concat("분").concat(" ");
                            return token;
                        }

                        bHandle = Boolean.FALSE;
                    } else {
//                        new String();
//                        new String();
//                        new String();
//                        new String();
                        if (token.endsWith("부터") || token.endsWith("까지")) {
                            curNum = token.substring(0, 4);
                            remainedWord = token.substring(4, 6);
                            onlyNum = token.substring(6, 8);
                            dd = token.substring(8, 10);
                            token = NumberUtils.convertDecimal2Hangle(curNum, false).concat("년").concat(" ").concat(NumberUtils.convertMonth2Hangle(remainedWord)).concat("월").concat(" ").concat(NumberUtils.convertDecimal2Hangle(onlyNum, false)).concat("일").concat(" ").concat(NumberUtils.convertHour2Hangle(dd)).concat("시").concat(token.substring(10, 12));
                            return token;
                        }
                    }
                }

                List aList;
                if (!bHandle && token.length() == 10) {
                    if (PPUtils.isDigitEx(token)) {
//                        new String();
//                        new String();
//                        new String();
//                        new String();
//                        new String();
//                        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_YYYYMMDDHH, token);
                        aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_YYYYMMDDHH, token);
                        if (!aList.isEmpty()) {
                            curNum = (String)aList.get(0);
                            remainedWord = curNum.substring(0, 4);
                            onlyNum = curNum.substring(4, 6);
                            dd = curNum.substring(6, 8);
                            HH = curNum.substring(8, 10);
                            token = NumberUtils.convertDecimal2Hangle(remainedWord, false).concat("년").concat(" ").concat(NumberUtils.convertMonth2Hangle(onlyNum)).concat("월").concat(" ").concat(NumberUtils.convertDecimal2Hangle(dd, false)).concat("일").concat(" ").concat(NumberUtils.convertHour2Hangle(HH)).concat("시").concat(" ");
                            return token;
                        }

                        bHandle = Boolean.FALSE;
                    } else {
//                        new String();
//                        new String();
//                        new String();
//                        new String();
                        if (token.endsWith("부터") || token.endsWith("까지")) {
                            remainedWord = token.substring(0, 4);
                            onlyNum = token.substring(4, 6);
                            dd = token.substring(6, 8);
                            token = NumberUtils.convertDecimal2Hangle(remainedWord, false).concat("년").concat(" ").concat(NumberUtils.convertMonth2Hangle(onlyNum)).concat("월").concat(" ").concat(NumberUtils.convertDecimal2Hangle(dd, false)).concat("일").concat(token.substring(8, 10));
                            return token;
                        }

                        aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_YYYYMMDD_EX, token);
                        if (!aList.isEmpty()) {
                            curNum = (String)aList.get(0);
                            if (curNum.length() > 0) {
                                if (curNum.contains("-")) {
                                    curNum = curNum.replaceAll("-", "");
                                }

                                if (curNum.contains("/")) {
                                    curNum = curNum.replaceAll("/", "");
                                }

                                if (curNum.contains(".")) {
                                    curNum = curNum.replaceAll(".", "");
                                }

                                remainedWord = curNum.substring(0, 4);
                                onlyNum = curNum.substring(4, 6);
                                dd = curNum.substring(6, 8);
                                token = NumberUtils.convertDecimal2Hangle(remainedWord, false).concat("년").concat(" ").concat(NumberUtils.convertMonth2Hangle(onlyNum)).concat("월").concat(" ").concat(NumberUtils.convertDecimal2Hangle(dd, false)).concat("일").concat(" ");
                            }

                            return token;
                        }

                        bHandle = Boolean.FALSE;
                    }
                }

                if (!bHandle && token.length() == 8) {
//                    new String();
//                    new String();
//                    new String();
//                    new String();
                    aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_YYYYMMDD, token);
                    if (!aList.isEmpty()) {
                        curNum = (String)aList.get(0);
                        remainedWord = curNum.substring(0, 4);
                        onlyNum = curNum.substring(4, 6);
                        dd = curNum.substring(6, 8);
                        token = NumberUtils.convertDecimal2Hangle(remainedWord, false).concat("년").concat(" ").concat(NumberUtils.convertMonth2Hangle(onlyNum)).concat("월").concat(" ").concat(NumberUtils.convertDecimal2Hangle(dd, false)).concat("일").concat(" ");
                        return token;
                    }

                    bHandle = Boolean.FALSE;
                }

                if (!bHandle && token.length() == 5) {
//                    new String();
//                    new String();
//                    new String();
//                    List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_HHMI_SEP, token);
                    aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_HHMI_SEP, token);
                    if (!aList.isEmpty()) {
                        curNum = (String)aList.get(0);
                        remainedWord = curNum.substring(0, 2);
                        onlyNum = curNum.substring(3, 5);
                        if ("00".equals(onlyNum)) {
                            token = NumberUtils.convertHour2Hangle(remainedWord).concat("시").concat(" ");
                        } else {
                            token = NumberUtils.convertHour2Hangle(remainedWord).concat("시").concat(" ").concat(NumberUtils.convertDecimal2Hangle(onlyNum, false)).concat("분").concat(" ");
                        }

                        return token;
                    }

                    bHandle = Boolean.FALSE;
                }

//                List aList;
                int i;
                if (!bHandle && token.endsWith("년")) {
//                    new String();
                    aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_YEAR, token);
                    if (!aList.isEmpty()) {
                        for(i = 0; i < aList.size(); ++i) {
                            curNum = ((String)aList.get(i)).substring(0, ((String)aList.get(i)).length() - 1);
                            token = NumberUtils.convertDecimal2Hangle(curNum, false).concat("년");
                        }

                        return token;
                    }

                    bHandle = Boolean.FALSE;
                }

                if (!bHandle && token.endsWith("월")) {
//                    new String();
                    aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_MONTH, token);
                    if (!aList.isEmpty()) {
                        for(i = 0; i < aList.size(); ++i) {
                            curNum = ((String)aList.get(i)).substring(0, ((String)aList.get(i)).length() - 1);
                            token = NumberUtils.convertMonth2Hangle(curNum).concat("월");
                        }

                        return token;
                    }

                    bHandle = Boolean.FALSE;
                }

                if (!bHandle && token.endsWith("개월")) {
//                    new String();
                    aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_BETWEEN_MONTH, token);
                    if (!aList.isEmpty()) {
                        for(i = 0; i < aList.size(); ++i) {
                            curNum = ((String)aList.get(i)).substring(0, ((String)aList.get(i)).length() - 2);
                            token = NumberUtils.convertDecimal2Hangle(curNum, false).concat("개월");
                        }

                        return token;
                    }

                    bHandle = Boolean.FALSE;
                }

                if (!bHandle && token.endsWith("일")) {
//                    new String();
                    aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_DAY, token);
                    if (!aList.isEmpty()) {
                        for(i = 0; i < aList.size(); ++i) {
                            curNum = ((String)aList.get(i)).substring(0, ((String)aList.get(i)).length() - 1);
                            token = NumberUtils.convertDecimal2Hangle(curNum, false).concat("일");
                        }

                        return token;
                    }

                    bHandle = Boolean.FALSE;
                }

                if (!bHandle && token.endsWith("시")) {
//                    new String();
                    aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_HOUR, token);
                    if (!aList.isEmpty()) {
                        for(i = 0; i < aList.size(); ++i) {
                            curNum = ((String)aList.get(i)).substring(0, ((String)aList.get(i)).length() - 1);
                            token = NumberUtils.convertHour2Hangle(curNum).concat("시");
                        }

                        return token;
                    }

                    bHandle = Boolean.FALSE;
                }

                if (!bHandle && token.endsWith("시부터")) {
                    curNum = token.replace("시부터", "");
                    token = NumberUtils.convertHour2Hangle(curNum).concat("시부터");
                    return token;
                } else {
                    if (!bHandle && token.contains("시까지")) {
                        if (!token.startsWith("오전") && !token.startsWith("오후")) {
                            int idx = token.indexOf("시까지");
                            remainedWord = token.substring(0, idx);
                            onlyNum = token.substring(idx);
                            token = NumberUtils.convertHour2Hangle(remainedWord).concat(onlyNum);
                            return token;
                        }

                        curNum = PPUtils.getDigits(token);
                        token = token.replace(curNum, " " + NumberUtils.convertHour2Hangle(curNum));
                    }

                    if (!bHandle && token.endsWith("분")) {
//                        new String();
                        aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_MINUTE, token);
                        if (!aList.isEmpty()) {
                            for(i = 0; i < aList.size(); ++i) {
                                curNum = ((String)aList.get(i)).substring(0, ((String)aList.get(i)).length() - 1);
                                token = NumberUtils.convertDecimal2Hangle(curNum, false).concat("분");
                            }

                            return token;
                        }

                        bHandle = Boolean.FALSE;
                    }

                    if (!bHandle && token.endsWith("초")) {
//                        new String();
                        aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_SEC, token);
                        if (!aList.isEmpty()) {
                            for(i = 0; i < aList.size(); ++i) {
                                curNum = ((String)aList.get(i)).substring(0, ((String)aList.get(i)).length() - 1);
                                token = NumberUtils.convertDecimal2Hangle(curNum, false).concat("초");
                            }

                            return token;
                        }

                        bHandle = Boolean.FALSE;
                    }

//                    boolean idxSec;
//                    int i;
                    int idxSec;
                    if (!bHandle && token.contains("년")) {
//                        new String();
//                        new String();
//                        new String();
//                        idxSec = false;
                        aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_YEAR_EX, token);
                        if (!aList.isEmpty()) {
                            for(i = 0; i < aList.size(); ++i) {
                                idxSec = ((String)aList.get(i)).indexOf("년");
                                curNum = ((String)aList.get(i)).substring(0, idxSec);
                                onlyNum = curNum.replaceAll(",", "");
                                remainedWord = ((String)aList.get(i)).substring(idxSec);
                                token = NumberUtils.convertDecimal2Hangle(onlyNum, false).concat(remainedWord);
                            }

                            return token;
                        }

                        bHandle = Boolean.FALSE;
                    }

                    if (!bHandle && token.contains("월")) {
//                        new String();
//                        new String();
//                        new String();
//                        idxSec = false;
                        aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_MONTH_EX, token);
                        if (!aList.isEmpty()) {
                            for(i = 0; i < aList.size(); ++i) {
                                idxSec = ((String)aList.get(i)).indexOf("월");
                                curNum = ((String)aList.get(i)).substring(0, idxSec);
                                onlyNum = curNum.replaceAll(",", "");
                                remainedWord = ((String)aList.get(i)).substring(idxSec);
                                token = NumberUtils.convertDecimal2Hangle(onlyNum, false).trim().concat(remainedWord);
                            }

                            return token;
                        }

                        bHandle = Boolean.FALSE;
                    }

                    if (!bHandle && token.contains("일")) {
//                        new String();
//                        new String();
//                        new String();
//                        idxSec = false;
                        aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_DAY_EX, token);
                        if (!aList.isEmpty()) {
                            for(i = 0; i < aList.size(); ++i) {
                                idxSec = ((String)aList.get(i)).indexOf("일");
                                curNum = ((String)aList.get(i)).substring(0, idxSec);
                                onlyNum = curNum.replaceAll(",", "");
                                remainedWord = ((String)aList.get(i)).substring(idxSec);
                                token = NumberUtils.convertDecimal2Hangle(onlyNum, false).concat(remainedWord);
                            }

                            return token;
                        }

                        bHandle = Boolean.FALSE;
                    }

                    if (!bHandle && token.contains("시간")) {
//                        new String();
//                        new String();
//                        new String();
//                        idxSec = false;
                        aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_HOUR_EX, token);
                        if (!aList.isEmpty()) {
                            for(i = 0; i < aList.size(); ++i) {
                                idxSec = ((String)aList.get(i)).indexOf("시");
                                curNum = ((String)aList.get(i)).substring(0, idxSec);
                                onlyNum = curNum.replaceAll(",", "");
                                remainedWord = ((String)aList.get(i)).substring(idxSec);
                                token = NumberUtils.convertOrdinalNumber2Hangle(onlyNum).concat(remainedWord);
                            }

                            return token;
                        }

                        bHandle = Boolean.FALSE;
                    }

                    if (!bHandle && token.contains("분")) {
//                        new String();
//                        new String();
//                        new String();
//                        idxSec = false;
                        if (token.contains(":")) {
                            idxSec = token.indexOf("분");
                            curNum = token.substring(0, idxSec);
                            String[] HHSS = curNum.split(":");
                            HH = NumberUtils.convertHour2Hangle(HHSS[0]).concat("시").concat(" ");
                            SS = NumberUtils.convertDecimal2Hangle(HHSS[1], false);
                            token = HH.concat(SS).concat(token.substring(idxSec));
                            return token;
                        }

                        aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_MINUTE_EX, token);
                        if (!aList.isEmpty()) {
                            for(i = 0; i < aList.size(); ++i) {
                                idxSec = ((String)aList.get(i)).indexOf("분");
                                curNum = ((String)aList.get(i)).substring(0, idxSec);
                                onlyNum = curNum.replaceAll(",", "");
                                remainedWord = ((String)aList.get(i)).substring(idxSec);
                                token = NumberUtils.convertDecimal2Hangle(onlyNum, false).concat(remainedWord);
                            }

                            return token;
                        }

                        bHandle = Boolean.FALSE;
                    }

                    if (!bHandle && token.contains("초")) {
//                        new String();
//                        new String();
//                        new String();
//                        idxSec = false;
                        aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_SEC_EX, token);
                        if (!aList.isEmpty()) {
                            for(i = 0; i < aList.size(); ++i) {
                                idxSec = ((String)aList.get(i)).indexOf("초");
                                curNum = ((String)aList.get(i)).substring(0, idxSec);
                                onlyNum = curNum.replaceAll(",", "");
                                remainedWord = ((String)aList.get(i)).substring(idxSec);
                                token = NumberUtils.convertDecimal2Hangle(onlyNum, false).concat(remainedWord);
                            }

                            return token;
                        }

                        bHandle = Boolean.FALSE;
                    }

                    return token;
                }
            }
        } else {
            return token;
        }
    }

    public static String convertAbnormalDateFormat(String token) {
        if (token.contains(".")) {
            try {
                StringTokenizer st = new StringTokenizer(token, ".");
                int cnt = st.countTokens();
                if (cnt == 3) {
                    StringBuilder sb = new StringBuilder();

                    for(int i = 0; i < cnt; ++i) {
                        String data = st.nextToken();
                        if (i == 0) {
                            sb.append(NumberUtils.convertDecimal2Hangle(data, false).concat("년")).append(" ");
                        }

                        if (i == 1) {
                            sb.append(NumberUtils.convertMonth2Hangle(data).concat("월")).append(" ");
                        }

                        if (i == 2) {
                            sb.append(NumberUtils.convertDay2Hangle(data).concat("일"));
                        }
                    }

                    return sb.toString();
                }
            } catch (Exception var6) {
                return token;
            }
        }

        return token;
    }

    public static String changeDateFormat(String token, String separator) {
        StringTokenizer st = new StringTokenizer(token, separator);
        StringBuilder sb = new StringBuilder();
        int cnt = st.countTokens();
        if (cnt == 3) {
            for(int i = 0; i < cnt; ++i) {
                String d = st.nextToken();
                if (i == 0) {
                    sb.append(d).append(separator);
                } else if (i == 1) {
                    if (d.length() == 1) {
                        d = "0".concat(d);
                    }

                    sb.append(d).append(separator);
                } else if (i == 2) {
                    if (d.length() == 1) {
                        d = "0".concat(d);
                    }

                    sb.append(d);
                }
            }

            token = sb.toString();
        }

        return token;
    }
}
