package maum.brain.tts.preprocess.kor;

import java.util.List;
import java.util.StringTokenizer;
import maum.brain.tts.preprocess.type.KorPatternType;
import maum.brain.tts.utils.PPUtils;

public class ConvertEnglishDigit {
    public ConvertEnglishDigit() {
    }

    public static String convertEnglishDigitFormat(String token) {
        int len = token.length();
        if (len != 6 && len != 8 && len != 10 && len != 12) {
            if (token.endsWith("mm")) {
                token = token.replace("mm", "");
                return convertDigit2Hangle(token, false).concat("밀리");
            } else if (!token.contains("/")) {
                return token;
            } else {
                StringTokenizer st = new StringTokenizer(token, "/");
                StringBuilder sb = new StringBuilder();

                while(st.hasMoreTokens()) {
                    String splitToken = st.nextToken();
                    sb.append(ConvertEnglish.convertEnglish(splitToken));
                }

                return sb.toString();
            }
        } else {
            List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_ENGLISHDIGIT, token);

            for(int i = 0; i < aList.size(); ++i) {
                token = converGISNumber2Hangle((String)aList.get(i));
            }

            return token;
        }
    }

    private static String convertDigit2Hangle(String token, boolean official) {
        int number = Integer.parseUnsignedInt(token);
        if (number < 0) {
            return "마이너스 " + convertDigit2Hangle(String.valueOf(number), official);
        } else if (number == 0) {
            return "영";
        } else {
            int len = token.length();
            StringBuilder sb = new StringBuilder();
            boolean subcount = false;

            for(int i = 0; i < len; ++i) {
                int base = len - i - 1;
                int baseMod4 = base % 4;
                int digit = token.charAt(i) - 48;
                if (digit != 0) {
                    subcount = true;
                    if (digit == 1) {
                        if (official || baseMod4 == 0) {
                            sb.append('일');
                        }
                    } else {
                        sb.append("이삼사오육칠팔구".charAt(digit - 2));
                    }

                    if (baseMod4 > 0) {
                        sb.append("십백천".charAt(baseMod4 - 1));
                    }
                }

                if (baseMod4 == 0) {
                    if (subcount && base > 0) {
                        sb.append("만억조경해".charAt(base / 4 - 1)).append(' ');
                    }

                    subcount = false;
                }
            }

            String tmp = sb.toString();
            if (tmp.startsWith("일만")) {
                tmp = tmp.substring(1);
            }

            return tmp;
        }
    }

    public static String convertNumber2Hangle(String text) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < text.length(); ++i) {
            if ("0".equals(String.valueOf(text.charAt(i)))) {
                sb.append("영");
            } else if ("1".equals(String.valueOf(text.charAt(i)))) {
                sb.append("일");
            } else if ("2".equals(String.valueOf(text.charAt(i)))) {
                sb.append("이");
            } else if ("3".equals(String.valueOf(text.charAt(i)))) {
                sb.append("삼");
            } else if ("4".equals(String.valueOf(text.charAt(i)))) {
                sb.append("사");
            } else if ("5".equals(String.valueOf(text.charAt(i)))) {
                sb.append("오");
            } else if ("6".equals(String.valueOf(text.charAt(i)))) {
                sb.append("육");
            } else if ("7".equals(String.valueOf(text.charAt(i)))) {
                sb.append("칠");
            } else if ("8".equals(String.valueOf(text.charAt(i)))) {
                sb.append("팔");
            } else if ("9".equals(String.valueOf(text.charAt(i)))) {
                sb.append("구");
            } else {
                sb.append(String.valueOf(text.charAt(i)));
            }
        }

        return sb.toString().trim();
    }

    private static String converGISNumber2Hangle(String token) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < token.length(); ++i) {
            if (i == 6) {
                if ("0".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("공").append(" ");
                }

                if ("1".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("하나").append(" ");
                }

                if ("2".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("둘").append(" ");
                }

                if ("3".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("삼").append(" ");
                }

                if ("4".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("넷").append(" ");
                }

                if ("5".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("오").append(" ");
                }

                if ("6".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("여섯").append(" ");
                }

                if ("7".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("칠").append(" ");
                }

                if ("8".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("팔").append(" ");
                }

                if ("9".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("아홉").append(" ");
                }
            } else {
                if ("0".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("공");
                }

                if ("1".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("하나");
                }

                if ("2".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("둘");
                }

                if ("3".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("삼");
                }

                if ("4".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("넷");
                }

                if ("5".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("오");
                }

                if ("6".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("여섯");
                }

                if ("7".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("칠");
                }

                if ("8".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("팔");
                }

                if ("9".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("아홉");
                }

                if ("a".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("알파");
                }

                if ("b".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("브라보");
                }

                if ("c".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("찰리");
                }

                if ("d".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("델타");
                }

                if ("e".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("에코");
                }

                if ("f".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("폭스트로");
                }

                if ("g".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("골프");
                }

                if ("h".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("호텔");
                }

                if ("i".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("인디아");
                }

                if ("j".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("줄리아");
                }

                if ("k".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("키로");
                }

                if ("l".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("리마");
                }

                if ("m".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("마이크");
                }

                if ("n".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("노멤버");
                }

                if ("o".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("오스카");
                }

                if ("p".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("파파");
                }

                if ("q".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("퀘백");
                }

                if ("r".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("로미오");
                }

                if ("s".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("시에라");
                }

                if ("t".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("탱고");
                }

                if ("u".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("유니폼");
                }

                if ("v".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("빅토로");
                }

                if ("w".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("위스키");
                }

                if ("x".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("엑스레이");
                }

                if ("y".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("양키");
                }

                if ("z".equals(String.valueOf(token.charAt(i)))) {
                    sb.append("줄루");
                }
            }
        }

        return sb.toString().trim();
    }
}
