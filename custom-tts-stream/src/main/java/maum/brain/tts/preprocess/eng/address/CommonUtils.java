package maum.brain.tts.preprocess.eng.address;

import java.util.concurrent.TimeUnit;

public class CommonUtils {
    public CommonUtils() {
    }

    public static <T> T nvl(T value, T replacement) {
        return value == null ? replacement : value;
    }

    public static double getElapsed(long startInMilli, TimeUnit unit) {
        double elapsed = (double)(System.currentTimeMillis() - startInMilli);
        if (unit == TimeUnit.MILLISECONDS) {
            return elapsed;
        } else if (unit == TimeUnit.DAYS) {
            return elapsed / 8.64E7D;
        } else if (unit == TimeUnit.HOURS) {
            return elapsed / 3600000.0D;
        } else if (unit == TimeUnit.MINUTES) {
            return elapsed / 60000.0D;
        } else if (unit == TimeUnit.SECONDS) {
            return elapsed / 1000.0D;
        } else {
            throw new UnsupportedOperationException(unit + " conversion is not supported");
        }
    }

    public static void printElapsed(long startInMilli, TimeUnit unit) {
        System.out.println("Elapsed time = " + getElapsed(startInMilli, unit) + " " + unit.name());
    }
}
