package maum.brain.tts.preprocess.kor;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
class HanjaToHangle {
    @XmlElement
    List<UnicodeMap> unicodeMap;

    HanjaToHangle() {
    }
}
