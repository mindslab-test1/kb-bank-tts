package maum.brain.tts.preprocess.eng;

import java.util.HashMap;
import java.util.Map;

public class ConvertSymbol {
    public static final Map<String, String> symbols = new HashMap();

    public ConvertSymbol() {
    }

    public static String convertSymbols(String token) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < token.length(); ++i) {
            char c = token.charAt(i);
            String key = String.valueOf(c);
            if (symbols.containsKey(key)) {
                sb.append(" ").append((String)symbols.get(key)).append(" ");
            } else {
                sb.append(c);
            }
        }

        return sb.toString().replaceAll("\\s+", " ");
    }

    static {
        symbols.put("@", "at");
        symbols.put("#", "hashtag");
        symbols.put("/", "forward slash");
        symbols.put("%", "per cent");
        symbols.put("+", "plus");
        symbols.put("=", "equals");
        symbols.put(">", "greater than");
        symbols.put("<", "less than");
        symbols.put("&", "and");
        symbols.put(":", "colon");
        symbols.put("(", "");
        symbols.put(")", "");
    }
}
