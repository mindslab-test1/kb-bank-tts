package maum.brain.tts.preprocess.kor;

import maum.brain.tts.utils.NumberUtils;

public class ConvertMixedTypeNumber {
    public ConvertMixedTypeNumber() {
    }

    public static String convertOrdinalNumberFormat(String site, String token) {
//        int unicodeNumber = false;
        StringBuilder digit = new StringBuilder();
        StringBuilder hangle = new StringBuilder();

        for(int i = 0; i < token.length(); ++i) {
            int unicodeNumber = token.codePointAt(i);
            if (unicodeNumber >= 48 && unicodeNumber <= 57) {
                digit.append(convertDigit2Hangle(site, String.valueOf(token.charAt(i))));
            } else {
                hangle.append(token.charAt(i));
            }
        }

        return digit.toString().concat(hangle.toString());
    }

    public static String convertOrdinalNumberFormatEx(String site, String token) {
//        int unicodeNumber = false;
        StringBuilder hangle = new StringBuilder();

        for(int i = 0; i < token.length(); ++i) {
            int unicodeNumber = token.codePointAt(i);
            if (unicodeNumber >= 48 && unicodeNumber <= 57) {
                hangle.append(convertDigit2HangleEx(site, String.valueOf(token.charAt(i))));
            } else {
                String str = String.valueOf(token.charAt(i));
                if ("회".equals(str)) {
                    if ("KBCard".equals(site)) {
                        hangle.append(str).append(",,,").append(" ");
                    } else {
                        hangle.append(str).append(".").append(" ");
                    }
                } else {
                    hangle.append(str);
                }
            }
        }

        return hangle.toString();
    }

    private static String convertDigit2HangleEx(String site, String text) {
        if ("KBCard".equals(site)) {
            if ("0".equals(text)) {
                return "공,,,".concat(" ");
            }

            if ("1".equals(text)) {
                return "일,,,".concat(" ");
            }

            if ("2".equals(text)) {
                return "이,,,".concat(" ");
            }

            if ("3".equals(text)) {
                return "삼,,,".concat(" ");
            }

            if ("4".equals(text)) {
                return "사,,,".concat(" ");
            }

            if ("5".equals(text)) {
                return "오,,,".concat(" ");
            }

            if ("6".equals(text)) {
                return "육,,,".concat(" ");
            }

            if ("7".equals(text)) {
                return "칠,,,".concat(" ");
            }

            if ("8".equals(text)) {
                return "팔,,,".concat(" ");
            }

            if ("9".equals(text)) {
                return "구,,,".concat(" ");
            }
        } else {
            if ("0".equals(text)) {
                return "공.".concat(" ");
            }

            if ("1".equals(text)) {
                return "일.".concat(" ");
            }

            if ("2".equals(text)) {
                return "이.".concat(" ");
            }

            if ("3".equals(text)) {
                return "삼.".concat(" ");
            }

            if ("4".equals(text)) {
                return "사.".concat(" ");
            }

            if ("5".equals(text)) {
                return "오.".concat(" ");
            }

            if ("6".equals(text)) {
                return "육.".concat(" ");
            }

            if ("7".equals(text)) {
                return "칠.".concat(" ");
            }

            if ("8".equals(text)) {
                return "팔.".concat(" ");
            }

            if ("9".equals(text)) {
                return "구.".concat(" ");
            }
        }

        return "";
    }

    private static String convertDigit2Hangle(String site, String text) {
        if ("KBCard".equals(site)) {
            if ("0".equals(text)) {
                return "공,,,".concat(" ");
            }

            if ("1".equals(text)) {
                return "일,,,".concat(" ");
            }

            if ("2".equals(text)) {
                return "이,,,".concat(" ");
            }

            if ("3".equals(text)) {
                return "삼,,,".concat(" ");
            }

            if ("4".equals(text)) {
                return "사,,,".concat(" ");
            }

            if ("5".equals(text)) {
                return "오,,,".concat(" ");
            }

            if ("6".equals(text)) {
                return "육,,,".concat(" ");
            }

            if ("7".equals(text)) {
                return "칠,,,".concat(" ");
            }

            if ("8".equals(text)) {
                return "팔,,,".concat(" ");
            }

            if ("9".equals(text)) {
                return "구,,,".concat(" ");
            }
        } else {
            if ("0".equals(text)) {
                return "공.";
            }

            if ("1".equals(text)) {
                return "일.";
            }

            if ("2".equals(text)) {
                return "이.";
            }

            if ("3".equals(text)) {
                return "삼.";
            }

            if ("4".equals(text)) {
                return "사.";
            }

            if ("5".equals(text)) {
                return "오.";
            }

            if ("6".equals(text)) {
                return "육.";
            }

            if ("7".equals(text)) {
                return "칠.";
            }

            if ("8".equals(text)) {
                return "팔.";
            }

            if ("9".equals(text)) {
                return "구.";
            }
        }

        return "";
    }

    public static String convertAgeNumberFormat(String token) {
//        int unicodeNumber = false;
        StringBuilder digit = new StringBuilder();
        StringBuilder hangle = new StringBuilder();

        for(int i = 0; i < token.length(); ++i) {
            int unicodeNumber = token.codePointAt(i);
            if (unicodeNumber >= 48 && unicodeNumber <= 57) {
                digit.append(token.charAt(i));
            } else {
                hangle.append(token.charAt(i));
            }
        }

        try {
            String age = NumberUtils.convertNumeral2Korean(digit.toString());
            if (age.contains("일십")) {
                age = age.replaceAll("일십", "십");
            }

            if (age.contains("일백")) {
                age = age.replaceAll("일백", "백");
            }

            if (age.contains("일천")) {
                age = age.replaceAll("일천", "천");
            }

            if (age.contains("일만")) {
                age = age.replaceAll("일만", "만");
            }

            return age.concat(hangle.toString());
        } catch (Exception var5) {
            return token;
        }
    }
}
