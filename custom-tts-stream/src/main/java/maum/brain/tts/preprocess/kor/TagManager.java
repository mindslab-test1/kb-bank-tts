package maum.brain.tts.preprocess.kor;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import maum.brain.tts.preprocess.type.KorPatternType;
import maum.brain.tts.utils.NumberUtils;
import maum.brain.tts.utils.PPUtils;
import org.apache.commons.lang3.StringUtils;

public class TagManager {
    private static boolean bLog;

    static {
        bLog = Boolean.FALSE;
    }

    public TagManager() {
    }

    private static void printLog(String text) {
        if (bLog) {
            System.out.println(text);
        }

    }

    public static String tagRange(String sentence) {
        String[] arrRange = StringUtils.substringsBetween(sentence, "<range>", "</range>");
        if (arrRange == null) {
            return sentence;
        } else {
            String[] var5 = arrRange;
            int var4 = arrRange.length;

            for(int var3 = 0; var3 < var4; ++var3) {
                String range = var5[var3];
                printLog("range value:" + range.trim());
                String newRange = range.replaceAll("\\s+", "");
                newRange = newRange.replace("~", "-");
                String newToken = ConvertRange.convertRangeFormat(newRange);
                String oriToken = "<range>".concat(range).concat("</range>");
                printLog("oriToken:" + oriToken);
                sentence = sentence.replace(oriToken, newToken);
                printLog("------------------->" + sentence);
            }

            return sentence;
        }
    }

    public static String tagName(String sentence) {
        String[] arrName = StringUtils.substringsBetween(sentence, "<name>", "</name>");
        if (arrName == null) {
            return sentence;
        } else {
            StringBuilder sb = new StringBuilder();
            String[] var6 = arrName;
            int var5 = arrName.length;

            for(int var4 = 0; var4 < var5; ++var4) {
                String name = var6[var4];
                sb.setLength(0);

                for(int i = 0; i < name.length(); ++i) {
                    if (!" ".equals(String.valueOf(name.charAt(i)))) {
                        sb.append(name.charAt(i)).append(": ");
                    }
                }

                sentence = sentence.replace("<name>".concat(name).concat("</name>"), sb.toString());
            }

            return sentence;
        }
    }

    public static String tagPause(String sentence) {
        String[] arrPause = StringUtils.substringsBetween(sentence, "<pause>", "</pause>");
        if (arrPause == null) {
            return sentence;
        } else {
            new String();
            String[] var6 = arrPause;
            int var5 = arrPause.length;

            for(int var4 = 0; var4 < var5; ++var4) {
                String pause = var6[var4];
                String newToken = pause.concat(",");
                sentence = sentence.replace("<pause>".concat(pause).concat("</pause>"), newToken);
            }

            return sentence;
        }
    }

    public static String tagPause2(String sentence) {
        String[] arrPause = StringUtils.substringsBetween(sentence, "<pause_2>", "</pause_2>");
        if (arrPause == null) {
            return sentence;
        } else {
            new String();
            String[] var6 = arrPause;
            int var5 = arrPause.length;

            for(int var4 = 0; var4 < var5; ++var4) {
                String pause = var6[var4];
                String newToken = pause.concat(",,");
                sentence = sentence.replace("<pause_2>".concat(pause).concat("</pause_2>"), newToken);
            }

            return sentence;
        }
    }

    public static String tagPause3(String sentence) {
        String[] arrPause = StringUtils.substringsBetween(sentence, "<pause_3>", "</pause_3>");
        if (arrPause == null) {
            return sentence;
        } else {
            new String();
            String[] var6 = arrPause;
            int var5 = arrPause.length;

            for(int var4 = 0; var4 < var5; ++var4) {
                String pause = var6[var4];
                String newToken = pause.concat(",,,");
                sentence = sentence.replace("<pause_3>".concat(pause).concat("</pause_3>"), newToken);
            }

            return sentence;
        }
    }

    public static String tagPhone(String sentence) {
        String[] arrPhone = StringUtils.substringsBetween(sentence, "<phone>", "</phone>");
        if (arrPhone == null) {
            return sentence;
        } else {
            new String();
            String[] var6 = arrPhone;
            int var5 = arrPhone.length;

            for(int var4 = 0; var4 < var5; ++var4) {
                String phone = var6[var4];
                String newToken = convertPhone(1, phone.replaceAll(" ", ""));
                sentence = sentence.replace("<phone>".concat(phone).concat("</phone>"), newToken);
            }

            return sentence;
        }
    }

    public static String tagPhone2(String sentence) {
        String[] arrPhone = StringUtils.substringsBetween(sentence, "<phone_2>", "</phone_2>");
        if (arrPhone == null) {
            return sentence;
        } else {
            new String();
            String[] var6 = arrPhone;
            int var5 = arrPhone.length;

            for(int var4 = 0; var4 < var5; ++var4) {
                String phone = var6[var4];
                String newToken = convertPhone(2, phone.replaceAll(" ", ""));
                sentence = sentence.replace("<phone_2>".concat(phone).concat("</phone_2>"), newToken);
            }

            return sentence;
        }
    }

    public static String convertPhone(int no, String phone) {
        if (phone.contains("-")) {
            phone = phone.replaceAll("-", "");
        }

        if (phone.contains("/")) {
            phone = phone.replaceAll("/", "");
        }

        if (phone.contains(".")) {
            phone = phone.replaceAll(".", "");
        }

        if (phone.length() == 11) {
            phone = ConvertPhoneNumber.tagPhoneNumberFormat(no, phone);
        }

        return phone;
    }

    public static String tagDateEx(String sentence) {
        String[] arrDate = StringUtils.substringsBetween(sentence, "<date>", "</date>");
        if (arrDate == null) {
            return sentence;
        } else {
            String newToken = new String();
            String[] var6 = arrDate;
            int var5 = arrDate.length;

            for(int var4 = 0; var4 < var5; ++var4) {
                String date = var6[var4];
                if (date.length() != 4 && date.length() != 5) {
                    if (date.length() == 8 || date.length() == 10) {
                        newToken = convertDate(date);
                    }
                } else {
                    newToken = convertDateEx(date);
                }

                sentence = sentence.replace("<date>".concat(date).concat("</date>"), newToken);
            }

            return sentence;
        }
    }

    public static String convertDateEx(String date) {
        if (date.contains("-")) {
            date = date.replaceAll("-", "");
        }

        if (date.contains("/")) {
            date = date.replaceAll("/", "");
        }

        if (date.contains(".")) {
            date = date.replaceAll(".", "");
        }

        if (date.length() == 4) {
            String MM = date.substring(0, 2);
            String dd = date.substring(2, 4);
            date = NumberUtils.convertMonth2Hangle(MM).concat("월").concat(" ").concat(NumberUtils.convertDecimal2Hangle(dd, false)).concat("일").concat(" ");
        }

        return date;
    }

    public static String convertDate(String date) {
        if (date.contains("-")) {
            date = date.replaceAll("-", "");
        }

        if (date.contains("/")) {
            date = date.replaceAll("/", "");
        }

        if (date.contains(".")) {
            date = date.replaceAll(".", "");
        }

        String yyyy = date.substring(0, 4);
        String MM = date.substring(4, 6);
        String dd = date.substring(6, 8);
        date = NumberUtils.convertDecimal2Hangle(yyyy, false).concat("년").concat(" ").concat(NumberUtils.convertMonth2Hangle(MM)).concat("월").concat(" ").concat(NumberUtils.convertDecimal2Hangle(dd, false)).concat("일").concat(" ");
        return date;
    }

    public static String tagDigit(String sentence) {
        String[] arrDigit = StringUtils.substringsBetween(sentence, "<digit>", "</digit>");
        if (arrDigit == null) {
            return sentence;
        } else {
            new String();
            String[] var6 = arrDigit;
            int var5 = arrDigit.length;

            for(int var4 = 0; var4 < var5; ++var4) {
                String digit = var6[var4];
                String tmpToken = digit.replaceAll(",", "").replaceAll("[.]", "");
                String newToken = ConvertDigit.tagDigitFormat(tmpToken);
                sentence = sentence.replace("<digit>".concat(digit).concat("</digit>"), newToken);
            }

            return sentence;
        }
    }

    public static String tagTime(String sentence) {
        String[] arrTime = StringUtils.substringsBetween(sentence, "<time>", "</time>");
        if (arrTime == null) {
            return sentence;
        } else {
            new String();
            String[] var6 = arrTime;
            int var5 = arrTime.length;

            for(int var4 = 0; var4 < var5; ++var4) {
                String time = var6[var4];
                String newToken = convertTime(time);
                sentence = sentence.replace("<time>".concat(time).concat("</time>"), newToken);
            }

            return sentence;
        }
    }

    public static String convertTime(String time) {
        if (time.contains(":")) {
            String HH = time.substring(0, 2);
            String MI = time.substring(3, 5);
            if ("00".equals(MI)) {
                if ("00".equals(HH)) {
                    return "영시".concat(" ");
                }

                time = NumberUtils.convertHour2Hangle(HH).concat("시").concat(" ");
            } else {
                if ("00".equals(HH)) {
                    return "영시".concat(" ").concat(NumberUtils.convertDecimal2Hangle(MI, false)).concat("분").concat(" ");
                }

                time = NumberUtils.convertHour2Hangle(HH).concat("시").concat(" ").concat(NumberUtils.convertDecimal2Hangle(MI, false)).concat("분").concat(" ");
            }
        }

        return time;
    }

    public static String tagMoney(String sentence) {
        String[] arrMoney = StringUtils.substringsBetween(sentence, "<money>", "</money>");
        if (arrMoney == null) {
            return sentence;
        } else {
            new String();
            new String();
            String[] var7 = arrMoney;
            int var6 = arrMoney.length;

            for(int var5 = 0; var5 < var6; ++var5) {
                String money = var7[var5];

                String token;
                String newToken;
                for(StringTokenizer st = new StringTokenizer(money, " "); st.hasMoreTokens(); sentence = sentence.replace("<money>".concat(token).concat("</money>"), newToken)) {
                    token = st.nextToken();
                    String tmpToken = token.replaceAll(",", "");
                    newToken = ConvertMoney.tagCurrencyFormat(tmpToken);
                }
            }

            return sentence;
        }
    }

    public static String tagAlpha(String sentence) {
        String[] arrAlpha = StringUtils.substringsBetween(sentence, "<alpha>", "</alpha>");
        if (arrAlpha == null) {
            return sentence;
        } else {
            new String();
            new String();
            String[] var7 = arrAlpha;
            int var6 = arrAlpha.length;

            for(int var5 = 0; var5 < var6; ++var5) {
                String alpha = var7[var5];
                printLog("alpha value:" + alpha);
                StringTokenizer st = new StringTokenizer(alpha, " ");

                while(st.hasMoreTokens()) {
                    String token = st.nextToken();
                    boolean bEng = PPUtils.isEnglish(token);
                    if (bEng) {
                        String newToken = ConvertEnglish.convertEnglish(token);
                        printLog("token[" + token + "]:new token[" + newToken + "]");
                        sentence = sentence.replace("<alpha>".concat(token).concat("</alpha>"), newToken);
                    }
                }
            }

            return sentence;
        }
    }

    public static String tagAddress(String sentence) {
        String[] arrAddress = StringUtils.substringsBetween(sentence, "<address>", "</address>");
        if (arrAddress == null) {
            return sentence;
        } else {
            new String();
            new String();
            new String();
            String[] var8 = arrAddress;
            int var7 = arrAddress.length;

            label43:
            for(int var6 = 0; var6 < var7; ++var6) {
                String address = var8[var6];
                printLog("address value:" + address);
                StringTokenizer st = new StringTokenizer(address, " ");

                while(true) {
                    while(true) {
                        if (!st.hasMoreTokens()) {
                            continue label43;
                        }

                        String token = st.nextToken();
                        String token2 = token.replaceAll(",", "");
                        boolean bDigit = PPUtils.isDigit(token2);
                        String newToken;
                        if (bDigit) {
                            newToken = NumberUtils.convertDecimal2Hangle(token2, false);
                            printLog("token[" + token + "]:new token[" + newToken + "]");
                            sentence = sentence.replace(token, newToken);
                        } else if (token.contains("-")) {
                            newToken = convertAddressNumber2Hangle(token2);
                            sentence = sentence.replace(token, newToken);
                        } else {
                            List<String> tokenList = extractPattern(KorPatternType.P_NOT_KOREAN, token);
                            if (tokenList.size() != 0) {
                                int notKorTokenSize = tokenList.size();

                                for(int k = 0; k < notKorTokenSize; ++k) {
                                    newToken = NumberUtils.convertDecimal2Hangle((String)tokenList.get(k), false);
                                    sentence = sentence.replaceFirst((String)tokenList.get(k), newToken);
                                }
                            }
                        }
                    }
                }
            }

            sentence = sentence.replaceAll("</address>", "");
            sentence = sentence.replaceAll("<address>", "");
            return sentence;
        }
    }

    private static List<String> extractPattern(Pattern p, String token) {
        ArrayList<String> pList = new ArrayList();
        Matcher m = p.matcher(token);

        while(m.find()) {
            pList.add(m.group().trim());
        }

        return pList;
    }

    private static String convertAddressNumber2Hangle(String token) {
        String[] tokens = token.split("-");
        return NumberUtils.convertDecimal2Hangle(tokens[0], false).concat(" ").concat("다시").concat(" ").concat(NumberUtils.convertDecimal2Hangle(tokens[1], false));
    }
}
