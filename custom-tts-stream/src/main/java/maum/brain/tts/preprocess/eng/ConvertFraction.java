package maum.brain.tts.preprocess.eng;

import maum.brain.tts.utils.PPUtils;

public class ConvertFraction {
    public ConvertFraction() {
    }

    public static String convertFraction(String token) {
        if (token.contains("/") || token.contains("⁄")) {
            token = token.replace("/", "⁄");
            String[] parts = token.split("⁄");
            if (parts.length == 2 && PPUtils.isDigit(parts[0]) && PPUtils.isDigit(parts[1])) {
                if (parts[0].equals(parts[1])) {
                    return "one".concat(" ");
                }

                if (parts[1].equals("2") && parts[0].equals("1")) {
                    return "one half".concat(" ");
                }

                if (parts[1].equals("4")) {
                    if (parts[0].equals("1")) {
                        return "one quarter".concat(" ");
                    }

                    if (parts[0].equals("3")) {
                        return "three quarters".concat(" ");
                    }
                }

                if (Integer.parseInt(parts[0]) > 1) {
                    return ConvertToken.convertNumber(parts[0]) + " " + ConvertToken.convertOrdinal(parts[1]) + "s";
                }

                return ConvertToken.convertNumber(parts[0]) + " " + ConvertToken.convertOrdinal(parts[1]);
            }
        }

        return token;
    }
}
