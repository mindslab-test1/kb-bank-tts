package maum.brain.tts.preprocess.kor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import kr.bydelta.koala.hnn.SentenceSplitter;
import maum.brain.tts.preprocess.type.KorPatternType;

public class NormalizeText {
    public NormalizeText() {
    }

    public static List<String> extractSentence(String paragraph) {
        List<String> list = new ArrayList();
        if (!paragraph.isEmpty()) {
            paragraph = paragraph.trim();
            paragraph = paragraph.replaceAll("\\(주\\)", "주식회사").replaceAll("\\(일\\)", "일요일").replaceAll("\\(월\\)", "월요일").replaceAll("\\(화\\)", "화요일").replaceAll("\\(수\\)", "수요일").replaceAll("\\(목\\)", "목요일").replaceAll("\\(금\\)", "금요일").replaceAll("\\(토\\)", "토요일");
            paragraph = removeBracketText(paragraph);
            paragraph = paragraph.replaceAll("\r\n", "\n");
            list = Arrays.asList(paragraph.split("(\\r|\\n|\\r\\n|\\n\\r)"));
        }

        return (List)list;
    }

    public static List<String> extractSentenceWithNLP(String paragraph) {
        SentenceSplitter splitter = new SentenceSplitter();
        paragraph = paragraph.trim();
        paragraph = paragraph.replaceAll("\\(주\\)", "주식회사").replaceAll("\\(일\\)", "일요일").replaceAll("\\(월\\)", "월요일").replaceAll("\\(화\\)", "화요일").replaceAll("\\(수\\)", "수요일").replaceAll("\\(목\\)", "목요일").replaceAll("\\(금\\)", "금요일").replaceAll("\\(토\\)", "토요일");
        paragraph = removeBracketText(paragraph);
        paragraph = paragraph.replaceAll("\r\n", "\n");
        return splitter.sentences(paragraph);
    }

    public static String normalizeText(String paragraph) {
        paragraph = paragraph.replaceAll("(\\r|\\n|\\r\\n|\\n\\r)", "");
        paragraph = paragraph.replaceAll(" +", " ");
        paragraph = paragraph.toLowerCase();
        return removeBracketText(paragraph);
    }

    public static String removeBracketText(String token) {
        Matcher m = KorPatternType.P_BRACKET.matcher(token);
        new String();

        while(m.find()) {
            int start = m.start();
            int end = m.end();
            String removeTextArea = token.substring(start, end);
            token = token.replace(removeTextArea, "");
            m = KorPatternType.P_BRACKET.matcher(token);
        }

        return token;
    }

    public static List<String> extractBracketText(String token) {
        ArrayList<String> bracketList = new ArrayList();
        Matcher m = KorPatternType.P_BRACKET.matcher(token);
        new String();

        while(m.find()) {
            String findText = token.substring(m.start(), m.end());
            bracketList.add(findText);
        }

        return bracketList;
    }
}
