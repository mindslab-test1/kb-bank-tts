package maum.brain.tts.preprocess.kor;

import java.util.List;
import maum.brain.tts.preprocess.type.KorPatternType;
import maum.brain.tts.utils.NumberUtils;
import maum.brain.tts.utils.PPUtils;

public class ConvertMoney {
    public ConvertMoney() {
    }

    public static String tagCurrencyFormat(String token) {
        new String();
        new String();
        if (!token.endsWith("원")) {
            token = convertDecimal2Hangle(token);
            return token.replace("만", "만 ").replace("억", "억 ");
        } else {
            List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_CURRENCY_DIGIT, token);
            new String();

            for(int i = 0; i < aList.size(); ++i) {
                String word = (String)aList.get(i);
                int idxWon = word.indexOf("원");
                String curNum = word.substring(0, idxWon);
                String curUnit = word.substring(idxWon);
                token = convertDecimal2Hangle(curNum).concat(curUnit);
            }

            return token.replace("만", "만 ").replace("억", "억 ");
        }
    }

    public static String tagCurrencyFormatEx(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_CURRENCY_DIGIT, token);
        new String();

        for(int i = 0; i < aList.size(); ++i) {
            String word = (String)aList.get(i);
            int idxWon = word.indexOf("원");
            String curNum = word.substring(0, idxWon);
            String curUnit = word.substring(idxWon);
            String newToken = convertDecimal2Hangle(curNum);
            String tmp = "^*".concat(word);
            token = token.replace(tmp, newToken.concat(curUnit));
        }

        return token.replace("만", "만 ").replace("억", "억 ");
    }

    public static String convertCurrencyFormat(String token) {
        new String();
        new String();
        String curNum;
        String curUnit;
        String lastStr;
        if (token.contains("원")) {
            int i;
            List aList;
            if (!token.contains("만원") && !token.contains("천원") && !token.contains("백원") && !token.contains("십원")) {
                aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_CURRENCY_DIGIT, token);
                new String();
                lastStr = (String)aList.get(0);
                i = lastStr.indexOf("원");
                curNum = lastStr.substring(0, i);
                curUnit = lastStr.substring(i);
                token = convertDecimal2Hangle(curNum).concat(curUnit);
                if (!token.endsWith("만원") && curUnit.length() == 1) {
                    token = token.replace("만", "만 ");
                }

                if (!token.endsWith("억원") && curUnit.length() == 1) {
                    token = token.replace("억", "억 ");
                }

                return token;
            } else {
                aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_CURRENCY_DIGIT, token);
                new String();

                for(i = 0; i < aList.size(); ++i) {
                    lastStr = (String)aList.get(i);
                    int idxWon = lastStr.indexOf("원");
                    curNum = lastStr.substring(0, idxWon - 1);
                    curUnit = lastStr.substring(idxWon - 1);
                    token = convertDecimal2Hangle(curNum).concat(curUnit);
                }

                return token;
            }
        } else {
            if (token.startsWith("$") || token.startsWith("€") || token.startsWith("¥") || token.startsWith("₩")) {
                int lastDigit = PPUtils.lastNumeric(token);
                curUnit = token.substring(0, 1);
                curNum = token.substring(1, lastDigit + 1);
                lastStr = token.substring(lastDigit + 1);
                if (curUnit.equals("$")) {
                    token = convertDecimal2Hangle(curNum).concat(convertCurrency2Hangle(curUnit));
                } else {
                    token = convertDecimal2Hangle(curNum).concat(convertCurrency2Hangle(curUnit));
                }

                if (!token.endsWith("만원")) {
                    token = token.replace("만", "만 ");
                }

                if (!token.endsWith("억원")) {
                    token = token.replace("억", "억 ");
                }

                if (!lastStr.isEmpty()) {
                    token = token.concat(lastStr);
                }

                if (token.endsWith("원로")) {
                    token = token.replace("원로", "원으로");
                }
            }

            return token;
        }
    }

    public static String convertCurrencyExFormat(String token) {
        String tmp = convertDecimal2Hangle(token);
        if (tmp.equals("일")) {
            tmp = "";
        }

        return tmp;
    }

    private static String convertCurrency2Hangle(String text) {
        if (text.equals("₩")) {
            return "원";
        } else if (text.equals("$")) {
            return "달러";
        } else if (text.equals("€")) {
            return "유로";
        } else {
            return text.equals("¥") ? "엔" : "";
        }
    }

    private static String convertDecimal2Hangle(String text) {
        String tmpText = text.replace(",", "");
        if (tmpText.length() < 3 && Integer.parseInt(tmpText) == 0) {
            return "영";
        } else {
            String sign = "";
            if (tmpText.toCharArray()[0] == '-') {
                sign = "마이너스".concat(" ");
                tmpText = tmpText.substring(1);
            }

            return NumberUtils.replaceNumber(sign + NumberUtils.replaceNumber2Hangle(tmpText));
        }
    }
}
