package maum.brain.tts.preprocess.kor;

import java.util.ArrayList;

public class PreProcess {
    public PreProcess() {
    }

    public static void main(String[] args) {
        String text = " <phone_2>010-2001-0909</phone_2> ";
        ArrayList<String> sentenceList = getTTSSentence(text);

        for(int i = 0; i < sentenceList.size(); ++i) {
            System.out.println("Result:[" + i + "]" + (String)sentenceList.get(i));
        }

    }

    public static ArrayList<String> getTTSSentence(String sentence) {
        ArrayList<String> newSentenceList = new ArrayList();
        new ArrayList();
        sentence = sentence.trim();
        if (sentence.contains("<phone_2>") && sentence.contains("</phone_2>")) {
//            int idx_start = false;
//            int idx_end = false;
            int idx_start = sentence.indexOf("<phone_2>");
            int idx_end = sentence.indexOf("</phone_2>");
            if (idx_start == 0) {
                newSentenceList.add(sentence.substring(idx_start, idx_end + "</phone_2>".length()).concat("\n"));
            } else {
                String sFirst = sentence.substring(0, idx_start - 1);
                newSentenceList = ConvertText.getTTSSentence(sFirst);
                newSentenceList.add(sentence.substring(idx_start, idx_end + "</phone_2>".length()).concat("\n"));
            }

            if (sentence.endsWith("</phone_2>")) {
                return newSentenceList;
            } else {
                ArrayList<String> newSentenceList2 = ConvertText.getTTSSentence(sentence.substring(idx_end + "</phone_2>".length()));
                ArrayList<String> joined = new ArrayList();
                joined.addAll(newSentenceList);
                joined.addAll(newSentenceList2);
                return joined;
            }
        } else {
            return ConvertText.getTTSSentence(sentence);
        }
    }
}
