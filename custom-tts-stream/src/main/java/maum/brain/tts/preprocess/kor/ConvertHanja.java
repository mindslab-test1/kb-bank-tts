package maum.brain.tts.preprocess.kor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.StringTokenizer;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class ConvertHanja {
    private static char[] HANJA_TO_HANGLE_MAP = new char[65565];
    private static String hanja2korFile = "src/main/resources/tts.kor.dict.hanja2kor.xml";
    private static boolean bLoad;

    public static void main(String[] args) {
        try {
            String text = "大韓民國의 主權은 國民에게 있고, 모든 權力은 國民으로부터 나온다.";
            String result = toHangle(text);
            System.out.println(result);
        } catch (Exception var3) {
        }

    }

    public ConvertHanja() throws FileNotFoundException, JAXBException {
    }

    public static void loadXMLData(String xmlFile) throws FileNotFoundException, JAXBException {
        hanja2korFile = xmlFile;
        JAXBContext context = JAXBContext.newInstance(new Class[]{HanjaToHangle.class});
        Unmarshaller unmarshaller = context.createUnmarshaller();
        HanjaToHangle root = (HanjaToHangle)unmarshaller.unmarshal(new FileInputStream(xmlFile));
        StringTokenizer st = new StringTokenizer(((UnicodeMap)root.unicodeMap.get(0)).code, ",");
        int i = 0;

        int value;
        for(boolean var6 = false; st.hasMoreTokens(); HANJA_TO_HANGLE_MAP[i++] = (char)value) {
            value = Integer.decode(st.nextToken().trim());
        }

    }

    public static String toHangle(String hanja) throws Exception {
        if (!bLoad) {
            loadXMLData(hanja2korFile);
            bLoad = Boolean.TRUE;
        }

        char unicode = 0;
        byte[] hanjaByte = hanja.getBytes("UTF-8");
        int i = 0;

        while(i < hanjaByte.length) {
            if ((hanjaByte[i] & 255) < 128) {
                ++i;
            } else if ((hanjaByte[i] & 255) < 224) {
                i += 2;
            } else {
                if ((hanjaByte[i] & 255) < 240) {
                    unicode = (char)(hanjaByte[i] & 15);
                    ++i;
                    unicode = (char)(unicode << 6);
                    unicode = (char)(unicode | hanjaByte[i] & 63);
                    ++i;
                    unicode = (char)(unicode << 6);
                    unicode = (char)(unicode | hanjaByte[i] & 63);
                    ++i;
                }

                if (HANJA_TO_HANGLE_MAP[unicode] != unicode) {
                    unicode = HANJA_TO_HANGLE_MAP[unicode];
                    hanjaByte[i - 1] = (byte)(unicode & 63 | 128);
                    hanjaByte[i - 2] = (byte)((unicode << 2 & 16128 | '耀') >> 8);
                    hanjaByte[i - 3] = (byte)((unicode << 4 & 4128768 | 14680064) >> 16);
                }
            }
        }

        return new String(hanjaByte, "UTF-8");
    }

    static {
        bLoad = Boolean.FALSE;
    }
}
