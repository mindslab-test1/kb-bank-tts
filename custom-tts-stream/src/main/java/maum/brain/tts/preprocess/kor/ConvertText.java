package maum.brain.tts.preprocess.kor;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import maum.brain.tts.preprocess.type.KorPatternType;
import maum.brain.tts.utils.NumberUtils;
import maum.brain.tts.utils.PPUtils;

public class ConvertText {
    private static boolean bLog;

    static {
        bLog = Boolean.FALSE;
    }

    public ConvertText() {
    }

    public static ArrayList<String> getTTSSentence(String text) {
        ArrayList<String> cleanSentencelist = new ArrayList();
        String site = System.getProperty("site");
        if (site == null || site.isEmpty()) {
            site = "KBCard";
        }

        text = PPUtils.removeKoreanConsonantAndVowel(text);
        text = text.trim();
        if (text.startsWith("<phone>") && text.endsWith("</phone>")) {
            cleanSentencelist.add(text);
            return cleanSentencelist;
        } else if (text.startsWith("<digit>") && text.endsWith("</digit>")) {
            text = text.replaceAll(",", "").replaceAll("[.]", "");
            cleanSentencelist.add(text);
            return cleanSentencelist;
        } else {
            List<String> sentenceList = null;
            sentenceList = NormalizeText.extractSentence(text);
            StringBuilder sb = new StringBuilder();
            StringTokenizer st = null;
            new String();
            new String();
            new String();
            new String();
            List<String> tokenList = new ArrayList();
            boolean bAlpha = Boolean.FALSE;
            boolean bDigit = Boolean.FALSE;
            boolean bFindDigit = Boolean.FALSE;

            for(int i = 0; sentenceList != null && i < sentenceList.size(); ++i) {
                String sentence = ((String)sentenceList.get(i)).trim();
                sentence = sentence.toLowerCase().replaceAll("\\s+", " ");
                printLog("Sentence[" + i + "]:[" + sentence + "]");
                if (!sentence.isEmpty()) {
                    sb.setLength(0);
                    if (sentence.contains("<") && sentence.contains("</")) {
                        sentence = TagManager.tagName(sentence);
                        sentence = TagManager.tagAddress(sentence);
                        sentence = TagManager.tagAlpha(sentence);
                        sentence = TagManager.tagMoney(sentence);
                        sentence = TagManager.tagDigit(sentence);
                        sentence = TagManager.tagDateEx(sentence);
                        sentence = TagManager.tagTime(sentence);
                        sentence = TagManager.tagPause(sentence);
                        sentence = TagManager.tagPause2(sentence);
                        sentence = TagManager.tagPause3(sentence);
                        sentence = TagManager.tagPhone(sentence);
                        sentence = TagManager.tagPhone2(sentence);
                        sentence = TagManager.tagRange(sentence);
                    }

                    st = new StringTokenizer(sentence, " ");

                    while(true) {
                        while(st.hasMoreTokens()) {
                            String token = st.nextToken();
                            String newToken = "";
                            String notKorToken = "";
                            bAlpha = Boolean.FALSE;
                            bDigit = Boolean.FALSE;
                            bFindDigit = Boolean.FALSE;
                            newToken = ConvertReplaceWord.replaceWords(token);
                            if (!newToken.equals(token) && newToken.length() != 0) {
                                printLog("replaceWords:[" + token + "]->[" + newToken + "]");
                                sb.append(newToken).append(" ");
                            } else {
                                tokenList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_NOT_KOREAN, token);
                                if (((List)tokenList).size() == 0) {
                                    newToken = ConvertHangle.ConvertHangleForSound(token, sentence.length());
                                    printLog("Type:PureHangle, Token[" + token + "]->[" + newToken + "]");
                                    sb.append(newToken).append(" ");
                                } else {
                                    int notKorTokenSize = ((List)tokenList).size();

                                    for(int k = 0; k < notKorTokenSize; ++k) {
                                        if (k == notKorTokenSize - 1) {
                                            notKorToken = notKorToken + (String)((List)tokenList).get(k);
                                        } else {
                                            notKorToken = notKorToken + (String)((List)tokenList).get(k) + " ";
                                        }
                                    }

                                    printLog("Type:Mixed, Token[" + token + "], Not Hangle:[" + notKorToken + "]");
                                    if (notKorToken.equals(".") && token.endsWith(".")) {
                                        sb.append(token).append(" ");
                                    } else if (notKorToken.equals("?") && token.endsWith("?")) {
                                        sb.append(token).append(" ");
                                    } else if (notKorToken.equals(",") && token.endsWith(",")) {
                                        sb.append(token).append(" ");
                                    } else {
                                        int k;
                                        String data;
                                        String checkToken;
                                        String engTmp;
                                        if (token.equals(notKorToken)) {
                                            if (token.equals("%")) {
                                                sb.append("퍼센트").append(" ");
                                                continue;
                                            }

                                            if (token.equals("*")) {
                                                sb.append("별").append(" ");
                                                continue;
                                            }

                                            if (token.equals("#")) {
                                                sb.append("샵").append(" ");
                                                continue;
                                            }

                                            StringTokenizer tmpST;
                                            if (token.endsWith(",,,")) {
                                                tmpST = new StringTokenizer(token, ",,,");
                                                k = tmpST.countTokens();
                                                if (k == 1) {
                                                    data = token.replaceAll(",", "");
                                                    if (PPUtils.isDigit(data)) {
                                                        newToken = ConvertDigit.convertDigitFormat(data);
                                                        if (!newToken.equals(data)) {
                                                            printLog("Type:No Hangle, Comma:DigitFormat:[" + token + "]->[" + newToken + "]");
                                                            token = token.replace(token, newToken);
                                                            sb.append(token).append(",,,").append(" ");
                                                            continue;
                                                        }
                                                    } else if (PPUtils.isEnglishDigit(data)) {
                                                        if (!PPUtils.hasDigit(data)) {
                                                            newToken = ConvertEnglish.convertEnglish(data);
                                                            if (!newToken.equals(data)) {
                                                                printLog("Type:No Hangle, Comma:EnglishFormat:[" + token + "]->[" + newToken + "]");
                                                                token = token.replace(token, newToken);
                                                                sb.append(token).append(",,,").append(" ");
                                                                continue;
                                                            }
                                                        } else {
                                                            newToken = ConvertDigit.convertAlphaDigitFormat(data);
                                                            if (!newToken.equals(data)) {
                                                                printLog("Type:No Hangle, Comma:AlphaDigitFormat:[" + token + "]->[" + newToken + "]");
                                                                token = token.replace(token, newToken);
                                                                sb.append(token).append(",,,").append(" ");
                                                                continue;
                                                            }
                                                        }
                                                    }
                                                }

                                                if (k > 1) {
                                                    while(tmpST.hasMoreTokens()) {
                                                        data = tmpST.nextToken();
                                                        if (PPUtils.isDigit(data)) {
                                                            newToken = ConvertDigit.convertDigitFormat(data);
                                                            if (!newToken.equals(data)) {
                                                                token = token.replace(token, newToken);
                                                                sb.append(token).append(",,,");
                                                            }
                                                        } else if (PPUtils.isEnglishDigit(data)) {
                                                            if (PPUtils.hasDigit(data)) {
                                                                newToken = ConvertDigit.convertAlphaDigitFormat(data);
                                                                if (!newToken.equals(data)) {
                                                                    token = token.replace(token, newToken);
                                                                    sb.append(token).append(",,,");
                                                                }
                                                            } else {
                                                                newToken = ConvertEnglish.convertEnglish(data);
                                                                if (!newToken.equals(data)) {
                                                                    token = token.replace(token, newToken);
                                                                    sb.append(token).append(",,,");
                                                                }
                                                            }
                                                        }
                                                    }

                                                    sb.append(" ");
                                                    continue;
                                                }
                                            }

                                            if (PPUtils.isDigit(token)) {
                                                if (token.equals("119")) {
                                                    sb.append("일일구").append(" ");
                                                    continue;
                                                }

                                                if (token.equals("112")) {
                                                    sb.append("일일이").append(" ");
                                                    continue;
                                                }

                                                if (token.equals("0123456789")) {
                                                    sb.append("영:일:이:삼:사:오:륙:칠:팔:구:").append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.endsWith(",")) {
                                                checkToken = notKorToken.replace(",", "");
                                                if (PPUtils.isAlphabetic(checkToken)) {
                                                    newToken = ConvertEnglish.convertEnglish(checkToken);
                                                    if (!newToken.equals(checkToken)) {
                                                        printLog("Type:No Hangle, CommaEnd:EnglishFormat:[" + checkToken + "]->[" + newToken + "]");
                                                        token = token.replaceAll(checkToken, newToken);
                                                        sb.append(token).append(" ");
                                                        continue;
                                                    }
                                                }
                                            }

                                            if (token.contains("@")) {
                                                newToken = ConvertEmail.convertEmailFormat(token);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:No Hangle, EmailFormat:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.contains("~")) {
                                                newToken = ConvertTidle.convertTidleFormat(token);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:No Hangle, DigitTidleFormat:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.contains("-") && token.contains(":") && token.length() == 11) {
                                                newToken = ConvertRange.convertRangeFormat(token);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:No Hangle, RangeRimeFormat:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.contains("/")) {
                                                newToken = ConvertTidle.convertFractionFormat(token);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:No Hangle, FractionFormat:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.equals("a.m")) {
                                                newToken = token.replace("a.m", "오전");
                                                if (!newToken.equals(token)) {
                                                    sb.append(newToken).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.equals("a.m.")) {
                                                newToken = token.replace("a.m.", "오전");
                                                if (!newToken.equals(token)) {
                                                    sb.append(newToken).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.equals("p.m")) {
                                                newToken = token.replace("p.m", "오후");
                                                if (!newToken.equals(token)) {
                                                    sb.append(newToken).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.equals("p.m.")) {
                                                newToken = token.replace("p.m.", "오후");
                                                if (!newToken.equals(token)) {
                                                    sb.append(newToken).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.contains("mm")) {
                                                token = token.replace("mm", "㎜");
                                            }

                                            if (token.contains("cm")) {
                                                token = token.replace("cm", "㎝");
                                            }

                                            if (token.contains("kg")) {
                                                token = token.replace("kg", "㎏");
                                            }

                                            if (token.contains("km")) {
                                                token = token.replace("km", "㎞");
                                            }

                                            if (token.contains("Hz".toLowerCase())) {
                                                token = token.replace("Hz".toLowerCase(), "㎐");
                                            }

                                            if (token.contains("-")) {
                                                tmpST = new StringTokenizer(token, "-");
                                                new String();

                                                while(tmpST.hasMoreTokens()) {
                                                    data = tmpST.nextToken();
                                                    if (PPUtils.isAlphabetic(data)) {
                                                        engTmp = ConvertEnglish.convertEnglish(data);
                                                        printLog("Type:No Hangle, EnglishFormat:[" + data + "]->[" + engTmp + "]");
                                                        token = token.replace(data, engTmp);
                                                    }
                                                }
                                            }

                                            if (PPUtils.isDigit(token) && !token.startsWith("0")) {
                                                newToken = ConvertDecimal.convertDecimalFormat(token);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:Mixed, DecimalFormat:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (!token.contains("%") && !token.contains("$") && !token.contains("€") && !token.contains("¥") && !token.contains("₩") && !token.contains("£")) {
                                                checkToken = token.replaceAll("[.]", "").replaceAll(",", "").replaceAll("-", "").replaceAll("-", "");
                                                if (PPUtils.isDigit(checkToken)) {
                                                    String[] numbers;
                                                    int dotCount;
                                                    if (!PPUtils.isAlphabetic(token) && token.contains(".")) {
                                                        numbers = token.split("[.]");
                                                        dotCount = numbers.length;
                                                        if (dotCount == 2) {
                                                            newToken = ConvertDecimal.convertDecimalFormat(token);
                                                            if (!newToken.equals(token)) {
                                                                printLog("Type:Mixed, DecimalFormat:[" + token + "]->[" + newToken + "]");
                                                                sb.append(newToken).append(" ");
                                                                continue;
                                                            }
                                                        }
                                                    }

                                                    if (!PPUtils.isAlphabetic(token) && token.contains(",")) {
                                                        if (token.contains(".")) {
                                                            numbers = token.split("[.]");
                                                            dotCount = numbers.length;
                                                            if (dotCount == 2) {
                                                                newToken = ConvertDecimal.convertDecimalFormat(token);
                                                                if (!newToken.equals(token)) {
                                                                    printLog("Type:Mixed, DecimalFormat:[" + token + "]->[" + newToken + "]");
                                                                    sb.append(newToken).append(" ");
                                                                    continue;
                                                                }
                                                            }
                                                        } else {
                                                            numbers = token.split("[.]");
                                                            dotCount = numbers.length;
                                                            if (dotCount == 1) {
                                                                newToken = ConvertDecimal.convertDecimalFormat(token);
                                                                if (!newToken.equals(token)) {
                                                                    printLog("Type:Mixed, DecimalFormat:[" + token + "]->[" + newToken + "]");
                                                                    sb.append(newToken).append(" ");
                                                                    continue;
                                                                }
                                                            }

                                                            if (dotCount == 2) {
                                                                newToken = ConvertDecimal.convertDecimalFormat(token);
                                                                if (!newToken.equals(token)) {
                                                                    printLog("Type:Mixed, DecimalFormat:[" + token + "]->[" + newToken + "]");
                                                                    sb.append(newToken).append(" ");
                                                                    continue;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        bFindDigit = PPUtils.hasDigit(notKorToken);
                                        if (bFindDigit) {
                                            if ((token.endsWith("십원") || token.endsWith("백원") || token.endsWith("천원") || token.endsWith("만원") || token.endsWith("억원")) && PPUtils.isDigit(notKorToken)) {
                                                newToken = ConvertMoney.convertCurrencyExFormat(notKorToken);
                                                if (!newToken.equals(notKorToken)) {
                                                    printLog("Type:Mixed, CurrencyExFormat:[" + notKorToken + "]->[" + newToken + "]");
                                                    token = token.replaceAll(notKorToken, newToken);
                                                    sb.append(token).append(" ");
                                                    continue;
                                                }
                                            }

                                            String[] tokenArr;
                                            if (token.endsWith("원,")) {
                                                tokenArr = notKorToken.split(" ");
                                                notKorToken = notKorToken.replace(",", "").trim();
                                                if (token.endsWith("만원,")) {
                                                    notKorToken = notKorToken.concat("00000");
                                                }

                                                if (PPUtils.isDigit(notKorToken)) {
                                                    newToken = ConvertMoney.convertCurrencyExFormat(notKorToken);
                                                    if (!newToken.equals(notKorToken)) {
                                                        printLog("Type:Mixed, CurrencyCommaFormat:[" + notKorToken + "]->[" + newToken + "]");
                                                        token = token.replaceAll(tokenArr[0], newToken);
                                                        if (token.endsWith("만원,")) {
                                                            token = token.replaceAll("만", "");
                                                        }

                                                        sb.append(token).append(" ");
                                                        continue;
                                                    }
                                                }
                                            }

                                            if (token.endsWith("원에")) {
                                                tokenArr = notKorToken.split(" ");
                                                notKorToken = notKorToken.replace(",", "").trim();
                                                if (token.endsWith("만원에")) {
                                                    notKorToken = notKorToken.concat("00000");
                                                }

                                                if (PPUtils.isDigit(notKorToken)) {
                                                    newToken = ConvertMoney.convertCurrencyExFormat(notKorToken);
                                                    if (!newToken.equals(notKorToken)) {
                                                        printLog("Type:Mixed, CurrencyAtFormat:[" + notKorToken + "]->[" + newToken + "]");
                                                        token = token.replaceAll(tokenArr[0], newToken);
                                                        if (token.endsWith("만원에")) {
                                                            token = token.replaceAll("만", "");
                                                        }

                                                        sb.append(token).append(" ");
                                                        continue;
                                                    }
                                                }
                                            }

                                            if (token.endsWith("원을")) {
                                                tokenArr = notKorToken.split(" ");
                                                notKorToken = notKorToken.replace(",", "").trim();
                                                if (token.endsWith("만원을")) {
                                                    notKorToken = notKorToken.concat("00000");
                                                }

                                                if (PPUtils.isDigit(notKorToken)) {
                                                    newToken = ConvertMoney.convertCurrencyExFormat(notKorToken);
                                                    if (!newToken.equals(notKorToken)) {
                                                        printLog("Type:Mixed, CurrencyObjectFormat:[" + notKorToken + "]->[" + newToken + "]");
                                                        token = token.replaceAll(tokenArr[0], newToken);
                                                        if (token.endsWith("만원을")) {
                                                            token = token.replaceAll("만", "");
                                                        }

                                                        sb.append(token).append(" ");
                                                        continue;
                                                    }
                                                }
                                            }

                                            if (token.endsWith("원이상")) {
                                                tokenArr = notKorToken.split(" ");
                                                notKorToken = notKorToken.replace(",", "").trim();
                                                if (token.endsWith("만원이상")) {
                                                    notKorToken = notKorToken.concat("0000");
                                                    printLog("--------------------:notKorToken -> [" + notKorToken + "]");
                                                }

                                                if (PPUtils.isDigit(notKorToken)) {
                                                    newToken = ConvertMoney.convertCurrencyExFormat(notKorToken);
                                                    if (!newToken.equals(notKorToken)) {
                                                        printLog("Type:Mixed, CurrencyOverFormat:[" + notKorToken + "]->[" + newToken + "]");
                                                        token = token.replaceAll(tokenArr[0], newToken);
                                                        if (token.endsWith("만원이상")) {
                                                            printLog("-----------------------------------------------:token -> [" + token + "]");
                                                            if (token.contains("만만")) {
                                                                token = token.replace("만만", "만");
                                                            } else {
                                                                token = token.replaceAll("만", "");
                                                            }

                                                            token = token.replace("억만원이상", "억원이상");
                                                            token = token.replace("조억", "조");
                                                        }

                                                        sb.append(token).append(" ");
                                                        continue;
                                                    }
                                                }
                                            }

                                            if (token.contains("^*") && token.contains("원")) {
                                                newToken = ConvertMoney.tagCurrencyFormatEx(token);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:Mixed, tagCuurencyExFormat:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.endsWith("호") && PPUtils.isDigit(notKorToken) && PPUtils.isDigit(notKorToken)) {
                                                newToken = ConvertMetrics.convertChinaPointFormat(notKorToken);
                                                if (!newToken.equals(notKorToken)) {
                                                    printLog("Type:Mixed, MetricsChinaFormat:[" + notKorToken + "]->[" + newToken + "]");
                                                    token = token.replaceAll(notKorToken, newToken.replace(" ", ""));
                                                    sb.append(token).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.endsWith("점")) {
                                                newToken = ConvertMetrics.convertChinaPointFormat(notKorToken);
                                                if (!newToken.equals(notKorToken)) {
                                                    printLog("Type:Mixed, MetricsChinaFormat:[" + notKorToken + "]->[" + newToken + "]");
                                                    token = token.replaceAll(notKorToken, newToken);
                                                    sb.append(token).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.endsWith("번")) {
                                                if (PPUtils.isDigit(notKorToken)) {
                                                    newToken = ConvertMetrics.convertChinaNumberFormat(token);
                                                    if (!newToken.equals(token)) {
                                                        printLog("Type:Mixed, MetricsChinaFormat:[" + token + "]->[" + newToken + "]");
                                                        sb.append(newToken).append(" ");
                                                        continue;
                                                    }
                                                } else if (token.contains(",")) {
                                                    token = token.replace(",", "");
                                                    newToken = ConvertMetrics.convertChinaNumberFormat(token);
                                                    if (!newToken.equals(token)) {
                                                        printLog("Type:Mixed, MetricsChinaFormat:[" + token + "]->[" + newToken + "]");
                                                        sb.append(newToken).append(" ");
                                                        continue;
                                                    }
                                                }
                                            }

                                            if (token.endsWith("번,")) {
                                                token = token.replace(",", "");
                                                newToken = ConvertMetrics.convertChinaNumberFormat(token);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:Mixed, MetricsChinaFormat:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append(",").append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.endsWith("세이상") && PPUtils.isDigit(notKorToken)) {
                                                newToken = ConvertMixedTypeNumber.convertAgeNumberFormat(notKorToken);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:Mixed, AgeNumberFormat:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append("세이상").append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.endsWith("세이하") && PPUtils.isDigit(notKorToken)) {
                                                newToken = ConvertMixedTypeNumber.convertAgeNumberFormat(notKorToken);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:Mixed, AgeNumberFormat:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append("세이하").append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.endsWith("세") && PPUtils.isDigit(notKorToken)) {
                                                newToken = ConvertMixedTypeNumber.convertAgeNumberFormat(notKorToken);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:Mixed, AgeNumberFormat:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append("세").append(" ");
                                                    continue;
                                                }
                                            }

                                            if (PPUtils.isDigit(notKorToken)) {
                                                if (token.endsWith("군단")) {
                                                    newToken = ConvertMixedTypeNumber.convertAgeNumberFormat(notKorToken);
                                                    if (!newToken.equals(token)) {
                                                        printLog("Type:Mixed, MilitaryNumberFormat:[" + token + "]->[" + newToken + "]");
                                                        sb.append(newToken).append("군단").append(" ");
                                                        continue;
                                                    }
                                                }

                                                if (token.endsWith("사단")) {
                                                    newToken = ConvertMixedTypeNumber.convertAgeNumberFormat(notKorToken);
                                                    if (!newToken.equals(token)) {
                                                        printLog("Type:Mixed, MilitaryNumberFormat:[" + token + "]->[" + newToken + "]");
                                                        sb.append(newToken).append("사단").append(" ");
                                                        continue;
                                                    }
                                                }

                                                if (token.endsWith("연대")) {
                                                    newToken = ConvertMixedTypeNumber.convertAgeNumberFormat(notKorToken);
                                                    if (!newToken.equals(token)) {
                                                        printLog("Type:Mixed, MilitaryNumberFormat:[" + token + "]->[" + newToken + "]");
                                                        sb.append(newToken).append("연대").append(" ");
                                                        continue;
                                                    }
                                                }

                                                if (token.endsWith("대대")) {
                                                    newToken = ConvertMixedTypeNumber.convertAgeNumberFormat(notKorToken);
                                                    if (!newToken.equals(token)) {
                                                        printLog("Type:Mixed, MilitaryNumberFormat:[" + token + "]->[" + newToken + "]");
                                                        sb.append(newToken).append("대대").append(" ");
                                                        continue;
                                                    }
                                                }

                                                if (token.endsWith("중대")) {
                                                    newToken = ConvertMixedTypeNumber.convertAgeNumberFormat(notKorToken);
                                                    if (!newToken.equals(token)) {
                                                        printLog("Type:Mixed, MilitaryNumberFormat:[" + token + "]->[" + newToken + "]");
                                                        sb.append(newToken).append("중대").append(" ");
                                                        continue;
                                                    }
                                                }

                                                if (token.endsWith("소대")) {
                                                    newToken = ConvertMixedTypeNumber.convertAgeNumberFormat(notKorToken);
                                                    if (!newToken.equals(token)) {
                                                        printLog("Type:Mixed, MilitaryNumberFormat:[" + token + "]->[" + newToken + "]");
                                                        sb.append(newToken).append("소대").append(" ");
                                                        continue;
                                                    }
                                                }
                                            }

                                            if (token.contains("월") && token.contains("일") && !token.contains(".")) {
                                                checkToken = token;

                                                for(k = 0; k < notKorTokenSize; ++k) {
                                                    data = (String)((List)tokenList).get(k);
                                                    if (PPUtils.hasDigit(data)) {
                                                        if (k == 0) {
                                                            newToken = NumberUtils.convertMonth2Hangle(data);
                                                            if (!newToken.equals(data)) {
                                                                checkToken = checkToken.replaceFirst(data, newToken);
                                                            }
                                                        }

                                                        if (k == 1) {
                                                            newToken = NumberUtils.convertDay2Hangle(data);
                                                            if (!newToken.equals(data)) {
                                                                checkToken = checkToken.replace(data, newToken);
                                                            }
                                                        }
                                                    }
                                                }

                                                if (!checkToken.equals(token)) {
                                                    printLog("Type:Mixed, DateFormatEx:[" + token + "]->[" + checkToken + "]");
                                                    sb.append(checkToken).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.endsWith("시,")) {
                                                token = token.replace(",", "");
                                                newToken = ConvertDate.convertDateFormat(token, notKorToken);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:Mixed, DateFormatHourComma:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append(",").append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.endsWith("시에")) {
                                                token = token.replace("에", "");
                                                newToken = ConvertDate.convertDateFormat(token, notKorToken);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:Mixed, DateFormatHourEx:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append("에").append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.endsWith("시입니다.")) {
                                                token = token.replace("입니다.", "");
                                                newToken = ConvertDate.convertDateFormat(token, notKorToken);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:Mixed, DateFormatHourEx:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append("입니다.").append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.endsWith("개월로")) {
                                                token = token.replace("로", "");
                                                newToken = ConvertDate.convertDateFormat(token, notKorToken);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:Mixed, DateFormatHourEx:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append("로").append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.contains("~") && token.endsWith("까지")) {
                                                newToken = ConvertTidle.convertTidleFormat(notKorToken);
                                                if (!newToken.equals(notKorToken)) {
                                                    printLog("Type:Mixed, DigitTidleFormat:[" + notKorToken + "]->[" + newToken + "]");
                                                    sb.append(newToken).append("까지").append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.startsWith("a.m")) {
                                                sb.append("오전").append(" ");
                                                token = token.replace("a.m", "");
                                            }

                                            if (token.startsWith("a.m.")) {
                                                sb.append("오전").append(" ");
                                                token = token.replace("a.m.", "");
                                            }

                                            if (token.startsWith("p.m")) {
                                                sb.append("오후").append(" ");
                                                token = token.replace("p.m", "");
                                            }

                                            if (token.startsWith("p.m.")) {
                                                sb.append("오후").append(" ");
                                                token = token.replace("p.m.", "");
                                            }

                                            if (token.contains(".")) {
                                                newToken = ConvertDate.convertAbnormalDateFormat(notKorToken);
                                                checkToken = token.replaceAll(notKorToken, newToken);
                                                if (!checkToken.equals(token)) {
                                                    printLog("Type:Mixed, AbnormalDateFormat:[" + notKorToken + "]->[" + newToken + "]");
                                                    sb.append(checkToken).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (token.lastIndexOf("를") > -1 || token.lastIndexOf("을") > -1) {
                                                checkToken = token.replace("를", "").replace("을", "");
                                                if (PPUtils.isDigit(checkToken)) {
                                                    newToken = ConvertMixedTypeNumber.convertOrdinalNumberFormat(site, token);
                                                    if (!newToken.equals(token)) {
                                                        printLog("Type:Mixed, OrdinalNumberFormat:[" + token + "]->[" + newToken + "]");
                                                        sb.append(newToken).append(" ");
                                                        continue;
                                                    }
                                                }
                                            }

                                            if (token.contains("개회")) {
                                                newToken = ConvertMixedTypeNumber.convertOrdinalNumberFormatEx(site, token);
                                                if (!newToken.equals(token)) {
                                                    printLog("Type:Mixed, OrdinalNumberFormatEx:[" + token + "]->[" + newToken + "]");
                                                    sb.append(newToken).append(" ");
                                                    continue;
                                                }
                                            }

                                            newToken = ConvertDate.convertDateFormat(token, notKorToken);
                                            if (!newToken.equals(token)) {
                                                printLog("Type:Mixed, DateFormat:[" + token + "]->[" + newToken + "]");
                                                sb.append(newToken).append(" ");
                                                continue;
                                            }

                                            newToken = ConvertMoney.convertCurrencyFormat(token);
                                            if (!newToken.equals(token)) {
                                                printLog("Type:Mixed, CurrencyFormat:[" + token + "]->[" + newToken + "]");
                                                sb.append(newToken).append(" ");
                                                continue;
                                            }

                                            newToken = ConvertPhoneNumber.convertPhoneNumberFormat(token);
                                            if (!newToken.equals(token)) {
                                                printLog("Type:Mixed, PhoneNumberFormat:[" + token + "]->[" + newToken + "]");
                                                if (!token.endsWith("로") && !token.endsWith("으로")) {
                                                    sb.append(newToken).append(" ");
                                                    continue;
                                                }

                                                if (!newToken.endsWith("일") && !newToken.endsWith("삼") && !newToken.endsWith("육") && !newToken.endsWith("륙") && !newToken.endsWith("칠") && !newToken.endsWith("팔")) {
                                                    sb.append(newToken).append("로").append(" ");
                                                    continue;
                                                }

                                                sb.append(newToken).append("으로").append(" ");
                                                continue;
                                            }

                                            newToken = ConvertRegidentNumber.convertRegidentNumberFormat(token);
                                            if (!newToken.equals(token)) {
                                                printLog("Type:Mixed, RegidentNumberFormat:[" + token + "]->[" + newToken + "]");
                                                sb.append(newToken).append(" ");
                                                continue;
                                            }

                                            newToken = ConvertMetrics.convertMetricsFormat(token);
                                            if (!newToken.equals(token)) {
                                                printLog("Type:Mixed, MetricsFormat:[" + token + "]->[" + newToken + "]");
                                                sb.append(newToken).append(" ");
                                                continue;
                                            }

                                            newToken = ConvertOrdinalNumber.convertOrdinalNumberFormat(token);
                                            if (!newToken.equals(token)) {
                                                printLog("Type:Mixed, OrdinalNumberFormat:[" + token + "]->[" + newToken + "]");
                                                sb.append(newToken).append(" ");
                                                continue;
                                            }

                                            newToken = ConvertChinaNumber.convertChinaNumberFormat(token);
                                            if (!newToken.equals(token)) {
                                                printLog("Type:Mixed, ChinaNumberFormat:[" + token + "]->[" + newToken + "]");
                                                sb.append(newToken).append(" ");
                                                continue;
                                            }
                                        }

                                        checkToken = token;

                                        for(k = 0; k < notKorTokenSize; ++k) {
                                            data = (String)((List)tokenList).get(k);
                                            if (PPUtils.isAlphabetic(data)) {
                                                newToken = ConvertEnglish.convertEnglish(data);
                                                printLog("Type:Mixed, EnglishFormat:[" + data + "]->[" + newToken + "]");
                                                checkToken = checkToken.replaceAll(data, newToken);
                                            }
                                        }

                                        if (!checkToken.equals(token)) {
                                            sb.append(checkToken).append(" ");
                                        } else if (PPUtils.isAlphabetic(notKorToken)) {
                                            newToken = ConvertEnglish.convertEnglish(notKorToken);
                                            printLog("Type:Mixed, EnglishFormat:[" + notKorToken + "]->[" + newToken + "]");
                                            token = token.replaceAll(notKorToken, newToken);
                                            sb.append(token).append(" ");
                                        } else {
                                            if (notKorToken.endsWith(",")) {
                                                if (notKorToken.contains("?")) {
                                                    engTmp = token.replaceAll("[?]", " ");
                                                    if (!token.equals(engTmp)) {
                                                        printLog("Type:Mixed, QuestionAndCommaEndFormat:[" + token + "]->[" + engTmp + "]");
                                                        sb.append(engTmp).append(" ");
                                                        continue;
                                                    }
                                                } else if (notKorToken.contains("·")) {
                                                    engTmp = token.replaceAll("·", " ");
                                                    if (!token.equals(engTmp)) {
                                                        printLog("Type:Mixed, MiddleDotAndCommaEndFormat:[" + token + "]->[" + engTmp + "]");
                                                        sb.append(engTmp).append(" ");
                                                        continue;
                                                    }
                                                }
                                            }

                                            if (notKorToken.contains("?")) {
                                                engTmp = token.replaceAll("[?]", " ");
                                                if (!token.equals(engTmp)) {
                                                    printLog("Type:Mixed, QuestionFormat:[" + token + "]->[" + engTmp + "]");
                                                    sb.append(engTmp).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (notKorToken.contains("·")) {
                                                engTmp = token.replaceAll("·", " ");
                                                if (!token.equals(engTmp)) {
                                                    printLog("Type:Mixed, MiddleDotFormat:[" + token + "]->[" + engTmp + "]");
                                                    sb.append(engTmp).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (notKorToken.contains("/")) {
                                                engTmp = token.replaceAll("/", " ");
                                                if (!token.equals(engTmp)) {
                                                    printLog("Type:Mixed, HangleDivideFormat:[" + token + "]->[" + engTmp + "]");
                                                    sb.append(engTmp).append(" ");
                                                    continue;
                                                }
                                            }

                                            if (notKorToken.equals("#") && token.startsWith("#")) {
                                                token = token.replaceAll("#", "샵");
                                                sb.append(token).append(" ");
                                            } else if (notKorToken.equals("*") && token.startsWith("*")) {
                                                token = token.replaceAll("\\*", "별");
                                                sb.append(token).append(" ");
                                            } else if (token.contains("-")) {
                                                token = token.replaceAll("-", " ");
                                                sb.append(token).append(" ");
                                            } else {
                                                if (token.startsWith("연") && token.endsWith("%")) {
                                                    newToken = ConvertMetrics.convertMetricsFormat(notKorToken);
                                                    if (!newToken.equals(notKorToken)) {
                                                        printLog("Typr:Mixed, MetricsYearFormat:[" + notKorToken + "]->[" + newToken + "]");
                                                        sb.append("연").append(" ").append(newToken).append(" ");
                                                        continue;
                                                    }
                                                }

                                                try {
                                                    newToken = ConvertHanja.toHangle(token);
                                                    if (!newToken.equals(token)) {
                                                        printLog("Type:Hanja, HanjaFormat:[" + token + "]->[" + newToken + "]");
                                                        sb.append(newToken).append(" ");
                                                        continue;
                                                    }
                                                } catch (Exception var19) {
                                                }

                                                bAlpha = PPUtils.isAlphabetic(token);
                                                if (bAlpha) {
                                                    printLog("Hangle Type:PureAlphabet:Alphabet[" + token + "]");
                                                    newToken = ConvertEnglish.convertEnglish(token);
                                                    if (!newToken.equals(token)) {
                                                        printLog("ConvertEnglish:[" + token + "]->[" + newToken + "]");
                                                        sb.append(newToken).append(" ");
                                                        continue;
                                                    }
                                                }

                                                bDigit = PPUtils.isDigit(token);
                                                if (bDigit) {
                                                    printLog("Hangle Type:PureDigit:Digit[" + token + "]");
                                                    if (token.length() == 11) {
                                                        newToken = ConvertPhoneNumber.convertPhoneNumberFormatEx(token);
                                                        if (!newToken.equals(token)) {
                                                            printLog("ConvertPhoneNumber, PhoneNumberFormatEx:[" + token + "]->[" + newToken + "]");
                                                            sb.append(newToken).append(" ");
                                                            continue;
                                                        }
                                                    } else {
                                                        newToken = ConvertDate.convertDateFormat(token);
                                                        if (!newToken.equals(token)) {
                                                            printLog("ConvertDate:[" + token + "]->[" + newToken + "]");
                                                            sb.append(newToken).append(" ");
                                                            continue;
                                                        }
                                                    }
                                                }

                                                Pattern pattern = Pattern.compile("[a-zA-Z0-9/]+");
                                                Matcher matcher = pattern.matcher(token);
                                                if (matcher.matches()) {
                                                    printLog("Hangle Type:PureAlphabetDigit:AlphabetDigit[" + token + "]");
                                                    newToken = ConvertEnglishDigit.convertEnglishDigitFormat(token);
                                                    if (!newToken.equals(token)) {
                                                        sb.append(newToken).append(" ");
                                                        continue;
                                                    }
                                                }

                                                sb.append(token).append(" ");
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        ((List)tokenList).clear();
                        String ppText = sb.toString().trim();
                        if (PPUtils.hasDigit(ppText) || PPUtils.hasEnglish(ppText)) {
                            ppText = ConvertPostProcess.convertNumber2Hangle(ppText);
                        }

                        ppText = CleanText.removeSpecialCharacters(ppText);
                        cleanSentencelist.add(ppText.concat("\n"));
                        break;
                    }
                }
            }

            return cleanSentencelist;
        }
    }

    private static void printLog(String text) {
        if (bLog) {
            System.out.println(text);
        }

    }
}
