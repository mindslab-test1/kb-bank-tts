package maum.brain.tts.preprocess.kor;

import java.util.List;
import maum.brain.tts.preprocess.type.KorPatternType;
import maum.brain.tts.utils.PPUtils;

public class ConvertPhoneNumber {
    public ConvertPhoneNumber() {
    }

    public static String tagPhoneNumberFormat(int no, String token) {
        StringBuilder sb = new StringBuilder();
        sb.append(token.substring(0, 3));
        sb.append("-");
        sb.append(token.substring(3, 7));
        sb.append("-");
        sb.append(token.substring(7, 11));
        sb.append("-");
        if (no == 1) {
            token = tagPhoneNumber2Hangle(sb.toString());
        } else if (no == 2) {
            token = tagPhoneNumber2Hangle2(sb.toString());
        }

        return token;
    }

    public static String tagPhoneNumber2Hangle(String text) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < text.length(); ++i) {
            if ("0".equals(String.valueOf(text.charAt(i)))) {
                sb.append("공:");
            }

            if ("1".equals(String.valueOf(text.charAt(i)))) {
                sb.append("일:");
            }

            if ("2".equals(String.valueOf(text.charAt(i)))) {
                sb.append("이:");
            }

            if ("3".equals(String.valueOf(text.charAt(i)))) {
                sb.append("삼:");
            }

            if ("4".equals(String.valueOf(text.charAt(i)))) {
                sb.append("사:");
            }

            if ("5".equals(String.valueOf(text.charAt(i)))) {
                sb.append("오:");
            }

            if ("6".equals(String.valueOf(text.charAt(i)))) {
                sb.append("륙:");
            }

            if ("7".equals(String.valueOf(text.charAt(i)))) {
                sb.append("칠:");
            }

            if ("8".equals(String.valueOf(text.charAt(i)))) {
                sb.append("팔:");
            }

            if ("9".equals(String.valueOf(text.charAt(i)))) {
                sb.append("구:");
            }

            if ("-".equals(String.valueOf(text.charAt(i)))) {
                sb.append("");
            }
        }

        return sb.toString();
    }

    public static String tagPhoneNumber2Hangle2(String text) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < text.length(); ++i) {
            if ("0".equals(String.valueOf(text.charAt(i)))) {
                sb.append("공::");
            }

            if ("1".equals(String.valueOf(text.charAt(i)))) {
                sb.append("일::");
            }

            if ("2".equals(String.valueOf(text.charAt(i)))) {
                sb.append("이::");
            }

            if ("3".equals(String.valueOf(text.charAt(i)))) {
                sb.append("삼::");
            }

            if ("4".equals(String.valueOf(text.charAt(i)))) {
                sb.append("사::");
            }

            if ("5".equals(String.valueOf(text.charAt(i)))) {
                sb.append("오::");
            }

            if ("6".equals(String.valueOf(text.charAt(i)))) {
                sb.append("륙::");
            }

            if ("7".equals(String.valueOf(text.charAt(i)))) {
                sb.append("칠::");
            }

            if ("8".equals(String.valueOf(text.charAt(i)))) {
                sb.append("팔::");
            }

            if ("9".equals(String.valueOf(text.charAt(i)))) {
                sb.append("구::");
            }

            if ("-".equals(String.valueOf(text.charAt(i)))) {
                sb.append("");
            }
        }

        return sb.toString();
    }

    public static String convertPhoneNumberFormatEx(String token) {
        if (token.length() == 11) {
            StringBuilder sb = new StringBuilder();
            sb.append(token.substring(0, 3));
            sb.append("-");
            sb.append(token.substring(3, 7));
            sb.append("-");
            sb.append(token.substring(7, 11));
            sb.append("-");
            token = convertPhoneNumber2Hangle(sb.toString());
        }

        return token;
    }

    public static String convertPhoneNumberFormat(String token) {
        List aList;
        int i;
        if (token.length() > 12) {
            aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_PHONE_NUMBER, token);

            for(i = 0; i < aList.size(); ++i) {
                token = convertPhoneNumber2Hangle((String)aList.get(i));
            }
        }

        if (token.length() == 11 || token.length() == 12) {
            aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_LANDLINE_NUMBER, token);

            for(i = 0; i < aList.size(); ++i) {
                token = convertPhoneNumber2Hangle((String)aList.get(i));
            }
        }

        if (token.length() == 9) {
            aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_CSLINE_NUMBER, token);

            for(i = 0; i < aList.size(); ++i) {
                token = converCSLineNumber2Hangle((String)aList.get(i));
            }
        }

        return token;
    }

    private static String converCSLineNumber2Hangle(String token) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < token.length(); ++i) {
            if ("0".equals(String.valueOf(token.charAt(i)))) {
                sb.append("공");
            }

            if ("1".equals(String.valueOf(token.charAt(i)))) {
                sb.append("일");
            }

            if ("2".equals(String.valueOf(token.charAt(i)))) {
                sb.append("이");
            }

            if ("3".equals(String.valueOf(token.charAt(i)))) {
                sb.append("삼");
            }

            if ("4".equals(String.valueOf(token.charAt(i)))) {
                sb.append("사");
            }

            if ("5".equals(String.valueOf(token.charAt(i)))) {
                sb.append("오");
            }

            if ("6".equals(String.valueOf(token.charAt(i)))) {
                sb.append("륙");
            }

            if ("7".equals(String.valueOf(token.charAt(i)))) {
                sb.append("칠");
            }

            if ("8".equals(String.valueOf(token.charAt(i)))) {
                sb.append("팔");
            }

            if ("9".equals(String.valueOf(token.charAt(i)))) {
                sb.append("구");
            }

            if ("-".equals(String.valueOf(token.charAt(i)))) {
                sb.append("에").append(" ");
            }

            if ("*".equals(String.valueOf(token.charAt(i)))) {
                sb.append("에").append(" ");
            }
        }

        return sb.toString().trim();
    }

    public static String convertPhoneNumber2Hangle(String text) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < text.length(); ++i) {
            if ("0".equals(String.valueOf(text.charAt(i)))) {
                sb.append("공");
            }

            if ("1".equals(String.valueOf(text.charAt(i)))) {
                sb.append("일");
            }

            if ("2".equals(String.valueOf(text.charAt(i)))) {
                sb.append("이");
            }

            if ("3".equals(String.valueOf(text.charAt(i)))) {
                sb.append("삼");
            }

            if ("4".equals(String.valueOf(text.charAt(i)))) {
                sb.append("사");
            }

            if ("5".equals(String.valueOf(text.charAt(i)))) {
                sb.append("오");
            }

            if ("6".equals(String.valueOf(text.charAt(i)))) {
                sb.append("륙");
            }

            if ("7".equals(String.valueOf(text.charAt(i)))) {
                sb.append("칠");
            }

            if ("8".equals(String.valueOf(text.charAt(i)))) {
                sb.append("팔");
            }

            if ("9".equals(String.valueOf(text.charAt(i)))) {
                sb.append("구");
            }

            if ("-".equals(String.valueOf(text.charAt(i)))) {
                sb.append(" ");
            }
        }

        return sb.toString();
    }
}
