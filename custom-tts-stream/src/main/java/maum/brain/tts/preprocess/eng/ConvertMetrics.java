package maum.brain.tts.preprocess.eng;

import maum.brain.tts.utils.PPUtils;

public class ConvertMetrics {
    private static boolean bLog;

    static {
        bLog = Boolean.TRUE;
    }

    public ConvertMetrics() {
    }

    public static String convertHertz(String token) {
        if (!token.equalsIgnoreCase("㎐") && !token.equalsIgnoreCase("Hz")) {
            if (!token.equalsIgnoreCase("㎑") && !token.equalsIgnoreCase("KHz")) {
                if (!token.equalsIgnoreCase("㎒") && !token.equalsIgnoreCase("MHz")) {
                    return !token.equalsIgnoreCase("㎓") && !token.equalsIgnoreCase("GHz") ? token : "gigahertz";
                } else {
                    return "megahertz";
                }
            } else {
                return "kilohertz";
            }
        } else {
            return "hertz";
        }
    }

    public static String convertMeters(String token) {
        int symbolLength;
        String numeral;
        if (token.endsWith("㎝") || token.endsWith("cm")) {
            if ("㎝".equals(token) || "cm".equals(token)) {
                return "centimeters";
            }

            if (token.endsWith("㎝")) {
                symbolLength = "㎝".length();
                numeral = token.substring(0, token.length() - symbolLength);
                if (PPUtils.isDigit(numeral)) {
                    if (numeral.equals("1")) {
                        return ConvertToken.convertNumber(numeral) + " " + "centimeter";
                    }

                    return ConvertToken.convertNumber(numeral) + " " + "centimeters";
                }
            }

            if (token.endsWith("cm")) {
                symbolLength = "cm".length();
                numeral = token.substring(0, token.length() - symbolLength);
                if (PPUtils.isDigit(numeral)) {
                    if (numeral.equals("1")) {
                        return ConvertToken.convertNumber(numeral) + " " + "centimeter";
                    }

                    return ConvertToken.convertNumber(numeral) + " " + "centimeters";
                }
            }
        }

        if (token.endsWith("㎞") || token.endsWith("km")) {
            if ("㎞".equals(token) || "km".equals(token)) {
                return "kilometers";
            }

            if (token.endsWith("㎞")) {
                symbolLength = "㎞".length();
                numeral = token.substring(0, token.length() - symbolLength);
                if (PPUtils.isDigit(numeral)) {
                    if (numeral.equals("1")) {
                        return ConvertToken.convertNumber(numeral) + " " + "kilometer";
                    }

                    return ConvertToken.convertNumber(numeral) + " " + "kilometers";
                }
            }

            if (token.endsWith("km")) {
                symbolLength = "km".length();
                numeral = token.substring(0, token.length() - symbolLength);
                if (PPUtils.isDigit(numeral)) {
                    if (numeral.equals("1")) {
                        return ConvertToken.convertNumber(numeral) + " " + "kilometer";
                    }

                    return ConvertToken.convertNumber(numeral) + " " + "kilometers";
                }
            }
        }

        return token;
    }

    public static String convertMPH(String token) {
        return token.equalsIgnoreCase("mph") ? "miles per hour" : token;
    }

    public static String convertNMI(String token) {
        return token.equalsIgnoreCase("nmi") ? "nautical miles" : token;
    }

    public static String convertKmPerHour(String token) {
        return token.equalsIgnoreCase("km/h") ? "kilometers per hour" : token;
    }

    public static String convertDegrees(String token) {
        if (token.endsWith("°")) {
            String numeral = token.substring(0, token.length() - 1);
            if (PPUtils.isDigit(numeral)) {
                return ConvertToken.convertNumber(numeral) + " " + "degrees";
            }
        }

        return token;
    }

    public static String convertCents(String token) {
        if (token.endsWith("¢")) {
            String numeral = token.substring(0, token.length() - 1);
            if (PPUtils.isDigit(numeral)) {
                if (numeral.equals("1")) {
                    return ConvertToken.convertNumber(numeral) + " " + "cent";
                }

                return ConvertToken.convertNumber(numeral) + " " + "cents";
            }
        }

        return token;
    }

    private static void printLog(String text) {
        if (bLog) {
            System.out.println(text);
        }

    }
}
