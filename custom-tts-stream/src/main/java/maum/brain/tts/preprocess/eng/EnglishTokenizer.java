package maum.brain.tts.preprocess.eng;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.SimpleTokenizer;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.tokenize.WhitespaceTokenizer;

public class EnglishTokenizer {
    public static String sentenceFile = "src/main/resources/en-sent.bin";
    public static String tokenFile = "src/main/resources/en-token.bin";

    public EnglishTokenizer() {
    }

    public static void main(String[] args) {
        String text = "His barber kept his word. But keeping such a huge secret to himself was driving him crazy. Finally, the barber went up a mountain and almost to the edge of a cliff. He dug a hole in the midst of some reeds. He looked about, to make sure no one was near.";

        try {
            List<String> result = anlyseWithSentenceDetectorME(text);

            for(int i = 0; i < result.size(); ++i) {
                System.out.println((String)result.get(i));
            }
        } catch (IOException var4) {
            var4.printStackTrace();
        }

    }

    public static List<String> anlyseWithSimpleTokenizer(String text) throws IOException {
        SimpleTokenizer tokenizer = SimpleTokenizer.INSTANCE;
        return tokenizeText(tokenizer, text);
    }

    public static List<String> anlyseWithWhitespaceTokenizer(String text) throws IOException {
        WhitespaceTokenizer tokenizer = WhitespaceTokenizer.INSTANCE;
        return tokenizeText(tokenizer, text);
    }

    public static List<String> anlyseWithTokenizerME(String text) throws IOException {
        File modelIn = new File(tokenFile);
        TokenizerModel model = new TokenizerModel(modelIn);
        TokenizerME tokenizer = new TokenizerME(model);
        return tokenizeText(tokenizer, text);
    }

    public static List<String> anlyseWithSentenceDetectorME(String text) throws IOException {
        InputStream inputStream = new FileInputStream(sentenceFile);
        SentenceModel model = new SentenceModel(inputStream);
        SentenceDetectorME tokenizer = new SentenceDetectorME(model);
        if (inputStream != null) {
            inputStream.close();
        }

        String[] result = tokenizer.sentDetect(text);
        ArrayList<String> newResult = new ArrayList();
        String sentence = null;

        for(int i = 0; i < result.length; ++i) {
            sentence = result[i];
            String[] arrSplit = sentence.split(System.lineSeparator());
            if (arrSplit.length == 1) {
                newResult.add(sentence);
            } else {
                for(int j = 0; j < arrSplit.length; ++j) {
                    newResult.add(arrSplit[j]);
                }
            }
        }

        return newResult;
    }

    private static List<String> tokenizeText(Tokenizer tokenizer, String text) throws IOException {
        if (text != null && text.trim().length() != 0) {
            return Arrays.asList(tokenizeSentence(tokenizer, text));
        } else {
            throw new IOException("text is null.");
        }
    }

    private static String[] tokenizeSentence(Tokenizer tokenizer, String text) throws IOException {
        return tokenizer.tokenize(text);
    }

    public static String[] splitWithSimpleTokenizer(String text) throws IOException {
        SimpleTokenizer tokenizer = SimpleTokenizer.INSTANCE;
        return tokenizeSentence(tokenizer, text);
    }

    public static String[] splitWithWhitespaceTokenizer(String text) throws IOException {
        WhitespaceTokenizer tokenizer = WhitespaceTokenizer.INSTANCE;
        return tokenizeSentence(tokenizer, text);
    }

    public static String[] splitWithTokenizerME(String text) throws IOException {
        File modelIn = new File(tokenFile);
        TokenizerModel model = new TokenizerModel(modelIn);
        TokenizerME tokenizer = new TokenizerME(model);
        return tokenizeSentence(tokenizer, text);
    }

    public static String[] splitWithSentenceDetectorME(String text) throws IOException {
        InputStream inputStream = new FileInputStream(sentenceFile);
        SentenceModel model = new SentenceModel(inputStream);
        SentenceDetectorME splitter = new SentenceDetectorME(model);
        if (inputStream != null) {
            inputStream.close();
        }

        return splitter.sentDetect(text);
    }
}

