package maum.brain.tts.preprocess.eng;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Hashtable;

public class ConvertAbbreviation {
    private static boolean bLog;
    public static Hashtable<String, String> dict;
    private static String abbreviationFile;

    public ConvertAbbreviation() {
    }

    public static void main(String[] args) {
        String token = "157";
        String value = getAbbreviation(token);
        System.out.println(value);
    }

    public static String getAbbreviation(String token) {
        String value = new String();

        try {
            if (dict.isEmpty()) {
                allocateDictionary(abbreviationFile.toLowerCase());
            }

            value = (String)dict.get(token);
            if (value != null && !value.isEmpty()) {
                printLog("ConvertAbbreviation:" + token + "->" + value);
                return value;
            } else {
                return token;
            }
        } catch (Exception var3) {
            printLog(var3.getMessage());
            return value;
        }
    }

    public static void allocateDictionary(String txtFile) throws Exception {
        abbreviationFile = txtFile;
        FileReader fr = new FileReader(txtFile);
        BufferedReader br = new BufferedReader(fr);
        new String();
        String[] st = null;

        String line;
        while((line = br.readLine()) != null) {
            st = line.split("=");
            if (!st[0].trim().startsWith("#")) {
                dict.put(st[0].toLowerCase(), st[1]);
            }
        }

        br.close();
        fr.close();
    }

    private static void printLog(String text) {
        if (bLog) {
            System.out.println(text);
        }

    }

    static {
        bLog = Boolean.FALSE;
        dict = new Hashtable();
        abbreviationFile = "src/main/resources/tts.eng.dict.abbreviation.properties";
    }
}
