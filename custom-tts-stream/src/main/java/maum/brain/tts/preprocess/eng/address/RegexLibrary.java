package maum.brain.tts.preprocess.eng.address;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

class RegexLibrary {
    public static String TXT_NUM_0_9 = "zero|one|two|three|four|five|six|seven|eight|nine";
    public static String TXT_NUM_10_19 = "ten|eleven|twelve|thirteen|fourteen|fifteen|sixteen|seventeen|eighteen|nineteen";
    private static final String ORDINAL_0_9 = "0[-]?th|1[-]?st|2[-]?nd|3[-]?rd|[0[4-9]][-]?th|1[0-9][-]?th";
    private static final String TXT_ORDINAL_1_9 = "first|second|third|fourth|forth|fifth|sixth|seventh|eighth|ninth|nineth";
    private static final String TXT_ORDINAL_10_19 = "tenth|eleventh|twelfth|twelveth|twelvth|thirteenth|fourteenth|fifteenth|sixteenth|seventeenth|enghteenth|nineteenth";
    public static String TXT_NUM_0_19;
    public static final String ORDINAL_ALL = "(?:[0-9]*(?:0[-]?th|1[-]?st|2[-]?nd|3[-]?rd|[0[4-9]][-]?th|1[0-9][-]?th))";
    public static final String TXT_ORDINAL_0_19 = "zeroth|(?:first|second|third|fourth|forth|fifth|sixth|seventh|eighth|ninth|nineth)|(?:tenth|eleventh|twelfth|twelveth|twelvth|thirteenth|fourteenth|fifteenth|sixteenth|seventeenth|enghteenth|nineteenth)";
    public static final String STREET_DESIGNATOR;
    public static final String US_STATES;
    public static final String DIRECTIONS;
    public static final String ADDR_UNIT;

    RegexLibrary() {
    }

    static {
        TXT_NUM_0_19 = "(?:" + TXT_NUM_0_9 + ")|(?:" + TXT_NUM_10_19 + ")";
        STREET_DESIGNATOR = RegexLibrary.UsAddressesData.getStreetDesignatorRegex();
        US_STATES = RegexLibrary.UsAddressesData.getStateRegex();
        DIRECTIONS = RegexLibrary.UsAddressesData.getDirectionRegex();
        ADDR_UNIT = RegexLibrary.UsAddressesData.getUnitDesignatorRegex();
    }

    private static class UsAddressesData {
        private UsAddressesData() {
        }

        public static String getDirectionRegex() {
            String abbrv = "N[ ]?E|S[ ]?E|S[ ]?W|N[ ]?W|N|S|E|W";
            return join("|", Data.getDIRECTIONAL_MAP().keySet()) + "|" + abbrv;
        }

        public static String getStateRegex() {
            return join("|", Data.getSTATE_CODE_MAP().values(), Data.getSTATE_CODE_MAP().keySet());
        }

        public static String getStreetDesignatorRegex() {
            return join("|", Data.getSTREET_TYPE_MAP().values(), Data.getSTREET_TYPE_MAP().keySet());
        }

        public static String getUnitDesignatorRegex() {
            return join("|", Data.getUNIT_MAP().values(), Data.getUNIT_MAP().keySet());
        }

        private static String join(String separator, Collection<String>... collections) {
            Set<String> union = new HashSet();
            Collection[] var3 = collections;
            int var4 = collections.length;

            for(int var5 = 0; var5 < var4; ++var5) {
                Collection<String> c = var3[var5];
                union.addAll(c);
            }

            String[] set = new String[union.size()];
            List<String> lst = Arrays.asList(union.toArray(set));
            Collections.sort(lst, new Comparator<String>() {
                public int compare(String o1, String o2) {
                    return Integer.valueOf(o2.length()).compareTo(o1.length());
                }
            });
            return StringUtils.join(lst, separator);
        }
    }
}
