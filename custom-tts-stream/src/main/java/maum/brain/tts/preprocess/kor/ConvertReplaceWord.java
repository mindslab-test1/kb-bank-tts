package maum.brain.tts.preprocess.kor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConvertReplaceWord {
    public static Hashtable<String, String> dict = new Hashtable();
    private static String replaceFile = "src/main/resources/tts.kor.dict.replace.properties";

    public ConvertReplaceWord() {
    }

    public static String replaceWords(String token) {
        String value = new String();

        try {
            if (dict.isEmpty()) {
                readTextFile(replaceFile);
            }

            value = (String)dict.get(token);
            if (value == null || value.isEmpty()) {
                return token;
            }
        } catch (Exception var3) {
        }

        return value;
    }

    public static void readTextFile(String txtFile) throws Exception {
        replaceFile = txtFile;
        FileReader filereader = new FileReader(txtFile);
        BufferedReader br = new BufferedReader(filereader);
        new String();
        String[] st = null;

        String line;
        while((line = br.readLine()) != null) {
            st = line.split("=");
            dict.put(st[0].toLowerCase(), st[1]);
        }

        br.close();
    }

    public static List<String> extractHangle(String text) {
        List<String> list = new ArrayList();
        Pattern p = Pattern.compile("([a-zA-Z]+)(\\s[ㄱ-ㅎ가-힣]+)");
        Matcher m = p.matcher(text);
        String[] var10000 = new String[]{"", ""};

        while(m.find()) {
            String[] s = m.group(0).replaceAll("\\s+", "=").split("=");
            list.add(m.group(0).replaceAll("\\s+", "="));
            dict.put(s[0].toLowerCase(), s[1].trim());
        }

        return list;
    }
}
