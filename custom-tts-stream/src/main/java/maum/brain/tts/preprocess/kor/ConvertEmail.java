package maum.brain.tts.preprocess.kor;

import java.util.List;
import maum.brain.tts.preprocess.type.KorPatternType;
import maum.brain.tts.utils.PPUtils;

public class ConvertEmail {
    public ConvertEmail() {
    }

    public static String convertEmailFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_EMAIL, token);
        new String();

        for(int i = 0; i < aList.size(); ++i) {
            String tmp;
            if (token.contains("mindslab.ai")) {
                tmp = ((String)aList.get(i)).substring(0, ((String)aList.get(i)).indexOf("@"));
                tmp = convertEmail2Hangle(tmp);
                return tmp.concat(" 마인즈랩 쩜 에이 아이");
            }

            if (token.contains("gmail.com")) {
                tmp = ((String)aList.get(i)).substring(0, ((String)aList.get(i)).indexOf("@"));
                tmp = convertEmail2Hangle(tmp);
                return tmp.concat(" 지메일 쩜 컴");
            }

            if (token.contains("naver.com")) {
                tmp = ((String)aList.get(i)).substring(0, ((String)aList.get(i)).indexOf("@"));
                tmp = convertEmail2Hangle(tmp);
                return tmp.concat(" 네이버 쩜 컴");
            }

            token = convertEmail2Hangle((String)aList.get(i));
        }

        return token;
    }

    private static String convertEmail2Hangle(String token) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < token.length(); ++i) {
            if ("a".equals(String.valueOf(token.charAt(i)))) {
                sb.append("에이").append(" ");
            }

            if ("b".equals(String.valueOf(token.charAt(i)))) {
                sb.append("비").append(" ");
            }

            if ("c".equals(String.valueOf(token.charAt(i)))) {
                sb.append("씨").append(" ");
            }

            if ("d".equals(String.valueOf(token.charAt(i)))) {
                sb.append("디").append(" ");
            }

            if ("e".equals(String.valueOf(token.charAt(i)))) {
                sb.append("이").append(" ");
            }

            if ("f".equals(String.valueOf(token.charAt(i)))) {
                sb.append("에프").append(" ");
            }

            if ("g".equals(String.valueOf(token.charAt(i)))) {
                sb.append("쥐").append(" ");
            }

            if ("h".equals(String.valueOf(token.charAt(i)))) {
                sb.append("에이치").append(" ");
            }

            if ("i".equals(String.valueOf(token.charAt(i)))) {
                sb.append("아이").append(" ");
            }

            if ("j".equals(String.valueOf(token.charAt(i)))) {
                sb.append("제이").append(" ");
            }

            if ("k".equals(String.valueOf(token.charAt(i)))) {
                sb.append("케이").append(" ");
            }

            if ("l".equals(String.valueOf(token.charAt(i)))) {
                sb.append("엘").append(" ");
            }

            if ("m".equals(String.valueOf(token.charAt(i)))) {
                sb.append("엠").append(" ");
            }

            if ("n".equals(String.valueOf(token.charAt(i)))) {
                sb.append("엔").append(" ");
            }

            if ("o".equals(String.valueOf(token.charAt(i)))) {
                sb.append("오").append(" ");
            }

            if ("p".equals(String.valueOf(token.charAt(i)))) {
                sb.append("피").append(" ");
            }

            if ("q".equals(String.valueOf(token.charAt(i)))) {
                sb.append("큐").append(" ");
            }

            if ("r".equals(String.valueOf(token.charAt(i)))) {
                sb.append("알").append(" ");
            }

            if ("s".equals(String.valueOf(token.charAt(i)))) {
                sb.append("에스").append(" ");
            }

            if ("t".equals(String.valueOf(token.charAt(i)))) {
                sb.append("티").append(" ");
            }

            if ("u".equals(String.valueOf(token.charAt(i)))) {
                sb.append("유").append(" ");
            }

            if ("v".equals(String.valueOf(token.charAt(i)))) {
                sb.append("브이").append(" ");
            }

            if ("w".equals(String.valueOf(token.charAt(i)))) {
                sb.append("더블유").append(" ");
            }

            if ("x".equals(String.valueOf(token.charAt(i)))) {
                sb.append("엑스").append(" ");
            }

            if ("y".equals(String.valueOf(token.charAt(i)))) {
                sb.append("와이").append(" ");
            }

            if ("z".equals(String.valueOf(token.charAt(i)))) {
                sb.append("지").append(" ");
            }

            if ("@".equals(String.valueOf(token.charAt(i)))) {
                sb.append("골뱅이").append(" ");
            }

            if ("-".equals(String.valueOf(token.charAt(i)))) {
                sb.append("다시").append(" ");
            }

            if (".".equals(String.valueOf(token.charAt(i)))) {
                sb.append("쩜").append(" ");
            }

            if ("_".equals(String.valueOf(token.charAt(i)))) {
                sb.append("언더스코어").append(" ");
            }
        }

        return sb.toString().trim();
    }
}
