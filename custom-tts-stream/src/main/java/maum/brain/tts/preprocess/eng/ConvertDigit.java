package maum.brain.tts.preprocess.eng;

import maum.brain.tts.utils.PPUtils;

public class ConvertDigit {
    public ConvertDigit() {
    }

    public static String convertDigit(String digit) {
        return PPUtils.isDigit(digit) ? replaceNumber(digit) : digit;
    }

    public static String convertAlpha(String alpha) {
        return PPUtils.isEnglish(alpha) ? replaceNumber(alpha) : alpha;
    }

    private static String replaceNumber(String number) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; number != null && i < number.length(); ++i) {
            if ("0".equals(String.valueOf(number.charAt(i)))) {
                sb.append("zero").append(" ");
            } else if ("1".equals(String.valueOf(number.charAt(i)))) {
                sb.append("one").append(" ");
            } else if ("2".equals(String.valueOf(number.charAt(i)))) {
                sb.append("two").append(" ");
            } else if ("3".equals(String.valueOf(number.charAt(i)))) {
                sb.append("three").append(" ");
            } else if ("4".equals(String.valueOf(number.charAt(i)))) {
                sb.append("four").append(" ");
            } else if ("5".equals(String.valueOf(number.charAt(i)))) {
                sb.append("five").append(" ");
            } else if ("6".equals(String.valueOf(number.charAt(i)))) {
                sb.append("six").append(" ");
            } else if ("7".equals(String.valueOf(number.charAt(i)))) {
                sb.append("seven").append(" ");
            } else if ("8".equals(String.valueOf(number.charAt(i)))) {
                sb.append("eight").append(" ");
            } else if ("9".equals(String.valueOf(number.charAt(i)))) {
                sb.append("nine").append(" ");
            } else {
                sb.append(String.valueOf(number.charAt(i))).append(" ");
            }
        }

        return sb.toString();
    }
}
