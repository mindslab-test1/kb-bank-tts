package maum.brain.tts.preprocess.eng.address;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

class SpecialData {
    public static final Map<String, List<String>> dict = new HashMap();
    private static String cityExceptionFile = "src/main/resources/tts.eng.dict.exception.city.properties";

    SpecialData() {
    }

    static {
        BufferedReader br = null;

        try {
            br = new BufferedReader(new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream(cityExceptionFile)));
            String line = null;
            HashMap tmp = new HashMap();

            while((line = br.readLine()) != null) {
                String[] items = line.split("\\s*->\\s*");
                String[] cities = items[1].split("[|]");
                String state = items[0];
                Set<String> set = (Set)tmp.get(state);
                if (set == null) {
                    set = new HashSet();
                    tmp.put(state, set);
                }

                String[] var7 = cities;
                int var8 = cities.length;

                for(int var9 = 0; var9 < var8; ++var9) {
                    String city = var7[var9];
                    ((Set)set).add(city);
                }
            }

            Iterator var20 = tmp.entrySet().iterator();

            while(var20.hasNext()) {
                Entry<String, Set<String>> e = (Entry)var20.next();
                String[] array = (String[])((Set)e.getValue()).toArray(new String[0]);
                Arrays.sort(array, new Comparator<String>() {
                    public int compare(String o1, String o2) {
                        return Integer.valueOf(o2.length()).compareTo(o1.length());
                    }
                });
                dict.put(e.getKey(), Arrays.asList(array));
            }
        } catch (Exception var18) {
            throw new Error("Unable to initalize exception_city", var18);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException var17) {
                }
            }

        }

    }
}
