package maum.brain.tts.preprocess.eng;

public class ConvertPuncts {
    private static final String[] puncts = new String[]{"@", "#", "\\^", "&", "[*]", "_", "=", "[+]", "\\{", "\\}", "\\(", "\\)", "\\[", "\\]", "[|]", "\\\\", ";", "\"", "'", "~", "`", "<", ">", "!", "[$]", "%", "/", "-"};

    public ConvertPuncts() {
    }

    public static String removeSpecialCharacters(String text) {
        String[] var1 = puncts;
        int var2 = var1.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            String p = var1[var3];
            text = text.replaceAll(p, "");
        }

        return text;
    }
}
