package maum.brain.tts.preprocess.kor;

import java.util.ArrayList;
import java.util.List;
import maum.brain.tts.preprocess.type.KorPatternType;
import maum.brain.tts.utils.NumberUtils;
import maum.brain.tts.utils.PPUtils;

public class ConvertChinaNumber {
    public ConvertChinaNumber() {
    }

    public static String convertChinaNumberFormat(String token) {
        String newToken = new String();
        new ArrayList();
        List aList;
        String extractDigit;
        if (token.contains(",")) {
            newToken = token.replace(",", "");
            aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_CHINA_UNIT, newToken);
            if (aList == null || aList.isEmpty()) {
                return token;
            }

            extractDigit = PPUtils.getDigit((String)aList.get(0));
            String unit = PPUtils.getKorean((String)aList.get(0));
            token = token.replace(token, convertDecimal2Hangle(extractDigit) + unit);
        } else {
            aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_CHINA_UNIT, token);
            if (aList == null || aList.isEmpty()) {
                return token;
            }

            extractDigit = PPUtils.getDigit(newToken);
            token = token.replace(extractDigit, convertDecimal2Hangle(extractDigit));
        }

        return token;
    }

    private static String convertDecimal2Hangle(String token) {
        StringBuilder sb = new StringBuilder();
        sb.append(NumberUtils.convertDecimal2Hangle(token, false));
        return sb.toString();
    }
}
