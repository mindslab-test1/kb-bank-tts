package maum.brain.tts.preprocess.type;

import java.util.regex.Pattern;

public class EngPatternType {
    public static final Pattern P_ROMAN = Pattern.compile("m{0,3}(c[md]|d?c{0,3})(x[cl]|l?x{0,3})(i[xv]|v?i{0,3})");
    public static final Pattern P_ROMAN_ORDINAL = Pattern.compile("\\b([C|I|L|M|V|X])+([st|nd|rd|th])+\\b");
    public static final Pattern P_ROMAN_MONARCHS = Pattern.compile("(([A-Z])+([a-z]+))+[\\s*]+[CILMVX]+");
    public static final Pattern P_FEET_INCH = Pattern.compile("(\\d+)('|\")(?:([0-12]+)(?!2)\")?");
    public static final Pattern P_SPOON = Pattern.compile("(\\d+)[\\s*]+(tsp|tbsp)+");
    public static final Pattern P_MONEY = Pattern.compile("([USD|GBP|EUR|$|£|€])(-?([\\d,]+\\.?\\d+|\\.\\d+)+)", 2);
    public static final Pattern P_NUMBERS = Pattern.compile("(\\d+)(?:'[sS]+)");
    public static final Pattern P_YRAE_NUMBERS = Pattern.compile("([bc|ad|b\\.c\\.|b\\.c|a\\.d\\.|a\\.d]+)[\\s*]+(\\d+)(?:'[sS]+)");
    public static final Pattern P_REALNUM = Pattern.compile("([-+])?([\\d,]+)?(\\.(\\d+)([%Pp])?)?");
    public static final Pattern P_ORDINAL = Pattern.compile("[\\d,]+(st|nd|rd|th)", 2);
    public static final Pattern P_YEAR = Pattern.compile("(\\d+)(bc|ad|b\\.c\\.|b\\.c|a\\.d\\.|a\\.d)", 2);
    public static final Pattern P_YEAR_EX = Pattern.compile("(\\d+)[\\s*]+(bc|ad|b\\.c\\.|b\\.c|a\\.d\\.|a\\.d)");
    public static final Pattern P_RANGE = Pattern.compile("([0-9]+)-([0-9]+)");
    public static final Pattern P_RANGE_EX = Pattern.compile("([0-9]+)–([0-9]+)");
    public static final Pattern P_MONTH_RANGE = Pattern.compile("(january|february|march|april|may|june|july|august|september|october|november|december)[\\s*]+([0-9]+)-([0-9]+)[\\s]+", 2);
    public static final Pattern P_TIME = Pattern.compile("((0?[0-9])|(1[0-1])|(1[2-9])|(2[0-3])):([0-5][0-9])(a\\.m\\.|am|pm|p\\.m\\.|a\\.m|p\\.m)?", 2);
    public static final Pattern P_TIME_EX = Pattern.compile("[\\s*]((0?[0-9])|(1[0-1])|(1[2-9])|(2[0-3]))[\\s*]?(A\\.M\\.|AM|PM|P\\.M\\.|A\\.M|P\\.M)+");
    public static final Pattern P_US_DATE = Pattern.compile("[']?(\\d{1,2})[/.-](\\d{1,2})[/.-]\\d{4}[']?");
    public static final Pattern P_US_DATE_EX = Pattern.compile("[']?(\\d{1,2})[/.-](\\d{1,2})[/.-]\\d{2}[']?");
    public static final Pattern P_US_MONTH_DATE_EX = Pattern.compile("[']?(january|february|march|april|may|june|july|august|september|october|november|december)[\\s*](\\d{1,2})[,\\s]+\\d{4}[']+?");
    public static final Pattern P_US_MONTH_DATE_EX2 = Pattern.compile("[']?(\\d{1,2}([th|rd|nd|st])+)[\\s*](january|february|march|april|may|june|july|august|september|october|november|december)[,\\s*]+\\d{4}[']?");
    public static final Pattern P_US_MONTH_DATE_EX3 = Pattern.compile("(january|february|march|april|may|june|july|august|september|october|november|december)[\\s*]+(3[01]|[12][0-9]|0?[1-9])+[,\\s]+", 2);
    public static final Pattern P_US_MONTH_DATE = Pattern.compile("[']?(january|february|march|april|may|june|july|august|september|october|november|december)[/.-](\\d{1,2})[/.-]\\d{4}[']?");
    public static final Pattern P_US_MONTH_ABBR_DATE = Pattern.compile("[']?(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)[/.-](\\d{1,2})[/.-]\\d{4}[']?");
    public static final Pattern P_US_MONTH_ABBR_DATE_EX = Pattern.compile("(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)[.]?[\\s*]+(\\d{1,2})[,]?[\\s*]+\\d{4}");
    public static final Pattern P_UK_MONTH_ABBR_DATE = Pattern.compile("[']?(\\d{1,2})[/.-](jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)[/.-]\\d{4}[']?");
    public static final Pattern P_ISO_DATE = Pattern.compile("[']?(\\d{4})[/.-](\\d{1,2})[/.-]\\d{1,2}[']?");
    public static final Pattern P_ISO_MONTH_ABBR_DATE = Pattern.compile("[']?(\\d{4})[/.-](jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)[/.-]\\d{1,2}[']?");
    public static final Pattern P_ISO_MONTH_DATE = Pattern.compile("[']?(\\d{4})[/.-](january|february|march|april|may|june|july|august|september|october|november|december)[/.-]\\d{1,2}[']?");
    public static final Pattern P_DURATION = Pattern.compile("(\\d+):([0-5][0-9]):([0-5][0-9])(:([0-5][0-9]))?");
    public static final Pattern P_DURATION_EX = Pattern.compile("((\\d+)h)+((\\d+)m)+((\\d+)s)+");
    public static final Pattern P_NUMBERWORD = Pattern.compile("([a-zA-Z]+[0-9]+|[0-9]+[a-zA-Z]+)\\w*");
    public static final Pattern P_URL = Pattern.compile("(https?://)?((www\\.)?([-a-zA-Z0-9@:%._\\\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\\\+.~#?&/=]*)))");
    public static final Pattern P_HASHTAG = Pattern.compile("(#)(\\w+)");
    public static final Pattern P_CONSONANT = Pattern.compile("[b-df-hj-np-tv-z]+", 2);
    public static final Pattern P_PUNCTUATION = Pattern.compile("\\p{Punct}");

    public EngPatternType() {
    }
}
