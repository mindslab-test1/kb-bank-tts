package maum.brain.tts.preprocess.type;

public class EnvType {
    public static final String ENV_SITE = "site";
    public static final String RPC_NAME = "preprocess";
    public static final String TTS_JOB_TIMEOUT = "tts.job.timeout";
    public static final String TTS_MAXLENGTH = "tts.maxlength";
    public static final String GRPC_ADDR_G2P = "GRPC_ADDR_G2P";
    public static final String GRPC_ADDR_G2P_EN = "GRPC_ADDR_G2P_EN";
    public static final String KR_GRPC_CORE_IP = "tts.ko_KR.tts.ip";
    public static final String KR_GRPC_CORE_PORT = "tts.ko_KR.tts.port";
    public static final String EN_GRPC_CORE_IP = "tts.en_US.tts.ip";
    public static final String EN_GRPC_CORE_PORT = "tts.en_US.tts.port";

    public EnvType() {
    }
}
