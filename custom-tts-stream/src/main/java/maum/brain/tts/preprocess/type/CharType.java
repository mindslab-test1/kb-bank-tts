package maum.brain.tts.preprocess.type;

public class CharType {
    public static final char ZERO = '0';
    public static final char ONE = '1';
    public static final char TWO = '2';
    public static final char THREE = '3';
    public static final char FOUR = '4';
    public static final char FIVE = '5';
    public static final char SIX = '6';
    public static final char SEVEN = '7';
    public static final char EIGHT = '8';
    public static final char NINE = '9';
    public static final char KR_ONE = '일';
    public static final char MINUS = '-';
    public static final char SPACE = ' ';
    public static final char KR_COUNT = '건';
    public static final char KR_COUNT_EX = '껀';

    public CharType() {
    }
}
