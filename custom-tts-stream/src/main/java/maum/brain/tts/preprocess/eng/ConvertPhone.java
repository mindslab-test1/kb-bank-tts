package maum.brain.tts.preprocess.eng;

import maum.brain.tts.utils.PPUtils;

public class ConvertPhone {
    public ConvertPhone() {
    }

    public static String convertUSPhone(String phone) {
        if (phone != null && !phone.isEmpty()) {
            if (phone.contains("ex.")) {
                String[] arrPhone = phone.split("ex.");
                String[] number = arrPhone[0].split("-");
                if (number.length == 4) {
                    String strNumber = replaceNumber(PPUtils.getDigit(number[0])) + " " + ConvertToken.convertNumber(number[1]) + " " + replaceNumber(PPUtils.getDigit(number[2])) + " " + replaceNumber(PPUtils.getDigit(number[3]));
                    return strNumber + " " + "extension" + " " + replaceNumber(PPUtils.getDigit(arrPhone[1]));
                } else {
                    return replaceNumber(PPUtils.getDigit(arrPhone[0])) + " " + "extension" + " " + replaceNumber(PPUtils.getDigit(arrPhone[1]));
                }
            } else {
                phone = replaceNumber(PPUtils.getDigit(phone));
                return phone;
            }
        } else {
            return phone;
        }
    }

    private static String replaceNumber(String number) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; number != null && i < number.length(); ++i) {
            if ("0".equals(String.valueOf(number.charAt(i)))) {
                sb.append("zero").append(" ");
            } else if ("1".equals(String.valueOf(number.charAt(i)))) {
                sb.append("one").append(" ");
            } else if ("2".equals(String.valueOf(number.charAt(i)))) {
                sb.append("two").append(" ");
            } else if ("3".equals(String.valueOf(number.charAt(i)))) {
                sb.append("three").append(" ");
            } else if ("4".equals(String.valueOf(number.charAt(i)))) {
                sb.append("four").append(" ");
            } else if ("5".equals(String.valueOf(number.charAt(i)))) {
                sb.append("five").append(" ");
            } else if ("6".equals(String.valueOf(number.charAt(i)))) {
                sb.append("six").append(" ");
            } else if ("7".equals(String.valueOf(number.charAt(i)))) {
                sb.append("seven").append(" ");
            } else if ("8".equals(String.valueOf(number.charAt(i)))) {
                sb.append("eight").append(" ");
            } else if ("9".equals(String.valueOf(number.charAt(i)))) {
                sb.append("nine").append(" ");
            } else {
                sb.append(String.valueOf(number.charAt(i)));
            }
        }

        return sb.toString();
    }
}
