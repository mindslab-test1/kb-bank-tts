package maum.brain.tts.preprocess.kor;

import maum.brain.tts.utils.NumberUtils;

public class ConvertDecimal {
    public ConvertDecimal() {
    }

    public static String convertDecimalFormat(String token) {
        Boolean bComma = false;
        if (token.endsWith(",")) {
            bComma = true;
        }

        token = token.replaceAll(",", "");
        if (bComma) {
            token = convertDecimal2Hangle(token).concat(",");
        } else {
            token = convertDecimal2Hangle(token);
        }

        return token;
    }

    private static String convertDecimal2Hangle(String token) {
        StringBuilder sb = new StringBuilder();
        if (token.contains(".")) {
            String[] arrNumber = token.split("[.]");
            if (arrNumber.length == 1) {
                sb.append(NumberUtils.convertDecimal2Hangle(arrNumber[0].substring(0, arrNumber[0].length()), false));
            } else if (arrNumber.length == 2) {
                sb.append(NumberUtils.convertDecimal2Hangle(arrNumber[0], false));
                sb.append("쩜");
                sb.append(NumberUtils.convertDigit2Hangle(arrNumber[1].substring(0, arrNumber[1].length())));
            }
        } else {
            sb.append(NumberUtils.convertDecimal2Hangle(token, false));
        }

        return sb.toString();
    }
}
