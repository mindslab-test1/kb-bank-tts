package maum.brain.tts.preprocess.eng.address;

public enum AddressComponent {
    NAME,
    PREDIR,
    NUMBER,
    STREET,
    POSTDIR,
    TYPE,
    LINE2,
    CITY,
    STATE,
    ZIP,
    PREDIR2,
    STREET2,
    POSTDIR2,
    TYPE2,
    LAT,
    LON,
    COUNTY,
    TLID;

    private AddressComponent() {
    }
}
