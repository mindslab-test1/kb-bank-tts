package maum.brain.tts.preprocess.type;

public class KorMoneyType {
    public static final String WON = "원";
    public static final String WON_COMMA = "원,";
    public static final String WON_AT = "원에";
    public static final String WON_OBJECT = "원을";
    public static final String WON_OVER = "원이상";
    public static final String WON_10 = "십원";
    public static final String WON_10_COMMA = "십원,";
    public static final String WON_100 = "백원";
    public static final String WON_100_COMMA = "백원,";
    public static final String WON_1000 = "천원";
    public static final String WON_1000_COMMA = "천원,";
    public static final String MANWON = "만원";
    public static final String MANWON_10 = "십만원";
    public static final String MANWON_100 = "백만원";
    public static final String MANWON_1000 = "천만원";
    public static final String MANWON_COMMA = "만원,";
    public static final String MANWON_10_COMMA = "십만원,";
    public static final String MANWON_100_COMMA = "백만원,";
    public static final String MANWON_1000_COMMA = "천만원,";
    public static final String MANWON_AT = "만원에";
    public static final String MANWON_10_AT = "십만원에";
    public static final String MANWON_100_AT = "백만원에";
    public static final String MANWON_1000_AT = "천만원에";
    public static final String MANWON_OBJECT = "만원을";
    public static final String MANWON_10_OBJECT = "십만원을";
    public static final String MANWON_100_OBJECT = "백만원을";
    public static final String MANWON_1000_OBJECT = "천만원을";
    public static final String MANWON_OVER = "만원이상";
    public static final String MANWON_10_OVER = "십만원이상";
    public static final String MANWON_100_OVER = "백만원이상";
    public static final String MANWON_1000_OVER = "천만원이상";
    public static final String UKWON = "억원";
    public static final String UKWON_10 = "십억원";
    public static final String UKWON_100 = "백억원";
    public static final String UKWON_1000 = "천억원";
    public static final String UKWON_COMMA = "억원,";
    public static final String UKWON_10_COMMA = "십억원,";
    public static final String UKWON_100_COMMA = "백억원,";
    public static final String UKWON_1000_COMMA = "천억원,";
    public static final String UKWON_AT = "억원에";
    public static final String UKWON_10_AT = "십억원에";
    public static final String UKWON_100_AT = "백억원에";
    public static final String UKWON_1000_AT = "천억원에";
    public static final String UKWON_OBJECT = "억원을";
    public static final String UKWON_10_OBJECT = "십억원을";
    public static final String UKWON_100_OBJECT = "백억원을";
    public static final String UKWON_1000_OBJECT = "천억원을";
    public static final String UKWON_OVER = "억원이상";
    public static final String UKWON_10_OVER = "십억원이상";
    public static final String UKWON_100_OVER = "백억원이상";
    public static final String UKWON_1000_OVER = "천억원이상";

    public KorMoneyType() {
    }
}
