package maum.brain.tts.preprocess.eng;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Hashtable;

public class ConvertContraction {
    private static boolean bLog;
    public static Hashtable<String, String> dict;
    private static String contractionFile;

    public ConvertContraction() {
    }

    public static void main(String[] args) {
        String token = "that's";
        String value = getContraction(token);
        System.out.println(value);
    }

    public static String getContraction(String key) {
        if (key.contains("'")) {
            new String();

            try {
                if (dict.isEmpty()) {
                    allocateDictionary(contractionFile);
                }

                String value = (String)dict.get(key.toLowerCase());
                if (value != null && !value.isEmpty()) {
                    return value;
                }

                return key;
            } catch (Exception var3) {
                printLog(var3.getMessage());
            }
        }

        return key;
    }

    public static void allocateDictionary(String txtFile) throws Exception {
        contractionFile = txtFile;
        FileReader fr = new FileReader(txtFile);
        BufferedReader br = new BufferedReader(fr);
        new String();
        String[] st = null;

        String line;
        while((line = br.readLine()) != null) {
            st = line.split("=");
            if (!st[0].trim().startsWith("#")) {
                dict.put(st[0].toLowerCase(), st[1]);
            }
        }

        br.close();
        fr.close();
    }

    private static void printLog(String text) {
        if (bLog) {
            System.out.println(text);
        }

    }

    static {
        bLog = Boolean.FALSE;
        dict = new Hashtable();
        contractionFile = "src/main/resources/tts.eng.dict.contraction.properties";
    }
}
