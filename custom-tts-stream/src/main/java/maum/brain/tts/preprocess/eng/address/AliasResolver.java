package maum.brain.tts.preprocess.eng.address;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

class AliasResolver {
    private static final Map<String, Map<String, String>> dict = new HashMap();
    private static String cityAliasFile = "src/main/resources/tts.eng.dict.city.alias.properties";

    AliasResolver() {
    }

    public static String resolveCityAlias(String city, String state) {
        if (!StringUtils.isBlank(city) && !StringUtils.isBlank(state)) {
            Map<String, String> aliasMap = (Map)dict.get(state);
            if (aliasMap == null) {
                return city;
            } else {
                String realCity = (String)aliasMap.get(city.replaceAll("\\s+", ""));
                return (String)CommonUtils.nvl(realCity, city);
            }
        } else {
            return city;
        }
    }

    static {
        BufferedReader br = null;

        try {
            br = new BufferedReader(new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream(cityAliasFile)));
            String line = null;
            HashMap allRealCitiesMap = new HashMap();

            while((line = br.readLine()) != null) {
                String[] items = line.split("\\s*=\\s*");
                String[] cs = items[0].split("<b>")[1].split("\\s*,\\s*");
                String city = cs[0];
                String state = cs[1];
                String[] alias = items[1].split("[|]");
                Map<String, String> aliasMap = (Map)dict.get(state);
                if (aliasMap == null) {
                    aliasMap = new HashMap();
                    dict.put(state, aliasMap);
                }

                String[] var9 = alias;
                int var10 = alias.length;

                for(int var11 = 0; var11 < var10; ++var11) {
                    String a = var9[var11];
                    String aa = a.split("\\s*,\\s*")[0];
                    String realCity = city.intern();
                    Set<String> allRealCities = (Set)allRealCitiesMap.get(state);
                    if (allRealCities == null) {
                        allRealCities = new HashSet();
                        allRealCitiesMap.put(state, allRealCities);
                    }

                    ((Set)allRealCities).add(realCity);
                    if (!((Set)allRealCities).contains(aa)) {
                        ((Map)aliasMap).put(aa.replaceAll("\\s+", "").intern(), city.intern());
                    }
                }
            }

            allRealCitiesMap.clear();
            allRealCitiesMap = null;
        } catch (IOException var23) {
            throw new Error("Unable to initalize City Alias Resolver", var23);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException var22) {
                }
            }

        }

    }
}
