package maum.brain.tts.preprocess.type;

public class AudioType {
    public static final String FORMAT_WAV = ".wav";
    public static final String MODEL = "model";
    public static final String PREFIX_TAG1 = "1_";
    public static final String PREFIX_TAG2 = "2_";
    public static final String PREFIX_TAG3 = "3_";
    public static final String PREFIX_MODEL1 = "model1_";
    public static final String PREFIX_MODEL2 = "model2_";
    public static final String PREFIX_MODEL3 = "model3_";
    public static final String PREFIX_DIR1 = "1/";
    public static final String PREFIX_DIR2 = "2/";
    public static final String PREFIX_DIR3 = "3/";
    public static final String DEFAULT_PHONENUMBER = "01000000000";
    public static final String DEFAULT_DIGITNUMBER = "0000";
    public static final String MD = "SHA-256";
    public static final String CHARSET = "UTF-8";

    public AudioType() {
    }
}
