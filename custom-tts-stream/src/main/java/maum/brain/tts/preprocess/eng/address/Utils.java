package maum.brain.tts.preprocess.eng.address;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Utils {
    private static final Pattern NAMED_GROUP_PATTERN = Pattern.compile("\\(\\?P<(.*?)>");

    Utils() {
    }

    public static Utils.NamedGroupPattern compile(String regex) {
        Matcher m = NAMED_GROUP_PATTERN.matcher(regex);
        Map<Integer, String> namedGroupMap = new HashMap();

        for(int i = 1; m.find(); ++i) {
            namedGroupMap.put(i, m.group(1).toUpperCase());
        }

        return new Utils.NamedGroupPattern(m.replaceAll("("), namedGroupMap);
    }

    public static <T> T nvl(T t, T tt) {
        return t == null ? tt : t;
    }

    public static class NamedGroupPattern {
        private final String _regex;
        private final Map<Integer, String> _namedGroupMap;

        public NamedGroupPattern(String regex, Map<Integer, String> namedGroupMap) {
            this._regex = regex;
            this._namedGroupMap = namedGroupMap;
        }

        public String getRegex() {
            return this._regex;
        }

        public Map<Integer, String> getNamedGroupMap() {
            return this._namedGroupMap;
        }
    }
}
