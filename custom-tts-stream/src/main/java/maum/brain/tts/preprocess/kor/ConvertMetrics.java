package maum.brain.tts.preprocess.kor;

import java.util.List;
import maum.brain.tts.preprocess.type.KorPatternType;
import maum.brain.tts.utils.NumberUtils;
import maum.brain.tts.utils.PPUtils;

public class ConvertMetrics {
    public ConvertMetrics() {
    }

    public static String convertMetricsFormat(String token) {
        if (token.contains("%p")) {
            return convertPercentPointFormat(token);
        } else if (token.contains("%")) {
            return convertPercentFormat(token);
        } else if (token.contains("㎧")) {
            return convertVelocityFormat(token);
        } else if (token.contains("℃")) {
            return convertTEMPCFormat(token);
        } else if (token.contains("℉")) {
            return convertTEMPFFormat(token);
        } else if (token.contains("㎜")) {
            return convertMMFormat(token);
        } else if (token.contains("㎝")) {
            return convertCMFormat(token);
        } else if (token.contains("㎞")) {
            return convertKMFormat(token);
        } else if (token.contains("㎏")) {
            return convertKGFormat(token);
        } else if (token.contains("㎡")) {
            return convertSquareFormat(token);
        } else if (token.contains("㏈")) {
            return convertDecibelFormat(token);
        } else if (token.contains("㎈")) {
            return convertCalorieFormat(token);
        } else {
            return token.contains("㎐") ? convertHertzFormat(token) : token;
        }
    }

    public static String convertChinaNumberFormat(String token) {
        token = token.replace("번", "");
        token = NumberUtils.convertDecimal2HangleEx(token, false);
        return token.concat("번");
    }

    public static String convertChinaPointFormat(String token) {
        token = token.replaceAll(",", "");
        token = NumberUtils.convertDecimal2HangleEx(token, false);
        return token.trim();
    }

    public static String convertPercentPointFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_PERCENT_POINT, token);
        new String();
        new String();

        for(int i = 0; i < aList.size(); ++i) {
            String word = (String)aList.get(i);
            int idxPercent = word.indexOf("%p");
            String curNum = word.substring(0, idxPercent);
            String curUnit = word.substring(idxPercent + 2);
            if (curUnit != null && curUnit.length() != 0) {
                token = convertPercentPoint2Hangle(curNum).concat(curUnit).concat(" ");
            } else {
                token = convertPercentPoint2Hangle(curNum).concat(" ");
            }
        }

        return token;
    }

    private static String convertPercentPoint2Hangle(String text) {
        StringBuilder sb = new StringBuilder();
        if (text.contains(".")) {
            String[] arrPercent = text.split("[.]");
            if (arrPercent.length == 1) {
                sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0].substring(0, arrPercent[0].length()), false));
                sb.append(" ").append("퍼센트 포인트");
            } else if (arrPercent.length == 2) {
                sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0], false));
                sb.append("쩜");
                sb.append(NumberUtils.convertDigit2Hangle(arrPercent[1].substring(0, arrPercent[1].length())));
                sb.append(" ").append("퍼센트 포인트");
            }
        } else {
            sb.append(NumberUtils.convertDecimal2HangleEx(text, false));
            sb.append(" ").append("퍼센트 포인트");
        }

        return sb.toString();
    }

    public static String convertPercentFormat(String token) {
        boolean bYear = false;
        if (token.startsWith("연")) {
            bYear = true;
        }

        Boolean bComma = false;
        if (token.endsWith(",")) {
            bComma = true;
        }

        token = token.replaceAll(",", "");
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_PERCENT, token);
        new String();
        new String();

        for(int i = 0; i < aList.size(); ++i) {
            String word = (String)aList.get(i);
            int idxPercent = word.indexOf("%");
            String curNum = word.substring(0, idxPercent);
            String curUnit = word.substring(idxPercent + 1);
            if (curUnit != null && curUnit.length() != 0) {
                if (bComma) {
                    token = convertPercent2Hangle(curNum).concat(curUnit).concat(",");
                } else {
                    token = convertPercent2Hangle(curNum).concat(curUnit);
                }
            } else if (bComma) {
                token = convertPercent2Hangle(curNum).concat(",");
            } else {
                token = convertPercent2Hangle(curNum);
            }
        }

        if (bYear) {
            token = "연 " + token;
        }

        return token;
    }

    private static String convertPercent2Hangle(String text) {
        StringBuilder sb = new StringBuilder();
        if (text.contains(".")) {
            String[] arrPercent = text.split("[.]");
            if (arrPercent.length == 1) {
                sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0].substring(0, arrPercent[0].length()), false));
                sb.append("퍼센트");
            } else if (arrPercent.length == 2) {
                sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0], false));
                sb.append("쩜");
                sb.append(NumberUtils.convertDigit2Hangle(arrPercent[1].substring(0, arrPercent[1].length())));
                sb.append("퍼센트");
            }
        } else {
            sb.append(NumberUtils.convertDecimal2HangleEx(text, false));
            sb.append("퍼센트");
        }

        return sb.toString();
    }

    public static String convertHertzFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_HERTZ, token);
        String changeToken = convertHertz2Hangle((String)aList.get(0));
        return token.replace((CharSequence)aList.get(0), changeToken);
    }

    private static String convertHertz2Hangle(String text) {
        String[] arrPercent = text.split("[.]");
        StringBuilder sb = new StringBuilder();
        if (arrPercent.length == 1) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0].substring(0, arrPercent[0].length() - 1), false));
            sb.append(" ").append("헤르츠");
        } else if (arrPercent.length == 2) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0], false));
            sb.append("쩜");
            sb.append(NumberUtils.convertDigit2Hangle(arrPercent[1].substring(0, arrPercent[1].length() - 1)));
            sb.append(" ").append("헤르츠");
        }

        return sb.toString();
    }

    public static String convertVelocityFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_VELOCITY, token);
        String changeToken = convertVelocity2Hangle((String)aList.get(0));
        return token.replace((CharSequence)aList.get(0), changeToken);
    }

    private static String convertVelocity2Hangle(String text) {
        String[] arrPercent = text.split("[.]");
        StringBuilder sb = new StringBuilder();
        if (arrPercent.length == 1) {
            sb.append("초속 ");
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0].substring(0, arrPercent[0].length() - 1), false));
            sb.append(" ");
            sb.append("미터").append(" ");
        } else if (arrPercent.length == 2) {
            sb.append("초속 " + NumberUtils.convertDecimal2HangleEx(arrPercent[0], false));
            sb.append("쩜");
            sb.append(NumberUtils.convertDigit2Hangle(arrPercent[1].substring(0, arrPercent[1].length() - 1))).append(" ");
            sb.append("미터").append(" ");
        }

        return sb.toString();
    }

    public static String convertCalorieFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_CALORIE, token);
        String changeToken = convertCalorie2Hangle((String)aList.get(0));
        return token.replace((CharSequence)aList.get(0), changeToken);
    }

    private static String convertCalorie2Hangle(String text) {
        String[] arrPercent = text.split("[.]");
        StringBuilder sb = new StringBuilder();
        if (arrPercent.length == 1) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0].substring(0, arrPercent[0].length() - 1), false));
            sb.append(" ").append("칼로리");
        } else if (arrPercent.length == 2) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0], false));
            sb.append("쩜");
            sb.append(NumberUtils.convertDigit2Hangle(arrPercent[1].substring(0, arrPercent[1].length() - 1)));
            sb.append(" ").append("칼로리");
        }

        return sb.toString();
    }

    public static String convertDecibelFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_DECIBEL, token);
        String changeToken = convertDecibel2Hangle((String)aList.get(0));
        return token.replace((CharSequence)aList.get(0), changeToken);
    }

    private static String convertDecibel2Hangle(String text) {
        String[] arrPercent = text.split("[.]");
        StringBuilder sb = new StringBuilder();
        if (arrPercent.length == 1) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0].substring(0, arrPercent[0].length() - 1), false));
            sb.append(" ").append("데시벨").append(" ");
        } else if (arrPercent.length == 2) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0], false));
            sb.append("쩜");
            sb.append(NumberUtils.convertDigit2Hangle(arrPercent[1].substring(0, arrPercent[1].length() - 1)));
            sb.append(" ").append("데시벨").append(" ");
        }

        return sb.toString();
    }

    public static String convertSquareFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_SQUARE, token);
        String changeToken = convertSquare2Hangle((String)aList.get(0));
        return token.replace((CharSequence)aList.get(0), changeToken);
    }

    private static String convertSquare2Hangle(String text) {
        String[] arrPercent = text.split("[.]");
        StringBuilder sb = new StringBuilder();
        if (arrPercent.length == 1) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0].substring(0, arrPercent[0].length() - 1), false));
            sb.append(" ").append("제곱미터").append(" ");
        } else if (arrPercent.length == 2) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0], false));
            sb.append("쩜");
            sb.append(NumberUtils.convertDigit2Hangle(arrPercent[1].substring(0, arrPercent[1].length() - 1)));
            sb.append(" ").append("제곱미터").append(" ");
        }

        return sb.toString();
    }

    public static String convertTEMPCFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_TEMP_C, token);
        String changeToken = convertTEMPC2Hangle((String)aList.get(0));
        return token.replace((CharSequence)aList.get(0), changeToken);
    }

    public static String convertTEMPFFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_TEMP_F, token);
        String changeToken = convertTEMPF2Hangle((String)aList.get(0));
        return token.replace((CharSequence)aList.get(0), changeToken);
    }

    private static String convertTEMPF2Hangle(String text) {
        String[] arrPercent = text.split("[.]");
        StringBuilder sb = new StringBuilder();
        if (arrPercent.length == 1) {
            sb.append("화씨").append(" ");
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0].substring(0, arrPercent[0].length() - 1), false));
            sb.append("도").append(" ");
        } else if (arrPercent.length == 2) {
            sb.append("화씨 " + NumberUtils.convertDecimal2HangleEx(arrPercent[0], false));
            sb.append("쩜");
            sb.append(NumberUtils.convertDigit2Hangle(arrPercent[1].substring(0, arrPercent[1].length() - 1)));
            sb.append("도").append(" ");
        }

        return sb.toString();
    }

    private static String convertTEMPC2Hangle(String text) {
        String[] arrPercent = text.split("[.]");
        StringBuilder sb = new StringBuilder();
        if (arrPercent.length == 1) {
            sb.append("섭씨").append(" ");
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0].substring(0, arrPercent[0].length() - 1), false));
            sb.append("도").append(" ");
        } else if (arrPercent.length == 2) {
            sb.append("섭씨").append(" ");
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0], false));
            sb.append("쩜");
            sb.append(NumberUtils.convertDigit2Hangle(arrPercent[1].substring(0, arrPercent[1].length() - 1)));
            sb.append("도").append(" ");
        }

        return sb.toString();
    }

    static String convertMMFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_LEN_MM, token);
        String changeToken = convertMM2Hangle((String)aList.get(0));
        return token.replace((CharSequence)aList.get(0), changeToken);
    }

    private static String convertMM2Hangle(String text) {
        String[] arrPercent = text.split("[.]");
        StringBuilder sb = new StringBuilder();
        if (arrPercent.length == 1) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0].substring(0, arrPercent[0].length() - 1), false));
            sb.append(" ").append("밀리미터").append(" ");
        } else if (arrPercent.length == 2) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0], false));
            sb.append("쩜");
            sb.append(NumberUtils.convertDigit2Hangle(arrPercent[1].substring(0, arrPercent[1].length() - 1)));
            sb.append(" ").append("밀리미터").append(" ");
        }

        return sb.toString();
    }

    public static String convertCMFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_LEN_CM, token);
        String changeToken = convertCM2Hangle((String)aList.get(0));
        return token.replace((CharSequence)aList.get(0), changeToken);
    }

    private static String convertCM2Hangle(String text) {
        String[] arrPercent = text.split("[.]");
        StringBuilder sb = new StringBuilder();
        if (arrPercent.length == 1) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0].substring(0, arrPercent[0].length() - 1), false));
            sb.append(" ").append("센치미터");
        } else if (arrPercent.length == 2) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0], false));
            sb.append("쩜");
            sb.append(NumberUtils.convertDigit2Hangle(arrPercent[1].substring(0, arrPercent[1].length() - 1)));
            sb.append(" ").append("센치미터").append(" ");
        }

        return sb.toString();
    }

    public static String convertKMFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_LEN_KM, token);
        String changeToken = convertKM2Hangle((String)aList.get(0));
        return token.replace((CharSequence)aList.get(0), changeToken);
    }

    private static String convertKM2Hangle(String text) {
        String[] arrPercent = text.split("[.]");
        StringBuilder sb = new StringBuilder();
        if (arrPercent.length == 1) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0].substring(0, arrPercent[0].length() - 1), false));
            sb.append(" ").append("킬로미터").append(" ");
        } else if (arrPercent.length == 2) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0], false));
            sb.append("쩜");
            sb.append(NumberUtils.convertDigit2Hangle(arrPercent[1].substring(0, arrPercent[1].length() - 1)));
            sb.append(" ").append("킬로미터").append(" ");
        }

        return sb.toString();
    }

    public static String convertKGFormat(String token) {
        List<String> aList = PPUtils.extractPatternFormatFromSentence(KorPatternType.P_KG, token);
        String changeToken = convertKG2Hangle((String)aList.get(0));
        return token.replace((CharSequence)aList.get(0), changeToken);
    }

    private static String convertKG2Hangle(String text) {
        String[] arrPercent = text.split("[.]");
        StringBuilder sb = new StringBuilder();
        if (arrPercent.length == 1) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0].substring(0, arrPercent[0].length() - 1), false));
            sb.append(" ").append("킬로그람").append(" ");
        } else if (arrPercent.length == 2) {
            sb.append(NumberUtils.convertDecimal2HangleEx(arrPercent[0], false));
            sb.append("쩜");
            sb.append(NumberUtils.convertDigit2Hangle(arrPercent[1].substring(0, arrPercent[1].length() - 1)));
            sb.append(" ").append("킬로그람").append(" ");
        }

        return sb.toString();
    }
}
