package maum.brain.tts.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class WaveHeader {
    private static final int HEADER_LENGTH = 44;
    public static final short FORMAT_PCM = 1;
    public static final short FORMAT_ALAW = 6;
    public static final short FORMAT_ULAW = 7;
    private short mFormat;
    private short mNumChannels;
    private int mSampleRate;
    private short mBitsPerSample;
    private int mNumBytes;

    public static void main(String[] args) {
        try {
            WaveHeader wh = new WaveHeader();
            InputStream is = new FileInputStream("C:/Temp/aaa1.wav");
            wh.read(is);
            System.out.println(wh.toString());
            is.close();
        } catch (FileNotFoundException var3) {
            var3.printStackTrace();
        } catch (IOException var4) {
            var4.printStackTrace();
        }

    }

    public WaveHeader() {
    }

    public WaveHeader(short format, short numChannels, int sampleRate, short bitsPerSample, int numBytes) {
        this.mFormat = format;
        this.mSampleRate = sampleRate;
        this.mNumChannels = numChannels;
        this.mBitsPerSample = bitsPerSample;
        this.mNumBytes = numBytes;
    }

    public short getFormat() {
        return this.mFormat;
    }

    public WaveHeader setFormat(short format) {
        this.mFormat = format;
        return this;
    }

    public short getNumChannels() {
        return this.mNumChannels;
    }

    public WaveHeader setNumChannels(short numChannels) {
        this.mNumChannels = numChannels;
        return this;
    }

    public int getSampleRate() {
        return this.mSampleRate;
    }

    public WaveHeader setSampleRate(int sampleRate) {
        this.mSampleRate = sampleRate;
        return this;
    }

    public short getBitsPerSample() {
        return this.mBitsPerSample;
    }

    public WaveHeader setBitsPerSample(short bitsPerSample) {
        this.mBitsPerSample = bitsPerSample;
        return this;
    }

    public int getNumBytes() {
        return this.mNumBytes;
    }

    public WaveHeader setNumBytes(int numBytes) {
        this.mNumBytes = numBytes;
        return this;
    }

    public int read(InputStream in) throws IOException {
        readId(in, "RIFF");
        readId(in, "WAVE");
        readId(in, "fmt ");
        if (16 != readInt(in)) {
            throw new IOException("fmt chunk length not 16");
        } else {
            this.mFormat = readShort(in);
            this.mNumChannels = readShort(in);
            this.mSampleRate = readInt(in);
            int byteRate = readInt(in);
            short blockAlign = readShort(in);
            this.mBitsPerSample = readShort(in);
            if (byteRate != this.mNumChannels * this.mSampleRate * this.mBitsPerSample / 8) {
                throw new IOException("fmt.ByteRate field inconsistent");
            } else if (blockAlign != this.mNumChannels * this.mBitsPerSample / 8) {
                throw new IOException("fmt.BlockAlign field inconsistent");
            } else {
                readId(in, "data");
                this.mNumBytes = readInt(in);
                return 44;
            }
        }
    }

    private static void readId(InputStream in, String id) throws IOException {
        for(int i = 0; i < id.length(); ++i) {
            if (id.charAt(i) != in.read()) {
                throw new IOException(id + " tag not present");
            }
        }

    }

    private static int readInt(InputStream in) throws IOException {
        return in.read() | in.read() << 8 | in.read() << 16 | in.read() << 24;
    }

    private static short readShort(InputStream in) throws IOException {
        return (short)(in.read() | in.read() << 8);
    }

    public int write(OutputStream out) throws IOException {
        writeId(out, "RIFF");
        writeInt(out, 36 + this.mNumBytes);
        writeId(out, "WAVE");
        writeId(out, "fmt ");
        writeInt(out, 16);
        writeShort(out, this.mFormat);
        writeShort(out, this.mNumChannels);
        writeInt(out, this.mSampleRate);
        writeInt(out, this.mNumChannels * this.mSampleRate * this.mBitsPerSample / 8);
        writeShort(out, (short)(this.mNumChannels * this.mBitsPerSample / 8));
        writeShort(out, this.mBitsPerSample);
        writeId(out, "data");
        writeInt(out, this.mNumBytes);
        return 44;
    }

    private static void writeId(OutputStream out, String id) throws IOException {
        for(int i = 0; i < id.length(); ++i) {
            out.write(id.charAt(i));
        }

    }

    private static void writeInt(OutputStream out, int val) throws IOException {
        out.write(val >> 0);
        out.write(val >> 8);
        out.write(val >> 16);
        out.write(val >> 24);
    }

    private static void writeShort(OutputStream out, short val) throws IOException {
        out.write(val >> 0);
        out.write(val >> 8);
    }

    public String toString() {
        return String.format("WaveHeader format=%d numChannels=%d sampleRate=%d bitsPerSample=%d numBytes=%d", this.mFormat, this.mNumChannels, this.mSampleRate, this.mBitsPerSample, this.mNumBytes);
    }
}
