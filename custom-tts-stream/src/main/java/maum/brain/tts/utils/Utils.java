package maum.brain.tts.utils;

import java.util.UUID;
import maum.brain.tts.CommonCode.Lang;
import maum.brain.tts.exception.TtsException;

public class Utils {
    public Utils() {
    }

    public static String makeJobId() {
        return UUID.randomUUID().toString();
    }

    public static Lang convertLang(maum.common.LangOuterClass.Lang lang) throws TtsException {
        if (lang == maum.common.LangOuterClass.Lang.ko_KR) {
            return Lang.ko_KR;
        } else if (lang == maum.common.LangOuterClass.Lang.en_US) {
            return Lang.en_US;
        } else {
            throw new TtsException(40003, "not supported language.");
        }
    }
}
