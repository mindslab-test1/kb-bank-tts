package maum.brain.tts.utils;

import org.apache.commons.lang3.StringUtils;

public class NumberUtils {
    public NumberUtils() {
    }

    public static String replaceNumber(String text) {
        return text.replace("영만", "만").replace("영억", "억").replace("영조", "조").replace("영경", "경").replace("영해", "해").replaceAll("일십", "십").replaceAll("일백", "백").replaceAll("일천", "천").replaceAll("영", "");
    }

    public static String replaceNumber2Hangle(String text) {
        String[] han1 = new String[]{"", "일", "이", "삼", "사", "오", "육", "칠", "팔", "구"};
        String[] han2 = new String[]{"", "십", "백", "천"};
        String[] han3 = new String[]{"", "만", "억", "조", "경", "해"};
        StringBuffer sb = new StringBuffer();
        int len = text.length();

        for(int i = len - 1; i >= 0; --i) {
            sb.append(han1[Integer.parseInt(text.substring(len - i - 1, len - i))]);
            if (Integer.parseInt(text.substring(len - i - 1, len - i)) > 0) {
                sb.append(han2[i % 4]);
            }

            if (i % 4 == 0) {
                sb.append(han3[i / 4]);
            }
        }

        return sb.toString();
    }

    public static String convertOrdinalNumber2Hangle(String token) {
        String newToken = convertDecimal2Hangle(token, false);
        if (token.length() == 1) {
            if (newToken.equals("공")) {
                newToken = newToken.replaceAll("공", "영");
            }

            if (newToken.equals("일")) {
                newToken = newToken.replaceAll("일", "한");
            }

            if (newToken.equals("이")) {
                newToken = newToken.replaceAll("이", "두");
            }

            if (newToken.equals("삼")) {
                newToken = newToken.replaceAll("삼", "세");
            }

            if (newToken.equals("사")) {
                newToken = newToken.replaceAll("사", "네");
            }

            if (newToken.equals("오")) {
                newToken = newToken.replaceAll("오", "다섯");
            }

            if (newToken.equals("육")) {
                newToken = newToken.replaceAll("육", "여섯");
            }

            if (newToken.equals("칠")) {
                newToken = newToken.replaceAll("칠", "일곱");
            }

            if (newToken.equals("팔")) {
                newToken = newToken.replaceAll("팔", "여덟");
            }

            if (newToken.equals("구")) {
                newToken = newToken.replaceAll("구", "아홉");
            }
        }

        if (token.length() == 1) {
            return newToken;
        } else if (token.length() == 2) {
            if (token.equals("10") && newToken.equals("십")) {
                newToken = newToken.replaceAll("십", "열");
            }

            if (newToken.contains("일십")) {
                newToken = newToken.replaceAll("일십", "십");
            }

            if (newToken.contains("일백")) {
                newToken = newToken.replaceAll("일백", "백");
            }

            if (newToken.contains("일천")) {
                newToken = newToken.replaceAll("일천", "천");
            }

            if (newToken.contains("일만")) {
                newToken = newToken.replaceAll("일만", "만");
            }

            if (newToken.startsWith("이십")) {
                newToken = newToken.replaceAll("이십", "스물");
            }

            if (newToken.startsWith("삼십")) {
                newToken = newToken.replaceAll("삼십", "서른");
            }

            if (newToken.startsWith("사십")) {
                newToken = newToken.replaceAll("사십", "마흔");
            }

            if (newToken.startsWith("오십")) {
                newToken = newToken.replaceAll("오십", "쉰");
            }

            if (newToken.startsWith("육십")) {
                newToken = newToken.replaceAll("육십", "예순");
            }

            if (newToken.startsWith("칠십")) {
                newToken = newToken.replaceAll("칠십", "일흔");
            }

            if (newToken.startsWith("팔십")) {
                newToken = newToken.replaceAll("팔십", "여든");
            }

            if (newToken.startsWith("구십")) {
                newToken = newToken.replaceAll("구십", "아흔");
            }

            if (!newToken.startsWith("일흔")) {
                newToken = newToken.replaceAll("일", "한").replaceAll("이", "두").replaceAll("삼", "세").replaceAll("사", "네").replaceAll("오", "다섯").replaceAll("육", "여섯").replaceAll("륙", "여섯").replaceAll("칠", "일곱").replaceAll("팔", "여덟").replaceAll("구", "아홉").replaceAll("십", "열");
            }

            return newToken.replaceAll("여덟", "여덜");
        } else {
            if (newToken.contains("일십")) {
                newToken = newToken.replaceAll("일십", "십");
            }

            if (newToken.contains("일백")) {
                newToken = newToken.replaceAll("일백", "백");
            }

            if (newToken.contains("일천")) {
                newToken = newToken.replaceAll("일천", "천");
            }

            if (newToken.contains("일만")) {
                newToken = newToken.replaceAll("일만", "만");
            }

            return newToken;
        }
    }

    public static String convertHour2Hangle(String text) {
        if (!"01".equals(text) && !"1".equals(text)) {
            if (!"02".equals(text) && !"2".equals(text)) {
                if (!"03".equals(text) && !"3".equals(text)) {
                    if (!"04".equals(text) && !"4".equals(text)) {
                        if (!"05".equals(text) && !"5".equals(text)) {
                            if (!"06".equals(text) && !"6".equals(text)) {
                                if (!"07".equals(text) && !"7".equals(text)) {
                                    if (!"08".equals(text) && !"8".equals(text)) {
                                        if (!"09".equals(text) && !"9".equals(text)) {
                                            if ("10".equals(text)) {
                                                return "열";
                                            } else if ("11".equals(text)) {
                                                return "열한";
                                            } else if ("12".equals(text)) {
                                                return "열두";
                                            } else if ("13".equals(text)) {
                                                return "십삼";
                                            } else if ("14".equals(text)) {
                                                return "십사";
                                            } else if ("15".equals(text)) {
                                                return "십오";
                                            } else if ("16".equals(text)) {
                                                return "십육";
                                            } else if ("17".equals(text)) {
                                                return "십칠";
                                            } else if ("18".equals(text)) {
                                                return "십팔";
                                            } else if ("19".equals(text)) {
                                                return "십구";
                                            } else if ("20".equals(text)) {
                                                return "이십";
                                            } else if ("21".equals(text)) {
                                                return "이십일";
                                            } else if ("22".equals(text)) {
                                                return "이십이";
                                            } else {
                                                return "23".equals(text) ? "이십삼" : convertDecimal2Hangle(text, false);
                                            }
                                        } else {
                                            return "아홉";
                                        }
                                    } else {
                                        return "여덟";
                                    }
                                } else {
                                    return "일곱";
                                }
                            } else {
                                return "여섯";
                            }
                        } else {
                            return "다섯";
                        }
                    } else {
                        return "네";
                    }
                } else {
                    return "세";
                }
            } else {
                return "두";
            }
        } else {
            return "한";
        }
    }

    public static String convertDay2Hangle(String text) {
        if (!"01".equals(text) && !"1".equals(text)) {
            if (!"02".equals(text) && !"2".equals(text)) {
                if (!"03".equals(text) && !"3".equals(text)) {
                    if (!"04".equals(text) && !"4".equals(text)) {
                        if (!"05".equals(text) && !"5".equals(text)) {
                            if (!"06".equals(text) && !"6".equals(text)) {
                                if (!"07".equals(text) && !"7".equals(text)) {
                                    if (!"08".equals(text) && !"8".equals(text)) {
                                        if (!"09".equals(text) && !"9".equals(text)) {
                                            if ("10".equals(text)) {
                                                return "십";
                                            } else if ("11".equals(text)) {
                                                return "십일";
                                            } else if ("12".equals(text)) {
                                                return "십이";
                                            } else if ("13".equals(text)) {
                                                return "십삼";
                                            } else if ("14".equals(text)) {
                                                return "십사";
                                            } else if ("15".equals(text)) {
                                                return "십오";
                                            } else if ("16".equals(text)) {
                                                return "십육";
                                            } else if ("17".equals(text)) {
                                                return "십칠";
                                            } else if ("18".equals(text)) {
                                                return "십팔";
                                            } else if ("19".equals(text)) {
                                                return "십구";
                                            } else if ("20".equals(text)) {
                                                return "이십";
                                            } else if ("21".equals(text)) {
                                                return "이십일";
                                            } else if ("22".equals(text)) {
                                                return "이십이";
                                            } else if ("23".equals(text)) {
                                                return "이십삼";
                                            } else if ("24".equals(text)) {
                                                return "이십사";
                                            } else if ("25".equals(text)) {
                                                return "이십오";
                                            } else if ("26".equals(text)) {
                                                return "이십육";
                                            } else if ("27".equals(text)) {
                                                return "이십칠";
                                            } else if ("28".equals(text)) {
                                                return "이십팔";
                                            } else if ("29".equals(text)) {
                                                return "이십구";
                                            } else if ("30".equals(text)) {
                                                return "삼십";
                                            } else {
                                                return "31".equals(text) ? "삼십일" : convertDecimal2Hangle(text, false);
                                            }
                                        } else {
                                            return "구";
                                        }
                                    } else {
                                        return "팔";
                                    }
                                } else {
                                    return "칠";
                                }
                            } else {
                                return "육";
                            }
                        } else {
                            return "오";
                        }
                    } else {
                        return "사";
                    }
                } else {
                    return "삼";
                }
            } else {
                return "이";
            }
        } else {
            return "일";
        }
    }

    public static String convertMonth2Hangle(String text) {
        if (!"01".equals(text) && !"1".equals(text)) {
            if (!"02".equals(text) && !"2".equals(text)) {
                if (!"03".equals(text) && !"3".equals(text)) {
                    if (!"04".equals(text) && !"4".equals(text)) {
                        if (!"05".equals(text) && !"5".equals(text)) {
                            if (!"06".equals(text) && !"6".equals(text)) {
                                if (!"07".equals(text) && !"7".equals(text)) {
                                    if (!"08".equals(text) && !"8".equals(text)) {
                                        if (!"09".equals(text) && !"9".equals(text)) {
                                            if ("10".equals(text)) {
                                                return "시";
                                            } else if ("11".equals(text)) {
                                                return "십일";
                                            } else {
                                                return "12".equals(text) ? "십이" : "";
                                            }
                                        } else {
                                            return "구";
                                        }
                                    } else {
                                        return "팔";
                                    }
                                } else {
                                    return "칠";
                                }
                            } else {
                                return "유";
                            }
                        } else {
                            return "오";
                        }
                    } else {
                        return "사";
                    }
                } else {
                    return "삼";
                }
            } else {
                return "이";
            }
        } else {
            return "일";
        }
    }

    public static String convertDigit2Hangle(String text) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < text.length(); ++i) {
            if ("0".equals(String.valueOf(text.charAt(i)))) {
                sb.append("영");
            }

            if ("1".equals(String.valueOf(text.charAt(i)))) {
                sb.append("일");
            }

            if ("2".equals(String.valueOf(text.charAt(i)))) {
                sb.append("이");
            }

            if ("3".equals(String.valueOf(text.charAt(i)))) {
                sb.append("삼");
            }

            if ("4".equals(String.valueOf(text.charAt(i)))) {
                sb.append("사");
            }

            if ("5".equals(String.valueOf(text.charAt(i)))) {
                sb.append("오");
            }

            if ("6".equals(String.valueOf(text.charAt(i)))) {
                sb.append("육");
            }

            if ("7".equals(String.valueOf(text.charAt(i)))) {
                sb.append("칠");
            }

            if ("8".equals(String.valueOf(text.charAt(i)))) {
                sb.append("팔");
            }

            if ("9".equals(String.valueOf(text.charAt(i)))) {
                sb.append("구");
            }

            if ("-".equals(String.valueOf(text.charAt(i)))) {
                sb.append(" ").append("다시").append(" ");
            }
        }

        return sb.toString();
    }

    public static String convertDigit2HangleEx(String text) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < text.length(); ++i) {
            if ("0".equals(String.valueOf(text.charAt(i)))) {
                sb.append("영");
            } else if ("1".equals(String.valueOf(text.charAt(i)))) {
                sb.append("일");
            } else if ("2".equals(String.valueOf(text.charAt(i)))) {
                sb.append("이");
            } else if ("3".equals(String.valueOf(text.charAt(i)))) {
                sb.append("삼");
            } else if ("4".equals(String.valueOf(text.charAt(i)))) {
                sb.append("사");
            } else if ("5".equals(String.valueOf(text.charAt(i)))) {
                sb.append("오");
            } else if ("6".equals(String.valueOf(text.charAt(i)))) {
                sb.append("육");
            } else if ("7".equals(String.valueOf(text.charAt(i)))) {
                sb.append("칠");
            } else if ("8".equals(String.valueOf(text.charAt(i)))) {
                sb.append("팔");
            } else if ("9".equals(String.valueOf(text.charAt(i)))) {
                sb.append("구");
            } else {
                sb.append(String.valueOf(text.charAt(i)));
            }
        }

        return sb.toString().trim();
    }

    public static String convertDecimal2Hangle(String token, boolean official) {
        if (token.equals("zero")) {
            return "영";
        } else {
            token = token.replaceAll("[.]", "");
            boolean bMinus = false;
            if (token.startsWith("-") || token.startsWith("-")) {
                bMinus = true;
            }

            token = token.replace("-", "").replace("-", "");
            int len = token.length();
            StringBuilder sb = new StringBuilder();
            boolean subcount = false;

            for(int i = 0; i < len; ++i) {
                int base = len - i - 1;
                int baseMod4 = base % 4;
                int digit = token.charAt(i) - 48;
                if (digit != 0) {
                    subcount = true;
                    if (digit == 1) {
                        if (official || baseMod4 == 0) {
                            sb.append('일');
                        }
                    } else {
                        sb.append("이삼사오육칠팔구".charAt(digit - 2));
                    }

                    if (baseMod4 > 0) {
                        sb.append("십백천".charAt(baseMod4 - 1));
                    }
                }

                if (baseMod4 == 0) {
                    if (subcount && base > 0) {
                        sb.append("만억조경해".charAt(base / 4 - 1)).append(' ');
                    }

                    subcount = false;
                }
            }

            String tmp = sb.toString();
            if (tmp.startsWith("일백") || tmp.startsWith("일천") || tmp.startsWith("일만")) {
                tmp = tmp.substring(1);
            }

            if (bMinus) {
                return "마이너스".concat(" ").concat(tmp);
            } else {
                return tmp;
            }
        }
    }

    public static String convertDecimal2HangleEx(String token, boolean official) {
        if (token.equals("0")) {
            return "영";
        } else {
            token = token.replaceAll("[.]", "");
            boolean bMinus = false;
            if (token.startsWith("-") || token.startsWith("-")) {
                bMinus = true;
            }

            token = token.replace("-", "").replace("-", "");
            int len = token.length();
            StringBuilder sb = new StringBuilder();
            boolean subcount = false;

            for(int i = 0; i < len; ++i) {
                int base = len - i - 1;
                int baseMod4 = base % 4;
                int digit = token.charAt(i) - 48;
                if (digit != 0) {
                    subcount = true;
                    if (digit == 1) {
                        if (official || baseMod4 == 0) {
                            sb.append('일');
                        }
                    } else {
                        sb.append("이삼사오육칠팔구".charAt(digit - 2));
                    }

                    if (baseMod4 > 0) {
                        sb.append("십백천".charAt(baseMod4 - 1));
                    }
                }

                if (baseMod4 == 0) {
                    if (subcount && base > 0) {
                        sb.append("만억조경해".charAt(base / 4 - 1));
                    }

                    subcount = false;
                }
            }

            String tmp = sb.toString();
            if (tmp.startsWith("일백") || tmp.startsWith("일천") || tmp.startsWith("일만")) {
                tmp = tmp.substring(1);
            }

            if (bMinus) {
                return "마이너스".concat(" ").concat(tmp);
            } else {
                return tmp;
            }
        }
    }

    public static String convertBaseMonetary2Korean(char num) {
        String[] baseMonetaryUnit = new String[]{"", "일", "이", "삼", "사", "오", "육", "칠", "팔", "구"};
        String rtnBaseMonetary = null;
        switch(num) {
            case '0':
                rtnBaseMonetary = baseMonetaryUnit[0];
                break;
            case '1':
                rtnBaseMonetary = baseMonetaryUnit[1];
                break;
            case '2':
                rtnBaseMonetary = baseMonetaryUnit[2];
                break;
            case '3':
                rtnBaseMonetary = baseMonetaryUnit[3];
                break;
            case '4':
                rtnBaseMonetary = baseMonetaryUnit[4];
                break;
            case '5':
                rtnBaseMonetary = baseMonetaryUnit[5];
                break;
            case '6':
                rtnBaseMonetary = baseMonetaryUnit[6];
                break;
            case '7':
                rtnBaseMonetary = baseMonetaryUnit[7];
                break;
            case '8':
                rtnBaseMonetary = baseMonetaryUnit[8];
                break;
            case '9':
                rtnBaseMonetary = baseMonetaryUnit[9];
        }

        return rtnBaseMonetary;
    }

    public static String convertNumeral2Korean(String money) throws Exception {
        String[] bigMonetaryUnit = new String[]{"만", "억", "조", "경"};
        String[] monetaryUnit = new String[]{"천", "백", "십"};
        StringBuffer tempMoney = new StringBuffer();
        String[] moneyArray = new String[0];
        String tempString = null;
        int moneyLength = money.length();
        int unitPos = money.length();
        int arrInx = moneyLength % 4 == 0 ? moneyLength / 4 : moneyLength / 4 + 1;
        moneyArray = new String[arrInx];

        int jnx;
        for(jnx = 0; unitPos > 0; ++jnx) {
            if (unitPos - 4 > 0) {
                tempString = money.substring(unitPos - 4, unitPos);
                moneyArray[jnx] = tempString;
                unitPos -= 4;
            } else {
                tempString = money.substring(0, unitPos);
                moneyArray[jnx] = StringUtils.leftPad(tempString, 4, '0');
                unitPos = 0;
            }
        }

        for(jnx = arrInx - 1; jnx >= 0; --jnx) {
            String tempMon = moneyArray[jnx];
            int tempMonLen = tempMon.length();
//            char num = false;

            for(int knx = 0; knx < tempMonLen; ++knx) {
                char num = tempMon.charAt(knx);
                tempMoney.append(convertBaseMonetary2Korean(num));
                if (num != '0') {
                    switch(knx) {
                        case 0:
                            tempMoney.append(monetaryUnit[knx]);
                            break;
                        case 1:
                            tempMoney.append(monetaryUnit[knx]);
                            break;
                        case 2:
                            tempMoney.append(monetaryUnit[knx]);
                    }
                }
            }

            if (castint(tempMon) > 0 && jnx > 0) {
                tempMoney.append(bigMonetaryUnit[jnx - 1]);
            }
        }

        String tmp = tempMoney.toString();
        if (tmp.startsWith("일백") || tmp.startsWith("일천") || tmp.startsWith("일만")) {
            tmp = tmp.substring(1);
        }

        return tmp;
    }

    private static int castint(Object value) throws Exception {
//        int out = false;
        int out;
        if (value != null && !"".equals(value)) {
            if (value instanceof Number) {
                out = ((Number)value).intValue();
            } else {
                out = Integer.parseInt(value.toString().trim());
            }
        } else {
            out = 0;
        }

        return out;
    }
}
