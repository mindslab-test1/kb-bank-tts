package maum.brain.tts.utils;

import java.lang.Character.UnicodeBlock;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PPUtils {
    public PPUtils() {
    }

    public static boolean isKorean(String token) {
        return token == null ? false : token.matches("^[ㄱ-하-ㅣ가-힣]+$");
    }

    public static boolean isKorean(char ch) {
        UnicodeBlock unicodeBlock = UnicodeBlock.of(ch);
        return UnicodeBlock.HANGUL_SYLLABLES == unicodeBlock || UnicodeBlock.HANGUL_COMPATIBILITY_JAMO == unicodeBlock || UnicodeBlock.HANGUL_JAMO == unicodeBlock;
    }

    public static boolean hasKorean(CharSequence token) {
        boolean hasKorean = false;
        char[] var5;
        int var4 = (var5 = token.toString().toCharArray()).length;

        for(int var3 = 0; var3 < var4; ++var3) {
            char c = var5[var3];
            if (UnicodeBlock.of(c) == UnicodeBlock.HANGUL_JAMO || UnicodeBlock.of(c) == UnicodeBlock.HANGUL_COMPATIBILITY_JAMO || UnicodeBlock.of(c) == UnicodeBlock.HANGUL_SYLLABLES) {
                hasKorean = true;
                break;
            }
        }

        return hasKorean;
    }

    public static boolean hasKorean(String token) {
        CharSequence cs = new StringBuffer(token);
        return hasKorean((CharSequence)cs);
    }

    public static String getKorean(String token) {
        StringBuilder sb = new StringBuilder();
        char[] charArray = token.toCharArray();
        char[] var6 = charArray;
        int var5 = charArray.length;

        for(int var4 = 0; var4 < var5; ++var4) {
            char ch = var6[var4];
            UnicodeBlock unicodeBlock = UnicodeBlock.of(ch);
            if (UnicodeBlock.HANGUL_SYLLABLES == unicodeBlock || UnicodeBlock.HANGUL_COMPATIBILITY_JAMO == unicodeBlock || UnicodeBlock.HANGUL_JAMO == unicodeBlock) {
                sb.append(ch);
            }
        }

        return sb.toString();
    }

    public static String removeKoreanConsonantAndVowel(String token) {
        token = token.replaceAll("[ㅏ-ㅣ]", "");
        token = token.replaceAll("[ㄱ-ㅎ()]", "");
        return token;
    }

    public static boolean isAlphabetic(String token) {
        if (token.isEmpty()) {
            return Boolean.FALSE;
        } else {
//            int unicodeNumber = false;

            for(int i = 0; i < token.length(); ++i) {
                int unicodeNumber = token.codePointAt(i);
                if (unicodeNumber < 97 || unicodeNumber > 122) {
                    return Boolean.FALSE;
                }
            }

            return Boolean.TRUE;
        }
    }

    public static boolean hasDigit(String token) {
        int size = token.length();

        for(int i = 0; i < size; ++i) {
            if (Character.isDigit(token.charAt(i))) {
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    public static String getDigit(String token) {
        int size = token.length();
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < size; ++i) {
            if (Character.isDigit(token.charAt(i))) {
                sb.append(token.charAt(i));
            }
        }

        return sb.toString();
    }

    public static boolean isDigitEx(String token) {
        int size = token.length();

        for(int i = 0; i < size; ++i) {
            if (!Character.isDigit(token.charAt(i))) {
                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    public static boolean isDigit(String token) {
        if (token.isEmpty()) {
            return Boolean.FALSE;
        } else {
//            int unicodeNumber = false;

            for(int i = 0; i < token.length(); ++i) {
                int unicodeNumber = token.codePointAt(i);
                if (unicodeNumber < 48 || unicodeNumber > 57) {
                    return Boolean.FALSE;
                }
            }

            return Boolean.TRUE;
        }
    }

    public static String getDigits(String token) {
        StringBuilder builder = new StringBuilder();

        for(int i = 0; i < token.length(); ++i) {
            char c = token.charAt(i);
            if (Character.isDigit(c)) {
                builder.append(c);
            }
        }

        return builder.toString();
    }

    public static boolean isEnglishDigit(String token) {
        return isEnglish(token) || isDigit(token);
    }

    public static boolean isEnglish(char ch) {
        UnicodeBlock unicodeBlock = UnicodeBlock.of(ch);
        return unicodeBlock == UnicodeBlock.BASIC_LATIN && (ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <= 'z');
    }

    public static boolean isEnglish(String token) {
        CharSequence cs = new StringBuffer(token);
        return isEnglish((CharSequence)cs);
    }

    public static boolean isEnglish(CharSequence token) {
        boolean isEnglish = true;
        char[] var5;
        int var4 = (var5 = token.toString().toCharArray()).length;

        for(int var3 = 0; var3 < var4; ++var3) {
            char c = var5[var3];
            if (UnicodeBlock.of(c) != UnicodeBlock.BASIC_LATIN) {
                isEnglish = false;
                break;
            }
        }

        return isEnglish;
    }

    public static boolean hasEnglish(String token) {
        CharSequence cs = new StringBuffer(token);
        return hasEnglish((CharSequence)cs);
    }

    public static boolean hasEnglish(CharSequence token) {
        boolean hasEnglish = false;
        char[] var5;
        int var4 = (var5 = token.toString().toCharArray()).length;

        for(int var3 = 0; var3 < var4; ++var3) {
            char c = var5[var3];
            if (UnicodeBlock.of(c) == UnicodeBlock.BASIC_LATIN) {
                hasEnglish = true;
                break;
            }
        }

        return hasEnglish;
    }

    public static boolean isHiragana(char ch) {
        return ch >= 12352 && ch <= 12447;
    }

    public static boolean isKatakana(char ch) {
        return ch >= 12448 && ch <= 12543;
    }

    public static boolean isJapanese(char ch) {
        return isHiragana(ch) || isKatakana(ch);
    }

    public static boolean hasJapanese(CharSequence token) {
        boolean hasJapanese = false;
        char[] var5;
        int var4 = (var5 = token.toString().toCharArray()).length;

        for(int var3 = 0; var3 < var4; ++var3) {
            char c = var5[var3];
            if (UnicodeBlock.of(c) == UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || UnicodeBlock.of(c) == UnicodeBlock.HIRAGANA || UnicodeBlock.of(c) == UnicodeBlock.KATAKANA || UnicodeBlock.of(c) == UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS || UnicodeBlock.of(c) == UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS || UnicodeBlock.of(c) == UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION) {
                hasJapanese = true;
                break;
            }
        }

        return hasJapanese;
    }

    public static boolean isChinese(char ch) {
        UnicodeBlock ub = UnicodeBlock.of(ch);
        return UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS == ub || UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS == ub || UnicodeBlock.CJK_COMPATIBILITY_FORMS == ub || UnicodeBlock.CJK_RADICALS_SUPPLEMENT == ub || UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A == ub || UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B == ub;
    }

    public static final int lastNumeric(String token) {
        for(int i = token.length() - 1; i >= 0; --i) {
            char c = token.charAt(i);
            if (Character.isDigit(c)) {
                return i;
            }
        }

        return -1;
    }

    public static List<String> extractPatternFormatFromSentence(Pattern p, String text) {
        ArrayList<String> pList = new ArrayList();
        Matcher m = p.matcher(text);

        while(m.find()) {
            pList.add(m.group().trim());
        }

        return pList;
    }
}
