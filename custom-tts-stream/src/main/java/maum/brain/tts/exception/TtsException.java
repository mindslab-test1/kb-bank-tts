package maum.brain.tts.exception;

public class TtsException extends Exception {
    private static final long serialVersionUID = 7712317009455723701L;
    private int errCode;

    public TtsException() {
    }

    public TtsException(String message) {
        super(message);
    }

    public TtsException(int errCode, String message) {
        super(message);
        this.errCode = errCode;
    }

    public TtsException(String message, Throwable cause) {
        super(message, cause);
    }

    public TtsException(Throwable cause) {
        super(cause);
    }

    public TtsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public int getErrCode() {
        return this.errCode;
    }
}
